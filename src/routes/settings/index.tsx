// project imports
import settingsContactsRoute from './contacts';
import settingsSocietyRoute from './society';
import settingsUsersRoute from './users';

// ==============================|| AUTH ROUTING ||============================== //

const SettingsRoutes = [...settingsSocietyRoute, ...settingsUsersRoute, ...settingsContactsRoute];

export default SettingsRoutes;
