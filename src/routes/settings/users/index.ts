import settingsUserRoutesManager from 'routes-manager/settings/users';
import { RouteType } from 'routes/MainRoutes';

const settingsUsersRoute: RouteType[] = [
    {
        path: settingsUserRoutesManager.users.path,
        element: settingsUserRoutesManager.users.element,
        key: settingsUserRoutesManager.users.id,
        verifiedAccess: true
    },
    {
        path: settingsUserRoutesManager.user.path,
        element: settingsUserRoutesManager.user.element,
        key: settingsUserRoutesManager.user.id,
        children: [
            {
                path: settingsUserRoutesManager.user.children.edit.path,
                element: settingsUserRoutesManager.user.children.edit.element,
                key: settingsUserRoutesManager.user.children.edit.id
            },
            {
                path: settingsUserRoutesManager.user.children.create.path,
                element: settingsUserRoutesManager.user.children.create.element,
                key: settingsUserRoutesManager.user.children.create.id
            }
        ],
        verifiedAccess: true
    },
    {
        path: settingsUserRoutesManager.roles.path,
        element: settingsUserRoutesManager.roles.element,
        key: settingsUserRoutesManager.roles.id,
        verifiedAccess: true
    }
];

export default settingsUsersRoute;
