import settingsSocietyRoutesManager from 'routes-manager/settings/society';
import { RouteType } from 'routes/MainRoutes';

const settingsSocietyRoute: RouteType[] = [
    {
        path: settingsSocietyRoutesManager.listSocieties.path,
        element: settingsSocietyRoutesManager.listSocieties.element,
        key: settingsSocietyRoutesManager.listSocieties.id,
        verifiedAccess: true
    },
    {
        path: settingsSocietyRoutesManager.society.path,
        element: settingsSocietyRoutesManager.society.element,
        key: settingsSocietyRoutesManager.society.id,
        children: [
            {
                path: settingsSocietyRoutesManager.society.children.edit.path,
                element: settingsSocietyRoutesManager.society.children.edit.element,
                key: settingsSocietyRoutesManager.society.children.edit.id
            },
            {
                path: settingsSocietyRoutesManager.society.children.add.path,
                element: settingsSocietyRoutesManager.society.children.add.element,
                key: settingsSocietyRoutesManager.society.children.add.id
            }
        ],
        verifiedAccess: true
    }
];

export default settingsSocietyRoute;
