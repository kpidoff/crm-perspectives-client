import settingsContactRoutesManager from 'routes-manager/settings/contacts';
import { RouteType } from 'routes/MainRoutes';

const settingsContactsRoute: RouteType[] = [
    {
        path: settingsContactRoutesManager.destinations.path,
        element: settingsContactRoutesManager.destinations.element,
        key: settingsContactRoutesManager.destinations.id,
        verifiedAccess: true
    },
    {
        path: settingsContactRoutesManager.destinationsProject.path,
        element: settingsContactRoutesManager.destinationsProject.element,
        key: settingsContactRoutesManager.destinationsProject.id,
        verifiedAccess: true
    },
    {
        path: settingsContactRoutesManager.prospectMotives.path,
        element: settingsContactRoutesManager.prospectMotives.element,
        key: settingsContactRoutesManager.prospectMotives.id,
        verifiedAccess: true
    },
    {
        path: settingsContactRoutesManager.reasonsForLostCall.path,
        element: settingsContactRoutesManager.reasonsForLostCall.element,
        key: settingsContactRoutesManager.reasonsForLostCall.id,
        verifiedAccess: true
    },
    {
        path: settingsContactRoutesManager.recoveryModes.path,
        element: settingsContactRoutesManager.recoveryModes.element,
        key: settingsContactRoutesManager.recoveryModes.id,
        verifiedAccess: true
    }
];

export default settingsContactsRoute;
