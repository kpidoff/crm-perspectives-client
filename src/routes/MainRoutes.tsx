// project imports
import MainLayout from 'layout/MainLayout';
import AuthGuard from 'utils/route-guard/AuthGuard';
import contactsRoutes from './contacts';
import dashboardRoutes from './dashboard';
import SettingsRoutes from './settings';
import userRoutes from './user';

// ==============================|| MAIN ROUTING ||============================== //

export type RouteType = {
    path: string;
    element?: JSX.Element;
    key: string;
    verifiedAccess?: boolean;
    children?: RouteType[];
};

export type MainRouteType = {
    path: string;
    element: JSX.Element;
    children: RouteType[];
};

const MainRoutes: MainRouteType = {
    path: '/',
    element: (
        <AuthGuard>
            <MainLayout />
        </AuthGuard>
    ),
    children: [...userRoutes, ...dashboardRoutes, ...SettingsRoutes, ...contactsRoutes]
};

export default MainRoutes;
