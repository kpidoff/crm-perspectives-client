import userRoutesManager from 'routes-manager/user';
import { RouteType } from 'routes/MainRoutes';

const userRoutes: RouteType[] = [
    {
        path: userRoutesManager.userProfile.path,
        element: userRoutesManager.userProfile.element,
        key: userRoutesManager.userProfile.id
    },
    {
        path: userRoutesManager.userNotification.path,
        element: userRoutesManager.userNotification.element,
        key: userRoutesManager.userNotification.id
    }
];

export default userRoutes;
