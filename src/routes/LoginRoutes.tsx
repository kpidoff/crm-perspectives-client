// project imports
import GuestGuard from 'utils/route-guard/GuestGuard';
import MinimalLayout from 'layout/MinimalLayout';
import NavMotion from 'layout/NavMotion';
import { loginRoutesManager } from 'routes-manager';

// ==============================|| AUTH ROUTING ||============================== //

const LoginRoutes = {
    path: '/',
    element: (
        <NavMotion>
            <GuestGuard>
                <MinimalLayout />
            </GuestGuard>
        </NavMotion>
    ),
    children: [
        {
            path: loginRoutesManager.login.path,
            element: loginRoutesManager.login.element
        },
        {
            path: loginRoutesManager.forgetPassword.path,
            element: loginRoutesManager.forgetPassword.element
        },
        {
            path: loginRoutesManager.resetPassword.path,
            element: loginRoutesManager.resetPassword.element
        }
    ]
};

export default LoginRoutes;
