import { lazy } from 'react';
import { Navigate, useRoutes } from 'react-router-dom';

// routes
import MainRoutes from './MainRoutes';
import LoginRoutes from './LoginRoutes';
import Loadable from 'ui-component/Loadable';
import useAuth from 'hooks/authentication/useAuth';
import { withUserAccess } from 'services/user.service';
import { loginRoutesManager } from 'routes-manager';

const PageError = Loadable(lazy(() => import('views/pages/maintenance/Error')));

// ==============================|| ROUTING RENDER ||============================== //
export default function ThemeRoutes() {
    const { user } = useAuth();
    return useRoutes([
        { path: '/', element: <Navigate to={loginRoutesManager.login.path} /> },
        LoginRoutes,
        withUserAccess(user, MainRoutes),
        { path: '*', element: <PageError /> }
    ]);
}
