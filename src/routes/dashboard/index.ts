import dashboardRoutesManager from 'routes-manager/dashboard';
import { RouteType } from 'routes/MainRoutes';

const dashboardRoutes: RouteType[] = [
    {
        path: dashboardRoutesManager.dashboard.path,
        key: dashboardRoutesManager.dashboard.id
    },
    {
        path: dashboardRoutesManager.dashboard.children.default.path,
        element: dashboardRoutesManager.dashboard.children.default.element,
        key: dashboardRoutesManager.dashboard.children.default.id
    },
    {
        path: dashboardRoutesManager.dashboard.children.analytique.path,
        element: dashboardRoutesManager.dashboard.children.analytique.element,
        key: dashboardRoutesManager.dashboard.children.analytique.id
    }
];

export default dashboardRoutes;
