// project imports
import contactsLandRoute from './lands';

// ==============================|| AUTH ROUTING ||============================== //

const contactsRoutes = [...contactsLandRoute];

export default contactsRoutes;
