import contactsRoutesManager from 'routes-manager/contacts/land';
import { RouteType } from 'routes/MainRoutes';

const contactsRoute: RouteType[] = [
    {
        path: contactsRoutesManager.lands.path,
        element: contactsRoutesManager.lands.element,
        key: contactsRoutesManager.lands.id,
        verifiedAccess: true
    },
    {
        path: contactsRoutesManager.fieldServicesProvider.path,
        element: contactsRoutesManager.fieldServicesProvider.element,
        key: contactsRoutesManager.fieldServicesProvider.id,
        verifiedAccess: true
    },
    {
        path: contactsRoutesManager.land.path,
        element: contactsRoutesManager.land.element,
        key: contactsRoutesManager.land.id,
        verifiedAccess: true
    }
];

export default contactsRoute;
