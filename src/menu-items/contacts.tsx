// assets
import { IconMap2 } from '@tabler/icons';
import { MenuProps } from 'menu-items';
import contactsRoutesManager from 'routes-manager/contacts';

// ==============================|| EXTRA PAGES MENU ITEMS ||============================== //

const contacts: MenuProps = {
    id: contactsRoutesManager.contacts.id,
    title: contactsRoutesManager.contacts.title,
    caption: contactsRoutesManager.contacts.caption,
    type: 'group',
    verifiedAccess: contactsRoutesManager.contacts.verifiedAccess,
    children: [
        {
            id: contactsRoutesManager.landsMenu.id,
            title: contactsRoutesManager.landsMenu.title,
            type: 'collapse',
            icon: IconMap2,
            verifiedAccess: contactsRoutesManager.landsMenu.verifiedAccess,
            children: [
                {
                    id: contactsRoutesManager.lands.id,
                    title: contactsRoutesManager.lands.title,
                    type: 'item',
                    url: contactsRoutesManager.lands.path,
                    verifiedAccess: contactsRoutesManager.lands.verifiedAccess
                },
                {
                    id: contactsRoutesManager.fieldServicesProvider.id,
                    title: contactsRoutesManager.fieldServicesProvider.title,
                    type: 'item',
                    url: contactsRoutesManager.fieldServicesProvider.path,
                    verifiedAccess: contactsRoutesManager.fieldServicesProvider.verifiedAccess,
                    breadcrumbs: false
                }
            ]
        }
    ]
};

export default contacts;
