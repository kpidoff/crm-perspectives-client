// assets
import { IconDashboard, IconDeviceAnalytics } from '@tabler/icons';
import { MenuProps } from 'menu-items';
import dashboardRoutesManager from 'routes-manager/dashboard';

// constant
const icons = {
    IconDashboard,
    IconDeviceAnalytics
};

// ==============================|| DASHBOARD MENU ITEMS ||============================== //

const dashboard: MenuProps = {
    id: dashboardRoutesManager.dashboard.id,
    title: dashboardRoutesManager.dashboard.title,
    type: 'group',
    children: [
        {
            id: dashboardRoutesManager.dashboard.children.default.id,
            title: dashboardRoutesManager.dashboard.children.default.title,
            type: 'item',
            url: dashboardRoutesManager.dashboard.children.default.path,
            icon: icons.IconDashboard,
            breadcrumbs: false
        },
        {
            id: dashboardRoutesManager.dashboard.children.analytique.id,
            title: dashboardRoutesManager.dashboard.children.analytique.title,
            type: 'item',
            url: dashboardRoutesManager.dashboard.children.analytique.path,
            icon: icons.IconDeviceAnalytics,
            breadcrumbs: false
        }
    ]
};

export default dashboard;
