import dashboard from './dashboard';
import { OverrideIcon } from 'types';
import settings from './settings';
import contacts from './contacts';

// ==============================|| MENU ITEMS ||============================== //

export interface MenuProps {
    id: string;
    title?: React.ReactNode | string;
    caption?: React.ReactNode | string;
    type: string;
    verifiedAccess?: boolean;
    children: {
        id: string;
        title: React.ReactNode | string;
        type: string;
        url?: string;
        icon?: OverrideIcon;
        breadcrumbs?: boolean;
        children?: MenuProps['children'];
        verifiedAccess?: boolean;
    }[];
}

const menuItems: { items: MenuProps[] } = {
    items: [dashboard, contacts, settings]
};

export default menuItems;
