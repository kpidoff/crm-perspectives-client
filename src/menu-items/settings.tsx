// assets
import { IconBuilding, IconUser, IconUsers } from '@tabler/icons';
import { MenuProps } from 'menu-items';
import settingsUserRoutesManager from 'routes-manager/settings/users';
import settingsSocietyRoutesManager from 'routes-manager/settings/society';
import { settingsRoutesManager } from 'routes-manager';
import settingsContactsRoutesManager from 'routes-manager/settings/contacts';

// ==============================|| EXTRA PAGES MENU ITEMS ||============================== //

const settings: MenuProps = {
    id: settingsRoutesManager.settings.id,
    title: settingsRoutesManager.settings.title,
    caption: settingsRoutesManager.settings.caption,
    type: 'group',
    verifiedAccess: settingsRoutesManager.settings.verifiedAccess,
    children: [
        {
            id: settingsSocietyRoutesManager.societies.id,
            title: settingsSocietyRoutesManager.societies.title,
            type: 'collapse',
            icon: IconBuilding,
            verifiedAccess: settingsSocietyRoutesManager.societies.verifiedAccess,
            children: [
                {
                    id: settingsSocietyRoutesManager.listSocieties.id,
                    title: settingsSocietyRoutesManager.listSocieties.title,
                    type: 'item',
                    url: settingsSocietyRoutesManager.listSocieties.path,
                    verifiedAccess: settingsSocietyRoutesManager.listSocieties.verifiedAccess
                }
            ]
        },
        {
            id: settingsUserRoutesManager.usersMenu.id,
            title: settingsUserRoutesManager.usersMenu.title,
            type: 'collapse',
            icon: IconUsers,
            verifiedAccess: settingsUserRoutesManager.usersMenu.verifiedAccess,
            children: [
                {
                    id: settingsUserRoutesManager.users.id,
                    title: settingsUserRoutesManager.users.title,
                    type: 'item',
                    url: settingsUserRoutesManager.users.path,
                    verifiedAccess: settingsUserRoutesManager.users.verifiedAccess
                },
                {
                    id: settingsUserRoutesManager.roles.id,
                    title: settingsUserRoutesManager.roles.title,
                    type: 'item',
                    url: settingsUserRoutesManager.roles.path,
                    verifiedAccess: settingsUserRoutesManager.roles.verifiedAccess,
                    breadcrumbs: false
                }
            ]
        },
        {
            id: settingsContactsRoutesManager.contactsMenu.id,
            title: settingsContactsRoutesManager.contactsMenu.title,
            type: 'collapse',
            icon: IconUser,
            verifiedAccess: settingsContactsRoutesManager.contactsMenu.verifiedAccess,
            breadcrumbs: false,
            children: [
                {
                    id: settingsContactsRoutesManager.destinations.id,
                    title: settingsContactsRoutesManager.destinations.title,
                    type: 'item',
                    url: settingsContactsRoutesManager.destinations.path,
                    verifiedAccess: settingsContactsRoutesManager.destinations.verifiedAccess,
                    breadcrumbs: false
                },
                {
                    id: settingsContactsRoutesManager.destinationsProject.id,
                    title: settingsContactsRoutesManager.destinationsProject.title,
                    type: 'item',
                    url: settingsContactsRoutesManager.destinationsProject.path,
                    verifiedAccess: settingsContactsRoutesManager.destinationsProject.verifiedAccess,
                    breadcrumbs: false
                },
                {
                    id: settingsContactsRoutesManager.prospectMotives.id,
                    title: settingsContactsRoutesManager.prospectMotives.title,
                    type: 'item',
                    url: settingsContactsRoutesManager.prospectMotives.path,
                    verifiedAccess: settingsContactsRoutesManager.prospectMotives.verifiedAccess,
                    breadcrumbs: false
                },
                {
                    id: settingsContactsRoutesManager.reasonsForLostCall.id,
                    title: settingsContactsRoutesManager.reasonsForLostCall.title,
                    type: 'item',
                    url: settingsContactsRoutesManager.reasonsForLostCall.path,
                    verifiedAccess: settingsContactsRoutesManager.reasonsForLostCall.verifiedAccess,
                    breadcrumbs: false
                },
                {
                    id: settingsContactsRoutesManager.recoveryModes.id,
                    title: settingsContactsRoutesManager.recoveryModes.title,
                    type: 'item',
                    url: settingsContactsRoutesManager.recoveryModes.path,
                    verifiedAccess: settingsContactsRoutesManager.recoveryModes.verifiedAccess,
                    breadcrumbs: false
                }
            ]
        }
    ]
};

export default settings;
