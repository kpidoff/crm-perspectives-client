import { FormattedMessage } from 'react-intl';
import { RoleType } from 'types/settings/role';

export type RoleTypeEnum = {
    [x: string]: {
        key: RoleType;
        label: React.ReactElement | string;
    };
};

export const roleTypeEnum: RoleTypeEnum = {
    NOT_DEFINED: {
        key: RoleType.NOT_DEFINED,
        label: <FormattedMessage id="not-defined" />
    },
    COMMERCIAL: {
        key: RoleType.COMMERCIAL,
        label: <FormattedMessage id="role-label-type-commercial" />
    },
    DRIVER: {
        key: RoleType.DRIVER,
        label: <FormattedMessage id="role-label-type-driver" />
    },
    ASSISTANT: {
        key: RoleType.ASSISTANT,
        label: <FormattedMessage id="role-label-type-assistant" />
    },
    GENERAL_DIRECTOR: {
        key: RoleType.GENERAL_DIRECTOR,
        label: <FormattedMessage id="role-label-type-general-director" />
    }
};
