import { FormattedMessage } from 'react-intl';
import { DestinationType } from 'types/settings/contact/destination';

export type DestinationTypeEnum = {
    [x: string]: {
        key: DestinationType;
        label: JSX.Element | string;
    };
};

export const destinationTypeEnum: DestinationTypeEnum = {
    NOT_DEFINED: {
        key: DestinationType.NOT_DEFINED,
        label: <FormattedMessage id="destination-label-type-not-defined" />
    },
    ACTIVE: {
        key: DestinationType.ACTIVE,
        label: <FormattedMessage id="destination-label-type-active" />
    },
    LOST_CALL: {
        key: DestinationType.LOST_CALL,
        label: <FormattedMessage id="destination-label-type-lost-call" />
    },
    LOST: {
        key: DestinationType.LOST,
        label: <FormattedMessage id="destination-label-type-lost" />
    },
    REVIVED: {
        key: DestinationType.REVIVED,
        label: <FormattedMessage id="destination-label-type-revived" />
    },
    PENDING: {
        key: DestinationType.PENDING,
        label: <FormattedMessage id="destination-label-type-pending" />
    },
    SIGN: {
        key: DestinationType.SIGN,
        label: <FormattedMessage id="destination-label-type-sign" />
    }
};
