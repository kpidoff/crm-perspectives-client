import config from 'config';
import { LAND_REFERENCE } from 'constants/reference';
import _ from 'lodash';
import { FormattedMessage } from 'react-intl';
import { Land, LandImage, LandSpecific, LandType } from 'types/contacts/land';

export function landDefaultImageUrl(land: Land | undefined) {
    if (land && land.defaultImage) {
        return `${config.urlDocument}/${land.societyId}/${land.defaultImage.fileManager.encryptedName}`;
    }
    return null;
}

export function landImageUrl(landImage: LandImage) {
    return `${config.urlDocument}/${landImage.land.societyId}/${landImage.fileManager.encryptedName}`;
}

export function landTypeLabel(type: LandType) {
    switch (type) {
        case LandType.DIFFUSE:
            return 'Diffus';
        case LandType.SUBDIVISION:
            return 'Lotissement';
        default:
            return <FormattedMessage id="not-defined" />;
    }
}

export function landSpecificLabel(type: LandSpecific) {
    switch (type) {
        case LandSpecific.BOUNDING:
            return <FormattedMessage id="land-bounding" />;
        case LandSpecific.SERVICED:
            return <FormattedMessage id="land-serviced" />;
        case LandSpecific.SEWER:
            return <FormattedMessage id="land-sewer" />;
        default:
            return <FormattedMessage id="not-defined" />;
    }
}

export function landFormatTitle(title: string) {
    return title.length > 0 ? title : <FormattedMessage id="not-specified" />;
}

export function landFormatReference(id: number) {
    return Array(LAND_REFERENCE + 1 - _.toString(id).length).join('0') + id;
}

export const landType = [LandType.NOT_DEFINED, LandType.SUBDIVISION, LandType.DIFFUSE];
export const landSpecificities = [LandSpecific.SERVICED, LandSpecific.SEWER, LandSpecific.BOUNDING];
