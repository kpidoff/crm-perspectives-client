import config from 'config';
import _ from 'lodash';
import { MainRouteType } from 'routes/MainRoutes';
import { UserDetails } from 'types/user';

export const userStatusEnum = {
    ACTIF: 'ACTIF',
    BLOCKED: 'BLOCKED',
    CONFIRMATION_OF_CREATION: 'CONFIRMATION_OF_CREATION',
    DELETED: 'DELETED'
};

export type TypeFlagDefault = 'access' | 'edit' | 'delete' | 'create' | string;

export function getUserAllowedAccess(user: Pick<UserDetails, 'allowedUserInterfaces'>, key: string, flag: TypeFlagDefault) {
    const access = _.find(user?.allowedUserInterfaces, (v) => v.interface.key === key);
    if (!access) {
        return false;
    }

    if (_.find(access.flags, (v) => v.flag === flag)) {
        return true;
    }

    return false;
}

export function withUserAccess(user: Pick<UserDetails, 'allowedUserInterfaces'> | null | undefined, routes: MainRouteType): MainRouteType {
    if (user) {
        _.remove(routes.children, (c) => {
            if (c.verifiedAccess) {
                return !(getUserAllowedAccess(user, c.key, 'access') || getUserAllowedAccess(user, c.key, 'edit'));
            }
            return false;
        });
        return routes;
    }
    return routes;
}

export function userFullName(lastname: string | undefined | null, firstname: string | undefined | null) {
    return `${firstname} ${lastname}`;
}

export function userAvatar(url: string | undefined | null) {
    if (url) {
        return config.urlDocument + url;
    }
    return 'Error';
}
