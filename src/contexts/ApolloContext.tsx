import React, { createContext, useCallback, useEffect, useReducer, useState } from 'react';

// third-party
import jwtDecode from 'jwt-decode';

// reducer - state management
import {
    ADD_FILES_MANAGER,
    ADD_NOTIFICATION,
    CHANGE_CURRENT_SOCIETY,
    CURRENT_USER,
    DELETE_NOTIFICATION,
    EMAIL_AUTHENTICATION,
    END_UPLOAD_FILES_MANAGER,
    LOGIN,
    LOGOUT,
    UPDATE_CURRENT_SOCIETY
} from 'store/actions';
import accountReducer from 'store/accountReducer';

// project imports
import Loader from 'ui-component/Loader';

// types
import { KeyedObject } from 'types';
import { InitialLoginContextProps, ApolloContextType } from 'types/auth';

// Apollo import
import { ApolloProvider as ApolloProviderApp } from '@apollo/client';
import client, { setTokenApollo } from 'utils/apollo';
import {
    LoginType,
    useCurrentUser,
    useLogin,
    useLoginWithCode,
    useForgotPassword,
    useResetPassword,
    useLogout
} from 'hooks/authentication/useLogin';
import { getUserAllowedAccess as getUserAllowedAccessUtils, TypeFlagDefault } from 'services/user.service';
import _ from 'lodash';
import { AuthSubscriptionProvider } from './AuthSubscription';
import { UserNotification } from 'types/user';
import { Society } from 'types/settings/society';
import { FileType } from 'utils/base64';

// constant
const initialState: InitialLoginContextProps = {
    isLoggedIn: false,
    isInitialized: false,
    user: null
};

const verifyToken: (st: string) => boolean = (serviceToken) => {
    if (!serviceToken) {
        return false;
    }
    const decoded: KeyedObject = jwtDecode(serviceToken);
    return decoded.exp > Date.now() / 1000;
};

const setSession = (serviceToken?: string | null) => {
    if (serviceToken) {
        localStorage.setItem('serviceToken', serviceToken);
        setTokenApollo(serviceToken);
    } else {
        localStorage.removeItem('serviceToken');
        setTokenApollo();
    }
};

// ==============================|| APOLLO CONTEXT & PROVIDER ||============================== //
const ApolloContext = createContext<ApolloContextType | null>(null);

const ApolloProvider = ({ children }: { children: React.ReactElement }) => {
    const [state, dispatch] = useReducer(accountReducer, initialState);
    const { loginWithMail, error, loading, type } = useLogin();
    const { loginWithCode } = useLoginWithCode();
    const { getCurrentUser } = useCurrentUser();
    const { forgotPassword, loading: loadingForgotPassword } = useForgotPassword();
    const { resetPassword, loading: loadingResetPassword } = useResetPassword();
    const { logout: userLogout } = useLogout();

    const [typeConnexion, setTypeConnexion] = useState<LoginType>('LOGIN');

    const changeCurrentSociety = useCallback((society: Society | undefined) => {
        dispatch({
            type: CHANGE_CURRENT_SOCIETY,
            payload: {
                society: society || null
            }
        });
    }, []);

    const currentSociety = state.society;

    useEffect(() => {
        type && setTypeConnexion(type);
    }, [type]);

    const logout = useCallback(() => {
        userLogout();
        setSession(null);
        dispatch({ type: LOGOUT });
    }, [userLogout]);

    useEffect(() => {
        const init = async () => {
            try {
                const serviceToken = window.localStorage.getItem('serviceToken');
                if (serviceToken && verifyToken(serviceToken)) {
                    setSession(serviceToken);
                    getCurrentUser()
                        .then((value) => {
                            dispatch({
                                type: CURRENT_USER,
                                payload: {
                                    isLoggedIn: true,
                                    user: value.data?.currentUser,
                                    notifications: value.data?.currentUser.notifications
                                }
                            });
                        })
                        .catch((err) => {
                            logout();
                        });
                } else {
                    dispatch({
                        type: LOGOUT
                    });
                }
            } catch (err) {
                console.error(err);
                dispatch({
                    type: LOGOUT
                });
            }
        };

        init();
    }, [getCurrentUser, logout]);

    const login = useCallback(
        (email: string, password: string) =>
            loginWithMail(email, password).then((result) => {
                if (result.data?.login.type === 'LOGIN') {
                    const token = result.data?.login.token;
                    setSession(token);
                    dispatch({
                        type: LOGIN,
                        payload: {
                            isLoggedIn: true,
                            user: result.data?.login.user,
                            notifications: result.data?.login.user.notifications
                        }
                    });
                } else {
                    dispatch({
                        type: EMAIL_AUTHENTICATION,
                        payload: {
                            isLoggedIn: false,
                            user: result.data?.login.user,
                            notifications: result.data?.login.user.notifications
                        }
                    });
                }
                let societySelect = _.find(result.data?.login.user.societiesAccess, (society) => society.default === true)?.society;
                if (!societySelect && result.data?.login.user.societiesAccess && result.data?.login.user.societiesAccess.length > 0) {
                    societySelect = result.data?.login.user.societiesAccess[0].society;
                }
                changeCurrentSociety(societySelect);
            }),
        [changeCurrentSociety, loginWithMail]
    );

    const loginCode = useCallback(
        (userId: number, code: string) =>
            loginWithCode(userId, code).then((result) => {
                const token = result.data?.loginAuthentication.token;
                setSession(token);
                setTypeConnexion('LOGIN');
                dispatch({
                    type: LOGIN,
                    payload: {
                        isLoggedIn: true,
                        user: result.data?.loginAuthentication.user,
                        notifications: result.data?.loginAuthentication.user.notifications
                    }
                });
                let societySelect = _.find(
                    result.data?.loginAuthentication.user.societiesAccess,
                    (society) => society.default === true
                )?.society;
                if (
                    !societySelect &&
                    result.data?.loginAuthentication.user.societiesAccess &&
                    result.data?.loginAuthentication.user.societiesAccess.length > 0
                ) {
                    societySelect = result.data?.loginAuthentication.user.societiesAccess[0].society;
                }
                changeCurrentSociety(societySelect);
            }),
        [changeCurrentSociety, loginWithCode]
    );

    const updateUser = useCallback(() => {
        getCurrentUser().then((value) => {
            dispatch({
                type: LOGIN,
                payload: {
                    isLoggedIn: true,
                    user: value.data?.currentUser,
                    notifications: value.data?.currentUser.notifications
                }
            });
        });
    }, [getCurrentUser]);

    const updateCurrentSociety = useCallback((society: Society) => {
        dispatch({
            type: UPDATE_CURRENT_SOCIETY,
            payload: {
                society
            }
        });
    }, []);

    const getUserAllowedAccess = useCallback(
        (key: string, flag: TypeFlagDefault | string) => {
            if (state.user) {
                return getUserAllowedAccessUtils(state.user, key, flag);
            }
            return false;
        },
        [state.user]
    );

    const isAdministratorSociety = useCallback(
        (societyId: number) => !!_.find(state.user?.societiesAccess, (society) => society.societyId === societyId && society.administrator),
        [state.user?.societiesAccess]
    );

    const getUserNotifications = useCallback(() => state.notifications, [state.notifications]);

    const addNotification = useCallback((notification: UserNotification) => {
        dispatch({
            type: ADD_NOTIFICATION,
            payload: {
                notification
            }
        });
    }, []);

    const deleteNotification = useCallback((notification: UserNotification) => {
        dispatch({
            type: DELETE_NOTIFICATION,
            payload: {
                notification
            }
        });
    }, []);

    const addFilesManager = useCallback((filesManager: FileType[]) => {
        dispatch({
            type: ADD_FILES_MANAGER,
            payload: {
                filesManager
            }
        });
    }, []);

    const getFilesManager = useCallback(() => state.filesManager, [state.filesManager]);

    const endUploadFilesManager = useCallback((fileManager: FileType) => {
        dispatch({
            type: END_UPLOAD_FILES_MANAGER,
            payload: {
                fileManager
            }
        });
    }, []);

    const isAdministrator = state.user?.administrator || false;

    if (state.isInitialized !== undefined && !state.isInitialized) {
        return <Loader />;
    }

    return (
        <ApolloContext.Provider
            value={{
                ...state,
                login,
                logout,
                forgotPassword,
                resetPassword,
                error,
                loading,
                type: typeConnexion,
                loginCode,
                loadingResetPassword,
                loadingForgotPassword,
                getUserAllowedAccess,
                updateUser,
                isAdministratorSociety,
                isAdministrator,
                getUserNotifications,
                addNotification,
                deleteNotification,
                changeCurrentSociety,
                currentSociety,
                updateCurrentSociety,
                addFilesManager,
                getFilesManager,
                endUploadFilesManager
            }}
        >
            {state.isLoggedIn ? <AuthSubscriptionProvider>{children}</AuthSubscriptionProvider> : children}
        </ApolloContext.Provider>
    );
};

export const ApolloWrapper = ({ children }: { children: React.ReactElement }) => (
    <ApolloProviderApp client={client}>
        <ApolloProvider>{children}</ApolloProvider>
    </ApolloProviderApp>
);

export default ApolloContext;
