import useAuth from 'hooks/authentication/useAuth';
import { useDeleteUserSubscriptionNotification, useUserSubscriptionAddNotification } from 'hooks/settings/user/useUser';
import { useFileManagerSubscriptionEndUploadFile } from 'hooks/utils/useFileManager';
import React, { createContext, useEffect } from 'react';
import { AuthSubscriptionType } from 'types/auth';
import { toastSuccess } from 'utils/toast';

// ==============================|| AUTH SUBSCRIPTION & PROVIDER ||============================== //
const AuthSubscriptionContext = createContext<AuthSubscriptionType | null>(null);

export const AuthSubscriptionProvider = ({ children }: { children: React.ReactElement }) => {
    const { addNotification, deleteNotification } = useAuth();
    const { notification } = useUserSubscriptionAddNotification();
    const { deleteNotification: deleteNotificationReponse } = useDeleteUserSubscriptionNotification();
    const { fileManager } = useFileManagerSubscriptionEndUploadFile();

    useEffect(() => {
        notification && addNotification(notification);
    }, [addNotification, notification]);

    useEffect(() => {
        deleteNotificationReponse && deleteNotification(deleteNotificationReponse);
    }, [deleteNotification, deleteNotificationReponse]);

    useEffect(() => {
        fileManager && toastSuccess(`'${fileManager.name}' Envoyé avec succès`);
    }, [fileManager]);

    return (
        <AuthSubscriptionContext.Provider
            value={{
                notification
            }}
        >
            {children}
        </AuthSubscriptionContext.Provider>
    );
};

export default AuthSubscriptionContext;
