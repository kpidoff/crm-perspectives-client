import { IconButton, Tooltip } from '@mui/material';
import { IconAlien, IconBrandChrome, IconBrandEdge, IconBrandSafari, IconBrowser, IconDeviceMobile, IconDeviceTablet } from '@tabler/icons';
import { AREA, DEVISE, THOUSAND_SEPARATOR } from 'constants/format';
import _ from 'lodash';
import { FormattedMessage } from 'react-intl';
import NumberFormat from 'react-number-format';
import { globalRoutesManager } from 'routes-manager';

export function scrollTop() {
    window.scroll({
        top: 0,
        left: 0,
        behavior: 'smooth'
    });
}

export function keyValArrToObj(values: { key: string; value: any }[]) {
    return values.reduce((acc, curr) => ({ ...acc, [curr.key]: curr.value }), {});
}

export function formatAddress(postalCode: string | undefined | null, city: string | undefined | null, address: string | undefined | null) {
    let addressFormat: string = '';
    if (!!postalCode && !!city) {
        addressFormat = `${postalCode}, ${city}`;
    }
    if (!postalCode && !!city) {
        addressFormat = city;
    }
    if (!!postalCode && !city) {
        addressFormat = postalCode;
    }

    return postalCode || city || address ? (
        <>
            {addressFormat} <br /> {address}
        </>
    ) : (
        'Non renseigné'
    );
}

export function formatPhone(phone: string | undefined | null) {
    return phone ? <NumberFormat value={phone} displayType="text" format="##.##.##.##.##" /> : 'Non renseigné';
}

export function formatEmail(email: string | undefined | null) {
    return email || 'Non renseigné';
}

export function formatPrice(price: number | undefined) {
    return price ? <NumberFormat value={price} displayType="text" thousandSeparator={THOUSAND_SEPARATOR} suffix={DEVISE} /> : `0${DEVISE}`;
}

export function formatArea(area: number | undefined) {
    return area ? <NumberFormat value={area} displayType="text" thousandSeparator={THOUSAND_SEPARATOR} suffix={AREA} /> : `0${AREA}`;
}

export function titleMenuByKey(key: string) {
    const menu = _.find(globalRoutesManager, (route) => route.id === key);
    console.log(globalRoutesManager);
    return menu?.title || key;
}

export function typeDeviceToIcon(icon: string) {
    switch (icon) {
        case 'mobile':
            return (
                <Tooltip title="typeDevice-mobile">
                    <IconButton>
                        <IconDeviceMobile />
                    </IconButton>
                </Tooltip>
            );

        case 'browser':
            return (
                <Tooltip title="typeDevice-browser">
                    <IconButton>
                        <IconBrowser />
                    </IconButton>
                </Tooltip>
            );
        case 'tablet':
            return (
                <Tooltip title="typeDevice-tablet">
                    <IconButton>
                        <IconDeviceTablet />
                    </IconButton>
                </Tooltip>
            );
        default:
            return (
                <Tooltip title={icon}>
                    <IconButton>
                        <IconAlien />
                    </IconButton>
                </Tooltip>
            );
    }
}

export function typeBrowserToIcon(browser: string) {
    switch (browser) {
        case 'Chrome':
            return (
                <Tooltip title="typeBrowser-chrome">
                    <IconButton>
                        <IconBrandChrome />
                    </IconButton>
                </Tooltip>
            );
        case 'Mobile Safari':
            return (
                <Tooltip title="typeBrowser-mobile-safari">
                    <IconButton>
                        <IconBrandSafari />
                    </IconButton>
                </Tooltip>
            );
        case 'Edge':
            return (
                <Tooltip title="typeBrowser-edge">
                    <IconButton>
                        <IconBrandEdge />
                    </IconButton>
                </Tooltip>
            );
        default:
            return (
                <Tooltip title={browser}>
                    <IconButton>
                        <IconAlien />
                    </IconButton>
                </Tooltip>
            );
    }
}

export function formatFullName(lastname: string | undefined, firstname: string | undefined) {
    return `${firstname || ''} ${lastname || ''}`;
}

export function booleanToString(boolean: boolean | undefined) {
    return boolean ? <FormattedMessage id="yes" /> : <FormattedMessage id="no" />;
}

export function capitalizeFirstLetter(value: string) {
    return value ? value.charAt(0).toUpperCase() + value.slice(1) : '';
}

export function forceCapitalizeFirstLetter(value: string) {
    return value ? value.charAt(0).toUpperCase() + value.slice(1).toLowerCase() : '';
}
