import { Grid, Typography } from '@mui/material';
import { toast } from 'react-toastify';
import { UserNotification } from 'types/user';
import AvatarUserNotificationType from 'ui-component/avatar/avatar-user-notification/AvatarUserNotificationType';
import parse from 'html-react-parser';

export const toastError = (content: string) => {
    toast.error(`😞 ${content}`, {
        position: 'top-right',
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined
    });
};

export const toastSuccess = (content: string) => {
    toast.success(`👋 ${content}`, {
        position: 'top-right',
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined
    });
};

export function toastNotification({ title, content, subtype, type, subTitle }: UserNotification) {
    toast(
        <>
            <Grid container>
                <Grid item xs={12} sx={{ paddingBottom: '10px' }}>
                    <Typography variant="h5">Nouvelle notification</Typography>
                </Grid>
                <Grid container>
                    <Grid item xs={3}>
                        <AvatarUserNotificationType type={type} />
                    </Grid>
                    <Grid item xs={9}>
                        <Grid item xs={12}>
                            <Typography variant="h6">{title}</Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <Typography variant="caption">{parse(content)}</Typography>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </>
    );
}
