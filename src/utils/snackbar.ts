import { AlertProps } from '@mui/material';
import { openSnackbar } from 'store/slices/snackbar';

function snackbar(type: AlertProps['severity'], message: string) {
    return openSnackbar({
        open: true,
        message,
        variant: 'alert',
        alert: {
            color: type,
            severity: type
        },
        anchorOrigin: {
            vertical: 'top',
            horizontal: 'center'
        },
        close: false
    });
}

export default snackbar;
