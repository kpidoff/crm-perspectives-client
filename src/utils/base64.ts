export type FileType = {
    base64: string;
    name: string;
    lastModified: number;
    type: string;
    size: number;
};

export type FileManagerNotification = FileType & {
    inProgress?: boolean;
};

const getBase64 = (file: Blob) =>
    new Promise((resolve) => {
        const reader = new FileReader();

        reader.readAsDataURL(file);
        reader.onload = () => {
            resolve(reader.result);
        };
    });

export const fileInputChange = async (file: File): Promise<FileType | null> =>
    getBase64(file)
        .then((result) => ({
            base64: result as string,
            name: file.name,
            lastModified: file.lastModified,
            type: file.type,
            size: file.size
        }))
        .catch((err) => {
            console.error(err);
            return err;
        });

export const base64ToBlob = (b64Data: string, contentType: string = '', sliceSize: number = 512) =>
    fetch(`data:${contentType};base64,${b64Data}`).then(async (res) => res.blob());
