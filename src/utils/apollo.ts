import { ApolloClient, InMemoryCache, createHttpLink, from, split } from '@apollo/client';
import { setContext } from '@apollo/client/link/context';
import { APOLLO_API } from 'config';
import { removeTypenameFromMutationLink } from 'apollo-remove-typename-mutation-link';
import { Agent } from 'https';
// import { onError } from '@apollo/client/link/error';
// import { loginRoutesManager } from 'routes-manager';
import { GraphQLWsLink } from '@apollo/client/link/subscriptions';
import { createClient } from 'graphql-ws';
import { getMainDefinition } from '@apollo/client/utilities';

const httpLink = createHttpLink({
    uri: APOLLO_API.uri,
    fetchOptions: {
        agent: new Agent({ rejectUnauthorized: false })
    }
});

export function wsLink(token?: string) {
    return new GraphQLWsLink(
        createClient({
            url: APOLLO_API.uriWs || '',
            connectionParams: {
                Authorization: token
            }
        })
    );
}

function splitLink(token?: string) {
    return split(
        ({ query }) => {
            const definition = getMainDefinition(query);
            return definition.kind === 'OperationDefinition' && definition.operation === 'subscription';
        },
        wsLink(token),
        link
    );
}

// const errorLink = onError(({ graphQLErrors, forward, operation }) => {
//     if (graphQLErrors)
//         graphQLErrors.forEach(({ message, locations, path }) => {
//             if (message === 'Not Authorised!') {
//                 window.location.href = loginRoutesManager.login.path;
//             }
//         });
// });

const link = from([removeTypenameFromMutationLink, httpLink]);

const client = new ApolloClient({
    link: splitLink(),
    cache: new InMemoryCache()
});

export function setTokenApollo(token?: string) {
    !token && client.clearStore();
    client.setLink(
        setContext(() => ({
            headers: {
                authorization: token ? `Bearer ${token}` : ''
            }
        })).concat(splitLink(token))
    );
}

export default client;
