// project imports
import { ApolloError, FetchResult } from '@apollo/client';
import { ForgotPasswordResponse, LoginType, ResetPasswordResponse } from 'hooks/authentication/useLogin';
import { TypeFlagDefault } from 'services/user.service';
import { FileManagerNotification, FileType } from 'utils/base64';
import { Society } from './settings/society';
import { UserLogin, UserNotification } from './user';

export interface JWTDataProps {
    userId: string;
}

export type ApolloContextType = {
    isLoggedIn: boolean;
    isInitialized?: boolean;
    user?: UserLogin | null | undefined;
    logout: () => void;
    login: (email: string, password: string) => Promise<void>;
    forgotPassword: (email: string) => Promise<FetchResult<ForgotPasswordResponse, Record<string, any>, Record<string, any>>>;
    resetPassword: (
        token: string,
        password: string
    ) => Promise<FetchResult<ResetPasswordResponse, Record<string, any>, Record<string, any>>>;
    loadingResetPassword: boolean;
    loadingForgotPassword: boolean;
    error: ApolloError | undefined;
    loading: boolean;
    type: LoginType | undefined;
    loginCode: (userId: number, code: string) => Promise<void>;
    getUserAllowedAccess: (key: string, flag: TypeFlagDefault | string) => boolean;
    updateUser: () => void;
    isAdministratorSociety: (societyId: number) => boolean;
    isAdministrator: boolean;
    getUserNotifications: () => UserNotification[] | undefined;
    addNotification: (notification: UserNotification) => void;
    deleteNotification: (notification: UserNotification) => void;
    changeCurrentSociety: (society: Society | undefined) => void;
    currentSociety: Society | null | undefined;
    updateCurrentSociety: (society: Society) => void;
    addFilesManager: (filesManager: FileType[]) => void;
    getFilesManager: () => FileManagerNotification[] | undefined;
    endUploadFilesManager: (fileManager: FileType) => void;
};

export interface InitialLoginContextProps {
    isLoggedIn: boolean;
    isInitialized?: boolean;
    user?: UserLogin | null | undefined;
    notifications?: UserNotification[];
    society?: Society | null;
    filesManager?: FileManagerNotification[];
}

export type AuthSubscriptionType = {};
