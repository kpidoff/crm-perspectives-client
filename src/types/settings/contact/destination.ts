export enum DestinationType {
    NOT_DEFINED = 'NOT_DEFINED',
    ACTIVE = 'ACTIVE',
    LOST_CALL = 'LOST_CALL',
    LOST = 'LOST',
    REVIVED = 'REVIVED',
    SIGN = 'SIGN',
    PENDING = 'PENDING'
}

export type Destination = {
    id: number;
    label: string;
    type: DestinationType;
};
