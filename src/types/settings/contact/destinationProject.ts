export type DestinationProject = {
    id: number;
    label: string;
};
