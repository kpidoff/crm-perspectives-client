export type ProspectMotive = {
    id: number;
    label: string;
};
