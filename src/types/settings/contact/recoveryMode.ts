export type RecoveryMode = {
    id: number;
    label: string;
};
