export type ReasonForLostCall = {
    id: number;
    label: string;
};
