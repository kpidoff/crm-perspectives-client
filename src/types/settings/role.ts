export enum RoleType {
    NOT_DEFINED = 'NOT_DEFINED',
    COMMERCIAL = 'COMMERCIAL',
    DRIVER = 'DRIVER',
    GENERAL_DIRECTOR = 'GENERAL_DIRECTOR',
    ASSISTANT = 'ASSISTANT'
}

export type Role = {
    id: number;
    type: RoleType;
    name: string;
};
