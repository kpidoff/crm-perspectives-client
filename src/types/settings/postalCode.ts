export type PostalCode = {
    id: number;
    city: string;
    postal_code: string;
    departement: string;
    insee: string;
    lat: number;
    lng: number;
};
