export type Society = {
    activated?: boolean;
    address?: string;
    ape?: string;
    city?: string;
    comment?: string;
    email?: string;
    id: number;
    interlocutor?: string;
    lat?: number;
    lng?: number;
    name: string;
    phone?: string;
    postalCode?: string;
    siret?: string;
    smtpActivated?: boolean;
    smtpMail?: string;
    smtpPassword?: string;
    smtpPort?: number;
    smtpServeur?: string;
    smtpTitle?: string;
};
export type SocietyAccess = {
    id: number;
    societyId: number;
    name: string;
    administrator: boolean;
    default: boolean;
    society: Society;
};
export type SocietyEdit = Partial<Omit<Society, 'id'>>;
export type SocietyList = Omit<
    Society,
    'smtpTitle' | 'smtpServeur' | 'smtpPort' | 'smtpPassword' | 'smtpMail' | 'smtpActivated' | 'siret' | 'siret' | 'lng' | 'lat' | 'ape'
>;
