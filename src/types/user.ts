import { Profile } from 'types/user-profile';
import { FileType } from 'utils/base64';
import { Flag } from './utils/flag';
import { Interface } from './interface';
import { Role } from './settings/role';
import { SocietyAccess } from './settings/society';

export interface FollowerCardProps {
    avatar: string;
    follow: number;
    location: string;
    name: string;
}

export interface FriendRequestCardProps extends Profile {
    mutual: number;
}

export interface FriendsCardProps {
    avatar: string;
    location: string;
    name: string;
}

export interface UserProfileCardProps extends UserProfile {
    profile: string;
}

export interface UserSimpleCardProps {
    avatar: string;
    name: string;
    status: string;
}

export interface UserStateProps {
    notifications: UserNotification[];
    error: object | string | null;
}

export type UserProfileStyle2 = {
    image: string;
    name: string;
    designation: string;
    badgeStatus: string;
    subContent: string;
    email: string;
    phone: string;
    location: string;
    progressValue: string;
};

// NEW
export type UserNotificationTypeEnum = 'ERROR' | 'INFO' | 'VALID' | 'WARNING';

export type UserNotificationSubTypeEnum = 'MESSAGE' | 'MESSAGE';

export type UserNotification = {
    active: boolean;
    content: string;
    createdAt: Date;
    id: number;
    params?: string | null;
    subTitle?: string | null;
    subtype: UserNotificationSubTypeEnum;
    title: string;
    type: UserNotificationTypeEnum;
    userId: number;
};

export type UserStatusEnum = 'ACTIF' | 'BLOCKED' | 'CONFIRMATION_OF_CREATION' | 'DELETED';

export type UserEdit = Omit<Partial<UserDetails>, 'societiesAccess' | 'id' | 'avatar'> & {
    id?: number;
    avatar?: FileType | undefined;
    societiesAccess?: Partial<SocietyAccess>[];
};

export type User = {
    id: number;
    email: string;
    administrator: boolean;
    civility?: number | null;
    firstname?: string | null;
    lastname?: string | null;
    status: UserStatusEnum;
    roleId?: number | null;
};

export type UserSocietiesAccess = User & {
    societiesAccess?: SocietyAccess[];
};

export type UserProfile = User & {
    city: string | null;
    postalCode: string | null;
    address: string | null;
    phone: string | null;
    role: Omit<Role, 'id' | 'type'>;
    avatar: string | undefined;
};

export type UserDetails = User &
    UserProfile &
    UserSocietiesAccess & {
        dateOfBirth?: Date | null;
        description?: string | null;
        emailAuthentication?: boolean;
        endDate?: Date | null;
        arrivalDate?: Date | null;
        userInterfacesAccess?: UserInterfaceAccess[];
        allowedUserInterfaces: {
            interface: Pick<Interface, 'key'>;
            flags: Pick<Flag, 'flag'>[];
        }[];
    };

export type UserLogin = UserDetails & {
    notifications: UserNotification[];
};

export type UserInterfaceAccess = {
    id: number;
    flagId: number;
    userId: number;
};

export type UserDevice = {
    id?: number;
    browserName: string;
    osName: string;
    osVersion: string;
    mobileModel: string;
    deviceType: string;
    lat: number;
    lng: number;
    IPv4: string;
    countryCode: string;
    actived?: boolean;
    uuid: string;
    createdAt: Date;
};
