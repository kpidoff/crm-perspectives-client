export type Paginate = {
    page?: number;
    perPage?: number;
};

export type PageCursor = {
    total: number;
    lastPage: number;
    currentPage: number;
    next: number | null;
    prev: number | null;
    perPage: number;
};

export type Sort = 'asc' | 'desc';

export type ListParams<T> = {
    paginate?: Paginate;
    sort?: Sort;
    filter?: T;
};
