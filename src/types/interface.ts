export type Interface = {
    id: number;
    key: string;
    parentId?: number | null;
};
