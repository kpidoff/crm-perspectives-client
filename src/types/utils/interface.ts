import { Flag } from './flag';

export type Interface = {
    id: number;
    key: string;
    parentId: number | null;
    flags: Flag[];
};

export type InterfaceCountChildren = Interface & {
    flags: Omit<Flag, 'interfaceId'>[];
    _count: {
        children: number;
    };
};
