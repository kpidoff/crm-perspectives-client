export type FileManager = {
    id: number;
    name: string;
    type: string;
    size: number;
    openedDate: Date;
    createdAt: Date;
    updatedAt: Date;
    ownerId: number;
    editUserId: number;
    parentId: number;
    societyId: number;
    encryptedName: string;
    _count: {
        children: number;
    };
};
