export type Flag = {
    flag: string;
    id: number;
    interfaceId: number;
};
