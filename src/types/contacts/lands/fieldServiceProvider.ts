export type FieldServiceProvider = {
    id: number;
    label: string;
    lastname: string;
    firstname: string;
    email: string;
    address: string;
    postalCode: string;
    city: string;
    phone: string;
    portable: string;
    societyId: number;
};
