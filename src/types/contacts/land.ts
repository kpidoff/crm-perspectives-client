import { User } from 'types/user';
import { FileManager } from 'types/utils/fileManager';

export enum LandType {
    NOT_DEFINED = 'NOT_DEFINED',
    SUBDIVISION = 'SUBDIVISION',
    DIFFUSE = 'DIFFUSE'
}

export enum LandSpecific {
    SERVICED = 'serviced',
    SEWER = 'sewer',
    BOUNDING = 'bounding'
}

export type LandSubdivision = Pick<Land, 'id' | 'title' | 'price' | 'area' | 'archived'>;

export type Land = {
    id: number;
    createdAt: Date;
    updatedAt: Date;
    createdDate: Date;
    address: string | null;
    postalCode: string | null;
    city: string;
    lat: number | null;
    lng: number | null;
    description: string | null;
    price: number;
    area: number;
    serviced: boolean;
    sewer: boolean;
    bounding: boolean;
    type: LandType;
    archived: boolean;
    subdivisionDescription: string | null;
    title: string;
    rate: number;
    societyId: number;
    fieldServiceProviderId: number | null;
    parentId: number | null;
    defaultImage: LandImage | null;
    subdivisions: LandSubdivision[] | null;
    commercialId: number | null;
    commercial: User;
};

export type LandImage = {
    id: number;
    default: boolean;
    landId: number;
    fileManager: Pick<FileManager, 'encryptedName'>;
    land: {
        societyId: Pick<Land, 'societyId'>;
    };
};

export type LandLocation = Pick<
    Land,
    | 'id'
    | 'title'
    | 'parentId'
    | 'price'
    | 'area'
    | 'address'
    | 'postalCode'
    | 'city'
    | 'lng'
    | 'lat'
    | 'type'
    | 'defaultImage'
    | 'description'
>;
