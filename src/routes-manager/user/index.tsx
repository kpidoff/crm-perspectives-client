import { lazy } from 'react';
import { FormattedMessage } from 'react-intl';
import Loadable from 'ui-component/Loadable';

const UserAccountProfile = Loadable(lazy(() => import('views/user/account-profile')));
const UserAccountNotification = Loadable(lazy(() => import('views/user/account-notification')));

const userRoutesManager = {
    userProfile: {
        id: 'user-profile',
        path: '/user/account-profile',
        element: <UserAccountProfile />,
        title: <FormattedMessage id="profil-menu-accountSetting" />
    },
    userNotification: {
        id: 'user-notification',
        path: '/user/account-notification',
        element: <UserAccountNotification />,
        title: <FormattedMessage id="profil-menu-accountSetting" />
    }
};

export default userRoutesManager;
