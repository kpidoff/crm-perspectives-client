import contactsRoutesManager from './contacts';
import dashboardRoutesManager from './dashboard';
import loginRoutes from './login';
import settingsRoutesManager from './settings';
import userRoutesManager from './user';

export { default as loginRoutesManager } from './login';
export { default as userRoutesManager } from './user';
export { default as settingsRoutesManager } from './settings';

export const globalRoutesManager = {
    ...loginRoutes,
    ...dashboardRoutesManager,
    ...userRoutesManager,
    ...settingsRoutesManager,
    ...contactsRoutesManager
};
