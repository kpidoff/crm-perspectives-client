import { FormattedMessage } from 'react-intl';
import landsRoutesManager from './land';

const contactsRoutesManager = {
    contacts: {
        id: 'contacts',
        path: '/contacts',
        title: <FormattedMessage id="menu-contacts" />,
        caption: 'Gestion des contacts',
        verifiedAccess: true
    },
    ...landsRoutesManager
};

export default contactsRoutesManager;
