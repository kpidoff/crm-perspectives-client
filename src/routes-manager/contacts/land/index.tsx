import { lazy } from 'react';
import { FormattedMessage } from 'react-intl';
import Loadable from 'ui-component/Loadable';

const Lands = Loadable(lazy(() => import('views/contacts/lands/lands')));
const Land = Loadable(lazy(() => import('views/contacts/land')));
const FieldServicesProvider = Loadable(lazy(() => import('views/contacts/lands/fieldServicesProvider/list')));

const landsRoutesManager = {
    landsMenu: {
        id: 'contacts-lands-menu',
        path: '/contacts/lands',
        title: <FormattedMessage id="menu-contacts-lands-menu" />,
        verifiedAccess: true
    },

    lands: {
        id: 'contacts-lands',
        path: '/contacts/lands',
        element: <Lands />,
        title: <FormattedMessage id="menu-contacts-lands" />,
        verifiedAccess: true
    },
    land: {
        id: 'contacts-land',
        path: '/contacts/land/:id',
        element: <Land />,
        title: <FormattedMessage id="menu-contacts-land" />,
        verifiedAccess: true,
        link: (landId: number) => landsRoutesManager.land.path.replace(':id', landId.toString())
    },

    fieldServicesProvider: {
        id: 'contacts-fieldServicesProvider',
        path: '/contacts/lands/field-services-provider/list',
        element: <FieldServicesProvider />,
        title: <FormattedMessage id="menu-contacts-fieldServicesProvider" />,
        verifiedAccess: true
    },
    fieldServiceProvider: {
        id: 'contacts-fieldServiceProvider',
        path: '/contacts/lands/field-services-provider/list',
        element: <FieldServicesProvider />,
        title: <FormattedMessage id="menu-contacts-fieldServiceProvider" />,
        verifiedAccess: true
    }
};
export default landsRoutesManager;
