import { lazy } from 'react';
import Loadable from 'ui-component/Loadable';

const AuthLogin = Loadable(lazy(() => import('views/authentication/authentication/login/index')));
const AuthForgotPassword = Loadable(lazy(() => import('views/authentication/authentication/forgot-password')));
const ResetPassword = Loadable(lazy(() => import('views/authentication/authentication/reset-password')));

const loginRoutes = {
    login: {
        id: 'login',
        path: '/login',
        element: <AuthLogin />,
        title: 'auth-menu-title'
    },
    forgetPassword: {
        id: 'forgot-password',
        path: '/forgot-password',
        element: <AuthForgotPassword />,
        title: 'forgot-menu-title'
    },
    resetPassword: {
        id: 'reset-password',
        path: '/reset-password/:token',
        element: <ResetPassword />,
        title: 'menu.settings.users'
    }
};

export default loginRoutes;
