import { lazy } from 'react';
import { FormattedMessage } from 'react-intl';
import Loadable from 'ui-component/Loadable';

const DashboardDefault = Loadable(lazy(() => import('views/dashboard/default')));
const DashboardAnalytique = Loadable(lazy(() => import('views/dashboard/analytique')));

const dashboardRoutesManager = {
    dashboard: {
        id: 'dashboard',
        path: '/dashboard',
        title: <FormattedMessage id="menu-dashboard" />,
        children: {
            default: {
                id: 'dashboard-default',
                path: '/dashboard/default',
                element: <DashboardDefault />,
                title: <FormattedMessage id="menu-dashboard-default" />
            },
            analytique: {
                id: 'dashboard-analytique',
                path: '/dashboard/analytique',
                element: <DashboardAnalytique />,
                title: <FormattedMessage id="menu-dashboard-analytics" />
            }
        }
    }
};

export default dashboardRoutesManager;
