import { lazy } from 'react';
import { FormattedMessage } from 'react-intl';
import Loadable from 'ui-component/Loadable';

const ListSocieties = Loadable(lazy(() => import('views/settings/societies/societies-list')));
const Society = Loadable(lazy(() => import('views/settings/societies/society')));

const settingsSocietyRoutesManager = {
    societies: {
        id: 'settings-menu-societies',
        path: '/settings/societies',
        title: <FormattedMessage id="menu-settings-societies" />,
        caption: '',
        verifiedAccess: true
    },
    listSocieties: {
        id: 'settings-societies',
        path: '/settings/societies/list',
        element: <ListSocieties />,
        title: <FormattedMessage id="menu-settings-societies-list" />,
        verifiedAccess: true
    },
    society: {
        id: 'settings-society',
        path: '/settings/society/',
        element: <Society />,
        title: <FormattedMessage id="menu-settings-societies" />,
        children: {
            edit: {
                id: 'settings-society-edit',
                path: '/settings/society/edit/:id',
                element: <Society />,
                title: <FormattedMessage id="menu-settings-user" />,
                link: (societyId: number) => settingsSocietyRoutesManager.society.children.edit.path.replace(':id', societyId.toString())
            },
            add: {
                id: 'setting-society-add',
                path: '/settings/society/add',
                element: <Society />,
                title: <FormattedMessage id="menu-settings-user" />
            }
        }
    }
};

export default settingsSocietyRoutesManager;
