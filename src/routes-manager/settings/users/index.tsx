import { lazy } from 'react';
import { FormattedMessage } from 'react-intl';
import Loadable from 'ui-component/Loadable';

const ListUsers = Loadable(lazy(() => import('views/settings/users/users-list')));
const User = Loadable(lazy(() => import('views/settings/users/user')));
const Roles = Loadable(lazy(() => import('views/settings/roles')));

const settingsUserRoutesManager = {
    usersMenu: {
        id: 'settings-users-menu',
        path: '/settings/users',
        title: <FormattedMessage id="menu-settings-users" />,
        verifiedAccess: true
    },

    users: {
        id: 'settings-users',
        path: '/settings/users/list',
        element: <ListUsers />,
        title: <FormattedMessage id="menu-settings-users-list" />,
        verifiedAccess: true
    },
    user: {
        id: 'settings-user',
        path: '/settings/users/',
        element: <User />,
        title: <FormattedMessage id="menu-settings-user" />,
        verifiedAccess: true,
        children: {
            edit: {
                id: 'settings-user',
                path: '/settings/users/edit/:id',
                element: <User />,
                title: <FormattedMessage id="menu-settings-user" />,
                link: (userId: number) => settingsUserRoutesManager.user.children.edit.path.replace(':id', userId.toString())
            },
            create: {
                id: 'settings-user',
                path: '/settings/users/add',
                element: <User />,
                title: <FormattedMessage id="menu-settings-user" />
            }
        }
    },

    roles: {
        id: 'settings-roles',
        path: '/settings/roles/',
        element: <Roles />,
        title: <FormattedMessage id="menu-settings-roles" />,
        verifiedAccess: true
    },
    role: {
        id: 'settings-role',
        path: '/settings/role/',
        element: <Roles />,
        title: <FormattedMessage id="menu-settings-role" />,
        verifiedAccess: true
    }
};

export default settingsUserRoutesManager;
