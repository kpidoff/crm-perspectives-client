import { FormattedMessage } from 'react-intl';
import settingsContactRoutesManager from './contacts';
import settingsSocietyRoutesManager from './society';
import settingsUserRoutesManager from './users';

const settingsRoutesManager = {
    settings: {
        id: 'settings',
        path: '/settings',
        title: <FormattedMessage id="menu-settings-title" />,
        caption: <FormattedMessage id="menu-settings-caption" />,
        verifiedAccess: true
    },
    ...settingsSocietyRoutesManager,
    ...settingsUserRoutesManager,
    ...settingsContactRoutesManager
};

export default settingsRoutesManager;
