import { lazy } from 'react';
import { FormattedMessage } from 'react-intl';
import Loadable from 'ui-component/Loadable';

const Destinations = Loadable(lazy(() => import('views/settings/contacts/destinations')));
const DestinationsProject = Loadable(lazy(() => import('views/settings/contacts/destinationsProject')));
const ProspectMotives = Loadable(lazy(() => import('views/settings/contacts/prospectMotives')));
const ReasonsForLostCall = Loadable(lazy(() => import('views/settings/contacts/reasonsForLostCall')));
const RecoveryModes = Loadable(lazy(() => import('views/settings/contacts/recoveryModes')));

const settingsContactsRoutesManager = {
    contactsMenu: {
        id: 'settings-contacts-menu',
        path: '/settings/contacts',
        title: <FormattedMessage id="menu-settings-contacts" />,
        verifiedAccess: true
    },

    destinations: {
        id: 'settings-contacts-destinations',
        path: '/settings/contacts/destinations',
        element: <Destinations />,
        title: <FormattedMessage id="menu-settings-contacts-destinations" />,
        verifiedAccess: true
    },
    destination: {
        id: 'settings-contacts-destination',
        path: '/settings/contacts/destination',
        title: <FormattedMessage id="menu-settings-contacts-destination" />,
        verifiedAccess: true
    },

    destinationsProject: {
        id: 'settings-contacts-destinationsProject',
        path: '/settings/contacts/destinationsProject',
        element: <DestinationsProject />,
        title: <FormattedMessage id="menu-settings-contacts-destinationsProject" />,
        verifiedAccess: true
    },
    destinationProject: {
        id: 'settings-contacts-destinationProject',
        path: '/settings/contacts/destinationProject',
        title: <FormattedMessage id="menu-settings-contacts-destinationProject" />,
        verifiedAccess: true
    },

    prospectMotives: {
        id: 'settings-contacts-prospectMotives',
        path: '/settings/contacts/prospectMotives',
        element: <ProspectMotives />,
        title: <FormattedMessage id="menu-settings-contacts-prospectMotives" />,
        verifiedAccess: true
    },
    prospectMotive: {
        id: 'settings-contacts-prospectMotive',
        path: '/settings/contacts/prospectMotive',
        title: <FormattedMessage id="menu-settings-contacts-prospectMotive" />,
        verifiedAccess: true
    },

    reasonsForLostCall: {
        id: 'settings-contacts-reasonsForLostCall',
        path: '/settings/contacts/reasonsForLostCall',
        element: <ReasonsForLostCall />,
        title: <FormattedMessage id="menu-settings-contacts-reasonsForLostCall" />,
        verifiedAccess: true
    },
    reasonForLostCall: {
        id: 'settings-contacts-reasonForLostCall',
        path: '/settings/contacts/reasonForLostCall',
        title: <FormattedMessage id="menu-settings-contacts-reasonForLostCall" />,
        verifiedAccess: true
    },

    recoveryModes: {
        id: 'settings-contacts-recoveryModes',
        path: '/settings/contacts/recoveryModes',
        element: <RecoveryModes />,
        title: <FormattedMessage id="menu-settings-contacts-recoveryModes" />,
        verifiedAccess: true
    },
    recoveryMode: {
        id: 'settings-contacts-recoveryMode',
        path: '/settings/contacts/recoveryMode',
        title: <FormattedMessage id="menu-settings-contacts-recoveryMode" />,
        verifiedAccess: true
    }
};

export default settingsContactsRoutesManager;
