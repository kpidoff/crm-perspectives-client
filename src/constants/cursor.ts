import CURSOR_MAP_RED_PNG from '../assets/cursors/map/map-marker-red-number.png';
import CURSOR_MAP_GREEN_PNG from '../assets/cursors/map/map-marker-green-number.png';
import CURSOR_MAP_ORANGE_PNG from '../assets/cursors/map/map-marker-orange-number.png';
import CURSOR_MAP_BLUE_PNG from '../assets/cursors/map/map-marker-blue-number.png';
import CURSOR_MAP_RED_BLUE_PNG from '../assets/cursors/map/map-marker-red-blue-number.png';

import CURSOR_MAP_BLUE_SVG from '../assets/cursors/map/map-marker-blue.svg';
import CURSOR_MAP_GREEN_SVG from '../assets/cursors/map/map-marker-green.svg';
import CURSOR_MAP_ORANGE_SVG from '../assets/cursors/map/map-marker-orange.svg';
import CURSOR_MAP_RED_SVG from '../assets/cursors/map/map-marker-red.svg';

import CURSOR_MAP_BLUE_STAR_SVG from '../assets/cursors/map/map-marker-blue-star.svg';
import CURSOR_MAP_GREEN_STAR_SVG from '../assets/cursors/map/map-marker-green-star.svg';
import CURSOR_MAP_ORANGE_STAR_SVG from '../assets/cursors/map/map-marker-orange-star.svg';
import CURSOR_MAP_RED_STAR_SVG from '../assets/cursors/map/map-marker-red-star.svg';

export type CursorsMapType = 'BLUE' | 'GREEN' | 'ORANGE' | 'RED' | 'RED_BLUE';
export type CursorMapType = 'default' | 'star' | 'number';
export const CURSORS_MAP_NUMBER = {
    BLUE: CURSOR_MAP_BLUE_PNG,
    GREEN: CURSOR_MAP_GREEN_PNG,
    ORANGE: CURSOR_MAP_ORANGE_PNG,
    RED: CURSOR_MAP_RED_PNG,
    RED_BLUE: CURSOR_MAP_RED_BLUE_PNG
};

export const CURSORS_MAP_STAR = {
    BLUE: CURSOR_MAP_BLUE_STAR_SVG,
    GREEN: CURSOR_MAP_GREEN_STAR_SVG,
    ORANGE: CURSOR_MAP_ORANGE_STAR_SVG,
    RED: CURSOR_MAP_RED_STAR_SVG,
    RED_BLUE: CURSOR_MAP_RED_STAR_SVG
};

export const CURSORS_MAP = {
    BLUE: CURSOR_MAP_BLUE_SVG,
    GREEN: CURSOR_MAP_GREEN_SVG,
    ORANGE: CURSOR_MAP_ORANGE_SVG,
    RED: CURSOR_MAP_RED_SVG,
    RED_BLUE: CURSOR_MAP_RED_SVG
};
