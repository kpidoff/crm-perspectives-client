import * as Yup from 'yup';

const validationSchema = Yup.object().shape({
    email: Yup.string().email('email-error-invalid').max(255).required('email-error-required')
});

export default validationSchema;
