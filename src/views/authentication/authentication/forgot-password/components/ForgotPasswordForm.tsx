// material-ui
import { Alert, Box, Fade } from '@mui/material';
import { useDispatch } from 'store';
import { useNavigate } from 'react-router-dom';

// third party
import { Formik } from 'formik';

// project imports
import useAuth from 'hooks/authentication/useAuth';
import useScriptRef from 'hooks/useScriptRef';
import validationSchema from './validation';
import InputOutline from 'ui-component/input/input-outline';
import { FormattedMessage, useIntl } from 'react-intl';
import snackbar from 'utils/snackbar';
import LoadingButton from '@mui/lab/LoadingButton';
import { loginRoutesManager } from 'routes-manager';

// ========================|| APOLLO - FORGOT PASSWORD ||======================== //

const AuthForgotPassword = ({ ...others }) => {
    const scriptedRef = useScriptRef();
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const intl = useIntl();

    const { forgotPassword, loadingForgotPassword } = useAuth();

    return (
        <Formik
            initialValues={{
                email: '',
                submit: null
            }}
            validationSchema={validationSchema}
            onSubmit={async (values, { setErrors, setStatus, setSubmitting }) => {
                try {
                    await forgotPassword(values.email).then(
                        (value) => {
                            setStatus({ success: true });
                            setSubmitting(false);
                            dispatch(snackbar('success', intl.formatMessage({ id: 'forgot-password-checkMail' })));
                            setTimeout(() => {
                                navigate(loginRoutesManager.login.path, { replace: true });
                            }, 1500);
                        },
                        (err: any) => {
                            if (scriptedRef.current) {
                                setStatus({ success: false });
                                setErrors({ submit: err.message });
                                setSubmitting(false);
                            }
                        }
                    );
                } catch (err: any) {
                    console.error(err);
                    if (scriptedRef.current) {
                        setStatus({ success: false });
                        setErrors({ submit: err.message });
                        setSubmitting(false);
                    }
                }
            }}
        >
            {({ errors, handleBlur, handleChange, handleSubmit, isSubmitting, touched, values, isValid, dirty }) => (
                <form noValidate onSubmit={handleSubmit} {...others}>
                    {errors.submit && (
                        <Fade in={!!errors.submit}>
                            <Alert severity="error">
                                <FormattedMessage id={errors.submit} />
                            </Alert>
                        </Fade>
                    )}
                    <InputOutline
                        value={values.email}
                        name="email"
                        id="email"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        label={<FormattedMessage id="email" />}
                        error={Boolean(touched.email && errors.email)}
                        errorMessage={errors.email}
                    />

                    <Box sx={{ mt: 2 }}>
                        <LoadingButton
                            fullWidth
                            size="large"
                            color="secondary"
                            disabled={isSubmitting || !isValid || !dirty}
                            loading={loadingForgotPassword}
                            variant="contained"
                            type="submit"
                        >
                            <FormattedMessage id="app-send-mail" />
                        </LoadingButton>
                    </Box>
                </form>
            )}
        </Formik>
    );
};

export default AuthForgotPassword;
