import * as Yup from 'yup';

const validationSchema = Yup.object().shape({
    password: Yup.string().max(255).required('password-required'),
    confirmPassword: Yup.string()
        .when('password', {
            is: (val: string) => !!(val && val.length > 0),
            then: Yup.string().oneOf([Yup.ref('password')], 'password-confirm-match-error')
        })
        .required('password-confirm-required')
});

export default validationSchema;
