import React from 'react';

// material-ui
import { Alert, Box, Fade } from '@mui/material';

// third party
import { Formik } from 'formik';

// project imports
import useScriptRef from 'hooks/useScriptRef';
import useAuth from 'hooks/authentication/useAuth';

// assets
import InputOutlinePassword from 'ui-component/input/input-outline-password';
import { FormattedMessage, useIntl } from 'react-intl';
import validationSchema from './validation';
import LoadingButton from '@mui/lab/LoadingButton';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'store';
import snackbar from 'utils/snackbar';
import { loginRoutesManager } from 'routes-manager';

// ========================|| FIREBASE - RESET PASSWORD ||======================== //

const ResetPasswordForm = ({ token }: { token: string }) => {
    const scriptedRef = useScriptRef();
    const { resetPassword, loadingResetPassword } = useAuth();
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const intl = useIntl();

    return (
        <Formik
            initialValues={{
                password: '',
                confirmPassword: '',
                submit: null
            }}
            validationSchema={validationSchema}
            onSubmit={async (values, { setErrors, setStatus, setSubmitting }) => {
                try {
                    await resetPassword(token, values.password).then(
                        (value) => {
                            setStatus({ success: true });
                            setSubmitting(false);
                            dispatch(snackbar('success', intl.formatMessage({ id: 'reset-password-success' })));
                            setTimeout(() => {
                                navigate(loginRoutesManager.login.path, { replace: true });
                            }, 1500);
                        },
                        (err: any) => {
                            if (scriptedRef.current) {
                                setStatus({ success: false });
                                setErrors({ submit: err.message });
                                setSubmitting(false);
                            }
                        }
                    );
                } catch (err: any) {
                    console.error(err);
                    if (scriptedRef.current) {
                        setStatus({ success: false });
                        setErrors({ submit: err.message });
                        setSubmitting(false);
                    }
                }
            }}
        >
            {({ errors, handleBlur, handleChange, handleSubmit, isSubmitting, touched, values, isValid, dirty }) => (
                <form noValidate onSubmit={handleSubmit}>
                    {errors.submit && (
                        <Fade in={!!errors.submit}>
                            <Alert severity="error">
                                <FormattedMessage id={errors.submit} />
                            </Alert>
                        </Fade>
                    )}
                    <InputOutlinePassword
                        value={values.password}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        label={<FormattedMessage id="password" />}
                        error={Boolean(touched.password && errors.password)}
                        errorMessage={errors.password}
                        name="password"
                        id="password"
                        strength
                    />
                    <InputOutlinePassword
                        value={values.confirmPassword}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        label={<FormattedMessage id="confirm-password" />}
                        error={Boolean(touched.confirmPassword && errors.confirmPassword)}
                        errorMessage={errors.confirmPassword}
                        name="confirmPassword"
                        id="confirmPassword"
                        strength
                    />
                    <Box sx={{ mt: 2 }}>
                        <LoadingButton
                            fullWidth
                            size="large"
                            color="secondary"
                            disabled={isSubmitting || !isValid || !dirty}
                            loading={loadingResetPassword}
                            variant="contained"
                            type="submit"
                        >
                            <FormattedMessage id="app-validate" />
                        </LoadingButton>
                    </Box>
                </form>
            )}
        </Formik>
    );
};

export default ResetPasswordForm;
