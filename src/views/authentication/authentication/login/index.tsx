// material-ui
import { useTheme } from '@mui/material/styles';
import { Grid, useMediaQuery } from '@mui/material';

// project imports
import BackgroundPattern2 from 'ui-component/cards/BackgroundPattern2';

// assets
import imgMain from 'assets/images/auth/img-a2-login.svg';
import useAuth from 'hooks/authentication/useAuth';
import AuthWrapper2 from 'views/authentication/components/AuthWrapper2';
import CodeVerification from './login-code';
import LoginBase from './login-base';
import AuthSlider from '../../components/AuthSlider';

// ================================|| AUTH2 - LOGIN ||================================ //

const Login = () => {
    const theme = useTheme();
    const matchDownSM = useMediaQuery(theme.breakpoints.down('md'));
    const { type, user } = useAuth();

    return (
        <AuthWrapper2>
            <Grid container justifyContent={matchDownSM ? 'center' : 'space-between'} alignItems="center">
                {type === 'EMAIL_AUTHENTICATION' ? <CodeVerification email={user?.email} /> : <LoginBase />}
                <Grid item md={6} lg={5} sx={{ position: 'relative', alignSelf: 'stretch', display: { xs: 'none', md: 'block' } }}>
                    <BackgroundPattern2>
                        <Grid item container justifyContent="center">
                            <AuthSlider />
                            <Grid item xs={12} sx={{ position: 'relative' }}>
                                <img
                                    alt="Auth method"
                                    src={imgMain}
                                    style={{
                                        maxWidth: '100%',
                                        margin: '0 auto',
                                        display: 'block',
                                        width: 300,
                                        position: 'relative',
                                        zIndex: 5
                                    }}
                                />
                            </Grid>
                        </Grid>
                    </BackgroundPattern2>
                </Grid>
            </Grid>
        </AuthWrapper2>
    );
};

export default Login;
