import { useState } from 'react';

// material-ui
import { useTheme } from '@mui/material/styles';
import { Alert, Button, Fade, Grid, Stack, Typography } from '@mui/material';

// third-party
import OtpInput from 'react-otp-input-rc-17';
import { FormattedMessage } from 'react-intl';
import useAuth from 'hooks/authentication/useAuth';
import { useResendCode } from 'hooks/authentication/useLogin';
import LoadingButton from '@mui/lab/LoadingButton';

// ============================|| STATIC - CODE VERIFICATION ||============================ //

const AuthCodeVerification = () => {
    const theme = useTheme();
    const [otp, setOtp] = useState<string>('');
    const borderColor = theme.palette.mode === 'dark' ? theme.palette.grey[200] : theme.palette.grey[300];
    const [error, setError] = useState<string>();

    const { loginCode, user } = useAuth();
    const { loading, resend } = useResendCode();

    const handleClick = () => {
        user &&
            loginCode(user?.id, otp).catch((err) => {
                setError(err.message);
            });
    };

    return user ? (
        <Grid container spacing={3}>
            <Grid item xs={12}>
                {error && (
                    <Fade in={!!error}>
                        <Alert severity="error">
                            <FormattedMessage id={error} />
                        </Alert>
                    </Fade>
                )}
                <OtpInput
                    value={otp}
                    onChange={(otpNumber: string) => setOtp(otpNumber)}
                    numInputs={4}
                    containerStyle={{ justifyContent: 'space-between' }}
                    inputStyle={{
                        width: '100%',
                        margin: '8px',
                        padding: '10px',
                        border: `1px solid ${borderColor}`,
                        borderRadius: 4,
                        ':hover': {
                            borderColor: theme.palette.primary.main
                        }
                    }}
                    focusStyle={{
                        outline: 'none',
                        border: `2px solid ${theme.palette.primary.main}`
                    }}
                />
            </Grid>
            <Grid item xs={12}>
                <Button disableElevation fullWidth size="large" type="submit" variant="contained" onClick={handleClick}>
                    <FormattedMessage id="app-validate" />
                </Button>
            </Grid>
            <Grid item xs={12}>
                <Stack direction="row" justifyContent="space-between" alignItems="baseline">
                    <Typography>
                        <FormattedMessage id="auth-code-libResend" />
                    </Typography>

                    <LoadingButton size="small" onClick={() => resend(user.email)} loading={loading}>
                        <FormattedMessage id="auth-code-resend-button-label" />
                    </LoadingButton>
                </Stack>
            </Grid>
        </Grid>
    ) : null;
};
export default AuthCodeVerification;
