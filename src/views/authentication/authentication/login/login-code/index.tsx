import { useMediaQuery, Grid, Stack, Typography } from '@mui/material';
import { useTheme } from '@mui/material/styles';
import { Box } from '@mui/system';
import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import AuthFooter from 'ui-component/cards/AuthFooter';
import Logo from 'ui-component/Logo';
import AuthCodeVerification from 'views/authentication/authentication/login/login-code/components/AuthCodeVerification';
import AuthCardWrapper from 'views/authentication/components/AuthCardWrapper';

const CodeVerification = ({ email }: { email: string | undefined }) => {
    const theme = useTheme();
    const matchDownSM = useMediaQuery(theme.breakpoints.down('md'));
    const matchDownMD = useMediaQuery(theme.breakpoints.down('lg'));
    return (
        <Grid item md={6} lg={7} xs={12} sx={{ minHeight: '100vh' }}>
            <Grid
                sx={{ minHeight: '100vh' }}
                container
                alignItems={matchDownSM ? 'center' : 'flex-start'}
                justifyContent={matchDownSM ? 'center' : 'space-between'}
            >
                <Grid item sx={{ display: { xs: 'none', md: 'block' }, m: 3 }}>
                    <Link to="#">
                        <Logo />
                    </Link>
                </Grid>
                <Grid
                    item
                    xs={12}
                    container
                    justifyContent="center"
                    alignItems="center"
                    sx={{ minHeight: { xs: 'calc(100vh - 68px)', md: 'calc(100vh - 152px)' } }}
                >
                    <Stack justifyContent="center" alignItems="center" spacing={5} m={2}>
                        <Box component={Link} to="#" sx={{ display: { xs: 'block', md: 'none' } }}>
                            <Logo />
                        </Box>
                        <AuthCardWrapper border={matchDownMD}>
                            <Grid container spacing={2} justifyContent="center">
                                <Grid item xs={12}>
                                    <Stack alignItems="center" justifyContent="center" spacing={1}>
                                        <Typography color={theme.palette.secondary.main} gutterBottom variant={matchDownSM ? 'h3' : 'h2'}>
                                            <FormattedMessage id="auth-code-title" />
                                        </Typography>
                                        <Typography variant="subtitle1" fontSize="1rem">
                                            <FormattedMessage id="auth-code-subTitle" />
                                        </Typography>
                                        <Typography variant="caption" fontSize="0.875rem" textAlign="center">
                                            <FormattedMessage
                                                id="auth-code-email"
                                                values={{
                                                    email
                                                }}
                                            />
                                        </Typography>
                                    </Stack>
                                </Grid>
                                <Grid item xs={12}>
                                    <AuthCodeVerification />
                                </Grid>
                            </Grid>
                        </AuthCardWrapper>
                    </Stack>
                </Grid>
                <Grid item xs={12} sx={{ m: 3 }}>
                    <AuthFooter />
                </Grid>
            </Grid>
        </Grid>
    );
};

export default CodeVerification;
