import { Grid, Stack, Typography, useMediaQuery } from '@mui/material';
import { Box } from '@mui/system';
import config from 'config';
import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import AuthFooter from 'ui-component/cards/AuthFooter';
import Logo from 'ui-component/Logo';
import AuthCardWrapper from 'views/authentication/components/AuthCardWrapper';
import { useTheme } from '@mui/material/styles';
import LoginApollo from './components/LoginForm';

const LoginBase = () => {
    const theme = useTheme();
    const matchDownSM = useMediaQuery(theme.breakpoints.down('md'));
    const matchDownMD = useMediaQuery(theme.breakpoints.down('lg'));
    return (
        <Grid item md={6} lg={7} xs={12} sx={{ minHeight: '100vh' }}>
            <Grid
                sx={{ minHeight: '100vh' }}
                container
                alignItems={matchDownSM ? 'center' : 'flex-start'}
                justifyContent={matchDownSM ? 'center' : 'space-between'}
            >
                <Grid item sx={{ display: { xs: 'none', md: 'block' }, m: 3 }}>
                    <Link to="#">
                        <Logo />
                    </Link>
                </Grid>
                <Grid
                    item
                    xs={12}
                    container
                    justifyContent="center"
                    alignItems="center"
                    sx={{ minHeight: { xs: 'calc(100vh - 68px)', md: 'calc(100vh - 152px)' } }}
                >
                    <Stack justifyContent="center" alignItems="center" spacing={5} m={2}>
                        <Box component={Link} to="#" sx={{ display: { xs: 'block', md: 'none' } }}>
                            <Logo />
                        </Box>
                        <AuthCardWrapper border={matchDownMD}>
                            <Grid container spacing={2} justifyContent="center">
                                <Grid item>
                                    <Stack alignItems="center" justifyContent="center" spacing={1}>
                                        <Typography color={theme.palette.secondary.main} gutterBottom variant={matchDownSM ? 'h3' : 'h2'}>
                                            <FormattedMessage
                                                id="auth-title"
                                                values={{
                                                    nameApp: config.nameApp
                                                }}
                                            />
                                        </Typography>
                                        <Typography variant="caption" fontSize="16px" textAlign={matchDownSM ? 'center' : 'inherit'}>
                                            <FormattedMessage id="auth-subTitle" />
                                        </Typography>
                                    </Stack>
                                </Grid>
                                <Grid item xs={12}>
                                    <LoginApollo />
                                </Grid>
                            </Grid>
                        </AuthCardWrapper>
                    </Stack>
                </Grid>
                <Grid item xs={12} sx={{ m: 3 }}>
                    <AuthFooter />
                </Grid>
            </Grid>
        </Grid>
    );
};

export default LoginBase;
