import * as Yup from 'yup';

const validationSchema = Yup.object().shape({
    email: Yup.string().email('invalid-email').max(255).required('email-required'),
    password: Yup.string().max(255).required('password-required')
});

export default validationSchema;
