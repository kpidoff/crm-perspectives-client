import { Link } from 'react-router-dom';

// material-ui
import { Alert, Box, Fade, Grid, Stack, Typography } from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';

// third party
import { Formik } from 'formik';

// project imports
import useAuth from 'hooks/authentication/useAuth';
import useScriptRef from 'hooks/useScriptRef';

// assets
import validationSchema from './validation';
import { FormattedMessage } from 'react-intl';
import InputPassword from 'ui-component/input/input-outline-password';
import InputOutline from 'ui-component/input/input-outline';
import { loginRoutesManager } from 'routes-manager';

// ============================|| APOLLO - LOGIN ||============================ //

const LoginForm = () => {
    const scriptedRef = useScriptRef();

    const { login, loading } = useAuth();

    return (
        <>
            <Grid container direction="column" justifyContent="center" spacing={2}>
                <Grid item xs={12} container alignItems="center" justifyContent="center">
                    <Box sx={{ mb: 2 }}>
                        <Typography variant="subtitle1">
                            <FormattedMessage id="auth-form-subTitle" />
                        </Typography>
                    </Box>
                </Grid>
            </Grid>

            <Formik
                initialValues={{
                    email: '',
                    password: '',
                    submit: null
                }}
                validationSchema={validationSchema}
                onSubmit={async (values, { setErrors, setStatus, setSubmitting }) => {
                    try {
                        await login(values.email, values.password).then((err: any) => {
                            if (scriptedRef.current) {
                                setStatus({ success: false });
                                setErrors({ submit: err.message });
                                setSubmitting(false);
                            }
                        });
                    } catch (err: any) {
                        if (scriptedRef.current) {
                            setStatus({ success: false });
                            setErrors({ submit: err.message });
                            setSubmitting(false);
                        }
                    }
                }}
            >
                {({ errors, handleBlur, handleChange, handleSubmit, isSubmitting, touched, values, isValid, dirty }) => (
                    <form noValidate onSubmit={handleSubmit}>
                        {errors.submit && (
                            <Fade in={!!errors.submit}>
                                <Alert severity="error">
                                    <FormattedMessage id={errors.submit} />
                                </Alert>
                            </Fade>
                        )}
                        <InputOutline
                            value={values.email}
                            name="email"
                            id="email"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            label={<FormattedMessage id="email" />}
                            error={Boolean(touched.email && errors.email)}
                            errorMessage={errors.email}
                        />
                        <InputPassword
                            value={values.password}
                            onBlur={handleBlur}
                            onChange={handleChange}
                            label={<FormattedMessage id="password" />}
                            error={Boolean(touched.password && errors.password)}
                            errorMessage={errors.password}
                            name="password"
                            id="password"
                        />
                        <Stack direction="row" alignItems="center" justifyContent="space-between" spacing={1}>
                            <Typography
                                variant="subtitle1"
                                component={Link}
                                to={loginRoutesManager.forgetPassword.path}
                                color="secondary"
                                sx={{ textDecoration: 'none' }}
                            >
                                <FormattedMessage id="auth-form-button-label-forgetPassword" />
                            </Typography>
                        </Stack>
                        <Box sx={{ mt: 2 }}>
                            <LoadingButton
                                fullWidth
                                size="large"
                                color="secondary"
                                disabled={isSubmitting || !isValid || !dirty}
                                loading={loading}
                                variant="contained"
                                type="submit"
                            >
                                <FormattedMessage id="auth-form-signIn" />
                            </LoadingButton>
                        </Box>
                    </form>
                )}
            </Formik>
        </>
    );
};

export default LoginForm;
