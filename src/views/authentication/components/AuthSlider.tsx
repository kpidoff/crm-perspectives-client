import { Grid, Typography } from '@mui/material';
import { AuthSliderProps } from 'types';
import Slider from 'react-slick';

const items: AuthSliderProps[] = [
    {
        title: 'Nous vous construisons une maison qui vous ressemble',
        description:
            'Nous dessinons des plans de maison 100% sur-mesure en respectant votre cahier des charges et votre budget. Chacune de nos maisons est unique !'
    },
    {
        title: "Plus de 30 ans d'expérience dans la construction de maison",
        description:
            'Découvrez des exemples de maisons individuelles que nous avons réalisées depuis 1988 dans différentes régions françaises.'
    },
    {
        title: "Vous manquez d'idées ? Consultez nos avant-projets !",
        description:
            'Découvrez des maisons en 3D dans tous les styles... Ce ne sont que quelques de ce que nous pouvons vous proposer comme constructeur de maison individuelle spécialiste du sur-mesure.'
    }
];

const settings = {
    autoplay: true,
    arrows: false,
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
};

const AuthSlider = () => (
    <Grid item xs={12}>
        <Grid item container justifyContent="center" sx={{ pb: 8 }}>
            <Grid item xs={10} lg={8} sx={{ '& .slick-list': { pb: 2 } }}>
                <Slider {...settings}>
                    {items.map((item, i) => (
                        <Grid key={i} container direction="column" alignItems="center" spacing={3} textAlign="center">
                            <Grid item>
                                <Typography variant="h1">{item.title}</Typography>
                            </Grid>
                            <Grid item>
                                <Typography variant="subtitle2">{item.description}</Typography>
                            </Grid>
                        </Grid>
                    ))}
                </Slider>
            </Grid>
        </Grid>
    </Grid>
);

export default AuthSlider;
