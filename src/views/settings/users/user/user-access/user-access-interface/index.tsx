// project imports
import { useState } from 'react';
import NavBarAccess from './components/NavBarAccess';
import { gridSpacing } from 'store/constant';
import MenuAccess from './components/MenuAccess';
import { InterfaceCountChildren } from 'types/utils/interface';
import { useInterfacesParentCountChildren } from 'hooks/utils/useInterface';
import { UserDetails, UserInterfaceAccess } from 'types/user';
import { useFormikContext } from 'formik';
import _ from 'lodash';
import { Grid } from '@mui/material';
import MenuAccessSkeleton from './components/MenuAccessSkeleton';
import NavBarAccessSkeleton from './components/NavBarAccessSkeleton';
// ==============================|| SAMPLE PAGE ||============================== //

const UserAccessInterface = () => {
    const [currentMenu, setCurrentMenu] = useState<InterfaceCountChildren | null>(null);
    const { interfaces, loading } = useInterfacesParentCountChildren();
    const {
        values: { userInterfacesAccess },
        setFieldValue
    } = useFormikContext<UserDetails>();

    const handleChangeUserAccess = (flagId: number, access: boolean) => {
        if (access) {
            const interfaceAccess: Partial<UserInterfaceAccess> = {
                flagId
            };
            setFieldValue('userInterfacesAccess', userInterfacesAccess ? [...userInterfacesAccess, interfaceAccess] : interfaceAccess);
        } else {
            setFieldValue(
                'userInterfacesAccess',
                _.filter(userInterfacesAccess, (access1) => access1.flagId !== flagId)
            );
        }
    };

    return (
        <Grid container spacing={gridSpacing}>
            <Grid item xs={12} md={3}>
                {!loading && interfaces ? <NavBarAccess interfaces={interfaces} onSelectMenu={setCurrentMenu} /> : <NavBarAccessSkeleton />}
            </Grid>
            <Grid item xs={12} md={9}>
                {!loading && interfaces ? (
                    <MenuAccess currentMenu={currentMenu} userAccess={userInterfacesAccess} setUserAccess={handleChangeUserAccess} />
                ) : (
                    <MenuAccessSkeleton />
                )}
            </Grid>
        </Grid>
    );
};

export default UserAccessInterface;
