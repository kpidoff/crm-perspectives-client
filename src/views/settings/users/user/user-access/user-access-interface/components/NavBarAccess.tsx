// material-ui
import {
    Accordion,
    AccordionDetails,
    AccordionSummary,
    CircularProgress,
    ListItemButton,
    ListItemIcon,
    ListItemText,
    Typography
} from '@mui/material';

// project imports
import { useInterfacesCountByParent } from 'hooks/utils/useInterface';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import _ from 'lodash';
import { InterfaceCountChildren } from 'types/utils/interface';
import useConfig from 'hooks/useConfig';
import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';
import { titleMenuByKey } from 'utils';
import { memo } from 'react';
import SubCard from 'ui-component/cards/SubCard';
import { FormattedMessage } from 'react-intl';

// ==============================|| SAMPLE PAGE ||============================== //

const NavBarAccess = ({
    interfaces,
    onSelectMenu
}: {
    interfaces: InterfaceCountChildren[];
    onSelectMenu: (interfaceChildren: InterfaceCountChildren) => void;
}) => {
    const { borderRadius } = useConfig();
    const NavMenuSingle = ({ inter }: { inter: InterfaceCountChildren }) => (
        <ListItemButton
            sx={{
                borderRadius: `${borderRadius}px`,
                mb: 0.5,
                alignItems: 'flex-start'
            }}
            onClick={() => onSelectMenu(inter)}
        >
            <ListItemIcon sx={{ my: 'auto', minWidth: 36 }}>
                <FiberManualRecordIcon
                    sx={{
                        width: 6,
                        height: 6
                    }}
                    fontSize="medium"
                />
            </ListItemIcon>
            <ListItemText primary={titleMenuByKey(inter.key)} />
        </ListItemButton>
    );

    const NavMenuGroup = ({ inter }: { inter: InterfaceCountChildren }) => {
        const { getInterfacesByParent, interfacesByParent, loading } = useInterfacesCountByParent();
        return (
            <Accordion
                sx={{ position: 'inherit' }}
                onClick={() => {
                    getInterfacesByParent(inter.id);
                }}
                disableGutters
            >
                <AccordionSummary
                    onClick={() => onSelectMenu(inter)}
                    sx={{ marginTop: 0 }}
                    expandIcon={loading ? <CircularProgress size={20} /> : <ExpandMoreIcon />}
                >
                    <Typography variant="h5">{titleMenuByKey(inter.key)}</Typography>
                </AccordionSummary>

                <AccordionDetails>{interfacesByParent && <NavMenu inters={interfacesByParent} />}</AccordionDetails>
            </Accordion>
        );
    };

    const NavMenu = ({ inters }: { inters: InterfaceCountChildren[] }) => (
        <>
            {_.map(inters, (inter) =>
                inter._count.children > 0 ? <NavMenuGroup key={inter.id} inter={inter} /> : <NavMenuSingle key={inter.id} inter={inter} />
            )}
        </>
    );

    return (
        <SubCard title={<FormattedMessage id="user-acces-list-interface-title" />}>{interfaces && <NavMenu inters={interfaces} />}</SubCard>
    );
};

export default memo(NavBarAccess);
