import { CardContent, Checkbox, List, ListItem, ListItemButton, ListItemText } from '@mui/material';
import _ from 'lodash';
import { FormattedMessage } from 'react-intl';
import { UserInterfaceAccess } from 'types/user';
import { InterfaceCountChildren } from 'types/utils/interface';
import SubCard from 'ui-component/cards/SubCard';
import { titleMenuByKey } from 'utils';

const MenuAccess = ({
    currentMenu,
    setUserAccess,
    userAccess
}: {
    currentMenu: InterfaceCountChildren | null;
    setUserAccess: (flagId: number, access: boolean) => void;
    userAccess: Partial<UserInterfaceAccess>[] | undefined;
}) =>
    currentMenu ? (
        <SubCard
            title={
                <>
                    <FormattedMessage id="user-access-interface-title" />

                    {titleMenuByKey(currentMenu.key)}
                </>
            }
        >
            <List dense>
                {_.map(currentMenu.flags, (flag) => {
                    const labelId = `checkbox-list-secondary-label-${flag.id}`;
                    return (
                        <ListItem
                            key={flag.id}
                            secondaryAction={
                                <Checkbox
                                    edge="end"
                                    onChange={(values) => setUserAccess(flag.id, values.currentTarget.checked)}
                                    checked={!!_.find(userAccess, (access) => access.flagId === flag.id)}
                                    inputProps={{ 'aria-labelledby': labelId }}
                                />
                            }
                            disablePadding
                        >
                            <ListItemButton>
                                <ListItemText id={labelId} primary={<FormattedMessage id={`flag-${flag.flag}`} />} />
                            </ListItemButton>
                        </ListItem>
                    );
                })}
            </List>
        </SubCard>
    ) : (
        <SubCard title={<FormattedMessage id="user-access-interface-title-none" />}>
            <CardContent>
                <FormattedMessage id="user-access-interface-content-none" />
            </CardContent>
        </SubCard>
    );

export default MenuAccess;
