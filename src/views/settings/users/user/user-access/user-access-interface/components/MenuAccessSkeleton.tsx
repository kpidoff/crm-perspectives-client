import { Skeleton } from '@mui/material';
import SubCard from 'ui-component/cards/SubCard';

const MenuAccessSkeleton = () => (
    <SubCard title={<span>Interface</span>}>
        <Skeleton />
        <Skeleton />
    </SubCard>
);

export default MenuAccessSkeleton;
