// material-ui
import { Skeleton } from '@mui/material';

// project imports
import SubCard from 'ui-component/cards/SubCard';

// ==============================|| SAMPLE PAGE ||============================== //

const NavBarAccessSkeleton = () => (
    <SubCard title="Liste des interfaces">
        <Skeleton />
        <Skeleton />
    </SubCard>
);

export default NavBarAccessSkeleton;
