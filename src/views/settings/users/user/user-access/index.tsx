// project imports
import { gridSpacing } from 'store/constant';
import { Grid } from '@mui/material';
import UserAccessInterface from './user-access-interface';
import UserAccessSocieties from './user-access-societies';
// ==============================|| SAMPLE PAGE ||============================== //

const UserAccess = () => (
    <Grid container spacing={gridSpacing}>
        <Grid item xs={12}>
            <UserAccessInterface />
        </Grid>
        <Grid item xs={12}>
            <UserAccessSocieties />
        </Grid>
    </Grid>
);

export default UserAccess;
