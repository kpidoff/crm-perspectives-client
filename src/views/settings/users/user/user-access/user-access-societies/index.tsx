// project imports
import { Checkbox, FormControlLabel, IconButton, ListItem, ListItemButton, ListItemIcon, ListItemText, Tooltip } from '@mui/material';
import { NUMER_PER_PAGE } from 'constants/paginate';
import { useFormikContext } from 'formik';
import useAuth from 'hooks/authentication/useAuth';
import { useSocieties } from 'hooks/settings/society/useSociety';
import _ from 'lodash';
import { useEffect, useState } from 'react';
import { UserDetails } from 'types/user';
import SubCard from 'ui-component/cards/SubCard';
import AdminPanelSettingsIcon from '@mui/icons-material/AdminPanelSettings';
import { SocietyAccess } from 'types/settings/society';
import { FormattedMessage } from 'react-intl';
import DatasListWrapper from 'ui-component/datas-components/components/datas-components-wrapper/datas-list-wrapper';
// ==============================|| SAMPLE PAGE ||============================== //

const UserAccessSocieties = () => {
    const { getSocieties, societies, pageCursor, loading } = useSocieties();
    const { isAdministratorSociety, isAdministrator, user } = useAuth();
    const [currentPage, setCurrentPage] = useState(1);

    useEffect(() => {
        getSocieties({
            paginate: {
                page: currentPage,
                perPage: NUMER_PER_PAGE
            }
        });
    }, [currentPage, getSocieties]);

    const {
        values: { societiesAccess, id, administrator },
        setFieldValue
    } = useFormikContext<UserDetails>();

    const handleChangeAccess = (societyId: number, access: boolean) => {
        if (access) {
            const societyAccess: Partial<SocietyAccess> = {
                societyId,
                administrator: false
            };
            setFieldValue('societiesAccess', societiesAccess ? [...societiesAccess, societyAccess] : societyAccess);
        } else {
            setFieldValue(
                'societiesAccess',
                _.filter(societiesAccess, (society) => society.societyId !== societyId)
            );
        }
    };

    const handleChangeAccessAdmin = (societyId: number, admin: boolean) => {
        setFieldValue(
            'societiesAccess',
            _.map(societiesAccess, (society) => {
                if (society.societyId === societyId) {
                    return {
                        ...society,
                        administrator: admin
                    };
                }
                return society;
            })
        );
    };

    const handleChangeDefault = (societyId: number, defaultSociety: boolean) => {
        setFieldValue(
            'societiesAccess',
            _.map(societiesAccess, (society) => {
                if (society.societyId === societyId) {
                    return {
                        ...society,
                        default: defaultSociety
                    };
                }
                return {
                    ...society,
                    default: false
                };
            })
        );
    };

    return (
        <SubCard title={<FormattedMessage id="user-access-societies-title" />}>
            <DatasListWrapper
                dataComponent={{
                    datas: societies,
                    itemsPerPage: NUMER_PER_PAGE,
                    loading
                }}
                datasComponentContent={{
                    component: (society: SocietyAccess) => {
                        const isAdministratorAccess = !!_.find(
                            societiesAccess,
                            (societyAccess) => societyAccess.societyId === society.id && societyAccess.administrator
                        );
                        const currentUserIsAdministrator = user?.id === id && isAdministrator;
                        return (
                            <ListItem
                                key={society.id}
                                disablePadding
                                secondaryAction={
                                    <>
                                        {isAdministratorSociety(society.id) || isAdministrator ? (
                                            <IconButton
                                                onClick={() => handleChangeAccessAdmin(society.id, !isAdministratorAccess)}
                                                color="primary"
                                                component="span"
                                                disabled={currentUserIsAdministrator || administrator}
                                            >
                                                {isAdministratorAccess ? (
                                                    <Tooltip title={<FormattedMessage id="user-access-societies-admin-disabled" />}>
                                                        <AdminPanelSettingsIcon
                                                            color={currentUserIsAdministrator ? 'disabled' : 'success'}
                                                        />
                                                    </Tooltip>
                                                ) : (
                                                    <Tooltip title={<FormattedMessage id="user-access-societies-admin-actived" />}>
                                                        <AdminPanelSettingsIcon color={currentUserIsAdministrator ? 'disabled' : 'error'} />
                                                    </Tooltip>
                                                )}
                                            </IconButton>
                                        ) : null}

                                        {!currentUserIsAdministrator && (
                                            <FormControlLabel
                                                control={
                                                    <Checkbox
                                                        disabled={
                                                            !_.find(
                                                                societiesAccess,
                                                                (societyAccess) => societyAccess.societyId === society.id
                                                            )
                                                        }
                                                        onChange={(event) => handleChangeDefault(society.id, event.currentTarget.checked)}
                                                        checked={
                                                            !!_.find(
                                                                societiesAccess,
                                                                (societyAccess) =>
                                                                    societyAccess.societyId === society.id && societyAccess.default
                                                            )
                                                        }
                                                    />
                                                }
                                                label="Défaut"
                                            />
                                        )}
                                    </>
                                }
                            >
                                <ListItemButton role={undefined} dense>
                                    <ListItemIcon>
                                        <Checkbox
                                            edge="start"
                                            checked={
                                                currentUserIsAdministrator
                                                    ? true
                                                    : !!_.find(societiesAccess, (societyAccess) => societyAccess.societyId === society.id)
                                            }
                                            onChange={(values) => handleChangeAccess(society.id, values.currentTarget.checked)}
                                            disabled={
                                                currentUserIsAdministrator || administrator ? true : !isAdministratorSociety(society.id)
                                            }
                                        />
                                    </ListItemIcon>
                                    <ListItemText id={society.name} primary={society.name} />
                                </ListItemButton>
                            </ListItem>
                        );
                    }
                }}
                datasComponentPaginate={{
                    currentPage: pageCursor?.currentPage,
                    numberOfPage: pageCursor?.lastPage,
                    onChangePage: (page) => setCurrentPage(page)
                }}
            />
        </SubCard>
    );
};

export default UserAccessSocieties;
