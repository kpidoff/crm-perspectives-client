// material-ui
import { LoadingButton } from '@mui/lab';
import { Button, Stack } from '@mui/material';
import { Form, Formik } from 'formik';
import useAuth from 'hooks/authentication/useAuth';
import { useCreateUser, useEditUser, useUserByEmail, useUserProfilById } from 'hooks/settings/user/useUser';
import _ from 'lodash';
import { useEffect, useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { useNavigate, useParams } from 'react-router-dom';
import settingsUserRoutesManager from 'routes-manager/settings/users';
import { gridSpacing } from 'store/constant';
import { UserDetails, UserEdit } from 'types/user';

// project imports
import MainCard from 'ui-component/cards/MainCard';
import TabContent, { TopContentProps } from 'ui-component/tab-content';
import UserChangeStatus from 'ui-component/users/components/UserChangeStatus';
import UserButtonChangeAdministrator from 'ui-component/users/user-button-change-administrator';
import UserAccess from './user-access';
import UserHistory from './user-history';
import UserInfo from './user-info';
import validationSchema from './validation';

// ==============================|| SAMPLE PAGE ||============================== //

const User = () => {
    const intl = useIntl();
    const { getUserAllowedAccess, isAdministrator } = useAuth();
    const navigate = useNavigate();
    const { userByEmail } = useUserByEmail();

    const { id } = useParams();
    const { user: userRequest, loading, getUser } = useUserProfilById();
    const [userEditing, setUserEditing] = useState<Partial<UserDetails> | Partial<UserEdit> | undefined>(undefined);
    const { setUser, loading: loadingEdit } = useEditUser();
    const { createUser, loading: loadingCreate, user: userCreated } = useCreateUser();
    const [open, setOpen] = useState(false);

    useEffect(() => {
        id && getUser(_.toInteger(id));
    }, [getUser, id, userRequest]);

    useEffect(() => {
        userRequest && setUserEditing(userRequest);
    }, [userRequest]);

    useEffect(() => {
        userCreated && navigate(settingsUserRoutesManager.user.children.edit.link(userCreated.id));
    }, [navigate, userCreated]);

    const tabAccess = getUserAllowedAccess(settingsUserRoutesManager.user.id, 'tab-access');
    const tabHistory = getUserAllowedAccess(settingsUserRoutesManager.user.id, 'tab-history');

    const tabContent: TopContentProps[] = [
        {
            title: intl.formatMessage({ id: 'user-tab-info' }),
            component: <UserInfo />
        },
        {
            title: intl.formatMessage({ id: 'user-tab-access' }),
            component: <UserAccess />,
            disabled: !tabAccess
        },
        {
            title: intl.formatMessage({ id: 'user-tab-history' }),
            component: userEditing && userEditing.id ? <UserHistory userId={userEditing.id} /> : null,
            disabled: !tabHistory || !userEditing
        }
    ];

    function buttonChangeStatut() {
        if (userEditing?.administrator && isAdministrator) return true;
        if (userEditing && userEditing.id) return true;
        return false;
    }

    const ButtonAction = () => (
        <Stack direction="row" spacing={gridSpacing}>
            {isAdministrator && userEditing && userEditing.id && (
                <UserButtonChangeAdministrator
                    variant="outlined"
                    userId={userEditing.id}
                    userAdministrator={userEditing.administrator}
                    fullWidth
                />
            )}
            {buttonChangeStatut() && userEditing && userEditing.id && userEditing.status && (
                <Button variant="outlined" color={userEditing.status === 'ACTIF' ? 'error' : 'success'} onClick={() => setOpen(true)}>
                    {userEditing.status === 'ACTIF' ? <FormattedMessage id="disabled" /> : <FormattedMessage id="activated" />}
                </Button>
            )}
        </Stack>
    );

    return (
        <>
            {userEditing && userEditing.id && userEditing.status && (
                <UserChangeStatus userId={userEditing.id} status={userEditing.status} open={open} onClose={() => setOpen(false)} />
            )}

            <MainCard
                title={id ? <FormattedMessage id="user-edit-title" /> : <FormattedMessage id="user-create-title" />}
                secondary={<ButtonAction />}
            >
                <Formik
                    initialValues={
                        userRequest
                            ? {
                                  ...userEditing
                              }
                            : {
                                  ...userEditing,
                                  dateOfBirth: null,
                                  endDate: null,
                                  arrivalDate: null,
                                  administrator: false,
                                  societiesAccess: [],
                                  userInterfacesAccess: []
                              }
                    }
                    validationSchema={validationSchema({
                        initialEmail: userRequest?.email,
                        userByEmail
                    })}
                    enableReinitialize
                    initialStatus={userRequest || loading ? !!userEditing : true}
                    onSubmit={async (values) => {
                        const newUser: UserEdit = {
                            firstname: values.firstname,
                            lastname: values.lastname,
                            address: values.address,
                            administrator: values.administrator,
                            arrivalDate: values.arrivalDate,
                            city: values.city,
                            civility: values.civility,
                            dateOfBirth: values.dateOfBirth,
                            description: values.description,
                            emailAuthentication: values.emailAuthentication,
                            endDate: values.endDate,
                            phone: values.phone,
                            postalCode: values.postalCode,
                            roleId: values.roleId,
                            status: values.status,
                            userInterfacesAccess:
                                values.userInterfacesAccess &&
                                values.userInterfacesAccess.length > 0 &&
                                !_.isEqual(values.userInterfacesAccess, userEditing?.userInterfacesAccess)
                                    ? values.userInterfacesAccess
                                    : undefined,
                            societiesAccess:
                                values.societiesAccess &&
                                values.societiesAccess.length > 0 &&
                                !_.isEqual(values.societiesAccess, userEditing?.societiesAccess)
                                    ? _.map(values.societiesAccess, (societyAcces) => ({
                                          id: societyAcces.id,
                                          societyId: societyAcces.societyId,
                                          administrator: societyAcces.administrator,
                                          default: societyAcces.default
                                      }))
                                    : undefined
                        };
                        values.id ? setUser(_.toInteger(id), newUser) : createUser({ ...newUser, email: values.email });
                    }}
                >
                    {({ isValid, dirty }) => (
                        <Form>
                            <TabContent content={tabContent} loading={loading} />
                            <LoadingButton
                                size="large"
                                color="secondary"
                                disabled={!isValid || !dirty}
                                loading={loadingEdit || loadingCreate}
                                variant="contained"
                                type="submit"
                            >
                                {userEditing?.id ? <FormattedMessage id="edit" /> : <FormattedMessage id="add" />}
                            </LoadingButton>
                        </Form>
                    )}
                </Formik>
            </MainCard>
        </>
    );
};

export default User;
