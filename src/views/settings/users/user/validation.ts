import { OperationVariables, QueryResult } from '@apollo/client';
import { UserByEmailResponse } from 'hooks/settings/user/useUser';
import * as Yup from 'yup';

function validationSchema(emailCheked: {
    userByEmail: (email: string) => Promise<QueryResult<UserByEmailResponse, OperationVariables>>;
    initialEmail: string | undefined;
}) {
    return Yup.object().shape({
        lastname: Yup.string().nullable().required('required-field'),
        firstname: Yup.string().nullable().required('required-field'),
        postalCode: Yup.string().nullable().required('required-field'),
        city: Yup.string().nullable().required('required-field'),
        address: Yup.string().nullable().required('required-field'),
        email: Yup.string()
            .nullable()
            .email('email-error-invalid')
            .required('required-field')
            .test('email-check', 'email-error-already-used', (email) => {
                if (email && email !== emailCheked.initialEmail) {
                    return emailCheked.userByEmail(email).then((value) => !value.data?.userByEmail);
                }
                return true;
            }),
        arrivalDate: Yup.date().nullable().required('required-field').typeError('invalid-date')
    });
}

export default validationSchema;
