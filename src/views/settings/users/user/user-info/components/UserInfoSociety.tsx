import { Grid } from '@mui/material';
import { useFormikContext } from 'formik';
import { FormattedMessage } from 'react-intl';
import { gridSpacing } from 'store/constant';
import { UserDetails } from 'types/user';
import SubCard from 'ui-component/cards/SubCard';
import { InputDate } from 'ui-component/input';

const UserInfoSociety = () => {
    const { status, values, setFieldValue, touched, errors } = useFormikContext<UserDetails>();
    return (
        <SubCard title={<FormattedMessage id="user-info-society-title" />}>
            <Grid container spacing={gridSpacing}>
                <Grid item xs={12}>
                    <InputDate
                        name="arrivalDate"
                        id="arrivalDate"
                        label={<FormattedMessage id="user-label-arrivalDate" />}
                        value={values.arrivalDate}
                        onChange={(value: any) => setFieldValue('arrivalDate', value)}
                        error={Boolean(touched.arrivalDate && errors.arrivalDate)}
                        errorMessage={errors.arrivalDate}
                        required
                        fullWidth
                        skeleton={!status}
                    />
                </Grid>
                <Grid item xs={12}>
                    <InputDate
                        name="endDate"
                        id="endDate"
                        label={<FormattedMessage id="user-label-endDate" />}
                        value={values.endDate}
                        onChange={(value) => setFieldValue('endDate', value)}
                        fullWidth
                        skeleton={!status}
                    />
                </Grid>
            </Grid>
        </SubCard>
    );
};

export default UserInfoSociety;
