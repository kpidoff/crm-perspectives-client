import { useFormikContext } from 'formik';
import { FormattedMessage } from 'react-intl';
import { UserDetails } from 'types/user';
import SubCard from 'ui-component/cards/SubCard';
import SelectRole from 'ui-component/select/select-role';

const UserInfoRole = () => {
    const { status, values, setFieldValue } = useFormikContext<UserDetails>();
    return (
        <SubCard title={<FormattedMessage id="user-info-role-title" />}>
            <SelectRole
                value={values.roleId || null}
                label="Séléction du rôle"
                onChange={(role) => setFieldValue('roleId', role?.id || null)}
                fullWidth
                skeleton={!status}
            />
        </SubCard>
    );
};

export default UserInfoRole;
