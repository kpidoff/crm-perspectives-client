import { Grid } from '@mui/material';
import { useFormikContext } from 'formik';
import { FormattedMessage } from 'react-intl';
import { gridSpacing } from 'store/constant';
import { UserDetails } from 'types/user';
import SubCard from 'ui-component/cards/SubCard';
import { InputDate, InputTextField } from 'ui-component/input';
import TextFieldCity from 'ui-component/input/input-postal-code/input-city';
import TextFieldPostalCode from 'ui-component/input/input-postal-code/input-postal-code';

const UserInfoGeneral = () => {
    const { status, values, handleBlur, handleChange, touched, errors, setFieldValue } = useFormikContext<UserDetails>();
    return (
        <SubCard title={<FormattedMessage id="user-info-general-title" />}>
            <Grid container spacing={gridSpacing}>
                <Grid item xs={12} md={6}>
                    <InputTextField
                        inputProps={{ style: { textTransform: 'uppercase' } }}
                        value={values.lastname}
                        name="lastname"
                        id="lastname"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        label={<FormattedMessage id="lastname" />}
                        error={Boolean(touched.lastname && errors.lastname)}
                        errorMessage={errors.lastname}
                        required
                        fullWidth
                        skeleton={!status}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <InputTextField
                        inputProps={{ style: { textTransform: 'capitalize' } }}
                        value={values.firstname}
                        name="firstname"
                        id="firstname"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        label={<FormattedMessage id="firstname" />}
                        error={Boolean(touched.firstname && errors.firstname)}
                        errorMessage={errors.firstname}
                        required
                        fullWidth
                        skeleton={!status}
                    />
                </Grid>
                <Grid item xs={12}>
                    <Grid container spacing={gridSpacing}>
                        <Grid item xs={12}>
                            <InputTextField
                                inputProps={{ style: { textTransform: 'capitalize' } }}
                                value={values.address}
                                name="address"
                                id="address"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                label={<FormattedMessage id="address" />}
                                error={Boolean(touched.address && errors.address)}
                                errorMessage={errors.address}
                                required
                                fullWidth
                                skeleton={!status}
                            />
                        </Grid>
                        <Grid item xs={12} md={4}>
                            <TextFieldPostalCode
                                onChange={(postalCode) => setFieldValue('postalCode', postalCode)}
                                onSelect={(postalCode) => {
                                    setFieldValue('city', postalCode.city);
                                    setFieldValue('postalCode', postalCode.postal_code);
                                }}
                                value={values.postalCode}
                                label={<FormattedMessage id="postalCode" />}
                                error={Boolean(touched.postalCode && errors.postalCode)}
                                errorMessage={errors.postalCode}
                                required
                                fullWidth
                                skeleton={!status}
                                name="postalCode"
                                id="postalCode"
                                placeholder="Code postal"
                            />
                        </Grid>
                        <Grid item xs={12} md={8}>
                            <TextFieldCity
                                value={values.city}
                                onChange={(city) => setFieldValue('city', city)}
                                onSelect={(postalCode) => {
                                    setFieldValue('city', postalCode.city);
                                    setFieldValue('postalCode', postalCode.postal_code);
                                }}
                                name="city"
                                id="city"
                                label={<FormattedMessage id="city" />}
                                error={Boolean(touched.city && errors.city)}
                                errorMessage={errors.city}
                                required
                                fullWidth
                                skeleton={!status}
                            />
                        </Grid>
                        <Grid item xs={12} lg={6} sm={6} md={5}>
                            <InputDate
                                name="dateOfBirth"
                                id="dateOfBirth"
                                label={<FormattedMessage id="user-label-dateOfBirth" />}
                                value={values.dateOfBirth}
                                onChange={(value) => setFieldValue('dateOfBirth', value)}
                                fullWidth
                                skeleton={!status}
                            />
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </SubCard>
    );
};

export default UserInfoGeneral;
