import { useFormikContext } from 'formik';
import { FormattedMessage } from 'react-intl';
import { UserDetails } from 'types/user';
import SubCard from 'ui-component/cards/SubCard';
import { InputEditor } from 'ui-component/input';

const UserInfoDescription = () => {
    const { status, values, setFieldValue } = useFormikContext<UserDetails>();
    return (
        <SubCard title={<FormattedMessage id="user-info-description-title" />}>
            <InputEditor
                id="description"
                value={values.description || ''}
                onChange={(e) => {
                    setFieldValue('description', e);
                }}
                skeleton={!status}
            />
        </SubCard>
    );
};

export default UserInfoDescription;
