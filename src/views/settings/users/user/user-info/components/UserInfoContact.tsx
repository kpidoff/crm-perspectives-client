import { Grid } from '@mui/material';
import { useFormikContext } from 'formik';
import { FormattedMessage } from 'react-intl';
import { gridSpacing } from 'store/constant';
import { UserDetails } from 'types/user';
import SubCard from 'ui-component/cards/SubCard';
import { InputPhone, InputTextField } from 'ui-component/input';

const UserInfoContact = () => {
    const { status, values, handleBlur, handleChange, touched, errors } = useFormikContext<UserDetails>();

    return (
        <SubCard title={<FormattedMessage id="user-info-contact-title" />}>
            <Grid container spacing={gridSpacing}>
                <Grid item xs={12}>
                    <InputTextField
                        value={values.email}
                        name="email"
                        id="email"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        label={<FormattedMessage id="user-label-email" />}
                        error={Boolean(touched.email && errors.email)}
                        errorMessage={errors.email}
                        required
                        fullWidth
                        skeleton={!status}
                    />
                </Grid>
                <Grid item xs={12}>
                    <InputPhone
                        value={values.phone}
                        name="phone"
                        id="phone"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        label={<FormattedMessage id="phone" />}
                        fullWidth
                        skeleton={!status}
                    />
                </Grid>
            </Grid>
        </SubCard>
    );
};

export default UserInfoContact;
