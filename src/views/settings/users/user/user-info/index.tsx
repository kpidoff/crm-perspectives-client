// material-ui
import { Grid } from '@mui/material';
import { gridSpacing } from 'store/constant';
import UserInfoDescription from './components/UserInfoDescription';
import UserInfoContact from './components/UserInfoContact';

// project imports
import UserInfoGeneral from './components/UserInfoGeneral';
import UserInfoSociety from './components/UserInfoSociety';
import UserInfoRole from './components/UserInfoRole';

// ==============================|| SAMPLE PAGE ||============================== //

const UserInfo = () => (
    <Grid container spacing={gridSpacing}>
        <Grid item xs={12} md={7}>
            <Grid container spacing={gridSpacing}>
                <Grid item xs={12}>
                    <UserInfoGeneral />
                </Grid>
                <Grid item xs={12}>
                    <UserInfoDescription />
                </Grid>
            </Grid>
        </Grid>
        <Grid item xs={12} md={5}>
            <Grid container spacing={gridSpacing}>
                <Grid item xs={12}>
                    <UserInfoContact />
                </Grid>
                <Grid item xs={12}>
                    <UserInfoSociety />
                </Grid>
                <Grid item xs={12}>
                    <UserInfoRole />
                </Grid>
            </Grid>
        </Grid>
    </Grid>
);

export default UserInfo;
