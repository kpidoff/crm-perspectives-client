// material-ui
import { Grid } from '@mui/material';
import { gridSpacing } from 'store/constant';
import UserDevices from 'ui-component/users/user-devices';

// ==============================|| SAMPLE PAGE ||============================== //

const UserHistory = ({ userId }: { userId: number }) => (
    <Grid container spacing={gridSpacing}>
        <Grid item xs={12}>
            <UserDevices userId={userId} />
        </Grid>
    </Grid>
);

export default UserHistory;
