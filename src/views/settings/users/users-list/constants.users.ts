import { NUMER_PER_PAGE } from 'constants/paginate';
import { UserFilterInput } from 'hooks/settings/user/useUser';

export const USERS_NUMER_PER_PAGE = NUMER_PER_PAGE;
export const USERS_DEFAULT_FILTER: UserFilterInput = {
    status: 'ACTIF'
};
