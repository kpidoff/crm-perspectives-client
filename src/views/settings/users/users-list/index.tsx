import { UserFilterInput, useUsers } from 'hooks/settings/user/useUser';
import { useCallback, useEffect, useState } from 'react';
import { USERS_DEFAULT_FILTER, USERS_NUMER_PER_PAGE } from './constants.users';
import useShemaFilter from './users.filters';
import DatasComponentsTable from 'ui-component/datas-components/components/datas-components-wrapper/datas-components-table-wrapper';
import useComponents from 'views/settings/users/users-list/components.user';
import { UserProfile } from 'types/user-profile';
import UserChangeStatus from 'ui-component/users/components/UserChangeStatus';

// ==============================|| USER LIST STYLE 2 ||============================== //
const UsersList = () => {
    const [filter, setFilter] = useState<UserFilterInput>(USERS_DEFAULT_FILTER);
    const [currentPage, setCurrentPage] = useState(1);
    const { getUsers, pageCursor, users, loading } = useUsers();
    const schema = useShemaFilter(filter);
    const { openUser, components, userBlock, setUserBlock } = useComponents();

    useEffect(() => {
        getUsers({
            paginate: {
                perPage: USERS_NUMER_PER_PAGE,
                page: currentPage
            },
            filter
        });
    }, [currentPage, filter, getUsers]);

    const updateFilter = useCallback((value: any, key: string) => {
        setFilter((prevState) => ({
            ...prevState,
            [key]: value
        }));
        setCurrentPage(1);
    }, []);

    const onClear = useCallback(() => {
        setFilter(USERS_DEFAULT_FILTER);
    }, []);

    return (
        <>
            {userBlock && (
                <UserChangeStatus
                    userId={userBlock.id}
                    status={userBlock.status}
                    open={!!userBlock}
                    onClose={() => setUserBlock(undefined)}
                />
            )}
            <DatasComponentsTable
                dataComponent={{
                    datas: users,
                    itemsPerPage: USERS_NUMER_PER_PAGE,
                    loading,
                    filter: {
                        schema,
                        onClear,
                        updateFilter
                    }
                }}
                datasComponentHeader={{
                    datasComponentsSearch: {
                        filterkey: 'search',
                        updateSearch: updateFilter
                    }
                }}
                datasComponentPaginate={{
                    currentPage: pageCursor?.currentPage,
                    numberOfPage: pageCursor?.lastPage,
                    onChangePage: (page) => setCurrentPage(page)
                }}
                datasComponentContent={{
                    onDoubleClick: (data: UserProfile | undefined) => data && openUser(data.id),
                    components
                }}
            />
        </>
    );
};

export default UsersList;
