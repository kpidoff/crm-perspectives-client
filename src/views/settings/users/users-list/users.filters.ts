import useAuth from 'hooks/authentication/useAuth';
import { UserFilterInput } from 'hooks/settings/user/useUser';
import { useIntl } from 'react-intl';
import { userStatusEnum } from 'services/user.service';
import { FilterType } from 'ui-component/datas-components/components/datas-components-drawer/filters.types';

const useShemaFilter = (filters: UserFilterInput): FilterType[] => {
    const intl = useIntl();
    const { user } = useAuth();
    return [
        {
            filterKey: 'society',
            type: 'societies',
            label: intl.formatMessage({ id: 'filter-societies' }),
            value: filters.society,
            defaultExpand: true,
            hidden: user?.societiesAccess && user?.societiesAccess?.length <= 1
        },
        {
            filterKey: 'status',
            type: 'select',
            label: intl.formatMessage({ id: 'users-filter-status' }),
            value: filters.status,
            mapper: (item) => item?.value,
            defaultExpand: true,
            all: true,
            items: [
                {
                    key: '1',
                    label: intl.formatMessage({ id: 'user-status-actif' }),
                    value: userStatusEnum.ACTIF
                },
                {
                    key: '2',
                    label: intl.formatMessage({ id: 'user-status-blocked' }),
                    value: userStatusEnum.BLOCKED
                },
                {
                    key: '3',
                    label: intl.formatMessage({ id: 'user-status-confirmation-of-creation' }),
                    value: userStatusEnum.CONFIRMATION_OF_CREATION
                },
                {
                    key: '4',
                    label: intl.formatMessage({ id: 'user-status-deleted' }),
                    value: userStatusEnum.DELETED
                }
            ]
        }
    ];
};

export default useShemaFilter;
