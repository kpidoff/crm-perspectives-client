import { Avatar, Grid, Typography } from '@mui/material';
import useAuth from 'hooks/authentication/useAuth';
import { FormattedMessage } from 'react-intl';
import { useNavigate } from 'react-router-dom';
import settingsUserRoutesManager from 'routes-manager/settings/users';
import { userAvatar, userFullName } from 'services/user.service';
import { DatasComponentsContentTableComponentsProps } from 'ui-component/datas-components/components/datas-components-content/dataComponentsContent.type';
import EditIcon from '@mui/icons-material/Edit';
import BlockIcon from '@mui/icons-material/Block';
import DoneIcon from '@mui/icons-material/Done';
import { useState } from 'react';
import { IconCrown } from '@tabler/icons';
import { useTheme } from '@mui/material/styles';
import { UserProfile } from 'types/user';
import UserChipRole from 'ui-component/users/components/UserChipRole';

function useComponents() {
    const theme = useTheme();
    const navigate = useNavigate();
    const { getUserAllowedAccess } = useAuth();
    const [userBlock, setUserBlock] = useState<UserProfile | undefined>(undefined);

    const openUser = (id: number | undefined) => {
        accessEdit && id && navigate(settingsUserRoutesManager.user.children.edit.link(id));
    };

    const accessEdit = getUserAllowedAccess(settingsUserRoutesManager.user.id, 'edit');

    const components: DatasComponentsContentTableComponentsProps[] = [
        {
            title: <FormattedMessage id="users-list-label-profile" />,
            content: (data: UserProfile | undefined) => ({
                type: 'header',
                component: data && (
                    <Grid container spacing={2} alignItems="center">
                        <Grid item>
                            <Avatar alt="User 1" src={userAvatar(data.avatar)} />
                        </Grid>
                        <Grid item xs zeroMinWidth>
                            <Typography align="left" variant="subtitle1" component="div">
                                {userFullName(data.lastname, data.firstname)}{' '}
                                {data.administrator && <IconCrown style={{ color: theme.palette.success.dark, width: 14, height: 14 }} />}
                            </Typography>
                            <Typography align="left" variant="subtitle2" noWrap>
                                {data.email}
                            </Typography>
                        </Grid>
                    </Grid>
                )
            })
        },
        {
            title: <FormattedMessage id="address" />,
            content: (data: UserProfile | undefined) => ({
                type: 'formatAddress',
                address: data?.address,
                city: data?.city,
                postalCode: data?.postalCode
            })
        },
        {
            title: <FormattedMessage id="phone" />,
            content: (data: UserProfile | undefined) => ({
                type: 'formatPhone',
                phone: data?.phone
            })
        },
        {
            title: <FormattedMessage id="email" />,
            content: (data: UserProfile | undefined) => ({
                type: 'formatEmail',
                email: data?.email
            })
        },
        {
            title: <FormattedMessage id="role" />,
            content: (data: UserProfile | undefined) => ({
                type: 'element',
                component: <UserChipRole name={data?.role?.name} />
            })
        },
        {
            title: <FormattedMessage id="actions" />,
            content: (data: UserProfile | undefined) => ({
                type: 'action',
                items: [
                    {
                        label: <FormattedMessage id="edit" />,
                        icon: EditIcon,
                        onClick: () => openUser(data?.id),
                        visible: accessEdit
                    },
                    {
                        label: data && data.status === 'ACTIF' ? <FormattedMessage id="disabled" /> : <FormattedMessage id="activated" />,
                        icon: data && data.status === 'ACTIF' ? BlockIcon : DoneIcon,
                        buttonProps: {
                            color: data && data.status === 'ACTIF' ? 'error' : 'success'
                        },
                        onClick: () => data && setUserBlock(data),
                        visible: accessEdit
                    }
                ]
            })
        }
    ];
    return {
        components,
        userBlock,
        setUserBlock,
        openUser
    };
}

export default useComponents;
