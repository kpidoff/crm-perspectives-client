import { RolesFilterInput } from 'hooks/settings/role/useRole';

// eslint-disable-next-line import/prefer-default-export
export const ROLES_DEFAULT_FILTER: RolesFilterInput = {
    type: undefined
};
