import { Grid, Typography, Button } from '@mui/material';
import { Role } from 'types/settings/role';
import { Form, Formik } from 'formik';
import { LoadingButton } from '@mui/lab';
import { FormattedMessage } from 'react-intl';
import RoleForm from './components/RoleForm';
import { gridSpacing } from 'store/constant';
import validationSchema from './validation';
import { useCreateRole, useDeleteRole, useEditRole, useRoleByName } from 'hooks/settings/role/useRole';
import { roleTypeEnum } from 'services/settings/contact/role.service';
import useAuth from 'hooks/authentication/useAuth';
import settingsUserRoutesManager from 'routes-manager/settings/users';
import ButtonConfirm from 'ui-component/button/button-confirm';
import { useEffect } from 'react';
import DatasListNameEditing from 'ui-component/datas-components/components/datas-components-wrapper/datas-list-name-with-editing/components/DatasListNameEditing';

interface RoleSheetProps {
    role?: Role | null;
    onClose?: () => void;
}

const RoleSheet = ({ role, onClose }: RoleSheetProps) => {
    const { getRoleByName } = useRoleByName();
    const { editRole, loading: loadingEdit, role: roleEditReponse } = useEditRole();
    const { createRole, loading: loadingCreate, role: roleCreateReponse } = useCreateRole();
    const { deleteRole, loading: loadingDelete, message: messageDelete } = useDeleteRole();
    const { getUserAllowedAccess } = useAuth();
    const deleteAccess = getUserAllowedAccess(settingsUserRoutesManager.role.id, 'delete');

    useEffect(() => {
        messageDelete && onClose?.();
    }, [messageDelete, onClose]);

    useEffect(() => {
        roleCreateReponse && onClose?.();
    }, [roleCreateReponse, onClose]);

    useEffect(() => {
        roleEditReponse && onClose?.();
    }, [roleEditReponse, onClose]);

    return (
        <DatasListNameEditing
            onClose={onClose}
            header={
                <>
                    <Typography variant="h5" component="div" sx={{ fontSize: '1rem' }}>
                        {role ? <FormattedMessage id="role-form-title-edit" /> : <FormattedMessage id="role-form-title-create" />}
                    </Typography>
                    {role && (
                        <Typography variant="caption" component="div">
                            {role.name}
                        </Typography>
                    )}
                </>
            }
        >
            <Formik
                initialValues={{
                    ...role,
                    type: !role ? roleTypeEnum.NOT_DEFINED.key : role.type
                }}
                validationSchema={validationSchema({
                    roleByName: getRoleByName,
                    initialName: role?.name || ''
                })}
                enableReinitialize
                onSubmit={async (values) => {
                    role && role.id ? editRole(values) : createRole(values);
                }}
            >
                {({ isValid, dirty }) => (
                    <Form>
                        <Grid container spacing={gridSpacing}>
                            <Grid item xs={12}>
                                <RoleForm />
                            </Grid>
                            <Grid item xs={12}>
                                <Grid container spacing={1}>
                                    <Grid item xs={deleteAccess && role ? 4 : 6}>
                                        <LoadingButton
                                            color="secondary"
                                            disabled={!isValid || !dirty}
                                            loading={loadingEdit || loadingCreate}
                                            variant="contained"
                                            type="submit"
                                            fullWidth
                                        >
                                            {role ? <FormattedMessage id="edit" /> : <FormattedMessage id="add" />}
                                        </LoadingButton>
                                    </Grid>
                                    <Grid item xs={deleteAccess && role ? 4 : 6}>
                                        <Button variant="outlined" fullWidth onClick={onClose}>
                                            <FormattedMessage id="cancel" />
                                        </Button>
                                    </Grid>
                                    {deleteAccess && role && (
                                        <Grid item xs={4}>
                                            <ButtonConfirm
                                                title={<FormattedMessage id="role-delete-title" />}
                                                content={<FormattedMessage id="role-delete-content" />}
                                                labelButtonCancel={<FormattedMessage id="cancel" />}
                                                labelButtonConfirm={<FormattedMessage id="yes" />}
                                                onValidate={() => deleteRole(role.id)}
                                                loading={loadingDelete}
                                                color="error"
                                                variant="outlined"
                                                fullWidth
                                            >
                                                <FormattedMessage id="app-delete" />
                                            </ButtonConfirm>
                                        </Grid>
                                    )}
                                </Grid>
                            </Grid>
                        </Grid>
                    </Form>
                )}
            </Formik>
        </DatasListNameEditing>
    );
};

export default RoleSheet;
