import { Grid } from '@mui/material';
import { useFormikContext } from 'formik';
import _ from 'lodash';
import { FormattedMessage } from 'react-intl';
import { roleTypeEnum } from 'services/settings/contact/role.service';
import { gridSpacing } from 'store/constant';
import { Role } from 'types/settings/role';
import { InputTextField } from 'ui-component/input';
import InputMultipleCheckbox, { InputMultipleCheckboxType } from 'ui-component/input/input-multiple-checkbox';

const RoleForm = () => {
    const { values, handleBlur, handleChange, touched, errors, setFieldValue } = useFormikContext<Role>();

    const handleChangeCheckbox = (value: string) => {
        setFieldValue('type', value);
    };

    const items: InputMultipleCheckboxType[] = _.map(roleTypeEnum, (type) => ({
        label: type.label,
        onChange: () => handleChangeCheckbox(type.key),
        checked: values.type === type.key
    }));

    return (
        <Grid container spacing={gridSpacing}>
            <Grid item xs={12}>
                <InputTextField
                    inputProps={{ style: { textTransform: 'capitalize' } }}
                    value={values.name}
                    name="name"
                    id="name"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    label={<FormattedMessage id="role-form-name-label" />}
                    error={Boolean(touched.name && errors.name)}
                    errorMessage={errors.name}
                    required
                    autoFocus
                    fullWidth
                />
            </Grid>
            <Grid item xs={12}>
                <InputMultipleCheckbox items={items} label={<FormattedMessage id="role-form-type-label" />} />
            </Grid>
        </Grid>
    );
};

export default RoleForm;
