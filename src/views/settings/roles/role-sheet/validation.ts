import { OperationVariables, QueryResult } from '@apollo/client';
import { RoleByNameResponse } from 'hooks/settings/role/useRole';
import * as Yup from 'yup';

function validationSchema(nameCheked: {
    roleByName: (name: string) => Promise<QueryResult<RoleByNameResponse, OperationVariables>>;
    initialName: string;
}) {
    return Yup.object().shape({
        name: Yup.string()
            .nullable()
            .required('role-name-required')
            .test('role-check', 'name-error-already-used', (name) => {
                if (name && name.toUpperCase() !== nameCheked.initialName.toUpperCase()) {
                    return nameCheked.roleByName(name).then((value) => !value.data?.roleByName);
                }
                return true;
            })
    });
}

export default validationSchema;
