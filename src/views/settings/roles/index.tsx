// material-ui
import { useCallback, useEffect, useState } from 'react';
import { FormattedMessage } from 'react-intl';

// project imports
import MainCard from 'ui-component/cards/MainCard';
import { NUMER_PER_PAGE } from 'constants/paginate';
import SpeedDialButton, { SpeedDialButtonType } from 'ui-component/speedDial';
import useAuth from 'hooks/authentication/useAuth';
import DatasListNameWithEditWrapper from 'ui-component/datas-components/components/datas-components-wrapper/datas-list-name-with-editing';
import { Typography } from '@mui/material';
import RoleSheet from './role-sheet';
import settingsUserRoutesManager from 'routes-manager/settings/users';
import { RolesFilterInput, useRoles } from 'hooks/settings/role/useRole';
import { Role } from 'types/settings/role';
import { roleTypeEnum } from 'services/settings/contact/role.service';
import useShemaFilter from './roles.filters';
import { ROLES_DEFAULT_FILTER } from './roles.constants';

const Roles = () => {
    const { getUserAllowedAccess } = useAuth();
    const [currentPage, setCurrentPage] = useState(1);
    const [filter, setFilter] = useState<RolesFilterInput>(ROLES_DEFAULT_FILTER);
    const { getRoles, roles, pageCursor, loading } = useRoles();
    const [role, setRole] = useState<Role | undefined | null>(undefined);

    const schema = useShemaFilter(filter);

    const editAccess = getUserAllowedAccess(settingsUserRoutesManager.role.id, 'edit');
    const createAccess = getUserAllowedAccess(settingsUserRoutesManager.role.id, 'create');

    useEffect(() => {
        getRoles({
            paginate: {
                perPage: NUMER_PER_PAGE,
                page: currentPage
            },
            filter
        });
    }, [currentPage, getRoles, filter]);

    const actions: SpeedDialButtonType[] = [
        {
            name: <FormattedMessage id="roles-add" />,
            action: () => setRole(null)
        }
    ];

    const updateFilter = useCallback((value: any, key: string) => {
        setFilter((prevState) => ({
            ...prevState,
            [key]: value
        }));
        setCurrentPage(1);
    }, []);

    const onClear = useCallback(() => {
        setFilter(ROLES_DEFAULT_FILTER);
    }, []);

    return (
        <MainCard title={<FormattedMessage id="roles-title" />}>
            <DatasListNameWithEditWrapper
                dataComponent={{
                    datas: roles,
                    itemsPerPage: NUMER_PER_PAGE,
                    loading,
                    sortName: 'name',
                    filter: {
                        schema,
                        onClear,
                        updateFilter
                    }
                }}
                datasComponentPaginate={{
                    currentPage: pageCursor?.currentPage,
                    numberOfPage: pageCursor?.lastPage,
                    onChangePage: (page) => setCurrentPage(page)
                }}
                datasComponentContent={{
                    component: (data: Role) => (
                        <>
                            <Typography>{data.name}</Typography>
                            <Typography variant="caption">{roleTypeEnum[data.type].label}</Typography>
                        </>
                    ),
                    onClick: editAccess ? (data: Role) => setRole(data) : undefined
                }}
                datasComponentsSearch={{
                    filterkey: 'search',
                    updateSearch: updateFilter
                }}
                datasListNameEditing={(data) => <RoleSheet role={data} onClose={() => setRole(undefined)} />}
                entity={role}
            />
            {createAccess && <SpeedDialButton actions={actions} />}
        </MainCard>
    );
};

export default Roles;
