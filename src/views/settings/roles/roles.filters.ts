import { RolesFilterInput } from 'hooks/settings/role/useRole';
import _ from 'lodash';
import { useIntl } from 'react-intl';
import { roleTypeEnum } from 'services/settings/contact/role.service';
import selectFilterMappers from 'ui-component/datas-components/components/datas-components-drawer/filter-item/select-filter/mappers';
import { FilterType } from 'ui-component/datas-components/components/datas-components-drawer/filters.types';

const useShemaFilter = (filters: RolesFilterInput): FilterType[] => {
    const intl = useIntl();
    return [
        {
            filterKey: 'type',
            type: 'select',
            label: intl.formatMessage({ id: 'roles-filter-type' }),
            value: filters.type,
            defaultExpand: true,
            all: true,
            mapper: selectFilterMappers.allOrKey,
            items: _.map(roleTypeEnum, (role) => ({
                key: role.key,
                label: role.label,
                value: role.key
            }))
        }
    ];
};

export default useShemaFilter;
