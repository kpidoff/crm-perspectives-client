import { Grid } from '@mui/material';
import { useFormikContext } from 'formik';
import { FormattedMessage } from 'react-intl';
import { gridSpacing } from 'store/constant';
import { RecoveryMode } from 'types/settings/contact/recoveryMode';
import { InputTextField } from 'ui-component/input';

const RecoveryModeForm = () => {
    const { values, handleBlur, handleChange, touched, errors } = useFormikContext<RecoveryMode>();

    return (
        <Grid container spacing={gridSpacing}>
            <Grid item xs={12}>
                <InputTextField
                    inputProps={{ style: { textTransform: 'capitalize' } }}
                    value={values.label}
                    name="label"
                    id="label"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    label={<FormattedMessage id="reasonForLostCall-form-label" />}
                    error={Boolean(touched.label && errors.label)}
                    errorMessage={errors.label}
                    required
                    autoFocus
                    fullWidth
                />
            </Grid>
        </Grid>
    );
};

export default RecoveryModeForm;
