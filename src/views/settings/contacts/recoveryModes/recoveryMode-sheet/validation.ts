import { OperationVariables, QueryResult } from '@apollo/client';
import { RecoveryModeByLabelResponse } from 'hooks/settings/contacts/useRecoveryMode';
import * as Yup from 'yup';

function validationSchema(labelCheked: {
    recoveryModeByLabel: (label: string) => Promise<QueryResult<RecoveryModeByLabelResponse, OperationVariables>>;
    initialLabel: string;
}) {
    return Yup.object().shape({
        label: Yup.string()
            .nullable()
            .required('label-required')
            .test('label-check', 'label-error-already-used', (label) => {
                if (label && label.toUpperCase() !== labelCheked.initialLabel.toUpperCase()) {
                    return labelCheked.recoveryModeByLabel(label).then((value) => !value.data?.recoveryModeByLabel);
                }
                return true;
            })
    });
}

export default validationSchema;
