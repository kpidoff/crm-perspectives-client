import { Grid, Typography, Button } from '@mui/material';
import { Form, Formik } from 'formik';
import { LoadingButton } from '@mui/lab';
import { FormattedMessage } from 'react-intl';
import { gridSpacing } from 'store/constant';
import validationSchema from './validation';
import useAuth from 'hooks/authentication/useAuth';
import ButtonConfirm from 'ui-component/button/button-confirm';
import { useEffect } from 'react';
import settingsContactsRoutesManager from 'routes-manager/settings/contacts';
import RecoveryModeForm from './components/RecoveryModesForm';
import {
    useCreateRecoveryMode,
    useDeleteRecoveryMode,
    useEditRecoveryMode,
    useRecoveryModeByLabel
} from 'hooks/settings/contacts/useRecoveryMode';
import { RecoveryMode } from 'types/settings/contact/recoveryMode';
import DataslistNameEditing from 'ui-component/datas-components/components/datas-components-wrapper/datas-list-name-with-editing/components/DatasListNameEditing';

interface RecoveryModeSheetProps {
    recoveryMode?: RecoveryMode | null;
    onClose?: () => void;
}

const RecoveryModeSheet = ({ recoveryMode, onClose }: RecoveryModeSheetProps) => {
    const { getRecoveryModeByLabel } = useRecoveryModeByLabel();
    const { editRecoveryMode, loading: loadingEdit, recoveryMode: recoveryModeEditReponse } = useEditRecoveryMode();
    const { createRecoveryMode, loading: loadingCreate, recoveryMode: recoveryModeCreateReponse } = useCreateRecoveryMode();
    const { deleteRecoveryMode, loading: loadingDelete, message: messageDelete } = useDeleteRecoveryMode();
    const { getUserAllowedAccess } = useAuth();
    const deleteAccess = getUserAllowedAccess(settingsContactsRoutesManager.recoveryMode.id, 'delete');

    useEffect(() => {
        messageDelete && onClose?.();
    }, [messageDelete, onClose]);

    useEffect(() => {
        recoveryModeCreateReponse && onClose?.();
    }, [recoveryModeCreateReponse, onClose]);

    useEffect(() => {
        recoveryModeEditReponse && onClose?.();
    }, [recoveryModeEditReponse, onClose]);

    return (
        <DataslistNameEditing
            onClose={onClose}
            header={
                <Typography variant="h5" component="div" sx={{ fontSize: '1rem' }}>
                    {recoveryMode ? (
                        <FormattedMessage id="recoveryMode-form-title-edit" />
                    ) : (
                        <FormattedMessage id="recoveryMode-form-title-create" />
                    )}
                </Typography>
            }
        >
            <Formik
                initialValues={{
                    ...recoveryMode
                }}
                validationSchema={validationSchema({
                    recoveryModeByLabel: getRecoveryModeByLabel,
                    initialLabel: recoveryMode?.label || ''
                })}
                enableReinitialize
                onSubmit={async (values) => {
                    recoveryMode && recoveryMode.id ? editRecoveryMode(values) : createRecoveryMode(values);
                }}
            >
                {({ isValid, dirty }) => (
                    <Form>
                        <Grid container spacing={gridSpacing}>
                            <Grid item xs={12}>
                                <RecoveryModeForm />
                            </Grid>
                            <Grid item xs={12}>
                                <Grid container spacing={1}>
                                    <Grid item xs={deleteAccess && recoveryMode ? 4 : 6}>
                                        <LoadingButton
                                            color="secondary"
                                            disabled={!isValid || !dirty}
                                            loading={loadingEdit || loadingCreate}
                                            variant="contained"
                                            type="submit"
                                            fullWidth
                                        >
                                            {recoveryMode ? <FormattedMessage id="edit" /> : <FormattedMessage id="add" />}
                                        </LoadingButton>
                                    </Grid>
                                    <Grid item xs={deleteAccess && recoveryMode ? 4 : 6}>
                                        <Button variant="outlined" fullWidth onClick={onClose}>
                                            <FormattedMessage id="cancel" />
                                        </Button>
                                    </Grid>
                                    {deleteAccess && recoveryMode && (
                                        <Grid item xs={4}>
                                            <ButtonConfirm
                                                title={<FormattedMessage id="recoveryMode-delete-title" />}
                                                content={<FormattedMessage id="recoveryMode-delete-content" />}
                                                labelButtonCancel={<FormattedMessage id="cancel" />}
                                                labelButtonConfirm={<FormattedMessage id="yes" />}
                                                onValidate={() => deleteRecoveryMode(recoveryMode.id)}
                                                loading={loadingDelete}
                                                color="error"
                                                variant="outlined"
                                                fullWidth
                                            >
                                                <FormattedMessage id="app-delete" />
                                            </ButtonConfirm>
                                        </Grid>
                                    )}
                                </Grid>
                            </Grid>
                        </Grid>
                    </Form>
                )}
            </Formik>
        </DataslistNameEditing>
    );
};

export default RecoveryModeSheet;
