// material-ui
import { useCallback, useEffect, useState } from 'react';
import { FormattedMessage } from 'react-intl';

// project imports
import MainCard from 'ui-component/cards/MainCard';
import { RecoveryMode } from 'types/settings/contact/recoveryMode';
import { NUMER_PER_PAGE } from 'constants/paginate';
import SpeedDialButton, { SpeedDialButtonType } from 'ui-component/speedDial';
import settingsContactsRoutesManager from 'routes-manager/settings/contacts';
import useAuth from 'hooks/authentication/useAuth';
import DatasListNameWithEditWrapper from 'ui-component/datas-components/components/datas-components-wrapper/datas-list-name-with-editing';
import { Typography } from '@mui/material';
import { useRecoveryModes } from 'hooks/settings/contacts/useRecoveryMode';
import RecoveryModeSheet from './recoveryMode-sheet';

const RecoveryModes = () => {
    const { getUserAllowedAccess } = useAuth();
    const [currentPage, setCurrentPage] = useState(1);
    const [search, setSearch] = useState('');
    const { getRecoveryModes, recoveryModes, pageCursor, loading } = useRecoveryModes();
    const [recoveryMode, setRecoveryMode] = useState<RecoveryMode | undefined | null>(undefined);

    const editAccess = getUserAllowedAccess(settingsContactsRoutesManager.recoveryMode.id, 'edit');
    const createAccess = getUserAllowedAccess(settingsContactsRoutesManager.recoveryMode.id, 'create');

    useEffect(() => {
        getRecoveryModes({
            paginate: {
                perPage: NUMER_PER_PAGE,
                page: currentPage
            },
            filter: {
                search
            }
        });
    }, [currentPage, getRecoveryModes, search]);

    const actions: SpeedDialButtonType[] = [
        {
            name: <FormattedMessage id="recoveryModes-add" />,
            action: () => setRecoveryMode(null)
        }
    ];

    const updateSearch = useCallback((value: string) => {
        setSearch(value);
    }, []);

    return (
        <MainCard title={<FormattedMessage id="recoveryModes-title" />}>
            <DatasListNameWithEditWrapper
                dataComponent={{
                    datas: recoveryModes,
                    itemsPerPage: NUMER_PER_PAGE,
                    loading,
                    sortName: 'label'
                }}
                datasComponentPaginate={{
                    currentPage: pageCursor?.currentPage,
                    numberOfPage: pageCursor?.lastPage,
                    onChangePage: (page) => setCurrentPage(page)
                }}
                datasComponentContent={{
                    component: (data: RecoveryMode) => <Typography>{data.label}</Typography>,
                    onClick: editAccess ? (data: RecoveryMode) => setRecoveryMode(data) : undefined
                }}
                datasComponentsSearch={{
                    filterkey: 'label',
                    updateSearch
                }}
                datasListNameEditing={(data) => <RecoveryModeSheet recoveryMode={data} onClose={() => setRecoveryMode(undefined)} />}
                entity={recoveryMode}
            />
            {createAccess && <SpeedDialButton actions={actions} />}
        </MainCard>
    );
};

export default RecoveryModes;
