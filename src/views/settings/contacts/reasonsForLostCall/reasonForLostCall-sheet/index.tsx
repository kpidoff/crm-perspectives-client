import { Grid, Typography, Button } from '@mui/material';
import { Form, Formik } from 'formik';
import { LoadingButton } from '@mui/lab';
import { FormattedMessage } from 'react-intl';
import { gridSpacing } from 'store/constant';
import validationSchema from './validation';
import useAuth from 'hooks/authentication/useAuth';
import ButtonConfirm from 'ui-component/button/button-confirm';
import { useEffect } from 'react';
import settingsContactsRoutesManager from 'routes-manager/settings/contacts';
import ReasonForLostCallForm from './components/ReasonsForLostCallForm';
import {
    useCreateReasonForLostCall,
    useDeleteReasonForLostCall,
    useEditReasonForLostCall,
    useReasonForLostCallByLabel
} from 'hooks/settings/contacts/useReasonForLostCall';
import { ReasonForLostCall } from 'types/settings/contact/reasonForLostCall';
import DataslistNameEditing from 'ui-component/datas-components/components/datas-components-wrapper/datas-list-name-with-editing/components/DatasListNameEditing';

interface ReasonForLostCallSheetProps {
    reasonForLostCall?: ReasonForLostCall | null;
    onClose?: () => void;
}

const ReasonForLostCallSheet = ({ reasonForLostCall, onClose }: ReasonForLostCallSheetProps) => {
    const { getReasonForLostCallByLabel } = useReasonForLostCallByLabel();
    const { editReasonForLostCall, loading: loadingEdit, reasonForLostCall: reasonForLostCallEditReponse } = useEditReasonForLostCall();
    const {
        createReasonForLostCall,
        loading: loadingCreate,
        reasonForLostCall: reasonForLostCallCreateReponse
    } = useCreateReasonForLostCall();
    const { deleteReasonForLostCall, loading: loadingDelete, message: messageDelete } = useDeleteReasonForLostCall();
    const { getUserAllowedAccess } = useAuth();
    const deleteAccess = getUserAllowedAccess(settingsContactsRoutesManager.reasonForLostCall.id, 'delete');

    useEffect(() => {
        messageDelete && onClose?.();
    }, [messageDelete, onClose]);

    useEffect(() => {
        reasonForLostCallCreateReponse && onClose?.();
    }, [reasonForLostCallCreateReponse, onClose]);

    useEffect(() => {
        reasonForLostCallEditReponse && onClose?.();
    }, [reasonForLostCallEditReponse, onClose]);

    return (
        <DataslistNameEditing
            onClose={onClose}
            header={
                <Typography variant="h5" component="div" sx={{ fontSize: '1rem' }}>
                    {reasonForLostCall ? (
                        <FormattedMessage id="reasonForLostCall-form-title-edit" />
                    ) : (
                        <FormattedMessage id="reasonForLostCall-form-title-create" />
                    )}
                </Typography>
            }
        >
            <Formik
                initialValues={{
                    ...reasonForLostCall
                }}
                validationSchema={validationSchema({
                    reasonForLostCallByLabel: getReasonForLostCallByLabel,
                    initialLabel: reasonForLostCall?.label || ''
                })}
                enableReinitialize
                onSubmit={async (values) => {
                    reasonForLostCall && reasonForLostCall.id ? editReasonForLostCall(values) : createReasonForLostCall(values);
                }}
            >
                {({ isValid, dirty }) => (
                    <Form>
                        <Grid container spacing={gridSpacing}>
                            <Grid item xs={12}>
                                <ReasonForLostCallForm />
                            </Grid>
                            <Grid item xs={12}>
                                <Grid container spacing={1}>
                                    <Grid item xs={deleteAccess && reasonForLostCall ? 4 : 6}>
                                        <LoadingButton
                                            color="secondary"
                                            disabled={!isValid || !dirty}
                                            loading={loadingEdit || loadingCreate}
                                            variant="contained"
                                            type="submit"
                                            fullWidth
                                        >
                                            {reasonForLostCall ? <FormattedMessage id="edit" /> : <FormattedMessage id="add" />}
                                        </LoadingButton>
                                    </Grid>
                                    <Grid item xs={deleteAccess && reasonForLostCall ? 4 : 6}>
                                        <Button variant="outlined" fullWidth onClick={onClose}>
                                            <FormattedMessage id="cancel" />
                                        </Button>
                                    </Grid>
                                    {deleteAccess && reasonForLostCall && (
                                        <Grid item xs={4}>
                                            <ButtonConfirm
                                                title={<FormattedMessage id="reasonForLostCall-delete-title" />}
                                                content={<FormattedMessage id="reasonForLostCall-delete-content" />}
                                                labelButtonCancel={<FormattedMessage id="cancel" />}
                                                labelButtonConfirm={<FormattedMessage id="yes" />}
                                                onValidate={() => deleteReasonForLostCall(reasonForLostCall.id)}
                                                loading={loadingDelete}
                                                color="error"
                                                variant="outlined"
                                                fullWidth
                                            >
                                                <FormattedMessage id="app-delete" />
                                            </ButtonConfirm>
                                        </Grid>
                                    )}
                                </Grid>
                            </Grid>
                        </Grid>
                    </Form>
                )}
            </Formik>
        </DataslistNameEditing>
    );
};

export default ReasonForLostCallSheet;
