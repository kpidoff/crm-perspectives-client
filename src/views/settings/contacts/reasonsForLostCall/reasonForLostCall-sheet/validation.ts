import { OperationVariables, QueryResult } from '@apollo/client';
import { ReasonForLostCallByLabelResponse } from 'hooks/settings/contacts/useReasonForLostCall';
import * as Yup from 'yup';

function validationSchema(labelCheked: {
    reasonForLostCallByLabel: (label: string) => Promise<QueryResult<ReasonForLostCallByLabelResponse, OperationVariables>>;
    initialLabel: string;
}) {
    return Yup.object().shape({
        label: Yup.string()
            .nullable()
            .required('label-required')
            .test('label-check', 'label-error-already-used', (label) => {
                if (label && label.toUpperCase() !== labelCheked.initialLabel.toUpperCase()) {
                    return labelCheked.reasonForLostCallByLabel(label).then((value) => !value.data?.reasonForLostCallByLabel);
                }
                return true;
            })
    });
}

export default validationSchema;
