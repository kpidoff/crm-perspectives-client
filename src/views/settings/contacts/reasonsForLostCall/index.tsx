// material-ui
import { useCallback, useEffect, useState } from 'react';
import { FormattedMessage } from 'react-intl';

// project imports
import MainCard from 'ui-component/cards/MainCard';
import { ReasonForLostCall } from 'types/settings/contact/reasonForLostCall';
import { NUMER_PER_PAGE } from 'constants/paginate';
import SpeedDialButton, { SpeedDialButtonType } from 'ui-component/speedDial';
import settingsContactsRoutesManager from 'routes-manager/settings/contacts';
import useAuth from 'hooks/authentication/useAuth';
import DatasListNameWithEditWrapper from 'ui-component/datas-components/components/datas-components-wrapper/datas-list-name-with-editing';
import { Typography } from '@mui/material';
import { useReasonsForLostCall } from 'hooks/settings/contacts/useReasonForLostCall';
import ReasonForLostCallSheet from './reasonForLostCall-sheet';

const ReasonsForLostCall = () => {
    const { getUserAllowedAccess } = useAuth();
    const [currentPage, setCurrentPage] = useState(1);
    const [search, setSearch] = useState('');
    const { getReasonsForLostCall, reasonsForLostCall, pageCursor, loading } = useReasonsForLostCall();
    const [reasonForLostCall, setReasonForLostCall] = useState<ReasonForLostCall | undefined | null>(undefined);

    const editAccess = getUserAllowedAccess(settingsContactsRoutesManager.reasonForLostCall.id, 'edit');
    const createAccess = getUserAllowedAccess(settingsContactsRoutesManager.reasonForLostCall.id, 'create');

    useEffect(() => {
        getReasonsForLostCall({
            paginate: {
                perPage: NUMER_PER_PAGE,
                page: currentPage
            },
            filter: {
                search
            }
        });
    }, [currentPage, getReasonsForLostCall, search]);

    const actions: SpeedDialButtonType[] = [
        {
            name: <FormattedMessage id="reasonsForLostCall-add" />,
            action: () => setReasonForLostCall(null)
        }
    ];

    const updateSearch = useCallback((value: string) => {
        setSearch(value);
    }, []);

    return (
        <MainCard title={<FormattedMessage id="reasonsForLostCall-title" />}>
            <DatasListNameWithEditWrapper
                dataComponent={{
                    datas: reasonsForLostCall,
                    itemsPerPage: NUMER_PER_PAGE,
                    loading,
                    sortName: 'label'
                }}
                datasComponentPaginate={{
                    currentPage: pageCursor?.currentPage,
                    numberOfPage: pageCursor?.lastPage,
                    onChangePage: (page) => setCurrentPage(page)
                }}
                datasComponentContent={{
                    component: (data: ReasonForLostCall) => <Typography>{data.label}</Typography>,
                    onClick: editAccess ? (data: ReasonForLostCall) => setReasonForLostCall(data) : undefined
                }}
                datasComponentsSearch={{
                    filterkey: 'label',
                    updateSearch
                }}
                datasListNameEditing={(data) => (
                    <ReasonForLostCallSheet reasonForLostCall={data} onClose={() => setReasonForLostCall(undefined)} />
                )}
                entity={reasonForLostCall}
            />
            {createAccess && <SpeedDialButton actions={actions} />}
        </MainCard>
    );
};

export default ReasonsForLostCall;
