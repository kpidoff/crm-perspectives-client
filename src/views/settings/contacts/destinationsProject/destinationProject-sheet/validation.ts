import { OperationVariables, QueryResult } from '@apollo/client';
import { DestinationProjectByLabelResponse } from 'hooks/settings/contacts/useDestinationProject';
import * as Yup from 'yup';

function validationSchema(labelCheked: {
    destinationProjectByLabel: (label: string) => Promise<QueryResult<DestinationProjectByLabelResponse, OperationVariables>>;
    initialLabel: string;
}) {
    return Yup.object().shape({
        label: Yup.string()
            .nullable()
            .required('label-required')
            .test('label-check', 'label-error-already-used', (label) => {
                if (label && label.toUpperCase() !== labelCheked.initialLabel.toUpperCase()) {
                    return labelCheked.destinationProjectByLabel(label).then((value) => !value.data?.destinationProjectByLabel);
                }
                return true;
            })
    });
}

export default validationSchema;
