import { Grid, Typography, Button } from '@mui/material';
import { Form, Formik } from 'formik';
import { LoadingButton } from '@mui/lab';
import { FormattedMessage } from 'react-intl';
import { gridSpacing } from 'store/constant';
import validationSchema from './validation';
import useAuth from 'hooks/authentication/useAuth';
import ButtonConfirm from 'ui-component/button/button-confirm';
import { useEffect } from 'react';
import { DestinationProject } from 'types/settings/contact/destinationProject';
import {
    useCreateDestinationProject,
    useDeleteDestinationProject,
    useDestinationProjectByLabel,
    useEditDestinationProject
} from 'hooks/settings/contacts/useDestinationProject';
import settingsContactsRoutesManager from 'routes-manager/settings/contacts';
import DestinationProjectForm from './components/DestinationProjectForm';
import DataslistNameEditing from 'ui-component/datas-components/components/datas-components-wrapper/datas-list-name-with-editing/components/DatasListNameEditing';

interface DestinationProjectSheetProps {
    destinationProject?: DestinationProject | null;
    onClose?: () => void;
}

const DestinationProjectSheet = ({ destinationProject, onClose }: DestinationProjectSheetProps) => {
    const { getDestinationProjectByLabel } = useDestinationProjectByLabel();
    const { editDestinationProject, loading: loadingEdit, destinationProject: destinationProjectEditReponse } = useEditDestinationProject();
    const {
        createDestinationProject,
        loading: loadingCreate,
        destinationProject: destinationProjectCreateReponse
    } = useCreateDestinationProject();
    const { deleteDestinationProject, loading: loadingDelete, message: messageDelete } = useDeleteDestinationProject();
    const { getUserAllowedAccess } = useAuth();
    const deleteAccess = getUserAllowedAccess(settingsContactsRoutesManager.destinationProject.id, 'delete');

    useEffect(() => {
        messageDelete && onClose?.();
    }, [messageDelete, onClose]);

    useEffect(() => {
        destinationProjectCreateReponse && onClose?.();
    }, [destinationProjectCreateReponse, onClose]);

    useEffect(() => {
        destinationProjectEditReponse && onClose?.();
    }, [destinationProjectEditReponse, onClose]);

    return (
        <DataslistNameEditing
            onClose={onClose}
            header={
                <Typography variant="h5" component="div" sx={{ fontSize: '1rem' }}>
                    {destinationProject ? (
                        <FormattedMessage id="destinationProject-form-title-edit" />
                    ) : (
                        <FormattedMessage id="destinationProject-form-title-create" />
                    )}
                </Typography>
            }
        >
            <Formik
                initialValues={{
                    ...destinationProject
                }}
                validationSchema={validationSchema({
                    destinationProjectByLabel: getDestinationProjectByLabel,
                    initialLabel: destinationProject?.label || ''
                })}
                enableReinitialize
                onSubmit={async (values) => {
                    destinationProject && destinationProject.id ? editDestinationProject(values) : createDestinationProject(values);
                }}
            >
                {({ isValid, dirty }) => (
                    <Form>
                        <Grid container spacing={gridSpacing}>
                            <Grid item xs={12}>
                                <DestinationProjectForm />
                            </Grid>
                            <Grid item xs={12}>
                                <Grid container spacing={1}>
                                    <Grid item xs={deleteAccess && destinationProject ? 4 : 6}>
                                        <LoadingButton
                                            color="secondary"
                                            disabled={!isValid || !dirty}
                                            loading={loadingEdit || loadingCreate}
                                            variant="contained"
                                            type="submit"
                                            fullWidth
                                        >
                                            {destinationProject ? <FormattedMessage id="edit" /> : <FormattedMessage id="add" />}
                                        </LoadingButton>
                                    </Grid>
                                    <Grid item xs={deleteAccess && destinationProject ? 4 : 6}>
                                        <Button variant="outlined" fullWidth onClick={onClose}>
                                            <FormattedMessage id="cancel" />
                                        </Button>
                                    </Grid>
                                    {deleteAccess && destinationProject && (
                                        <Grid item xs={4}>
                                            <ButtonConfirm
                                                title={<FormattedMessage id="destinationProject-delete-title" />}
                                                content={<FormattedMessage id="destinationProject-delete-content" />}
                                                labelButtonCancel={<FormattedMessage id="cancel" />}
                                                labelButtonConfirm={<FormattedMessage id="yes" />}
                                                onValidate={() => deleteDestinationProject(destinationProject.id)}
                                                loading={loadingDelete}
                                                color="error"
                                                variant="outlined"
                                                fullWidth
                                            >
                                                <FormattedMessage id="app-delete" />
                                            </ButtonConfirm>
                                        </Grid>
                                    )}
                                </Grid>
                            </Grid>
                        </Grid>
                    </Form>
                )}
            </Formik>
        </DataslistNameEditing>
    );
};

export default DestinationProjectSheet;
