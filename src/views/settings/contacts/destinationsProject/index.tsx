// material-ui
import { useCallback, useEffect, useState } from 'react';
import { FormattedMessage } from 'react-intl';

// project imports
import MainCard from 'ui-component/cards/MainCard';
import { DestinationProject } from 'types/settings/contact/destinationProject';
import { NUMER_PER_PAGE } from 'constants/paginate';
import SpeedDialButton, { SpeedDialButtonType } from 'ui-component/speedDial';
import settingsContactsRoutesManager from 'routes-manager/settings/contacts';
import useAuth from 'hooks/authentication/useAuth';
import DatasListNameWithEditWrapper from 'ui-component/datas-components/components/datas-components-wrapper/datas-list-name-with-editing';
import { Typography } from '@mui/material';
import { useDestinationsProject } from 'hooks/settings/contacts/useDestinationProject';
import DestinationProjectSheet from './destinationProject-sheet';

const DestinationsProject = () => {
    const { getUserAllowedAccess } = useAuth();
    const [currentPage, setCurrentPage] = useState(1);
    const [search, setSearch] = useState('');
    const { getDestinationsProject, destinationsProject, pageCursor, loading } = useDestinationsProject();
    const [destinationProject, setDestinationProject] = useState<DestinationProject | undefined | null>(undefined);

    const editAccess = getUserAllowedAccess(settingsContactsRoutesManager.destinationProject.id, 'edit');
    const createAccess = getUserAllowedAccess(settingsContactsRoutesManager.destinationProject.id, 'create');

    useEffect(() => {
        getDestinationsProject({
            paginate: {
                perPage: NUMER_PER_PAGE,
                page: currentPage
            },
            filter: {
                search
            }
        });
    }, [currentPage, getDestinationsProject, search]);

    const actions: SpeedDialButtonType[] = [
        {
            name: <FormattedMessage id="destinationsProject-add" />,
            action: () => setDestinationProject(null)
        }
    ];

    const updateSearch = useCallback((value: string) => {
        setSearch(value);
    }, []);

    return (
        <MainCard title={<FormattedMessage id="destinationsProject-title" />}>
            <DatasListNameWithEditWrapper
                dataComponent={{
                    datas: destinationsProject,
                    itemsPerPage: NUMER_PER_PAGE,
                    loading,
                    sortName: 'label'
                }}
                datasComponentPaginate={{
                    currentPage: pageCursor?.currentPage,
                    numberOfPage: pageCursor?.lastPage,
                    onChangePage: (page) => setCurrentPage(page)
                }}
                datasComponentContent={{
                    component: (data: DestinationProject) => <Typography>{data.label}</Typography>,
                    onClick: editAccess ? (data: DestinationProject) => setDestinationProject(data) : undefined
                }}
                datasComponentsSearch={{
                    filterkey: 'label',
                    updateSearch
                }}
                datasListNameEditing={(data) => (
                    <DestinationProjectSheet destinationProject={data} onClose={() => setDestinationProject(undefined)} />
                )}
                entity={destinationProject}
            />
            {createAccess && <SpeedDialButton actions={actions} />}
        </MainCard>
    );
};

export default DestinationsProject;
