import { Grid, Typography, Button } from '@mui/material';
import { Form, Formik } from 'formik';
import { LoadingButton } from '@mui/lab';
import { FormattedMessage } from 'react-intl';
import { gridSpacing } from 'store/constant';
import validationSchema from './validation';
import useAuth from 'hooks/authentication/useAuth';
import ButtonConfirm from 'ui-component/button/button-confirm';
import { useEffect } from 'react';
import settingsContactsRoutesManager from 'routes-manager/settings/contacts';
import {
    useCreateProspectMotive,
    useDeleteProspectMotive,
    useEditProspectMotive,
    useProspectMotiveByLabel
} from 'hooks/settings/contacts/useProspectMotive';
import { ProspectMotive } from 'types/settings/contact/prospectMotive';
import ProspectMotiveForm from './components/ProspectMotivesForm';
import DataslistNameEditing from 'ui-component/datas-components/components/datas-components-wrapper/datas-list-name-with-editing/components/DatasListNameEditing';

interface ProspectMotiveSheetProps {
    prospectMotive?: ProspectMotive | null;
    onClose?: () => void;
}

const ProspectMotiveSheet = ({ prospectMotive, onClose }: ProspectMotiveSheetProps) => {
    const { getProspectMotiveByLabel } = useProspectMotiveByLabel();
    const { editProspectMotive, loading: loadingEdit, prospectMotive: prospectMotiveEditReponse } = useEditProspectMotive();
    const { createProspectMotive, loading: loadingCreate, prospectMotive: prospectMotiveCreateReponse } = useCreateProspectMotive();
    const { deleteProspectMotive, loading: loadingDelete, message: messageDelete } = useDeleteProspectMotive();
    const { getUserAllowedAccess } = useAuth();
    const deleteAccess = getUserAllowedAccess(settingsContactsRoutesManager.prospectMotive.id, 'delete');

    useEffect(() => {
        messageDelete && onClose?.();
    }, [messageDelete, onClose]);

    useEffect(() => {
        prospectMotiveCreateReponse && onClose?.();
    }, [prospectMotiveCreateReponse, onClose]);

    useEffect(() => {
        prospectMotiveEditReponse && onClose?.();
    }, [prospectMotiveEditReponse, onClose]);

    return (
        <DataslistNameEditing
            onClose={onClose}
            header={
                <Typography variant="h5" component="div" sx={{ fontSize: '1rem' }}>
                    {prospectMotive ? (
                        <FormattedMessage id="prospectMotive-form-title-edit" />
                    ) : (
                        <FormattedMessage id="prospectMotive-form-title-create" />
                    )}
                </Typography>
            }
        >
            <Formik
                initialValues={{
                    ...prospectMotive
                }}
                validationSchema={validationSchema({
                    prospectMotiveByLabel: getProspectMotiveByLabel,
                    initialLabel: prospectMotive?.label || ''
                })}
                enableReinitialize
                onSubmit={async (values) => {
                    prospectMotive && prospectMotive.id ? editProspectMotive(values) : createProspectMotive(values);
                }}
            >
                {({ isValid, dirty }) => (
                    <Form>
                        <Grid container spacing={gridSpacing}>
                            <Grid item xs={12}>
                                <ProspectMotiveForm />
                            </Grid>
                            <Grid item xs={12}>
                                <Grid container spacing={1}>
                                    <Grid item xs={deleteAccess && prospectMotive ? 4 : 6}>
                                        <LoadingButton
                                            color="secondary"
                                            disabled={!isValid || !dirty}
                                            loading={loadingEdit || loadingCreate}
                                            variant="contained"
                                            type="submit"
                                            fullWidth
                                        >
                                            {prospectMotive ? <FormattedMessage id="edit" /> : <FormattedMessage id="add" />}
                                        </LoadingButton>
                                    </Grid>
                                    <Grid item xs={deleteAccess && prospectMotive ? 4 : 6}>
                                        <Button variant="outlined" fullWidth onClick={onClose}>
                                            <FormattedMessage id="cancel" />
                                        </Button>
                                    </Grid>
                                    {deleteAccess && prospectMotive && (
                                        <Grid item xs={4}>
                                            <ButtonConfirm
                                                title={<FormattedMessage id="prospectMotive-delete-title" />}
                                                content={<FormattedMessage id="prospectMotive-delete-content" />}
                                                labelButtonCancel={<FormattedMessage id="cancel" />}
                                                labelButtonConfirm={<FormattedMessage id="yes" />}
                                                onValidate={() => deleteProspectMotive(prospectMotive.id)}
                                                loading={loadingDelete}
                                                color="error"
                                                variant="outlined"
                                                fullWidth
                                            >
                                                <FormattedMessage id="app-delete" />
                                            </ButtonConfirm>
                                        </Grid>
                                    )}
                                </Grid>
                            </Grid>
                        </Grid>
                    </Form>
                )}
            </Formik>
        </DataslistNameEditing>
    );
};

export default ProspectMotiveSheet;
