import { OperationVariables, QueryResult } from '@apollo/client';
import { ProspectMotiveByLabelResponse } from 'hooks/settings/contacts/useProspectMotive';
import * as Yup from 'yup';

function validationSchema(labelCheked: {
    prospectMotiveByLabel: (label: string) => Promise<QueryResult<ProspectMotiveByLabelResponse, OperationVariables>>;
    initialLabel: string;
}) {
    return Yup.object().shape({
        label: Yup.string()
            .nullable()
            .required('label-required')
            .test('label-check', 'label-error-already-used', (label) => {
                if (label && label.toUpperCase() !== labelCheked.initialLabel.toUpperCase()) {
                    return labelCheked.prospectMotiveByLabel(label).then((value) => !value.data?.prospectMotiveByLabel);
                }
                return true;
            })
    });
}

export default validationSchema;
