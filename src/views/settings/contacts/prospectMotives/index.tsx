// material-ui
import { useCallback, useEffect, useState } from 'react';
import { FormattedMessage } from 'react-intl';

// project imports
import MainCard from 'ui-component/cards/MainCard';
import { ProspectMotive } from 'types/settings/contact/prospectMotive';
import { NUMER_PER_PAGE } from 'constants/paginate';
import SpeedDialButton, { SpeedDialButtonType } from 'ui-component/speedDial';
import settingsContactsRoutesManager from 'routes-manager/settings/contacts';
import useAuth from 'hooks/authentication/useAuth';
import DatasListNameWithEditWrapper from 'ui-component/datas-components/components/datas-components-wrapper/datas-list-name-with-editing';
import { Typography } from '@mui/material';
import { useProspectMotives } from 'hooks/settings/contacts/useProspectMotive';
import ProspectMotiveSheet from './prospectMotive-sheet';

const ProspectMotives = () => {
    const { getUserAllowedAccess } = useAuth();
    const [currentPage, setCurrentPage] = useState(1);
    const [search, setSearch] = useState('');
    const { getProspectMotives, prospectMotives, pageCursor, loading } = useProspectMotives();
    const [prospectMotive, setProspectMotive] = useState<ProspectMotive | undefined | null>(undefined);

    const editAccess = getUserAllowedAccess(settingsContactsRoutesManager.prospectMotive.id, 'edit');
    const createAccess = getUserAllowedAccess(settingsContactsRoutesManager.prospectMotive.id, 'create');

    useEffect(() => {
        getProspectMotives({
            paginate: {
                perPage: NUMER_PER_PAGE,
                page: currentPage
            },
            filter: {
                search
            }
        });
    }, [currentPage, getProspectMotives, search]);

    const actions: SpeedDialButtonType[] = [
        {
            name: <FormattedMessage id="prospectMotives-add" />,
            action: () => setProspectMotive(null)
        }
    ];

    const updateSearch = useCallback((value: string) => {
        setSearch(value);
    }, []);

    return (
        <MainCard title={<FormattedMessage id="prospectMotives-title" />}>
            <DatasListNameWithEditWrapper
                dataComponent={{
                    datas: prospectMotives,
                    itemsPerPage: NUMER_PER_PAGE,
                    loading,
                    sortName: 'label'
                }}
                datasComponentPaginate={{
                    currentPage: pageCursor?.currentPage,
                    numberOfPage: pageCursor?.lastPage,
                    onChangePage: (page) => setCurrentPage(page)
                }}
                datasComponentContent={{
                    component: (data: ProspectMotive) => <Typography>{data.label}</Typography>,
                    onClick: editAccess ? (data: ProspectMotive) => setProspectMotive(data) : undefined
                }}
                datasComponentsSearch={{
                    filterkey: 'label',
                    updateSearch
                }}
                datasListNameEditing={(data) => <ProspectMotiveSheet prospectMotive={data} onClose={() => setProspectMotive(undefined)} />}
                entity={prospectMotive}
            />
            {createAccess && <SpeedDialButton actions={actions} />}
        </MainCard>
    );
};

export default ProspectMotives;
