import { Grid, Typography, Button } from '@mui/material';
import { Form, Formik } from 'formik';
import { LoadingButton } from '@mui/lab';
import { FormattedMessage } from 'react-intl';
import { gridSpacing } from 'store/constant';
import validationSchema from './validation';
import useAuth from 'hooks/authentication/useAuth';
import ButtonConfirm from 'ui-component/button/button-confirm';
import { useEffect } from 'react';
import { Destination } from 'types/settings/contact/destination';
import {
    useCreateDestination,
    useDeleteDestination,
    useDestinationByLabel,
    useEditDestination
} from 'hooks/settings/contacts/useDestination';
import settingsContactsRoutesManager from 'routes-manager/settings/contacts';
import DestinationForm from './components/DestinationForm';
import { destinationTypeEnum } from 'services/settings/contact/destination.service';
import DataslistNameEditing from 'ui-component/datas-components/components/datas-components-wrapper/datas-list-name-with-editing/components/DatasListNameEditing';

interface DestinationSheetProps {
    destination?: Destination | null;
    onClose?: () => void;
}

const DestinationSheet = ({ destination, onClose }: DestinationSheetProps) => {
    const { getDestinationByLabel } = useDestinationByLabel();
    const { editDestination, loading: loadingEdit, destination: destinationEditReponse } = useEditDestination();
    const { createDestination, loading: loadingCreate, destination: destinationCreateReponse } = useCreateDestination();
    const { deleteDestination, loading: loadingDelete, message: messageDelete } = useDeleteDestination();
    const { getUserAllowedAccess } = useAuth();
    const deleteAccess = getUserAllowedAccess(settingsContactsRoutesManager.destination.id, 'delete');

    useEffect(() => {
        messageDelete && onClose?.();
    }, [messageDelete, onClose]);

    useEffect(() => {
        destinationCreateReponse && onClose?.();
    }, [destinationCreateReponse, onClose]);

    useEffect(() => {
        destinationEditReponse && onClose?.();
    }, [destinationEditReponse, onClose]);

    return (
        <DataslistNameEditing
            onClose={onClose}
            header={
                <Typography variant="h5" component="div" sx={{ fontSize: '1rem' }}>
                    {destination ? (
                        <FormattedMessage id="destination-form-title-edit" />
                    ) : (
                        <FormattedMessage id="destination-form-title-create" />
                    )}
                </Typography>
            }
        >
            <Formik
                initialValues={{
                    ...destination,
                    type: !destination ? destinationTypeEnum.NOT_DEFINED.key : destination.type
                }}
                validationSchema={validationSchema({
                    destinationByLabel: getDestinationByLabel,
                    initialLabel: destination?.label || ''
                })}
                enableReinitialize
                onSubmit={async (values) => {
                    destination && destination.id ? editDestination(values) : createDestination(values);
                }}
            >
                {({ isValid, dirty }) => (
                    <Form>
                        <Grid container spacing={gridSpacing}>
                            <Grid item xs={12}>
                                <DestinationForm />
                            </Grid>
                            <Grid item xs={12}>
                                <Grid container spacing={1}>
                                    <Grid item xs={deleteAccess && destination ? 4 : 6}>
                                        <LoadingButton
                                            color="secondary"
                                            disabled={!isValid || !dirty}
                                            loading={loadingEdit || loadingCreate}
                                            variant="contained"
                                            type="submit"
                                            fullWidth
                                        >
                                            {destination ? <FormattedMessage id="edit" /> : <FormattedMessage id="add" />}
                                        </LoadingButton>
                                    </Grid>
                                    <Grid item xs={deleteAccess && destination ? 4 : 6}>
                                        <Button variant="outlined" fullWidth onClick={onClose}>
                                            <FormattedMessage id="cancel" />
                                        </Button>
                                    </Grid>
                                    {deleteAccess && destination && (
                                        <Grid item xs={4}>
                                            <ButtonConfirm
                                                title={<FormattedMessage id="destination-delete-title" />}
                                                content={<FormattedMessage id="destination-delete-content" />}
                                                labelButtonCancel={<FormattedMessage id="cancel" />}
                                                labelButtonConfirm={<FormattedMessage id="yes" />}
                                                onValidate={() => deleteDestination(destination.id)}
                                                loading={loadingDelete}
                                                color="error"
                                                variant="outlined"
                                                fullWidth
                                            >
                                                <FormattedMessage id="app-delete" />
                                            </ButtonConfirm>
                                        </Grid>
                                    )}
                                </Grid>
                            </Grid>
                        </Grid>
                    </Form>
                )}
            </Formik>
        </DataslistNameEditing>
    );
};

export default DestinationSheet;
