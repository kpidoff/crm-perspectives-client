import { Grid } from '@mui/material';
import { useFormikContext } from 'formik';
import _ from 'lodash';
import { FormattedMessage } from 'react-intl';
import { destinationTypeEnum } from 'services/settings/contact/destination.service';
import { gridSpacing } from 'store/constant';
import { Destination } from 'types/settings/contact/destination';
import { InputTextField } from 'ui-component/input';
import InputMultipleCheckbox, { InputMultipleCheckboxType } from 'ui-component/input/input-multiple-checkbox';

const DestinationForm = () => {
    const { values, handleBlur, handleChange, touched, errors, setFieldValue } = useFormikContext<Destination>();

    const handleChangeCheckbox = (value: string) => {
        setFieldValue('type', value);
    };

    const items: InputMultipleCheckboxType[] = _.map(destinationTypeEnum, (type) => ({
        label: type.label,
        onChange: () => handleChangeCheckbox(type.key),
        checked: values.type === type.key
    }));

    return (
        <Grid container spacing={gridSpacing}>
            <Grid item xs={12}>
                <InputTextField
                    inputProps={{ style: { textTransform: 'capitalize' } }}
                    value={values.label}
                    name="label"
                    id="label"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    label={<FormattedMessage id="destination-form-label" />}
                    error={Boolean(touched.label && errors.label)}
                    errorMessage={errors.label}
                    required
                    autoFocus
                    fullWidth
                />
            </Grid>
            <Grid item xs={12}>
                <InputMultipleCheckbox items={items} label={<FormattedMessage id="role-form-type-label" />} />
            </Grid>
        </Grid>
    );
};

export default DestinationForm;
