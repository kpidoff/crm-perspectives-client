import { OperationVariables, QueryResult } from '@apollo/client';
import { DestinationByLabelResponse } from 'hooks/settings/contacts/useDestination';
import * as Yup from 'yup';

function validationSchema(labelCheked: {
    destinationByLabel: (label: string) => Promise<QueryResult<DestinationByLabelResponse, OperationVariables>>;
    initialLabel: string;
}) {
    return Yup.object().shape({
        label: Yup.string()
            .nullable()
            .required('label-required')
            .test('label-check', 'label-error-already-used', (label) => {
                if (label && label.toUpperCase() !== labelCheked.initialLabel.toUpperCase()) {
                    return labelCheked.destinationByLabel(label).then((value) => !value.data?.destinationByLabel);
                }
                return true;
            })
    });
}

export default validationSchema;
