// material-ui
import { useCallback, useEffect, useState } from 'react';
import { FormattedMessage } from 'react-intl';

// project imports
import MainCard from 'ui-component/cards/MainCard';
import { useDestinations } from 'hooks/settings/contacts/useDestination';
import { Destination } from 'types/settings/contact/destination';
import { NUMER_PER_PAGE } from 'constants/paginate';
import SpeedDialButton, { SpeedDialButtonType } from 'ui-component/speedDial';
import settingsContactsRoutesManager from 'routes-manager/settings/contacts';
import useAuth from 'hooks/authentication/useAuth';
import DatasListNameWithEditWrapper from 'ui-component/datas-components/components/datas-components-wrapper/datas-list-name-with-editing';
import { Typography } from '@mui/material';
import { destinationTypeEnum } from 'services/settings/contact/destination.service';
import DestinationSheet from './destination-sheet';

const Destinations = () => {
    const { getUserAllowedAccess } = useAuth();
    const [currentPage, setCurrentPage] = useState(1);
    const [search, setSearch] = useState('');
    const { getDestinations, destinations, pageCursor, loading } = useDestinations();
    const [destination, setDestination] = useState<Destination | undefined | null>(undefined);

    const editAccess = getUserAllowedAccess(settingsContactsRoutesManager.destination.id, 'edit');
    const createAccess = getUserAllowedAccess(settingsContactsRoutesManager.destination.id, 'create');

    useEffect(() => {
        getDestinations({
            paginate: {
                perPage: NUMER_PER_PAGE,
                page: currentPage
            },
            filter: {
                search
            }
        });
    }, [currentPage, getDestinations, search]);

    const actions: SpeedDialButtonType[] = [
        {
            name: <FormattedMessage id="destinations-add" />,
            action: () => setDestination(null)
        }
    ];

    const updateSearch = useCallback((value: string) => {
        setSearch(value);
    }, []);

    return (
        <MainCard title={<FormattedMessage id="destinations-title" />}>
            <DatasListNameWithEditWrapper
                dataComponent={{
                    datas: destinations,
                    itemsPerPage: NUMER_PER_PAGE,
                    loading,
                    sortName: 'label'
                }}
                datasComponentPaginate={{
                    currentPage: pageCursor?.currentPage,
                    numberOfPage: pageCursor?.lastPage,
                    onChangePage: (page) => setCurrentPage(page)
                }}
                datasComponentContent={{
                    component: (data: Destination) => (
                        <>
                            <Typography>{data.label}</Typography>
                            <Typography variant="caption">{destinationTypeEnum[data.type].label}</Typography>
                        </>
                    ),
                    onClick: editAccess ? (data: Destination) => setDestination(data) : undefined
                }}
                datasComponentsSearch={{
                    filterkey: 'label',
                    updateSearch
                }}
                datasListNameEditing={(data) => <DestinationSheet destination={data} onClose={() => setDestination(undefined)} />}
                entity={destination}
            />
            {createAccess && <SpeedDialButton actions={actions} />}
        </MainCard>
    );
};

export default Destinations;
