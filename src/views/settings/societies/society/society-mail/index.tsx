// material-ui
import { Checkbox, FormControlLabel, Grid } from '@mui/material';
import { useFormikContext } from 'formik';
import { FormattedMessage } from 'react-intl';
import { gridSpacing } from 'store/constant';
import { Society } from 'types/settings/society';

// project imports
import SubCard from 'ui-component/cards/SubCard';
import { InputPassword, InputTextField } from 'ui-component/input';

// ==============================|| SAMPLE PAGE ||============================== //

const SocietyMail = () => {
    const { values, handleBlur, handleChange, touched, errors, status } = useFormikContext<Society>();
    return (
        status && (
            <Grid container>
                <SubCard title={<FormattedMessage id="society-mail-title" />}>
                    <Grid container spacing={gridSpacing}>
                        <Grid item xs={12}>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        value={values.smtpActivated || false}
                                        defaultChecked={values.smtpActivated || false}
                                        name="smtpActivated"
                                        id="smtpActivated"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                    />
                                }
                                label={<FormattedMessage id="society-label-smtpServeur" />}
                            />
                        </Grid>

                        <Grid item xs={12} md={10}>
                            <InputTextField
                                value={values.smtpServeur}
                                name="smtpServeur"
                                id="smtpServeur"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                label={<FormattedMessage id="society-label-smtpServeur" />}
                                error={Boolean(touched.smtpServeur && errors.smtpServeur)}
                                errorMessage={errors.smtpServeur}
                                required
                                fullWidth
                                disabled={!values.smtpActivated}
                            />
                        </Grid>
                        <Grid item xs={12} md={2}>
                            <InputTextField
                                value={values.smtpPort}
                                name="smtpPort"
                                id="smtpPort"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                label={<FormattedMessage id="society-label-smtpPort" />}
                                error={Boolean(touched.smtpPort && errors.smtpPort)}
                                errorMessage={errors.smtpPort}
                                type="number"
                                required
                                fullWidth
                                disabled={!values.smtpActivated}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <InputTextField
                                value={values.smtpTitle}
                                name="smtpTitle"
                                id="smtpTitle"
                                onBlur={handleBlur}
                                onChange={handleChange}
                                label={<FormattedMessage id="society-label-smtpTitle" />}
                                error={Boolean(touched.smtpTitle && errors.smtpTitle)}
                                errorMessage={errors.smtpTitle}
                                required
                                fullWidth
                                disabled={!values.smtpActivated}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Grid container spacing={2}>
                                <Grid item xs={12} md={6}>
                                    <InputTextField
                                        value={values.smtpMail}
                                        name="smtpMail"
                                        id="smtpMail"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        label={<FormattedMessage id="society-label-smtpMail" />}
                                        error={Boolean(touched.smtpMail && errors.smtpMail)}
                                        errorMessage={errors.smtpMail}
                                        required
                                        fullWidth
                                        disabled={!values.smtpActivated}
                                    />
                                </Grid>
                                <Grid item xs={12} md={6}>
                                    <InputPassword
                                        value={values.smtpPassword}
                                        name="smtpPassword"
                                        id="smtpPassword"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        label={<FormattedMessage id="society-label-smtpPassword" />}
                                        error={Boolean(touched.smtpPassword && errors.smtpPassword)}
                                        errorMessage={errors.smtpPassword}
                                        required
                                        fullWidth
                                        disabled={!values.smtpActivated}
                                    />
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </SubCard>
            </Grid>
        )
    );
};

export default SocietyMail;
