// material-ui
import { LoadingButton } from '@mui/lab';
import { Button } from '@mui/material';
import { Form, Formik } from 'formik';
import useAuth from 'hooks/authentication/useAuth';
import { useCreateSociety, useEditSociety, useSocietyById, useSocietyByName } from 'hooks/settings/society/useSociety';
import _ from 'lodash';
import { useEffect, useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { useNavigate, useParams } from 'react-router-dom';
import settingsSocietyRoutesManager from 'routes-manager/settings/society';
import { Society as SocietyProps } from 'types/settings/society';

// project imports
import MainCard from 'ui-component/cards/MainCard';
import SocietyChangeStatus from 'ui-component/societies/society-change-status';
// import SocietyButtonChangeStatut from 'ui-component/societies/society-button-change-status';
import TabContent, { TopContentProps } from 'ui-component/tab-content';
import SocietyInfo from './society-info';
import SocietyMail from './society-mail';
import validationSchema from './validation';

// ==============================|| SAMPLE PAGE ||============================== //

const Society = () => {
    const intl = useIntl();
    const { id } = useParams();
    const navigate = useNavigate();
    const { getUserAllowedAccess, isAdministratorSociety, isAdministrator } = useAuth();

    const { getSociety, society, loading } = useSocietyById();
    const { getSocietyByName } = useSocietyByName();
    const { setSociety, loading: loadingEdit } = useEditSociety();
    const { createSociety, loading: loadingCreate, society: societyCreate } = useCreateSociety();
    const [societyEditing, setSocietyEditing] = useState<Partial<SocietyProps> | undefined>(undefined);
    const [open, setOpen] = useState(false);

    useEffect(() => {
        id && getSociety(_.toInteger(id));
    }, [getSociety, id, society]);

    useEffect(() => {
        society && setSocietyEditing(society);
    }, [society]);

    useEffect(() => {
        societyCreate && navigate(settingsSocietyRoutesManager.society.children.edit.link(societyCreate.id));
    }, [navigate, societyCreate]);

    const tabMessaging = getUserAllowedAccess(settingsSocietyRoutesManager.society.id, 'tab-messaging');

    const tabContent: TopContentProps[] = [
        {
            title: intl.formatMessage({ id: 'society-tab-info' }),
            component: <SocietyInfo />
        },
        {
            title: intl.formatMessage({ id: 'society-tab-mail' }),
            component: <SocietyMail />,
            disabled: !tabMessaging
        }
    ];

    return (
        <>
            {societyEditing && societyEditing.id && (
                <SocietyChangeStatus
                    societyId={societyEditing.id}
                    activated={societyEditing.activated}
                    open={open}
                    onClose={() => setOpen(false)}
                />
            )}
            <MainCard
                title={society ? <FormattedMessage id="society-edit" /> : <FormattedMessage id="society-add" />}
                secondary={
                    (isAdministratorSociety(_.toInteger(id)) || isAdministrator) &&
                    societyEditing &&
                    societyEditing.id && (
                        <Button variant="outlined" color={societyEditing.activated ? 'error' : 'success'} onClick={() => setOpen(true)}>
                            {societyEditing.activated ? <FormattedMessage id="disabled" /> : <FormattedMessage id="activated" />}
                        </Button>
                    )
                }
            >
                <Formik
                    initialValues={{
                        ...societyEditing
                    }}
                    validationSchema={validationSchema({
                        societyByName: getSocietyByName,
                        initialName: societyEditing?.name || ''
                    })}
                    enableReinitialize
                    initialStatus={society || loading ? !!societyEditing : true}
                    onSubmit={async (values) => {
                        society ? setSociety(society.id, values) : createSociety(values);
                    }}
                >
                    {({ isValid, dirty }) => (
                        <Form>
                            <TabContent content={tabContent} loading={loading} />
                            <LoadingButton
                                size="large"
                                color="secondary"
                                disabled={!isValid || !dirty}
                                loading={loadingEdit || loadingCreate}
                                variant="contained"
                                type="submit"
                            >
                                {society ? <FormattedMessage id="edit" /> : <FormattedMessage id="add" />}
                            </LoadingButton>
                        </Form>
                    )}
                </Formik>
            </MainCard>
        </>
    );
};

export default Society;
