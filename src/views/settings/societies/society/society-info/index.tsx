// material-ui
import { Grid } from '@mui/material';
import { useFormikContext } from 'formik';
import { useGeocode } from 'hooks/utils/useGeocode';
import _ from 'lodash';
import { useEffect } from 'react';
import { FormattedMessage } from 'react-intl';
import { gridSpacing } from 'store/constant';
import { Society } from 'types/settings/society';

// project imports
import SubCard from 'ui-component/cards/SubCard';
import { InputApe, InputEditor, InputPhone, InputSiret, InputTextField } from 'ui-component/input';
import TextFieldCity from 'ui-component/input/input-postal-code/input-city';
import TextFieldPostalCode from 'ui-component/input/input-postal-code/input-postal-code';

// ==============================|| SAMPLE PAGE ||============================== //

const SocietyInfo = () => {
    const { values, handleBlur, handleChange, touched, errors, setFieldValue, status, initialValues } = useFormikContext<Society>();
    const { fromAddress, geometry } = useGeocode();
    useEffect(() => {
        const debounce = _.debounce(() => {
            if (
                values.postalCode !== initialValues.postalCode ||
                values.city !== initialValues.city ||
                values.address !== initialValues.address
            ) {
                fromAddress({
                    address: values.address,
                    city: values.city,
                    postalCode: values.postalCode
                });
            }
        }, 250);

        debounce();

        return () => {
            debounce.cancel();
        };
    }, [fromAddress, initialValues.address, initialValues.city, initialValues.postalCode, values.address, values.city, values.postalCode]);

    useEffect(() => {
        if (geometry) {
            setFieldValue('lat', geometry.lat);
            setFieldValue('lng', geometry.lng);
        }
    }, [geometry, setFieldValue]);

    return (
        <Grid container spacing={gridSpacing}>
            <Grid item xs={12} md={8}>
                <Grid container spacing={gridSpacing}>
                    <Grid item xs={12}>
                        <SubCard title={<FormattedMessage id="society-info-title" />}>
                            <Grid container spacing={gridSpacing}>
                                <Grid item xs={12}>
                                    <InputTextField
                                        inputProps={{ style: { textTransform: 'uppercase' } }}
                                        value={values.name}
                                        name="name"
                                        id="name"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        label={<FormattedMessage id="society-label-name" />}
                                        error={Boolean(touched.name && errors.name)}
                                        errorMessage={errors.name}
                                        required
                                        fullWidth
                                        skeleton={!status}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <InputTextField
                                        inputProps={{ style: { textTransform: 'capitalize' } }}
                                        value={values.interlocutor}
                                        name="interlocutor"
                                        id="interlocutor"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        label={<FormattedMessage id="society-label-interlocutor" />}
                                        error={Boolean(touched.interlocutor && errors.interlocutor)}
                                        errorMessage={errors.interlocutor}
                                        required
                                        fullWidth
                                        skeleton={!status}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <Grid container spacing={gridSpacing}>
                                        <Grid item xs={12} md={4}>
                                            <TextFieldPostalCode
                                                onChange={(postalCode) => setFieldValue('postalCode', postalCode)}
                                                onSelect={(postalCode) => {
                                                    setFieldValue('city', postalCode.city);
                                                    setFieldValue('postalCode', postalCode.postal_code);
                                                }}
                                                value={values.postalCode}
                                                label={<FormattedMessage id="postalCode" />}
                                                error={Boolean(touched.postalCode && errors.postalCode)}
                                                errorMessage={errors.postalCode}
                                                required
                                                fullWidth
                                                skeleton={!status}
                                                name="postalCode"
                                                id="postalCode"
                                                placeholder="Code postal"
                                            />
                                        </Grid>
                                        <Grid item xs={12} md={8}>
                                            <TextFieldCity
                                                value={values.city}
                                                onChange={(city) => setFieldValue('city', city)}
                                                onSelect={(postalCode) => {
                                                    setFieldValue('city', postalCode.city);
                                                    setFieldValue('postalCode', postalCode.postal_code);
                                                }}
                                                name="city"
                                                id="city"
                                                label={<FormattedMessage id="city" />}
                                                error={Boolean(touched.city && errors.city)}
                                                errorMessage={errors.city}
                                                required
                                                fullWidth
                                                skeleton={!status}
                                            />
                                        </Grid>
                                    </Grid>
                                </Grid>
                                <Grid item xs={12}>
                                    <InputTextField
                                        inputProps={{ style: { textTransform: 'capitalize' } }}
                                        value={values.address}
                                        name="address"
                                        id="address"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        label={<FormattedMessage id="address" />}
                                        error={Boolean(touched.address && errors.address)}
                                        errorMessage={errors.address}
                                        required
                                        fullWidth
                                        skeleton={!status}
                                    />
                                </Grid>
                            </Grid>
                        </SubCard>
                    </Grid>
                    <Grid item xs={12}>
                        <SubCard title={<FormattedMessage id="society-comment-title" />}>
                            <InputEditor
                                id="comment"
                                value={values.comment}
                                onChange={(e) => {
                                    setFieldValue('comment', e);
                                }}
                                skeleton={!status}
                            />
                        </SubCard>
                    </Grid>
                </Grid>
            </Grid>
            <Grid item xs={12} md={4}>
                <Grid container spacing={gridSpacing}>
                    <Grid item xs={12}>
                        <SubCard title={<FormattedMessage id="society-contact-title" />}>
                            <Grid container spacing={gridSpacing}>
                                <Grid item xs={12}>
                                    <InputTextField
                                        value={values.email}
                                        name="email"
                                        id="email"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        label={<FormattedMessage id="society-label-email" />}
                                        error={Boolean(touched.email && errors.email)}
                                        errorMessage={errors.email}
                                        required
                                        fullWidth
                                        skeleton={!status}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <InputPhone
                                        value={values.phone}
                                        name="phone"
                                        id="phone"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        label={<FormattedMessage id="phone" />}
                                        fullWidth
                                        skeleton={!status}
                                    />
                                </Grid>
                            </Grid>
                        </SubCard>
                    </Grid>
                    <Grid item xs={12}>
                        <SubCard title={<FormattedMessage id="society-supp-title" />}>
                            <Grid container spacing={gridSpacing}>
                                <Grid item xs={12}>
                                    <InputSiret
                                        value={values.siret}
                                        name="siret"
                                        id="siret"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        label={<FormattedMessage id="society-label-siret" />}
                                        fullWidth
                                        skeleton={!status}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <InputApe
                                        value={values.ape}
                                        name="ape"
                                        id="ape"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        label={<FormattedMessage id="society-label-ape" />}
                                        fullWidth
                                        skeleton={!status}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <Grid container spacing={2}>
                                        <Grid item xs={12} md={6}>
                                            <InputTextField
                                                value={values.lat}
                                                name="lat"
                                                id="lat"
                                                onBlur={handleBlur}
                                                onChange={handleChange}
                                                label={<FormattedMessage id="society-label-lat" />}
                                                type="number"
                                                fullWidth
                                                skeleton={!status}
                                            />
                                        </Grid>
                                        <Grid item xs={12} md={6}>
                                            <InputTextField
                                                value={values.lng}
                                                name="lng"
                                                id="lng"
                                                onBlur={handleBlur}
                                                onChange={handleChange}
                                                label={<FormattedMessage id="society-label-lng" />}
                                                type="number"
                                                fullWidth
                                                skeleton={!status}
                                            />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </SubCard>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default SocietyInfo;
