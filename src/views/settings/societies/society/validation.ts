import { OperationVariables, QueryResult } from '@apollo/client';
import { SocietyByNameResponse } from 'hooks/settings/society/useSociety';
import * as Yup from 'yup';

function validationSchema(nameCheked: {
    societyByName: (email: string) => Promise<QueryResult<SocietyByNameResponse, OperationVariables>>;
    initialName: string;
}) {
    return Yup.object().shape({
        name: Yup.string()
            .nullable()
            .required('society-name-required')
            .test('name-check', 'name-error-already-used', (name) => {
                if (name && name.toUpperCase() !== nameCheked.initialName.toUpperCase()) {
                    return nameCheked.societyByName(name).then((value) => !value.data?.societyByName);
                }
                return true;
            }),
        interlocutor: Yup.string().nullable().required('society-interlocutor-required'),
        postalCode: Yup.string().nullable().required('postalCode-required'),
        city: Yup.string().nullable().required('city-required'),
        address: Yup.string().nullable().required('address-required'),
        email: Yup.string().nullable().email('email-error-invalid').required('email-required'),
        smtpActivated: Yup.bool(),
        smtpServeur: Yup.string()
            .nullable()
            .when('smtpActivated', {
                is: true,
                then: Yup.string().nullable().required('society-smtp-server-required')
            }),
        smtpPort: Yup.number()
            .nullable()
            .when('smtpActivated', {
                is: true,
                then: Yup.number().nullable().required('society-smtp-port-required')
            }),
        smtpTitle: Yup.string()
            .nullable()
            .when('smtpActivated', {
                is: true,
                then: Yup.string().nullable().required('society-smtp-title-required')
            }),
        smtpMail: Yup.string()
            .nullable()
            .when('smtpActivated', {
                is: true,
                then: Yup.string().email('email-error-invalid').nullable().required('email-required')
            }),
        smtpPassword: Yup.string()
            .nullable()
            .when('smtpActivated', {
                is: true,
                then: Yup.string().nullable().required('password-required')
            })
    });
}

export default validationSchema;
