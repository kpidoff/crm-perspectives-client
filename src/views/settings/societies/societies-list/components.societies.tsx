import { Grid, Typography } from '@mui/material';
import useAuth from 'hooks/authentication/useAuth';
import { FormattedMessage } from 'react-intl';
import { useNavigate } from 'react-router-dom';
import settingsSocietyRoutesManager from 'routes-manager/settings/society';
import { Society } from 'types/settings/society';
import { DatasComponentsContentTableComponentsProps } from 'ui-component/datas-components/components/datas-components-content/dataComponentsContent.type';
import EditIcon from '@mui/icons-material/Edit';
import BlockIcon from '@mui/icons-material/Block';
import DoneIcon from '@mui/icons-material/Done';
import { useState } from 'react';

function useComponents() {
    const navigate = useNavigate();
    const { getUserAllowedAccess } = useAuth();
    const [societyBlock, setSocietyBlock] = useState<Society | undefined>(undefined);

    const openSociety = (id: number | undefined) => {
        accessEdit && id && navigate(settingsSocietyRoutesManager.society.children.edit.link(id));
    };

    const accessEdit = getUserAllowedAccess(settingsSocietyRoutesManager.society.id, 'edit');

    const components: DatasComponentsContentTableComponentsProps[] = [
        {
            title: 'Agence',
            content: (data: Society | undefined) => ({
                type: 'header',
                component: (
                    <Grid container spacing={2} alignItems="center">
                        <Grid item xs zeroMinWidth>
                            <Typography align="left" variant="subtitle1" component="div">
                                {data?.name}
                            </Typography>
                            <Typography align="left" variant="subtitle2" noWrap>
                                {data?.interlocutor}
                            </Typography>
                        </Grid>
                    </Grid>
                )
            })
        },
        {
            title: 'Adresse',
            content: (data: Society | undefined) => ({
                type: 'formatAddress',
                address: data?.address,
                city: data?.city,
                postalCode: data?.postalCode
            })
        },
        {
            title: 'Téléphone',
            content: (data: Society | undefined) => ({
                type: 'formatPhone',
                phone: data?.phone
            })
        },
        {
            title: 'Adresse mail',
            content: (data: Society | undefined) => ({
                type: 'formatEmail',
                email: data?.email
            })
        },
        {
            title: 'Status',
            content: (data: Society | undefined) => ({
                type: 'chipActive',
                active: data?.activated
            })
        },
        {
            title: 'Actions',
            content: (data: Society | undefined) => ({
                type: 'action',
                items: [
                    {
                        label: <FormattedMessage id="edit" />,
                        icon: EditIcon,
                        onClick: () => openSociety(data?.id),
                        visible: accessEdit
                    },
                    {
                        label: data && data.activated ? <FormattedMessage id="disabled" /> : <FormattedMessage id="activated" />,
                        icon: data && data.activated ? BlockIcon : DoneIcon,
                        buttonProps: {
                            color: data && data.activated ? 'error' : 'success'
                        },
                        onClick: () => data && setSocietyBlock(data),
                        visible: accessEdit
                    }
                ]
            })
        }
    ];
    return {
        components,
        societyBlock,
        setSocietyBlock,
        openSociety
    };
}

export default useComponents;
