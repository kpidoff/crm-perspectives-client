import { SocietyFilterInput } from 'hooks/settings/society/useSociety';
import { FilterType } from 'ui-component/datas-components/components/datas-components-drawer/filters.types';

const useShemaFilter = (filters: SocietyFilterInput): FilterType[] => [
    {
        filterKey: 'activated',
        type: 'select',
        label: 'Active',
        value: filters.activated,
        defaultExpand: true,
        mapper: (item) => item?.value,
        items: [
            {
                key: '1',
                label: 'Oui',
                value: true
            },
            {
                key: '2',
                label: 'Non',
                value: false
            }
        ]
    }
];

export default useShemaFilter;
