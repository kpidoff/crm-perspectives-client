import { NUMER_PER_PAGE } from 'constants/paginate';
import { SocietyFilterInput } from 'hooks/settings/society/useSociety';

export const SOCIETIES_NUMER_PER_PAGE = NUMER_PER_PAGE;
export const DEFAULT_FILTER: SocietyFilterInput = {
    activated: true
};
