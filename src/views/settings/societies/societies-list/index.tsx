import { SocietyFilterInput, useSocieties } from 'hooks/settings/society/useSociety';
import { useCallback, useEffect, useState } from 'react';
import { DEFAULT_FILTER, SOCIETIES_NUMER_PER_PAGE } from './constants.societies';
import useShemaFilter from './filters.societies';
import DatasComponentsTable from 'ui-component/datas-components/components/datas-components-wrapper/datas-components-table-wrapper';
import { Society } from 'types/settings/society';
import { useNavigate } from 'react-router-dom';
import settingsSocietyRoutesManager from 'routes-manager/settings/society';
import useAuth from 'hooks/authentication/useAuth';
import SocietyChangeStatus from '../../../../ui-component/societies/society-change-status';
import { FormattedMessage } from 'react-intl';
import SpeedDialButton, { SpeedDialButtonType } from 'ui-component/speedDial';
import DomainAddIcon from '@mui/icons-material/DomainAdd';
import useComponents from './components.societies';

const SocietiesList = () => {
    const navigate = useNavigate();
    const { getUserAllowedAccess } = useAuth();
    const [filter, setFilter] = useState<SocietyFilterInput>(DEFAULT_FILTER);
    const [currentPage, setCurrentPage] = useState(1);
    const { getSocieties, pageCursor, societies, loading } = useSocieties();
    const schema = useShemaFilter(filter);
    const { components, setSocietyBlock, societyBlock, openSociety } = useComponents();

    const accessCreate = getUserAllowedAccess(settingsSocietyRoutesManager.society.id, 'create');

    useEffect(() => {
        getSocieties({
            paginate: {
                perPage: SOCIETIES_NUMER_PER_PAGE,
                page: currentPage
            },
            filter
        });
    }, [currentPage, filter, getSocieties]);

    const updateFilter = useCallback((value: any, key: string) => {
        setFilter((prevState) => ({
            ...prevState,
            [key]: value
        }));
        setCurrentPage(1);
    }, []);

    const onClear = useCallback(() => {
        setFilter(DEFAULT_FILTER);
    }, []);

    const handleCreateSociety = () => {
        navigate(settingsSocietyRoutesManager.society.children.add.path);
    };

    const actions: SpeedDialButtonType[] = [
        { icon: <DomainAddIcon />, name: <FormattedMessage id="societies-list-add" />, action: handleCreateSociety }
    ];

    return (
        <>
            {societyBlock && (
                <SocietyChangeStatus
                    societyId={societyBlock.id}
                    activated={societyBlock.activated}
                    open={!!societyBlock}
                    onClose={() => setSocietyBlock(undefined)}
                />
            )}
            <DatasComponentsTable
                dataComponent={{
                    datas: societies,
                    itemsPerPage: SOCIETIES_NUMER_PER_PAGE,
                    loading,
                    filter: {
                        schema,
                        onClear,
                        updateFilter
                    }
                }}
                datasComponentHeader={{
                    datasComponentsSearch: {
                        filterkey: 'search',
                        updateSearch: updateFilter
                    }
                }}
                datasComponentPaginate={{
                    currentPage: pageCursor?.currentPage,
                    numberOfPage: pageCursor?.lastPage,
                    onChangePage: (page) => setCurrentPage(page)
                }}
                datasComponentContent={{
                    onDoubleClick: (data: Society | undefined) => data && openSociety(data.id),
                    components
                }}
            />
            {accessCreate && <SpeedDialButton actions={actions} />}
        </>
    );
};

export default SocietiesList;
