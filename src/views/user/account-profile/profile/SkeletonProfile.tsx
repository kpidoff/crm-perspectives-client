import { Grid, Typography, Skeleton } from '@mui/material';
import { FormattedMessage } from 'react-intl';
import { gridSpacing } from 'store/constant';
import SubCard from 'ui-component/cards/SubCard';
import SkeletonTextField from 'ui-component/input/input-textField/components/SkeletonTextField';

const SkeletonProfile = () => (
    <Grid container spacing={gridSpacing}>
        <Grid item sm={6} md={4}>
            <SubCard title={<FormattedMessage id="profile-picture-title" />} contentSX={{ textAlign: 'center' }}>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <Skeleton variant="circular" width={100} height={100} sx={{ margin: '0 auto' }} />
                    </Grid>
                    <Grid item xs={12}>
                        <Typography variant="subtitle2" align="center">
                            <Skeleton />
                        </Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Skeleton height={70} width={170} sx={{ margin: '0 auto' }} />
                    </Grid>
                </Grid>
            </SubCard>
        </Grid>
        <Grid item sm={6} md={8}>
            <SubCard title={<FormattedMessage id="profile-edit-details-title" />}>
                <Grid container spacing={gridSpacing}>
                    <Grid item xs={6}>
                        <SkeletonTextField />
                    </Grid>
                    <Grid item xs={6}>
                        <SkeletonTextField />
                    </Grid>
                    <Grid item md={12}>
                        <SkeletonTextField />
                    </Grid>
                    <Grid item md={6} xs={12}>
                        <SkeletonTextField />
                    </Grid>
                    <Grid item md={6} xs={12}>
                        <SkeletonTextField />
                    </Grid>
                    <Grid item xs={12}>
                        <Skeleton height={70} width={100} />
                    </Grid>
                </Grid>
            </SubCard>
        </Grid>
    </Grid>
);

export default SkeletonProfile;
