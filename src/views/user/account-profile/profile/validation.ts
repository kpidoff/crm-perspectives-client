import * as Yup from 'yup';

const validationSchema = Yup.object().shape({
    lastname: Yup.string().required('lastname-required'),
    firstname: Yup.string().required('firstname-required')
});

export default validationSchema;
