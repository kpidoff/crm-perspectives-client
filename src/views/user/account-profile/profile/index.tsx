// third party
import LoadingButton from '@mui/lab/LoadingButton';
import { Avatar, Button, Grid, Typography } from '@mui/material';
import config from 'config';
import { Formik, Form } from 'formik';
import { useEditUser } from 'hooks/settings/user/useUser';
import { useEffect, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { gridSpacing } from 'store/constant';

// assets

// project imports
import { UserDetails } from 'types/user';
import SubCard from 'ui-component/cards/SubCard';
import InputDate from 'ui-component/input/input-date';
import InputPhone from 'ui-component/input/input-phone';
import InputTextField from 'ui-component/input/input-textField';
import { fileInputChange, FileType } from 'utils/base64';
import validationSchema from './validation';

// ==============================|| PROFILE 3 - PROFILE ||============================== //

const Profile = ({ user }: { user: UserDetails }) => {
    const [editUser, setEditUser] = useState<UserDetails>(user);
    const [fileAvatar, setFileAvatar] = useState<FileType | null>(null);
    const [avatar, setAvatar] = useState<string | undefined>(config.urlDocument + user?.avatar);
    const { setUser, loading, user: userReponse } = useEditUser();

    const handleFileInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.files != null) {
            fileInputChange(e.target.files[0]).then((values) => {
                setFileAvatar(values);
                setAvatar(values?.base64);
            });
        }
    };

    useEffect(() => {
        userReponse && setEditUser(userReponse);
    }, [userReponse]);

    return (
        <Formik
            initialValues={{
                ...editUser,
                file: ''
            }}
            validationSchema={validationSchema}
            enableReinitialize
            onSubmit={async (values) => {
                setUser(user.id, {
                    address: values.address,
                    avatar: fileAvatar || undefined,
                    city: values.city,
                    civility: values.civility,
                    dateOfBirth: values.dateOfBirth,
                    firstname: values.firstname,
                    lastname: values.lastname,
                    phone: values.phone
                });
            }}
        >
            {({ errors, handleBlur, handleChange, touched, values, isValid, dirty, setFieldValue }) => (
                <Form>
                    <Grid container spacing={gridSpacing}>
                        <Grid item sm={6} md={4}>
                            <SubCard title={<FormattedMessage id="profile-picture-title" />} contentSX={{ textAlign: 'center' }}>
                                <Grid container spacing={2}>
                                    <Grid item xs={12}>
                                        <Avatar alt="User 1" src={avatar} sx={{ width: 100, height: 100, margin: '0 auto' }} />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Typography variant="subtitle2" align="center">
                                            <FormattedMessage id="profile-picture-label" />
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Button variant="contained" component="label">
                                            <FormattedMessage id="app-upload-file" />
                                            <input
                                                type="file"
                                                hidden
                                                name="file"
                                                id="file"
                                                onBlur={handleBlur}
                                                onChange={async (e) => {
                                                    handleChange(e);
                                                    handleFileInputChange(e);
                                                }}
                                            />
                                        </Button>
                                    </Grid>
                                </Grid>
                            </SubCard>
                        </Grid>
                        <Grid item sm={6} md={8}>
                            <SubCard title={<FormattedMessage id="profile-edit-details-title" />}>
                                <Grid container spacing={gridSpacing}>
                                    <Grid item md={6} xs={12}>
                                        <InputTextField
                                            inputProps={{ style: { textTransform: 'uppercase' } }}
                                            value={values.lastname}
                                            name="lastname"
                                            id="lastname"
                                            onBlur={handleBlur}
                                            onChange={handleChange}
                                            label={<FormattedMessage id="user-label-lastname" />}
                                            error={Boolean(touched.lastname && errors.lastname)}
                                            errorMessage={errors.lastname}
                                            required
                                            fullWidth
                                        />
                                    </Grid>
                                    <Grid item md={6} xs={12}>
                                        <InputTextField
                                            inputProps={{ style: { textTransform: 'capitalize' } }}
                                            value={values.firstname}
                                            name="firstname"
                                            id="firstname"
                                            onBlur={handleBlur}
                                            onChange={handleChange}
                                            label={<FormattedMessage id="user-label-firstname" />}
                                            error={Boolean(touched.firstname && errors.firstname)}
                                            errorMessage={errors.firstname}
                                            required
                                            fullWidth
                                        />
                                    </Grid>
                                    <Grid item md={12} xs={12}>
                                        <InputTextField
                                            value={values.email}
                                            name="email"
                                            id="email"
                                            onBlur={handleBlur}
                                            onChange={handleChange}
                                            label={<FormattedMessage id="user-label-email" />}
                                            error={Boolean(touched.email && errors.email)}
                                            errorMessage={errors.email}
                                            required
                                            fullWidth
                                            disabled
                                        />
                                    </Grid>
                                    <Grid item md={6} xs={12}>
                                        <InputPhone
                                            value={values.phone}
                                            name="phone"
                                            id="phone"
                                            onBlur={handleBlur}
                                            onChange={handleChange}
                                            label={<FormattedMessage id="phone" />}
                                            error={Boolean(touched.phone && errors.phone)}
                                            errorMessage={errors.phone}
                                            fullWidth
                                        />
                                    </Grid>
                                    <Grid item md={6} xs={12}>
                                        <InputDate
                                            name="dateOfBirth"
                                            id="dateOfBirth"
                                            label={<FormattedMessage id="user-label-dateOfBirth" />}
                                            value={values.dateOfBirth}
                                            onChange={(value) => setFieldValue('dateOfBirth', value)}
                                            fullWidth
                                        />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <LoadingButton
                                            size="large"
                                            color="secondary"
                                            disabled={!isValid || !dirty}
                                            loading={loading}
                                            variant="contained"
                                            type="submit"
                                        >
                                            <FormattedMessage id="edit" />
                                        </LoadingButton>
                                    </Grid>
                                </Grid>
                            </SubCard>
                        </Grid>
                    </Grid>
                </Form>
            )}
        </Formik>
    );
};

export default Profile;
