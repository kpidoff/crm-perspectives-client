import { LoadingButton } from '@mui/lab';
import { Grid, Typography, Stack } from '@mui/material';
import { useTheme } from '@mui/material/styles';
import { useEditUser } from 'hooks/settings/user/useUser';
import { FormattedMessage } from 'react-intl';
import { UserDetails } from 'types/user';

const UserTwoAuthentication = ({ user }: { user: Pick<UserDetails, 'emailAuthentication' | 'id'> }) => {
    const theme = useTheme();
    const { loading, activeTwoAuthentication } = useEditUser();
    return (
        <Grid container spacing={2}>
            <Grid item xs={12}>
                <Typography variant="body1">
                    <FormattedMessage id="user-security-doubleAuthenticationByEmail-body" />
                </Typography>
            </Grid>
            <Grid item xs={12}>
                <Stack direction="row">
                    <LoadingButton
                        size="small"
                        onClick={() => activeTwoAuthentication(user.id, !user.emailAuthentication)}
                        sx={
                            user.emailAuthentication
                                ? {
                                      color: theme.palette.error.main,
                                      borderColor: theme.palette.error.main,
                                      '&:hover': {
                                          background: theme.palette.error.light + 25,
                                          borderColor: theme.palette.error.main
                                      }
                                  }
                                : {
                                      color: theme.palette.success.main,
                                      borderColor: theme.palette.success.main,
                                      '&:hover': {
                                          background: theme.palette.success.light + 25,
                                          borderColor: theme.palette.success.main
                                      }
                                  }
                        }
                        loading={loading}
                        variant="outlined"
                    >
                        <FormattedMessage id={user.emailAuthentication ? 'app-desable' : 'app-enable'} />
                    </LoadingButton>
                </Stack>
            </Grid>
        </Grid>
    );
};

export default UserTwoAuthentication;
