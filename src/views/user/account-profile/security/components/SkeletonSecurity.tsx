import { Grid, Typography, Skeleton } from '@mui/material';
import { FormattedMessage } from 'react-intl';
import { gridSpacing } from 'store/constant';
import SubCard from 'ui-component/cards/SubCard';
import SkeletonTextField from 'ui-component/input/input-textField/components/SkeletonTextField';

const SkeletonSecurity = () => (
    <Grid container spacing={gridSpacing}>
        <Grid item sm={6} md={8}>
            <Grid container spacing={gridSpacing}>
                <Grid item xs={12}>
                    <SubCard title={<FormattedMessage id="user-security-changePassword-title" />}>
                        <Grid container spacing={gridSpacing}>
                            <Grid item xs={6}>
                                <SkeletonTextField />
                            </Grid>
                            <Grid item xs={6}>
                                <SkeletonTextField />
                            </Grid>
                            <Grid item xs={12}>
                                <Skeleton height={70} width={200} />
                            </Grid>
                        </Grid>
                    </SubCard>
                </Grid>
            </Grid>
        </Grid>
        <Grid item sm={6} md={4}>
            <Grid container spacing={gridSpacing}>
                <Grid item xs={12}>
                    <SubCard title={<FormattedMessage id="user-security-doubleAuthenticationByEmail-title" />}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <Typography variant="body1">
                                    <Skeleton />
                                    <Skeleton />
                                    <Skeleton />
                                    <Skeleton />
                                    <Skeleton />
                                </Typography>
                            </Grid>
                            <Grid item xs={12}>
                                <Skeleton height={70} width={90} />
                            </Grid>
                        </Grid>
                    </SubCard>
                </Grid>
            </Grid>
        </Grid>
    </Grid>
);

export default SkeletonSecurity;
