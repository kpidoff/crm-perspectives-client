import { LoadingButton } from '@mui/lab';
import { Grid, Stack } from '@mui/material';
import { Formik, Form } from 'formik';
import { useChangePasswordUser } from 'hooks/settings/user/useUser';
import { FormattedMessage } from 'react-intl';
import { gridSpacing } from 'store/constant';
import { InputPassword } from 'ui-component/input';
import validationSchema from './validation';

const UserChangePassword = () => {
    const { changePassword, loading } = useChangePasswordUser();
    return (
        <Formik
            initialValues={{
                password: '',
                confirmPassword: ''
            }}
            validationSchema={validationSchema}
            onSubmit={async (values, { resetForm }) => {
                changePassword(values.password).then(() => {
                    resetForm();
                });
            }}
        >
            {({ errors, handleBlur, handleChange, touched, values, isValid, dirty }) => (
                <Form>
                    <Grid container spacing={gridSpacing}>
                        <Grid item xs={12} md={6}>
                            <InputPassword
                                value={values.password}
                                onBlur={handleBlur}
                                onChange={handleChange}
                                label={<FormattedMessage id="password" />}
                                error={Boolean(touched.password && errors.password)}
                                errorMessage={errors.password}
                                name="password"
                                id="password"
                                strength
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={12} md={6}>
                            <InputPassword
                                value={values.confirmPassword}
                                onBlur={handleBlur}
                                onChange={handleChange}
                                label={<FormattedMessage id="confirm-password" />}
                                error={Boolean(touched.confirmPassword && errors.confirmPassword)}
                                errorMessage={errors.confirmPassword}
                                name="confirmPassword"
                                id="confirmPassword"
                                strength
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Stack direction="row">
                                <LoadingButton
                                    size="large"
                                    color="secondary"
                                    disabled={!isValid || !dirty}
                                    loading={loading}
                                    variant="contained"
                                    type="submit"
                                >
                                    <FormattedMessage id="profile-security-button-label-submit" />
                                </LoadingButton>
                            </Stack>
                        </Grid>
                    </Grid>
                </Form>
            )}
        </Formik>
    );
};

export default UserChangePassword;
