// material-ui
import { Grid } from '@mui/material';

// project imports
import SubCard from 'ui-component/cards/SubCard';
import { gridSpacing } from 'store/constant';
import UserChangePassword from './components/UserChangePassword';
import UserTwoAuthentication from './components/UserTwoAuthentication';
import { UserDetails } from 'types/user';
import UserDevices from '../../../../ui-component/users/user-devices';
import { FormattedMessage } from 'react-intl';

// ==============================|| PROFILE 3 - SECURITY ||============================== //

const Security = ({ user }: { user: UserDetails }) => (
    <Grid container spacing={gridSpacing}>
        <Grid item sm={6} md={8}>
            <Grid container spacing={gridSpacing}>
                <Grid item xs={12}>
                    <SubCard title={<FormattedMessage id="user-security-changePassword-title" />}>
                        <UserChangePassword />
                    </SubCard>
                </Grid>
            </Grid>
        </Grid>
        <Grid item sm={6} md={4}>
            <Grid container spacing={gridSpacing}>
                <Grid item xs={12}>
                    <SubCard title={<FormattedMessage id="user-security-doubleAuthenticationByEmail-title" />}>
                        <UserTwoAuthentication user={user} />
                    </SubCard>
                </Grid>
            </Grid>
        </Grid>
        <Grid item xs={12}>
            <Grid container spacing={gridSpacing}>
                <Grid item xs={12}>
                    <UserDevices userId={user.id} />
                </Grid>
            </Grid>
        </Grid>
    </Grid>
);

export default Security;
