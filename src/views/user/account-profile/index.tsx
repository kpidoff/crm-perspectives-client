// project imports
import ProfileForm from './profile';
import Security from './security';
import NotificationsForm from './notifications';
import MainCard from 'ui-component/cards/MainCard';
import TabContent, { TopContentProps } from 'ui-component/tab-content';
import useAuth from 'hooks/authentication/useAuth';
import { FormattedMessage, useIntl } from 'react-intl';
import SkeletonProfile from './profile/SkeletonProfile';
import SkeletonSecurity from './security/components/SkeletonSecurity';

// ==============================|| PROFILE 3 ||============================== //

const Profile = () => {
    const { user } = useAuth();
    const intl = useIntl();

    const tabContent: TopContentProps[] = [
        {
            title: intl.formatMessage({ id: 'profile-tab-profile' }),
            component: user ? <ProfileForm user={user} /> : null,
            skeleton: <SkeletonProfile />
        },
        {
            title: intl.formatMessage({ id: 'profile-tab-security' }),
            component: user ? <Security user={user} /> : null,
            skeleton: <SkeletonSecurity />
        },
        {
            title: intl.formatMessage({ id: 'profile-tab-notification' }),
            component: <NotificationsForm />,
            disabled: true
        }
    ];

    return (
        <MainCard title={<FormattedMessage id="profile-title" />}>
            <TabContent content={tabContent} />
        </MainCard>
    );
};

export default Profile;
