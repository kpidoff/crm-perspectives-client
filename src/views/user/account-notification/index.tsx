import {
    useDeleteUserSubscriptionNotification,
    UserNotificationFilterInput,
    useUserNotifications,
    useUserSubscriptionAddNotification
} from 'hooks/settings/user/useUser';
import { useCallback, useEffect, useState } from 'react';
import { UserNotification } from 'types/user';
// import { PageListFilterWrapper, Paginate } from 'ui-component/data/page-generate';
// import { SearchType } from 'ui-component/data/components/search/search.types';
import { DEFAULT_FILTER, NOTIFICATIONS_NUMER_PER_PAGE } from './notifications.societies';
import useShemaFilter from './notificationsFilters';
import UserNotificationList from 'ui-component/users/components/UserNotificationList';
import useAuth from 'hooks/authentication/useAuth';
import DatasListWrapper from 'ui-component/datas-components/components/datas-components-wrapper/datas-list-wrapper';
import MainCard from 'ui-component/cards/MainCard';
// import Search from 'ui-component/data/components/search';
// import Filters from 'ui-component/data/components/filters';

// const search: SearchType = {
//     filterKey: 'search',
//     type: 'text',
//     label: 'Recherche',
//     wait: 250
// };

// ==============================|| USER LIST STYLE 2 ||============================== //
const AccountNotification = () => {
    const { getUserNotifications } = useAuth();
    const [filter, setFilter] = useState<UserNotificationFilterInput>(DEFAULT_FILTER);
    const [currentPage, setCurrentPage] = useState(1);
    const { getNotifications, pageCursor, notifications, loading, refetch } = useUserNotifications();
    const schema = useShemaFilter(filter);
    const { notification: notificationAdd } = useUserSubscriptionAddNotification();
    const { deleteNotification: deleteNotificationReponse } = useDeleteUserSubscriptionNotification();
    const [notificationsList, setNotificationList] = useState<UserNotification[]>([]);

    useEffect(() => {
        getNotifications({
            paginate: {
                perPage: NOTIFICATIONS_NUMER_PER_PAGE,
                page: currentPage
            },
            filter
        });
    }, [currentPage, filter, getNotifications, getUserNotifications]);

    useEffect(() => {
        notifications && setNotificationList(notifications);
    }, [notifications]);

    useEffect(() => {
        notificationAdd && filter.active && refetch();
    }, [filter.active, notificationAdd, refetch]);

    useEffect(() => {
        deleteNotificationReponse && filter.active && refetch();
    }, [filter.active, deleteNotificationReponse, refetch]);

    useEffect(() => {
        deleteNotificationReponse && !filter.active && refetch();
    }, [filter.active, deleteNotificationReponse, refetch]);

    const updateFilter = useCallback((value: any, key: string) => {
        setFilter((prevState) => ({
            ...prevState,
            [key]: value
        }));
        setCurrentPage(1);
    }, []);

    const onClear = useCallback(() => {
        setFilter(DEFAULT_FILTER);
    }, []);

    return (
        <MainCard title="Liste des notifications">
            <DatasListWrapper
                dataComponent={{
                    datas: notificationsList,
                    itemsPerPage: NOTIFICATIONS_NUMER_PER_PAGE,
                    loading,
                    filter: {
                        onClear,
                        schema,
                        updateFilter
                    }
                }}
                datasComponentsSearch={{
                    filterkey: 'search',
                    updateSearch: updateFilter
                }}
                datasComponentContent={{
                    component: (value: UserNotification) => <UserNotificationList notification={value} />
                }}
                datasComponentPaginate={{
                    currentPage: pageCursor?.currentPage,
                    numberOfPage: pageCursor?.lastPage,
                    onChangePage: (page) => setCurrentPage(page)
                }}
            />
        </MainCard>
    );
};

export default AccountNotification;
