import { NUMER_PER_PAGE } from 'constants/paginate';
import { UserNotificationFilterInput } from 'hooks/settings/user/useUser';

export const NOTIFICATIONS_NUMER_PER_PAGE = NUMER_PER_PAGE;
export const DEFAULT_FILTER: UserNotificationFilterInput = {
    active: true
};
