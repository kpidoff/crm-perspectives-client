import { UserNotificationFilterInput } from 'hooks/settings/user/useUser';
import { FilterType } from 'ui-component/datas-components/components/datas-components-drawer/filters.types';

const useShemaFilter = (filters: UserNotificationFilterInput): FilterType[] => [
    {
        filterKey: 'active',
        type: 'select',
        label: 'Affiché les supprimées',
        value: filters.active,
        mapper: (item) => item?.value,
        defaultExpand: true,
        items: [
            {
                key: '1',
                label: 'Oui',
                value: false
            },
            {
                key: '2',
                label: 'Non',
                value: true
            }
        ]
    }
];

export default useShemaFilter;
