import { useMediaQuery } from '@mui/material';
import { useTheme } from '@mui/material/styles';
import Slider from 'react-slick';

const RelatedLand = () => {
    const theme = useTheme();
    const matchDownXl = useMediaQuery(theme.breakpoints.down('xl'));
    const matchDownLG = useMediaQuery(theme.breakpoints.down('xl'));
    const matchDownMD = useMediaQuery(theme.breakpoints.down('lg'));
    const matchDownSM = useMediaQuery(theme.breakpoints.down('md'));

    let noItems = 5;
    noItems = matchDownXl ? 4 : noItems;
    noItems = matchDownLG ? 3 : noItems;
    noItems = matchDownMD ? 2 : noItems;
    noItems = matchDownSM ? 1 : noItems;

    const settings = {
        dots: false,
        infinite: false,
        centerMode: true,
        swipeToSlide: true,
        focusOnSelect: true,
        centerPadding: '0px',
        slidesToShow: noItems
    };

    console.log(noItems);
    console.log(matchDownXl, matchDownLG, matchDownMD, matchDownSM);
    return (
        <>
            {/* <Slider {...settings}>
                <p>test</p>
            </Slider> */}
        </>
    );
};

export default RelatedLand;
