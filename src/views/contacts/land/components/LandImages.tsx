import { useLandImages } from 'hooks/contacts/lands/useLand';
import { useEffect } from 'react';
import _ from 'lodash';
import SliderView from 'ui-component/slider/Slider';
import { landImageUrl } from 'services/contacts/lands/land.service';

const LandImages = ({ landId }: { landId: number | undefined }) => {
    const { getLandImages, landImages, loading } = useLandImages();

    useEffect(() => {
        landId && getLandImages(landId);
    }, [getLandImages, landId]);

    const defaultImage = _.chain(landImages)
        .filter((landImage) => landImage.default)
        .first()
        .value();

    return (
        <SliderView
            items={_.map(landImages, (landImage) => landImageUrl(landImage))}
            defaultItem={defaultImage && landImageUrl(defaultImage)}
            loading={loading}
        />
    );
};

export default LandImages;
