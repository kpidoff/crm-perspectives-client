import * as Yup from 'yup';

const validationSchema = Yup.object().shape({
    title: Yup.string().required('required-field'),
    postalCode: Yup.string().required('required-field'),
    city: Yup.string().required('required-field'),
    createdDate: Yup.date().nullable().required('required-field').typeError('invalid-date'),

    price: Yup.number().required('required-field'),
    area: Yup.number().required('required-field')
});

export default validationSchema;
