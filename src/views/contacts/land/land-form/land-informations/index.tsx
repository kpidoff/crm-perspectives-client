import { Collapse, Grid, MenuItem } from '@mui/material';
import { useFormikContext } from 'formik';
import _ from 'lodash';
import { FormattedMessage } from 'react-intl';
import { landType, landTypeLabel } from 'services/contacts/lands/land.service';
import { gridSpacing } from 'store/constant';
import { Land } from 'types/contacts/land';
import InputArea from 'ui-component/input/input-area';
import InputPrice from 'ui-component/input/input-price';
import SelectBase from 'ui-component/select/components/SelectBase';
import SelectUserCommercial from 'ui-component/select/select-user-commercial';
import LandSpecific from './components/LandSpecific';
import LandSubdivision from './components/LandSubdivision';

const LandInformations = () => {
    const { status, values, handleBlur, handleChange, touched, errors, setFieldValue } = useFormikContext<Land>();
    console.log(values.commercialId);
    return (
        <Grid container spacing={gridSpacing}>
            <Grid item xs={12}>
                <Grid container spacing={gridSpacing}>
                    <Grid item xs={12} md={6}>
                        <InputPrice
                            value={values.price}
                            name="price"
                            id="price"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            label={<FormattedMessage id="price" />}
                            error={Boolean(touched.price && errors.price)}
                            errorMessage={errors.price}
                            required
                            fullWidth
                            skeleton={status}
                        />
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <InputArea
                            value={values.area}
                            name="area"
                            id="area"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            label={<FormattedMessage id="area" />}
                            error={Boolean(touched.area && errors.area)}
                            errorMessage={errors.area}
                            required
                            fullWidth
                            skeleton={status}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <SelectBase
                            value={values.type}
                            name="type"
                            id="type"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            label={<FormattedMessage id="type" />}
                            error={Boolean(touched.type && errors.type)}
                            errorMessage={errors.type}
                            required
                            fullWidth
                            skeleton={status}
                        >
                            {_.map(landType, (type) => (
                                <MenuItem key={type} value={type}>
                                    {landTypeLabel(type)}
                                </MenuItem>
                            ))}
                        </SelectBase>
                    </Grid>
                    <Grid item xs={12}>
                        <Collapse in={values.type === 'SUBDIVISION'}>
                            <LandSubdivision />
                        </Collapse>
                    </Grid>
                    <Grid item xs={12}>
                        <LandSpecific />
                    </Grid>
                    <Grid item xs={12}>
                        <SelectUserCommercial
                            societyId={values.societyId}
                            label="Commercial"
                            value={values.commercialId}
                            onChange={(user) => setFieldValue('commercialId', user?.id || null)}
                            skeleton={status}
                        />
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default LandInformations;
