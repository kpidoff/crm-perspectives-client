import { FormGroup, FormControlLabel, Switch, Typography } from '@mui/material';
import { useFormikContext } from 'formik';
import _ from 'lodash';
import { FormattedMessage } from 'react-intl';
import { landSpecificities, landSpecificLabel } from 'services/contacts/lands/land.service';
import { Land, LandSpecific as LandSpecificType } from 'types/contacts/land';

const LandSpecific = () => {
    const { values, setFieldValue } = useFormikContext<Land>();
    const handleChange = (specific: LandSpecificType, checked: boolean) => {
        setFieldValue(specific, checked);
    };
    return (
        <>
            <Typography>
                <FormattedMessage id="land-specificities" />
            </Typography>
            <FormGroup>
                {_.map(landSpecificities, (specific) => (
                    <FormControlLabel
                        key={specific}
                        onChange={() => handleChange(specific, !values[specific])}
                        control={<Switch value={values[specific]} />}
                        label={landSpecificLabel(specific)}
                    />
                ))}
            </FormGroup>
        </>
    );
};

export default LandSpecific;
