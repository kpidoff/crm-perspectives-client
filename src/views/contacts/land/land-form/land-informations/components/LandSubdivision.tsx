import { Divider, Grid, IconButton, List, ListItem, Stack, Tooltip, Typography } from '@mui/material';
import { useFormikContext } from 'formik';
import _ from 'lodash';
import { FormattedMessage } from 'react-intl';
import { landFormatTitle } from 'services/contacts/lands/land.service';
import { Land, LandSubdivision as LandSubdivisionType } from 'types/contacts/land';
import { formatArea, formatPrice } from 'utils';
import DeleteIcon from '@mui/icons-material/Delete';
import UnarchiveIcon from '@mui/icons-material/Unarchive';
import AddIcon from '@mui/icons-material/Add';
import LandsList from 'ui-component/lands/LandsList';
import React, { useState } from 'react';
import { toastError } from 'utils/toast';
import ArchiveIcon from '@mui/icons-material/Archive';

const LandSubdivision = () => {
    const { values, setFieldValue } = useFormikContext<Land>();
    const [openList, setOpenList] = useState(false);

    const handleAdd = () => {
        setOpenList(true);
    };

    const handleDelete = (landSubdivision: LandSubdivisionType) => {
        setFieldValue('subdivisions', [..._.filter(values.subdivisions!, (subdivision) => subdivision.id !== landSubdivision.id)]);
    };

    const handleArchived = (landSubdivision: LandSubdivisionType) => {
        const newArray = [
            ..._.filter(values.subdivisions!, (subdivision) => subdivision.id !== landSubdivision.id),
            {
                ...landSubdivision,
                archived: !landSubdivision.archived
            }
        ];
        setFieldValue('subdivisions', newArray);
    };

    const handleClose = (land: Land | undefined) => {
        if (_.find(values.subdivisions, (subdivision) => subdivision.id === land?.id)) {
            toastError('Ce terrain fait déjà partie du lotissement');
            setOpenList(false);
            return;
        }
        land &&
            setFieldValue('subdivisions', [
                ...values.subdivisions!,
                {
                    id: land.id,
                    title: land.title,
                    price: land.price,
                    area: land.area
                }
            ]);
        setOpenList(false);
    };

    return (
        <>
            <LandsList open={openList} onClose={handleClose} />
            <Grid container>
                <Grid item xs={12}>
                    <>
                        Liste des lots
                        <IconButton sx={{ marginTop: '-5px' }} onClick={handleAdd}>
                            <AddIcon color="success" />
                        </IconButton>
                    </>
                </Grid>

                <Grid item xs={12}>
                    <List>
                        {_.map(values.subdivisions, (subdivision) => (
                            <React.Fragment key={subdivision.id}>
                                <ListItem
                                    secondaryAction={
                                        <Stack direction="row" spacing={3}>
                                            <Tooltip title={<FormattedMessage id={subdivision.archived ? 'unarchived' : 'archived'} />}>
                                                <IconButton
                                                    edge="end"
                                                    color={subdivision.archived ? 'success' : 'error'}
                                                    onClick={() => handleArchived(subdivision)}
                                                >
                                                    {subdivision.archived ? <UnarchiveIcon /> : <ArchiveIcon />}
                                                </IconButton>
                                            </Tooltip>

                                            <IconButton edge="end" color="error" onClick={() => handleDelete(subdivision)}>
                                                <DeleteIcon />
                                            </IconButton>
                                        </Stack>
                                    }
                                >
                                    <Grid container>
                                        <Grid item xs={12}>
                                            {landFormatTitle(subdivision.title)}
                                        </Grid>
                                        <Grid item xs={12}>
                                            <Stack>
                                                <Typography component="span" variant="subtitle2">
                                                    <FormattedMessage id="area" />: {formatArea(subdivision.area)}
                                                </Typography>
                                                <Typography component="span" variant="subtitle2">
                                                    <FormattedMessage id="price" />: {formatPrice(subdivision.price)}
                                                </Typography>
                                            </Stack>
                                        </Grid>
                                    </Grid>
                                </ListItem>
                                <Divider />
                            </React.Fragment>
                        ))}
                    </List>
                </Grid>
            </Grid>
        </>
    );
};

export default LandSubdivision;
