import { Grid } from '@mui/material';
import { useFormikContext } from 'formik';
import _ from 'lodash';
import { FormattedMessage } from 'react-intl';
import { gridSpacing } from 'store/constant';
import { Land } from 'types/contacts/land';
import { InputDate, InputTextField } from 'ui-component/input';
import TextFieldCity from 'ui-component/input/input-postal-code/input-city';
import TextFieldPostalCode from 'ui-component/input/input-postal-code/input-postal-code';

const LandDetails = () => {
    const { status, values, handleBlur, handleChange, touched, errors, setFieldValue } = useFormikContext<Land>();
    return (
        <Grid container spacing={gridSpacing}>
            <Grid item xs={12}>
                <Grid container spacing={gridSpacing}>
                    <Grid item xs={12} lg={4}>
                        <InputDate
                            name="createdDate"
                            id="createdDate"
                            label={<FormattedMessage id="createdDate" />}
                            value={values.createdDate}
                            onChange={(value: any) => setFieldValue('createdDate', value)}
                            error={Boolean(touched.createdDate && errors.createdDate)}
                            errorMessage={_.toString(errors.createdDate)}
                            required
                            fullWidth
                            skeleton={status}
                        />
                    </Grid>
                    <Grid item xs={12} lg={8}>
                        <InputTextField
                            value={values.title}
                            capitalizeFirstCaractere
                            name="title"
                            id="title"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            label={<FormattedMessage id="title" />}
                            error={Boolean(touched.title && errors.title)}
                            errorMessage={errors.title}
                            required
                            fullWidth
                            skeleton={status}
                        />
                    </Grid>
                </Grid>
            </Grid>
            <Grid item xs={12}>
                <InputTextField
                    value={values.description}
                    capitalizeFirstCaractere
                    name="description"
                    id="description"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    label={<FormattedMessage id="description" />}
                    error={Boolean(touched.description && errors.description)}
                    errorMessage={errors.description}
                    fullWidth
                    multiline
                    rows={4}
                    skeleton={status}
                />
            </Grid>

            <Grid item xs={12}>
                <InputTextField
                    value={values.address}
                    name="address"
                    id="address"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    label={<FormattedMessage id="address" />}
                    error={Boolean(touched.address && errors.address)}
                    errorMessage={errors.address}
                    fullWidth
                    skeleton={status}
                />
            </Grid>
            <Grid item xs={12} md={4}>
                <TextFieldPostalCode
                    onChange={(postalCode) => setFieldValue('postalCode', postalCode)}
                    onSelect={(postalCode) => {
                        setFieldValue('city', postalCode.city);
                        setFieldValue('postalCode', postalCode.postal_code);
                    }}
                    value={values.postalCode}
                    label={<FormattedMessage id="postalCode" />}
                    error={Boolean(touched.postalCode && errors.postalCode)}
                    errorMessage={errors.postalCode}
                    required
                    fullWidth
                    skeleton={status}
                    name="postalCode"
                    id="postalCode"
                    placeholder="Code postal"
                />
            </Grid>
            <Grid item xs={12} md={8}>
                <TextFieldCity
                    value={values.city}
                    onChange={(city) => setFieldValue('city', city)}
                    onSelect={(postalCode) => {
                        setFieldValue('city', postalCode.city);
                        setFieldValue('postalCode', postalCode.postal_code);
                    }}
                    name="city"
                    id="city"
                    label={<FormattedMessage id="city" />}
                    error={Boolean(touched.city && errors.city)}
                    errorMessage={errors.city}
                    required
                    fullWidth
                    capitalizeFirstCaractere
                    skeleton={status}
                />
            </Grid>
        </Grid>
    );
};

export default LandDetails;
