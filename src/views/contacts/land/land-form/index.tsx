import { LoadingButton } from '@mui/lab';
import { Grid } from '@mui/material';
import { Form, Formik, useFormikContext } from 'formik';
import { useLandById } from 'hooks/contacts/lands/useLand';
import { useEffect } from 'react';
import { Land } from 'types/contacts/land';
import TabContent, { TopContentProps } from 'ui-component/tab-content';
import LandDetails from './land-details';
import LandInformations from './land-informations';
import LandServiceProvider from './land-service-provider';
import validationSchema from './validation';

const LandContentForm = () => {
    const { errors } = useFormikContext<Land>();

    const tabContent: TopContentProps[] = [
        {
            title: 'Details',
            component: <LandDetails />,
            error: !!errors.title || !!errors.postalCode || !!errors.city || !!errors.createdDate
        },
        {
            title: 'Informations',
            component: <LandInformations />,
            error: !!errors.area || !!errors.price
        },
        {
            title: 'Prestataire',
            component: <LandServiceProvider />
        }
    ];
    return (
        <>
            <TabContent content={tabContent} />
        </>
    );
};

const LandForm = ({ landId }: { landId: number }) => {
    const { getLandById, land, loading } = useLandById();

    useEffect(() => {
        getLandById(landId);
    }, [getLandById, landId]);

    return (
        <Grid container spacing={2}>
            <Grid item xs={12}>
                <Formik
                    initialValues={{ ...land }}
                    initialStatus={loading}
                    enableReinitialize
                    validationSchema={validationSchema}
                    onSubmit={async (values) => {
                        console.log(values);
                    }}
                >
                    {({ isValid, dirty }) => (
                        <Form>
                            <LandContentForm />
                            <LoadingButton size="large" color="secondary" disabled={!isValid || !dirty} variant="contained" type="submit">
                                ok
                            </LoadingButton>
                        </Form>
                    )}
                </Formik>
            </Grid>
        </Grid>
    );
};

export default LandForm;
