import { Grid } from '@mui/material';
import _ from 'lodash';
import { useParams } from 'react-router-dom';
import { gridSpacing } from 'store/constant';
import MainCard from 'ui-component/cards/MainCard';
import LandImages from './components/LandImages';
import LandForm from './land-form';

export const Land = () => {
    const { id } = useParams();
    // return <FilesManagerWrapper />;

    return (
        <Grid container alignItems="center" justifyContent="center" spacing={gridSpacing}>
            <Grid item xs={12} lg={10}>
                <MainCard>
                    <Grid container spacing={gridSpacing}>
                        <Grid item xs={12} md={6}>
                            <LandImages landId={_.toInteger(id)} />
                        </Grid>
                        <Grid item xs={12} md={6}>
                            <LandForm landId={_.toInteger(id)} />
                        </Grid>
                    </Grid>
                </MainCard>
            </Grid>
        </Grid>
    );
};

export default Land;
