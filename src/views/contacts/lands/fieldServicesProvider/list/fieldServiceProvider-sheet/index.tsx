import { Grid, Typography, Button } from '@mui/material';
import { Form, Formik } from 'formik';
import { LoadingButton } from '@mui/lab';
import { FormattedMessage } from 'react-intl';
import { gridSpacing } from 'store/constant';
import validationSchema from './validation';
import useAuth from 'hooks/authentication/useAuth';
import ButtonConfirm from 'ui-component/button/button-confirm';
import { useEffect } from 'react';
import DatasListNameEditing from 'ui-component/datas-components/components/datas-components-wrapper/datas-list-name-with-editing/components/DatasListNameEditing';
import {
    useEditFieldServiceProvider,
    useCreateFieldServiceProvider,
    useDeleteFieldServiceProvider,
    useFieldServiceProviderByLabel
} from 'hooks/contacts/lands/useFieldServiceProvicer';
import { FieldServiceProvider } from 'types/contacts/lands/fieldServiceProvider';
import contactsRoutesManager from 'routes-manager/contacts/land';
import FieldServiceProviderForm from './components/FieldServiceProviderForm';

interface FieldServiceProviderSheetProps {
    fieldServiceProvider?: FieldServiceProvider | null;
    onClose?: () => void;
    societyId?: number;
}

const FieldServiceProviderSheet = ({ fieldServiceProvider, onClose, societyId }: FieldServiceProviderSheetProps) => {
    const { getFieldServiceProviderByLabel } = useFieldServiceProviderByLabel();
    const {
        editFieldServiceProvider,
        loading: loadingEdit,
        fieldServiceProvider: fieldServiceProviderEditReponse
    } = useEditFieldServiceProvider();
    const {
        createFieldServiceProvider,
        loading: loadingCreate,
        fieldServiceProvider: fieldServiceProviderCreateReponse
    } = useCreateFieldServiceProvider();
    const { deleteFieldServiceProvider, loading: loadingDelete, message: messageDelete } = useDeleteFieldServiceProvider();
    const { getUserAllowedAccess } = useAuth();
    const deleteAccess = getUserAllowedAccess(contactsRoutesManager.fieldServiceProvider.id, 'delete');

    useEffect(() => {
        messageDelete && onClose?.();
    }, [messageDelete, onClose]);

    useEffect(() => {
        fieldServiceProviderCreateReponse && onClose?.();
    }, [fieldServiceProviderCreateReponse, onClose]);

    useEffect(() => {
        fieldServiceProviderEditReponse && onClose?.();
    }, [fieldServiceProviderEditReponse, onClose]);

    return (
        <DatasListNameEditing
            onClose={onClose}
            header={
                <Typography variant="h5" component="div" sx={{ fontSize: '1rem' }}>
                    {fieldServiceProvider ? (
                        <FormattedMessage id="fieldServiceProvider-form-title-edit" />
                    ) : (
                        <FormattedMessage id="fieldServiceProvider-form-title-create" />
                    )}
                </Typography>
            }
        >
            <Formik
                initialValues={{
                    ...fieldServiceProvider,
                    societyId
                }}
                validationSchema={validationSchema(
                    {
                        fieldServiceProviderByLabel: getFieldServiceProviderByLabel,
                        initialLabel: fieldServiceProvider?.label || ''
                    },
                    !societyId
                )}
                enableReinitialize
                onSubmit={async (values) => {
                    fieldServiceProvider && fieldServiceProvider.id
                        ? editFieldServiceProvider(values)
                        : createFieldServiceProvider(values, values.societyId);
                }}
            >
                {({ isValid, dirty }) => (
                    <Form>
                        <Grid container spacing={gridSpacing}>
                            <Grid item xs={12}>
                                <FieldServiceProviderForm />
                            </Grid>
                            <Grid item xs={12}>
                                <Grid container spacing={1}>
                                    <Grid item xs={deleteAccess && fieldServiceProvider ? 4 : 6}>
                                        <LoadingButton
                                            color="secondary"
                                            disabled={!isValid || !dirty}
                                            loading={loadingEdit || loadingCreate}
                                            variant="contained"
                                            type="submit"
                                            fullWidth
                                        >
                                            {fieldServiceProvider ? <FormattedMessage id="edit" /> : <FormattedMessage id="add" />}
                                        </LoadingButton>
                                    </Grid>
                                    <Grid item xs={deleteAccess && fieldServiceProvider ? 4 : 6}>
                                        <Button variant="outlined" fullWidth onClick={onClose}>
                                            <FormattedMessage id="cancel" />
                                        </Button>
                                    </Grid>
                                    {deleteAccess && fieldServiceProvider && (
                                        <Grid item xs={4}>
                                            <ButtonConfirm
                                                title={<FormattedMessage id="fieldServiceProvider-delete-title" />}
                                                content={<FormattedMessage id="fieldServiceProvider-delete-content" />}
                                                labelButtonCancel={<FormattedMessage id="cancel" />}
                                                labelButtonConfirm={<FormattedMessage id="yes" />}
                                                onValidate={() => deleteFieldServiceProvider(fieldServiceProvider)}
                                                loading={loadingDelete}
                                                color="error"
                                                variant="outlined"
                                                fullWidth
                                            >
                                                <FormattedMessage id="app-delete" />
                                            </ButtonConfirm>
                                        </Grid>
                                    )}
                                </Grid>
                            </Grid>
                        </Grid>
                    </Form>
                )}
            </Formik>
        </DatasListNameEditing>
    );
};

export default FieldServiceProviderSheet;
