import { Grid } from '@mui/material';
import { useFormikContext } from 'formik';
import { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { gridSpacing } from 'store/constant';
import { FieldServiceProvider } from 'types/contacts/lands/fieldServiceProvider';
import { InputPhone, InputTextField } from 'ui-component/input';
import TextFieldCity from 'ui-component/input/input-postal-code/input-city';
import TextFieldPostalCode from 'ui-component/input/input-postal-code/input-postal-code';
import SelectAgencyAccess from 'ui-component/select/select-agency-access';

const FieldServiceProviderForm = () => {
    const { values, handleBlur, handleChange, touched, errors, setFieldValue } = useFormikContext<FieldServiceProvider>();
    const [society] = useState<number | undefined>(values.societyId);

    return (
        <Grid container spacing={gridSpacing}>
            {!society && (
                <Grid item xs={12}>
                    <SelectAgencyAccess
                        onChange={(value) => setFieldValue('societyId', value)}
                        label="Agence"
                        value={values.societyId}
                        required
                        autoFocus={!society}
                        error={Boolean(touched.societyId && errors.societyId)}
                        errorMessage={errors.societyId}
                    />
                </Grid>
            )}
            <Grid item xs={12}>
                <InputTextField
                    inputProps={{ style: { textTransform: 'capitalize' } }}
                    value={values.label}
                    name="label"
                    id="label"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    label={<FormattedMessage id="fieldServiceProvider-form-label" />}
                    error={Boolean(touched.label && errors.label)}
                    errorMessage={errors.label}
                    autoFocus={!!society}
                    required
                    fullWidth
                />
            </Grid>
            <Grid item xs={12} md={6}>
                <InputTextField
                    inputProps={{ style: { textTransform: 'uppercase' } }}
                    value={values.lastname}
                    name="lastname"
                    id="lastname"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    label={<FormattedMessage id="lastname" />}
                    error={Boolean(touched.lastname && errors.lastname)}
                    errorMessage={errors.lastname}
                    fullWidth
                />
            </Grid>
            <Grid item xs={12} md={6}>
                <InputTextField
                    inputProps={{ style: { textTransform: 'capitalize' } }}
                    value={values.firstname}
                    name="firstname"
                    id="firstname"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    label={<FormattedMessage id="firstname" />}
                    error={Boolean(touched.firstname && errors.firstname)}
                    errorMessage={errors.firstname}
                    fullWidth
                />
            </Grid>
            <Grid item xs={12}>
                <InputTextField
                    inputProps={{ style: { textTransform: 'capitalize' } }}
                    value={values.address}
                    name="address"
                    id="address"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    label={<FormattedMessage id="address" />}
                    error={Boolean(touched.address && errors.address)}
                    errorMessage={errors.address}
                    fullWidth
                />
            </Grid>
            <Grid item xs={12} md={4}>
                <TextFieldPostalCode
                    onChange={(postalCode) => setFieldValue('postalCode', postalCode)}
                    onSelect={(postalCode) => {
                        setFieldValue('city', postalCode.city);
                        setFieldValue('postalCode', postalCode.postal_code);
                    }}
                    value={values.postalCode}
                    label={<FormattedMessage id="postalCode" />}
                    error={Boolean(touched.postalCode && errors.postalCode)}
                    errorMessage={errors.postalCode}
                    fullWidth
                    name="postalCode"
                    id="postalCode"
                    placeholder="Code postal"
                />
            </Grid>
            <Grid item xs={12} md={8}>
                <TextFieldCity
                    value={values.city}
                    onChange={(city) => setFieldValue('city', city)}
                    onSelect={(postalCode) => {
                        setFieldValue('city', postalCode.city);
                        setFieldValue('postalCode', postalCode.postal_code);
                    }}
                    name="city"
                    id="city"
                    label={<FormattedMessage id="city" />}
                    error={Boolean(touched.city && errors.city)}
                    errorMessage={errors.city}
                    fullWidth
                />
            </Grid>
            <Grid item xs={12}>
                <InputPhone
                    value={values.phone}
                    name="phone"
                    id="phone"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    label={<FormattedMessage id="phone" />}
                    fullWidth
                />
            </Grid>
            <Grid item xs={12}>
                <InputPhone
                    value={values.portable}
                    name="portable"
                    id="portable"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    label={<FormattedMessage id="portable" />}
                    fullWidth
                />
            </Grid>
            <Grid item xs={12}>
                <InputTextField
                    value={values.email}
                    name="email"
                    id="email"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    label={<FormattedMessage id="user-label-email" />}
                    error={Boolean(touched.email && errors.email)}
                    errorMessage={errors.email}
                    fullWidth
                />
            </Grid>
        </Grid>
    );
};

export default FieldServiceProviderForm;
