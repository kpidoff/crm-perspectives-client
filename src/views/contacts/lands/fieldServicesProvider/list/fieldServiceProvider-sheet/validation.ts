import { OperationVariables, QueryResult } from '@apollo/client';
import { FieldServiceProviderByLabelResponse } from 'hooks/contacts/lands/useFieldServiceProvicer';
import * as Yup from 'yup';

function validationSchema(
    labelCheked: {
        fieldServiceProviderByLabel: (label: string) => Promise<QueryResult<FieldServiceProviderByLabelResponse, OperationVariables>>;
        initialLabel: string;
    },
    selectSociety: boolean
) {
    return Yup.object().shape({
        label: Yup.string()
            .nullable()
            .required('label-required')
            .test('label-check', 'label-error-already-used', (label) => {
                if (label && label.toUpperCase() !== labelCheked.initialLabel.toUpperCase()) {
                    return labelCheked.fieldServiceProviderByLabel(label).then((value) => !value.data?.fieldServiceProviderByLabel);
                }
                return true;
            }),
        societyId: Yup.number()
            .nullable()
            .test('society-check', 'society-error-required', (societyId) => {
                if (selectSociety) {
                    return !!societyId;
                }
                return true;
            })
    });
}

export default validationSchema;
