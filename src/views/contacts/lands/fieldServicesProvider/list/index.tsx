import { Grid, Typography } from '@mui/material';
import { NUMER_PER_PAGE } from 'constants/paginate';
import useAuth from 'hooks/authentication/useAuth';
import { useFieldServicesProvider } from 'hooks/contacts/lands/useFieldServiceProvicer';
import { useCallback, useEffect, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import contactsRoutesManager from 'routes-manager/contacts/land';
import { FieldServiceProvider } from 'types/contacts/lands/fieldServiceProvider';
import MainCard from 'ui-component/cards/MainCard';
import DatasListNameWithEditWrapper from 'ui-component/datas-components/components/datas-components-wrapper/datas-list-name-with-editing';
import SpeedDialButton, { SpeedDialButtonType } from 'ui-component/speedDial';
import { formatEmail, formatFullName, formatPhone } from 'utils';
import FieldServiceProviderSheet from './fieldServiceProvider-sheet';
import useFilter from './fieldServicesProvider.filters';

const FieldServicesProvider = () => {
    const { schema, filter, setFilter, defaultFilter } = useFilter();
    const { getUserAllowedAccess } = useAuth();

    const [currentPage, setCurrentPage] = useState(1);
    const { getFieldServicesProvider, pageCursor, fieldServicesProvider, loading } = useFieldServicesProvider();
    const [fieldServiceProvider, setFieldServiceProvider] = useState<FieldServiceProvider | undefined | null>(undefined);

    const editAccess = getUserAllowedAccess(contactsRoutesManager.fieldServiceProvider.id, 'edit');
    const createAccess = getUserAllowedAccess(contactsRoutesManager.fieldServiceProvider.id, 'create');

    useEffect(() => {
        getFieldServicesProvider({
            paginate: {
                perPage: NUMER_PER_PAGE,
                page: currentPage
            },
            filter
        });
    }, [currentPage, filter, getFieldServicesProvider]);

    const updateFilter = useCallback(
        (value: any, key: string) => {
            setFilter((prevState) => ({
                ...prevState,
                [key]: value
            }));
            setCurrentPage(1);
        },
        [setFilter]
    );

    const onClear = useCallback(() => {
        setFilter(defaultFilter);
    }, [defaultFilter, setFilter]);

    const actions: SpeedDialButtonType[] = [
        {
            name: <FormattedMessage id="fieldServicesProvider-add" />,
            action: () => setFieldServiceProvider(null)
        }
    ];

    return (
        <MainCard title={<FormattedMessage id="fieldServicesProvider-title" />}>
            <DatasListNameWithEditWrapper
                dataComponent={{
                    datas: fieldServicesProvider,
                    itemsPerPage: NUMER_PER_PAGE,
                    sortName: 'label',
                    loading,
                    filter: {
                        schema,
                        onClear,
                        updateFilter
                    }
                }}
                datasComponentsSearch={{
                    filterkey: 'search',
                    updateSearch: updateFilter
                }}
                datasComponentPaginate={{
                    currentPage: pageCursor?.currentPage,
                    numberOfPage: pageCursor?.lastPage,
                    onChangePage: (page) => setCurrentPage(page)
                }}
                datasComponentContent={{
                    onClick: editAccess ? (data: FieldServiceProvider) => setFieldServiceProvider(data) : undefined,
                    component: (data: FieldServiceProvider) => (
                        <Grid container>
                            <Grid item xs={4}>
                                <Typography>{data.label}</Typography>
                                <Typography variant="caption">{formatFullName(data.lastname, data.firstname)}</Typography>
                            </Grid>
                            <Grid item xs={4}>
                                <Typography>Tél: {formatPhone(data.phone)}</Typography>
                                <Typography>Portable: {formatPhone(data.portable)}</Typography>
                            </Grid>
                            <Grid item xs={4}>
                                <Typography>Email: {formatEmail(data.email)}</Typography>
                            </Grid>
                        </Grid>
                    )
                }}
                datasListNameEditing={(data) => (
                    <FieldServiceProviderSheet
                        fieldServiceProvider={data}
                        onClose={() => setFieldServiceProvider(undefined)}
                        societyId={filter.societyId}
                    />
                )}
                entity={fieldServiceProvider}
            />
            {createAccess && <SpeedDialButton actions={actions} />}
        </MainCard>
    );
};

export default FieldServicesProvider;
