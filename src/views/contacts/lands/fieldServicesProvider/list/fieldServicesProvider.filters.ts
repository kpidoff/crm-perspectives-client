import useAuth from 'hooks/authentication/useAuth';
import { FieldServicesProviderFilterInput } from 'hooks/contacts/lands/useFieldServiceProvicer';
import { useEffect, useState } from 'react';
import { useIntl } from 'react-intl';
import { FilterType } from 'ui-component/datas-components/components/datas-components-drawer/filters.types';

const useFilter = () => {
    const intl = useIntl();
    const { currentSociety, user } = useAuth();
    const defaultFilter: FieldServicesProviderFilterInput = {
        societyId: currentSociety?.id!
    };

    const [filter, setFilter] = useState<FieldServicesProviderFilterInput>(defaultFilter);

    useEffect(() => {
        setFilter((prevState) => ({
            ...prevState,
            societyId: currentSociety?.id!
        }));
    }, [currentSociety]);

    const schema: FilterType[] = [
        {
            filterKey: 'societyId',
            type: 'societies',
            label: intl.formatMessage({ id: 'filter-societies' }),
            value: filter.societyId,
            defaultExpand: true,
            hidden: user?.societiesAccess && user?.societiesAccess?.length <= 1
        }
    ];

    return {
        schema,
        filter,
        setFilter,
        defaultFilter
    };
};

export default useFilter;
