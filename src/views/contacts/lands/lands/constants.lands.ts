import { NUMER_PER_PAGE } from 'constants/paginate';

export const LANDS_NUMBER_PER_PAGE = NUMER_PER_PAGE;
export const AREA_FILTER_MIN = 0;
export const AREA_FILTER_MAX = 2000;
export const AREA_FILTER_STEP = 100;
export const AREA_FILTER_DEFAULT = [0, 1200];

export const PRICE_FILTER_MIN = 0;
export const PRICE_FILTER_MAX = 250000;
export const PRICE_FILTER_STEP = 5000;
export const PRICE_FILTER_DEFAULT = [0, 200000];
