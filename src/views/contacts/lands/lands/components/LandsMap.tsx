import { Avatar, Box, Card, Collapse, List, ListItemAvatar, ListItemButton, ListItemText, Typography } from '@mui/material';
import { BoxProps } from '@mui/system';
import { Circle, InfoWindow } from '@react-google-maps/api';
import config from 'config';
import useAuth from 'hooks/authentication/useAuth';
import { LandsFilterInput, useLandsLocation } from 'hooks/contacts/lands/useLand';
import _ from 'lodash';
import { useEffect, useState } from 'react';
import { landTypeLabel } from 'services/contacts/lands/land.service';
import { LandLocation } from 'types/contacts/land';
import { textTruncate } from 'ui-component/datas-components/datasComponents.utils';
import GoogleMapWrapper from 'ui-component/google-map-wrapper';
import MarkerIcon from 'ui-component/google-map-wrapper/components/MarkerIcon';
import { formatArea, formatPrice } from 'utils';
import './styles.css';

type LandsMapProps = BoxProps & {
    open: boolean;
    filter?: LandsFilterInput;
};

const LandsMap = ({ open, filter, ...props }: LandsMapProps) => {
    const { currentSociety } = useAuth();
    const { getLandsLocation, lands } = useLandsLocation();
    const [activeMarker, setActiveMarker] = useState<number | null>(null);

    const landsGenerate: {
        lat: number | null;
        lng: number | null;
        lands: LandLocation[];
    }[] = [];

    const handleActiveMarker = (id: number) => {
        if (id !== activeMarker) {
            setActiveMarker(id);
        }
    };

    useEffect(() => {
        getLandsLocation(
            filter?.location?.radius
                ? {
                      filter: {
                          ...filter,
                          location: undefined
                      }
                  }
                : {
                      filter
                  }
        );
    }, [filter, getLandsLocation]);

    _.map(lands, (land) => {
        const landFindIndex = _.findIndex(landsGenerate, (landGenerate) => land.lng === landGenerate.lng && land.lat === landGenerate.lat);
        if (landFindIndex === -1) {
            landsGenerate.push({
                lat: land.lat,
                lng: land.lng,
                lands: [land]
            });
        } else {
            landsGenerate[landFindIndex].lands = [...landsGenerate[landFindIndex].lands, land];
        }
    });

    return (
        <Box {...props}>
            <Collapse in={open}>
                <Card>
                    <GoogleMapWrapper
                        center={{
                            lat: currentSociety?.lat,
                            lng: currentSociety?.lng
                        }}
                    >
                        {open ? (
                            <>
                                {filter?.location && filter?.location?.lat && filter?.location?.lng && filter?.location?.radius && (
                                    <Circle
                                        center={{
                                            lat: filter?.location?.lat,
                                            lng: filter?.location?.lng
                                        }}
                                        radius={filter?.location?.radius * 1000}
                                        options={{ fillColor: 'red', strokeColor: 'red' }}
                                    />
                                )}
                                {_.map(
                                    landsGenerate,
                                    (landList) =>
                                        landList.lng &&
                                        landList.lat && (
                                            <MarkerIcon
                                                key={landList.lands[0].id}
                                                position={{
                                                    lat: landList.lat,
                                                    lng: landList.lng
                                                }}
                                                icon={
                                                    _.find(landList.lands, (land) => land.type === 'SUBDIVISION')
                                                        ? {
                                                              type: 'number',
                                                              icon: 'RED_BLUE'
                                                          }
                                                        : {
                                                              type: 'number',
                                                              icon: 'RED'
                                                          }
                                                }
                                                label={{
                                                    text: _.toString(landList.lands.length),
                                                    className: 'Marker'
                                                }}
                                                onClick={() => handleActiveMarker(landList.lands[0].id)}
                                            >
                                                {activeMarker === landList.lands[0].id ? (
                                                    <InfoWindow onCloseClick={() => setActiveMarker(null)}>
                                                        <Box sx={{ width: '100%', bgcolor: 'background.paper' }}>
                                                            <Typography>{_.capitalize(landList.lands[0].city)}</Typography>
                                                            <List component="nav">
                                                                {_.map(landList.lands, (land) => (
                                                                    <ListItemButton key={land.id}>
                                                                        <ListItemAvatar>
                                                                            <Avatar
                                                                                alt={land.city}
                                                                                src={config.urlDocument + land.defaultImage}
                                                                            />
                                                                        </ListItemAvatar>
                                                                        <ListItemText
                                                                            primary={land.title}
                                                                            secondary={
                                                                                <>
                                                                                    <Typography
                                                                                        sx={{ display: 'block' }}
                                                                                        component="span"
                                                                                        variant="body2"
                                                                                        color="text.primary"
                                                                                    >
                                                                                        {landTypeLabel(land.type)}
                                                                                    </Typography>

                                                                                    <Typography component="span" sx={{ display: 'block' }}>
                                                                                        {land.description &&
                                                                                            _.capitalize(textTruncate(land.description))}
                                                                                    </Typography>
                                                                                    <Typography
                                                                                        sx={{ display: 'inline' }}
                                                                                        component="span"
                                                                                        variant="body2"
                                                                                        color="text.primary"
                                                                                    >
                                                                                        {formatPrice(land.price)}
                                                                                    </Typography>
                                                                                    {' - '}
                                                                                    {formatArea(land.area)}
                                                                                </>
                                                                            }
                                                                        />
                                                                    </ListItemButton>
                                                                ))}
                                                            </List>
                                                        </Box>
                                                    </InfoWindow>
                                                ) : null}
                                            </MarkerIcon>
                                        )
                                )}
                            </>
                        ) : (
                            <></>
                        )}
                    </GoogleMapWrapper>
                </Card>
            </Collapse>
        </Box>
    );
};

export default LandsMap;
