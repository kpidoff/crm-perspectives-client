import useAuth from 'hooks/authentication/useAuth';
import { LandsFilterInput } from 'hooks/contacts/lands/useLand';
import _ from 'lodash';
import { useEffect, useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { LandSpecific, LandType } from 'types/contacts/land';
import checkboxesFilterMappers from 'ui-component/datas-components/components/datas-components-drawer/filter-item/checkboxes-filter/mappers';
import sliderRangeFilterMappers from 'ui-component/datas-components/components/datas-components-drawer/filter-item/slider-range-filter/mappers';
import { FilterType } from 'ui-component/datas-components/components/datas-components-drawer/filters.types';
import { booleanToString, formatArea, formatPrice } from 'utils';
import {
    AREA_FILTER_DEFAULT,
    AREA_FILTER_MAX,
    AREA_FILTER_MIN,
    AREA_FILTER_STEP,
    PRICE_FILTER_DEFAULT,
    PRICE_FILTER_MAX,
    PRICE_FILTER_MIN,
    PRICE_FILTER_STEP
} from './constants.lands';

const useFilter = () => {
    const intl = useIntl();
    const { currentSociety, user } = useAuth();
    const defaultFilter: LandsFilterInput = {
        societyId: currentSociety?.id!,
        location: undefined,
        types: [LandType.NOT_DEFINED, LandType.DIFFUSE, LandType.SUBDIVISION],
        specific: {
            bounding: false,
            serviced: false,
            sewer: false
        },
        rating: 0,
        archived: false,
        area: {
            min: AREA_FILTER_DEFAULT[0],
            max: AREA_FILTER_DEFAULT[1]
        },
        price: {
            min: PRICE_FILTER_DEFAULT[0],
            max: PRICE_FILTER_DEFAULT[1]
        }
    };

    const [filter, setFilter] = useState<LandsFilterInput>(defaultFilter);

    useEffect(() => {
        setFilter((prevState) => ({
            ...prevState,
            societyId: currentSociety?.id!
        }));
    }, [currentSociety]);

    const schema: FilterType[] = [
        {
            filterKey: 'societyId',
            type: 'societies',
            label: intl.formatMessage({ id: 'filter-societies' }),
            value: filter.societyId,
            defaultExpand: true,
            hidden: user?.societiesAccess && user?.societiesAccess?.length <= 1
        },
        {
            filterKey: 'location',
            type: 'location',
            label: 'Localisation',
            wait: 250,
            defaultExpand: true,
            value: filter.location
        },
        {
            filterKey: 'archived',
            type: 'select',
            label: (
                <FormattedMessage
                    id="lands-filter-archived"
                    values={{
                        archived: booleanToString(filter.archived)
                    }}
                />
            ),
            mapper: (item) => item?.value,
            value: filter.archived,
            items: [
                {
                    key: '1',
                    label: 'Oui',
                    value: true
                },
                {
                    key: '1',
                    label: 'Non',
                    value: false
                }
            ]
        },
        {
            filterKey: 'types',
            type: 'checkboxes',
            label: (
                <FormattedMessage
                    id="lands-filter-types"
                    values={{
                        select: filter.types?.length
                    }}
                />
            ),
            mapper: checkboxesFilterMappers.checkboxesToArrayKey,
            items: [
                {
                    key: LandType.NOT_DEFINED,
                    label: 'Non renseigné',
                    value: _.findIndex(filter.types, (t) => t === LandType.NOT_DEFINED) > -1
                },
                {
                    key: LandType.DIFFUSE,
                    label: 'Diffus',
                    value: _.findIndex(filter.types, (t) => t === LandType.DIFFUSE) > -1
                },
                {
                    key: LandType.SUBDIVISION,
                    label: 'Lotissement',
                    value: _.findIndex(filter.types, (t) => t === LandType.SUBDIVISION) > -1
                }
            ]
        },
        {
            filterKey: 'specific',
            type: 'checkboxes',
            label: (
                <FormattedMessage
                    id="lands-filter-specific"
                    values={{
                        select: _.filter(filter.specific, (specific) => specific).length
                    }}
                />
            ),
            mapper: checkboxesFilterMappers.checkboxesToArray,
            items: [
                {
                    key: LandSpecific.SERVICED,
                    label: 'Viabilisé',
                    value: filter.specific && filter.specific[LandSpecific.SERVICED]
                },
                {
                    key: LandSpecific.SEWER,
                    label: "Tout à l'égout",
                    value: filter.specific && filter.specific[LandSpecific.SEWER]
                },
                {
                    key: LandSpecific.BOUNDING,
                    label: 'Bornage',
                    value: filter.specific && filter.specific[LandSpecific.BOUNDING]
                }
            ]
        },
        {
            filterKey: 'area',
            type: 'sliderRange',
            label: (
                <FormattedMessage
                    id="lands-filter-area"
                    values={
                        filter.area
                            ? {
                                  min: formatArea(filter?.area?.min),
                                  max: formatArea(filter?.area?.max)
                              }
                            : {
                                  min: formatArea(AREA_FILTER_DEFAULT[0]),
                                  max: formatArea(AREA_FILTER_DEFAULT[1])
                              }
                    }
                />
            ),
            wait: 250,
            min: AREA_FILTER_MIN,
            max: AREA_FILTER_MAX,
            step: AREA_FILTER_STEP,
            value: filter.area ? [filter.area.min, filter.area.max] : AREA_FILTER_DEFAULT,
            mapper: sliderRangeFilterMappers.toMinMax
        },
        {
            filterKey: 'price',
            type: 'sliderRange',
            label: (
                <FormattedMessage
                    id="lands-filter-price"
                    values={
                        filter.price
                            ? {
                                  min: formatPrice(filter?.price?.min),
                                  max: formatPrice(filter?.price?.max)
                              }
                            : {
                                  min: formatPrice(PRICE_FILTER_DEFAULT[0]),
                                  max: formatPrice(PRICE_FILTER_DEFAULT[1])
                              }
                    }
                />
            ),
            wait: 250,
            min: PRICE_FILTER_MIN,
            max: PRICE_FILTER_MAX,
            step: PRICE_FILTER_STEP,
            value: filter.price ? [filter.price.min, filter.price.max] : PRICE_FILTER_DEFAULT,
            mapper: sliderRangeFilterMappers.toMinMax
        },
        {
            filterKey: 'rating',
            type: 'rating',
            label: filter.rating ? (
                <FormattedMessage
                    id="lands-filter-rate-params"
                    values={{
                        rate: filter.rating
                    }}
                />
            ) : (
                <FormattedMessage
                    id="lands-filter-rate"
                    values={{
                        rate: filter.rating
                    }}
                />
            ),
            value: filter.rating,
            precision: 1
        }
    ];

    return {
        schema,
        filter,
        setFilter,
        defaultFilter
    };
};

export default useFilter;
