import { useCallback, useEffect, useState } from 'react';
import { LANDS_NUMBER_PER_PAGE } from './constants.lands';
import { useLands } from 'hooks/contacts/lands/useLand';
import DatasGridWrapper from 'ui-component/datas-components/components/datas-components-wrapper/datas-grid-wrapper';
import { Land } from 'types/contacts/land';
import useFilter from './lands.filters';
import LandsMap from './components/LandsMap';
import MapIcon from '@mui/icons-material/Map';
import CardLand from 'ui-component/land/CardLand';
import { useNavigate } from 'react-router-dom';
import contactsRoutesManager from 'routes-manager/contacts/land';
import useAuth from 'hooks/authentication/useAuth';
import DatasListWrapper from 'ui-component/datas-components/components/datas-components-wrapper/datas-list-wrapper';

// ==============================|| USER LIST STYLE 2 ||============================== //
const Lands = () => {
    const navigate = useNavigate();
    const { getUserAllowedAccess } = useAuth();
    const [currentPage, setCurrentPage] = useState(1);
    const { getLands, pageCursor, lands, loading } = useLands();
    const { defaultFilter, filter, schema, setFilter } = useFilter();
    const [mapOpen, setMapOpen] = useState(false);

    useEffect(() => {
        getLands({
            paginate: {
                perPage: LANDS_NUMBER_PER_PAGE,
                page: currentPage
            },
            filter
        });
    }, [currentPage, filter, getLands]);

    function openLand(landId: number) {
        navigate(contactsRoutesManager.land.link(landId));
    }

    const updateFilter = useCallback(
        (value: any, key: string) => {
            setFilter((prevState) => ({
                ...prevState,
                [key]: value
            }));
            setCurrentPage(1);
        },
        [setFilter]
    );

    const onClear = useCallback(() => {
        setFilter(defaultFilter);
    }, [defaultFilter, setFilter]);

    return (
        <>
            <DatasListWrapper
                dataComponent={{
                    datas: lands,
                    itemsPerPage: LANDS_NUMBER_PER_PAGE,
                    loading,
                    filter: {
                        schema,
                        onClear,
                        updateFilter
                    }
                }}
                datasComponentPaginate={{
                    currentPage: pageCursor?.currentPage,
                    numberOfPage: pageCursor?.lastPage,
                    onChangePage: (page) => setCurrentPage(page)
                }}
                datasComponentsSearch={{
                    filterkey: 'search',
                    updateSearch: updateFilter
                }}
                // datasComponentContent={{
                //     contentType: 'gridComponent',
                //     component: (data: Land, loadingLands) => <CardLand land={data} loading={loadingLands} />,
                //     onClick: (land: Land) => getUserAllowedAccess(contactsRoutesManager.land.id, 'edit') && openLand(land.id)
                // }}
                datasComponentContent={{
                    component: (data: Land, loadingLands) => <CardLand land={data} loading={loadingLands} />,
                    spacing: 15,
                    onClick: (land: Land) => getUserAllowedAccess(contactsRoutesManager.land.id, 'edit') && openLand(land.id)
                }}
                datasHeaderComponent={<LandsMap open={mapOpen} filter={filter} sx={{ paddingBottom: mapOpen ? '30px' : 0 }} />}
                actions={[
                    {
                        icon: MapIcon,
                        label: 'Carte',
                        onClick: () => setMapOpen(!mapOpen)
                    }
                ]}
            />
        </>
    );
};

export default Lands;
