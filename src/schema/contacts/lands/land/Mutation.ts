import gql from 'graphql-tag';
import { FRAGMENT_LAND } from './Fragment';

// eslint-disable-next-line import/prefer-default-export
export const MUTATION_LAND_ARCHIVED = gql`
    ${FRAGMENT_LAND}
    mutation Mutation($id: Int!, $archived: Boolean!) {
        landArchived(id: $id, archived: $archived) {
            ...Land
        }
    }
`;
