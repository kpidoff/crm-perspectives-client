import gql from 'graphql-tag';
import { FRAGMENT_USER } from 'schema/user/Fragment';

export const FRAGMENT_LAND_IMAGE = gql`
    fragment LandImage on LandImage {
        id
        fileManager {
            encryptedName
        }
        default
        landId
        land {
            societyId
        }
    }
`;

export const FRAGMENT_LAND_SUBDIVISION = gql`
    fragment LandSubdivision on LandSubdivision {
        id
        title
        price
        area
        archived
    }
`;

export const FRAGMENT_LAND = gql`
    ${FRAGMENT_LAND_IMAGE}
    ${FRAGMENT_USER}
    ${FRAGMENT_LAND_SUBDIVISION}
    fragment Land on Land {
        id
        createdDate
        address
        postalCode
        city
        description
        price
        area
        serviced
        sewer
        bounding
        type
        archived
        rate
        title
        subdivisionDescription
        lng
        lat
        fieldServiceProviderId
        societyId
        parentId
        defaultImage {
            ...LandImage
        }
        subdivisions {
            ...LandSubdivision
        }
        commercialId
        commercial {
            ...User
        }
    }
`;
