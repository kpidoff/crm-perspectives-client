import gql from 'graphql-tag';
import { FRAGMENT_PAGINATE } from 'schema/utils/Fragment';
import { FRAGMENT_LAND, FRAGMENT_LAND_IMAGE } from './Fragment';

export const QUERY_LANDS = gql`
    ${FRAGMENT_PAGINATE}
    ${FRAGMENT_LAND_IMAGE}
    query Land($paginate: Paginate, $sort: Sort, $filter: LandFilterInput) {
        lands(paginate: $paginate, sort: $sort, filter: $filter) {
            data {
                id
                createdDate
                city
                description
                price
                area
                serviced
                sewer
                bounding
                rate
                title
                societyId
                defaultImage {
                    ...LandImage
                }
            }
            pageCursor {
                ...Paginate
            }
        }
    }
`;

export const QUERY_LANDS_LOCATION = gql`
    query Land($sort: Sort, $filter: LandFilterInput) {
        lands(sort: $sort, filter: $filter) {
            data {
                title
                description
                id
                parentId
                price
                area
                address
                postalCode
                city
                lng
                lat
                type
            }
        }
    }
`;

export const QUERY_LAND_BY_ID = gql`
    ${FRAGMENT_LAND}
    query LandById($id: Int!, $societyId: Int!) {
        landById(id: $id, societyId: $societyId) {
            ...Land
        }
    }
`;

export const QUERY_LAND_IMAGES = gql`
    ${FRAGMENT_LAND_IMAGE}
    query LandImages($landId: Int!, $societyId: Int!) {
        landImages(landId: $landId, societyId: $societyId) {
            ...LandImage
        }
    }
`;
