import gql from 'graphql-tag';
import { FRAGMENT_PAGINATE } from 'schema/utils/Fragment';
import { FRAGMENT_FIELD_SERVICE_PROVIDER } from './Fragment';

export const QUERY_FIELD_SERVICES_PROVIDER = gql`
    ${FRAGMENT_PAGINATE}
    ${FRAGMENT_FIELD_SERVICE_PROVIDER}
    query FieldServicesProvider($paginate: Paginate, $sort: Sort, $filter: FieldServiceProviderFilterInput) {
        fieldServicesProvider(paginate: $paginate, sort: $sort, filter: $filter) {
            data {
                ...FieldServiceProvider
            }
            pageCursor {
                ...Paginate
            }
        }
    }
`;

export const QUERY_FIELD_SERVICE_PROVIDER_BY_LABEL = gql`
    ${FRAGMENT_FIELD_SERVICE_PROVIDER}
    query FieldServiceProviderByLabel($label: String!, $societyId: Int!) {
        fieldServiceProviderByLabel(label: $label, societyId: $societyId) {
            ...FieldServiceProvider
        }
    }
`;
