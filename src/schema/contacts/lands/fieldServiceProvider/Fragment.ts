import gql from 'graphql-tag';

// eslint-disable-next-line import/prefer-default-export
export const FRAGMENT_FIELD_SERVICE_PROVIDER = gql`
    fragment FieldServiceProvider on FieldServiceProvider {
        id
        label
        lastname
        firstname
        email
        address
        postalCode
        city
        phone
        portable
        societyId
    }
`;
