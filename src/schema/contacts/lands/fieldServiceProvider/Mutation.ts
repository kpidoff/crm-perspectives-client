import gql from 'graphql-tag';
import { FRAGMENT_FIELD_SERVICE_PROVIDER } from './Fragment';

export const MUTATION_CREATE_FIELD_SERVICE_PROVIDER = gql`
    ${FRAGMENT_FIELD_SERVICE_PROVIDER}
    mutation CreateFieldServiceProvider($fieldServiceProvider: FieldServiceProviderInput!, $societyId: Int!) {
        createFieldServiceProvider(fieldServiceProvider: $fieldServiceProvider, societyId: $societyId) {
            ...FieldServiceProvider
        }
    }
`;

export const MUTATION_DELETE_FIELD_SERVICE_PROVIDER = gql`
    mutation DeleteFieldServiceProvider($id: Int!, $societyId: Int!) {
        deleteFieldServiceProvider(id: $id, societyId: $societyId) {
            message
        }
    }
`;

export const MUTATION_EDIT_FIELD_SERVICE_PROVIDER = gql`
    ${FRAGMENT_FIELD_SERVICE_PROVIDER}
    mutation EditFieldServiceProvider($id: Int!, $fieldServiceProvider: FieldServiceProviderInput!, $societyId: Int!) {
        editFieldServiceProvider(id: $id, fieldServiceProvider: $fieldServiceProvider, societyId: $societyId) {
            ...FieldServiceProvider
        }
    }
`;
