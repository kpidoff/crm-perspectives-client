import gql from 'graphql-tag';

export const QUERY_INTERFACE_PARENT_CHILDREN_COUNT = gql`
    query InterfacesParent {
        interfacesParent {
            id
            key
            parentId
            flags {
                id
                flag
            }
            _count {
                children
            }
        }
    }
`;

export const QUERY_INTERFACE_CHILDREN_COUNT_BY_PARENT = gql`
    query InterfacesByParent($parentId: Int!) {
        interfacesByParent(parentId: $parentId) {
            id
            key
            parentId
            flags {
                id
                flag
            }
            _count {
                children
            }
        }
    }
`;
