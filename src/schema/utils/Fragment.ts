import gql from 'graphql-tag';

// eslint-disable-next-line import/prefer-default-export
export const FRAGMENT_PAGINATE = gql`
    fragment Paginate on PageCursor {
        total
        lastPage
        currentPage
        next
        prev
        perPage
    }
`;
