import gql from 'graphql-tag';

// eslint-disable-next-line import/prefer-default-export
export const FRAGMENT_FILE_MANAGER = gql`
    fragment FileManager on FileManager {
        id
        name
        type
        size
        openedDate
        createdAt
        updatedAt
        editedUserId
        ownerId
        societyId
        parentId
        encryptedName
        _count {
            children
        }
    }
`;
