import gql from 'graphql-tag';
import { FRAGMENT_FILE_MANAGER } from './Fragment';

// eslint-disable-next-line import/prefer-default-export
export const SUBSCRIPTION_END_UPLOAD_FILE = gql`
    ${FRAGMENT_FILE_MANAGER}
    subscription Subscription {
        uploadFiles {
            ...FileManager
        }
    }
`;
