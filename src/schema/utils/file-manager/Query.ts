import gql from 'graphql-tag';
import { FRAGMENT_FILE_MANAGER } from 'schema/utils/file-manager/Fragment';

export const QUERY_FILE_MANAGER_BY_ID = gql`
    ${FRAGMENT_FILE_MANAGER}
    query FileManagerById($societyId: Int!, $id: Int!) {
        fileManagerById(societyId: $societyId, id: $id) {
            ...FileManager
        }
    }
`;

export const QUERY_FILES_MANAGER_BY_PARENT_ID = gql`
    ${FRAGMENT_FILE_MANAGER}
    query FilesManagerByParentId($societyId: Int!, $id: Int!) {
        filesManagerByParentId(societyId: $societyId, id: $id) {
            ...FileManager
        }
    }
`;

export const QUERY_DOWLOAD_FILES = gql`
    query FilesManagerDowload($ids: [Int!]!, $societyId: Int!) {
        filesManagerDowload(ids: $ids, societyId: $societyId) {
            id
            name
            type
            size
            base64
        }
    }
`;
