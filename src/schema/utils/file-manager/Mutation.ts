import gql from 'graphql-tag';
import { FRAGMENT_FILE_MANAGER } from './Fragment';

export const MUTATION_ADD_FOLDER = gql`
    ${FRAGMENT_FILE_MANAGER}
    mutation Mutation($name: String!, $societyId: Int!, $parentId: Int) {
        fileManagerCreateFolder(name: $name, societyId: $societyId, parentId: $parentId) {
            ...FileManager
        }
    }
`;

export const MUTATION_UPLOAD_FILES = gql`
    mutation FilesManagerUpload($files: [ImageUpload!]!, $societyId: Int!, $parentId: Int) {
        filesManagerUpload(files: $files, societyId: $societyId, parentId: $parentId) {
            message
        }
    }
`;

export const MUTATION_DELETE_FILES = gql`
    mutation FilesManagerDelete($ids: [Int!]!, $societyId: Int!) {
        filesManagerDelete(ids: $ids, societyId: $societyId) {
            count
        }
    }
`;

export const MUTATION_MOVE_FILES = gql`
    mutation FilesManagerMove($ids: [Int!]!, $parentId: Int!, $societyId: Int!) {
        filesManagerMove(ids: $ids, parentId: $parentId, societyId: $societyId) {
            count
        }
    }
`;

export const MUTATION_RENAME_FILES = gql`
    ${FRAGMENT_FILE_MANAGER}
    mutation FileManagerRename($id: Int!, $name: String!, $societyId: Int!) {
        fileManagerRename(id: $id, name: $name, societyId: $societyId) {
            ...FileManager
        }
    }
`;
