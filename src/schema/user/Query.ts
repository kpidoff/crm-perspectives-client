import gql from 'graphql-tag';
import { FRAGMENT_PAGINATE } from 'schema/utils/Fragment';
import { FRAGMENT_USER, FRAGMENT_USER_LIST, FRAGMENT_USER_NOTIFICATION } from './Fragment';

export const USER_BY_ID = gql`
    ${FRAGMENT_USER}
    query UserById($userId: Int!) {
        userById(userId: $userId) {
            ...User
        }
    }
`;

export const USER_BY_EMAIL = gql`
    query UserByEmail($email: String!) {
        userByEmail(email: $email) {
            id
            email
            administrator
            civility
            firstname
            lastname
            status
            roleId
        }
    }
`;

export const QUERY_USERS = gql`
    ${FRAGMENT_PAGINATE}
    ${FRAGMENT_USER_LIST}
    query Users($paginate: Paginate, $sort: Sort, $filter: UserFilterInput) {
        users(paginate: $paginate, sort: $sort, filter: $filter) {
            data {
                ...UserList
            }
            pageCursor {
                ...Paginate
            }
        }
    }
`;

export const QUERY_USERS_COMMERCIAL = gql`
    ${FRAGMENT_PAGINATE}
    ${FRAGMENT_USER_LIST}
    query UsersCommercial($paginate: Paginate, $societyId: Int!) {
        usersCommercial(paginate: $paginate, societyId: $societyId) {
            data {
                ...UserList
            }
            pageCursor {
                ...Paginate
            }
        }
    }
`;

export const QUERY_USER_INTERFACE_ACCESS = gql`
    query UserInterfaceAccess($userId: Int!) {
        userInterfacesAccess(userId: $userId) {
            id
            flagId
        }
    }
`;

export const QUERY_USER_DEVICES = gql`
    ${FRAGMENT_PAGINATE}
    query userDevices($userId: Int!, $paginate: Paginate!) {
        userDevices(userId: $userId, paginate: $paginate) {
            data {
                id
                browserName
                osName
                osVersion
                mobileModel
                lat
                deviceType
                lng
                IPv4
                countryCode
                actived
                createdAt
            }
            pageCursor {
                ...Paginate
            }
        }
    }
`;

export const QUERY_USER_DISCONNECT_EVERYWHERE = gql`
    query Query($userId: Int) {
        disconnectedEverywhere(userId: $userId) {
            message
        }
    }
`;

export const QUERY_USER_NOTIFICATIONS = gql`
    ${FRAGMENT_USER_NOTIFICATION}
    ${FRAGMENT_PAGINATE}
    query Notifications($filter: UserNotificationFilterInput, $paginate: Paginate) {
        notifications(filter: $filter, paginate: $paginate) {
            data {
                ...UserNotification
            }
            pageCursor {
                ...Paginate
            }
        }
    }
`;
