import gql from 'graphql-tag';
import { FRAGMENT_SOCIETY } from 'schema/settings/society/Fragment';

export const FRAGMENT_USER = gql`
    ${FRAGMENT_SOCIETY}
    fragment User on User {
        id
        lastname
        email
        avatar
        status
        administrator
        emailAuthentication
        civility
        roleId
        firstname
        phone
        address
        postalCode
        city
        description
        arrivalDate
        dateOfBirth
        endDate
        role {
            id
            name
            type
        }
        societiesAccess {
            id
            societyId
            name
            administrator
            default
            society {
                ...Society
            }
        }
        userInterfacesAccess {
            id
            flagId
        }
        allowedUserInterfaces {
            interface {
                key
            }
            flags {
                flag
            }
        }
    }
`;

export const FRAGMENT_USER_LIST = gql`
    fragment UserList on User {
        id
        lastname
        email
        avatar
        status
        administrator
        civility
        firstname
        phone
        address
        postalCode
        city
        role {
            name
        }
        societiesAccess {
            societyId
            name
        }
    }
`;

export const FRAGMENT_USER_NOTIFICATION = gql`
    fragment UserNotification on UserNotification {
        userId
        id
        type
        subtype
        title
        subTitle
        content
        active
        createdAt
        params
    }
`;
