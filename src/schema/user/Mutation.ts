import gql from 'graphql-tag';
import { FRAGMENT_USER, FRAGMENT_USER_NOTIFICATION } from './Fragment';

export const EDIT_USER = gql`
    ${FRAGMENT_USER}
    mutation EditUser($id: Int!, $user: UserEditInput!) {
        editUser(userId: $id, user: $user) {
            message
            user {
                ...User
            }
        }
    }
`;

export const CREATE_USER = gql`
    ${FRAGMENT_USER}
    mutation Mutation($user: UserCreateInput) {
        createUser(user: $user) {
            message
            user {
                ...User
            }
        }
    }
`;

export const CHANGE_PASSWORD_USER = gql`
    mutation Mutation($password: String!) {
        changePassword(password: $password) {
            message
            id
        }
    }
`;

export const QUERY_USER_DELETE_NOTIFICATIONS = gql`
    ${FRAGMENT_USER_NOTIFICATION}
    mutation Mutation($notificationId: Int!) {
        deleteNotification(notificationId: $notificationId) {
            ...UserNotification
        }
    }
`;
