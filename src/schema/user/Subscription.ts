import gql from 'graphql-tag';
import { FRAGMENT_USER_NOTIFICATION } from './Fragment';

export const SUBSCRIPTION_USER_ADD_NOTIFICATION = gql`
    ${FRAGMENT_USER_NOTIFICATION}
    subscription Subscription {
        notification {
            ...UserNotification
        }
    }
`;

export const SUBSCRIPTION_USER_DELETE_NOTIFICATION = gql`
    ${FRAGMENT_USER_NOTIFICATION}
    subscription Subscription {
        deleteNotification {
            ...UserNotification
        }
    }
`;
