import gql from 'graphql-tag';

// eslint-disable-next-line import/prefer-default-export
export const MUTATION_FORGOT_PASSWORD = gql`
    mutation ForgotPassword($email: String!) {
        forgotPassword(email: $email) {
            message
        }
    }
`;

export const MUTATION_RESET_PASSWORD = gql`
    mutation ResetPassword($password: String!, $token: String!) {
        resetPassword(password: $password, token: $token) {
            message
            id
        }
    }
`;
