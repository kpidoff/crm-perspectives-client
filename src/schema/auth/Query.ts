import gql from 'graphql-tag';
import { FRAGMENT_USER, FRAGMENT_USER_NOTIFICATION } from 'schema/user/Fragment';

export const QUERY_LOGIN = gql`
    ${FRAGMENT_USER}
    ${FRAGMENT_USER_NOTIFICATION}
    query Login($email: String!, $password: String!, $device: InputUserDevice!) {
        login(email: $email, password: $password, device: $device) {
            token
            type
            user {
                ...User
                notifications {
                    ...UserNotification
                }
            }
        }
    }
`;

export const QUERY_LOGOUT = gql`
    query Logout {
        logout {
            message
        }
    }
`;

export const QUERY_LOGIN_WITH_CODE = gql`
    ${FRAGMENT_USER}
    ${FRAGMENT_USER_NOTIFICATION}
    query LoginWithCode($userId: Int!, $code: String!, $device: InputUserDevice!) {
        loginAuthentication(userId: $userId, code: $code, device: $device) {
            type
            token
            user {
                ...User
                notifications {
                    ...UserNotification
                }
            }
        }
    }
`;

export const QUERY_RESEND_CODE = gql`
    query ResendCodeAuthentication($email: String!) {
        resendCodeAuthentication(email: $email) {
            message
        }
    }
`;

export const QUERY_CURRENT_USER = gql`
    ${FRAGMENT_USER}
    ${FRAGMENT_USER_NOTIFICATION}
    query CurrentUser {
        currentUser {
            ...User
            notifications {
                ...UserNotification
            }
        }
    }
`;
