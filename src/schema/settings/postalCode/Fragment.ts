import gql from 'graphql-tag';

// eslint-disable-next-line import/prefer-default-export
export const FRAGMENT_POSTAL_CODE = gql`
    fragment PostalCode on PostalCode {
        id
        city
        postal_code
        departement
        insee
        lat
        lng
    }
`;
