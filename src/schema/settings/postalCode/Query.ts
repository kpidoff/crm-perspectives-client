import gql from 'graphql-tag';
import { FRAGMENT_POSTAL_CODE } from './Fragment';

// eslint-disable-next-line import/prefer-default-export
export const QUERY_POSTAL_CODE_BY_CODE = gql`
    ${FRAGMENT_POSTAL_CODE}
    query PostalCodesByCode($code: String!) {
        postalCodesByCode(code: $code) {
            ...PostalCode
        }
    }
`;

export const QUERY_POSTAL_CODE_BY_CITY = gql`
    ${FRAGMENT_POSTAL_CODE}
    query PostalCodesByCity($city: String!) {
        postalCodesByCity(city: $city) {
            ...PostalCode
        }
    }
`;
