import gql from 'graphql-tag';

// eslint-disable-next-line import/prefer-default-export
export const FRAGMENT_ROLE = gql`
    fragment Role on Role {
        id
        name
        type
    }
`;
