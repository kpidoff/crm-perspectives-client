import gql from 'graphql-tag';
import { FRAGMENT_ROLE } from './Fragment';

// eslint-disable-next-line import/prefer-default-export
export const MUTATION_EDIT_ROLE = gql`
    ${FRAGMENT_ROLE}
    mutation Mutation($id: Int!, $role: RoleInput!) {
        editRole(id: $id, role: $role) {
            ...Role
        }
    }
`;

export const MUTATION_CREATE_ROLE = gql`
    ${FRAGMENT_ROLE}
    mutation Mutation($role: RoleInput!) {
        createRole(role: $role) {
            ...Role
        }
    }
`;

export const MUTATION_DELETE_ROLE = gql`
    mutation Mutation($id: Int!) {
        deleteRole(id: $id) {
            message
        }
    }
`;
