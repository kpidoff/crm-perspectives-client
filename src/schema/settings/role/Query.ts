import gql from 'graphql-tag';
import { FRAGMENT_PAGINATE } from 'schema/utils/Fragment';
import { FRAGMENT_ROLE } from './Fragment';

// eslint-disable-next-line import/prefer-default-export
export const QUERY_ROLES = gql`
    ${FRAGMENT_PAGINATE}
    ${FRAGMENT_ROLE}
    query Roles($filter: RoleFilterInput, $sort: Sort, $paginate: Paginate) {
        roles(filter: $filter, sort: $sort, paginate: $paginate) {
            data {
                ...Role
            }
            pageCursor {
                ...Paginate
            }
        }
    }
`;

export const QUERY_ROLE_BY_NAME = gql`
    ${FRAGMENT_ROLE}
    query RoleByName($name: String!) {
        roleByName(name: $name) {
            ...Role
        }
    }
`;
