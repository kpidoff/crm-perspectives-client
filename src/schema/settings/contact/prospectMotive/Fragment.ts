import gql from 'graphql-tag';

// eslint-disable-next-line import/prefer-default-export
export const FRAGMENT_PROSPECT_MOTIVE = gql`
    fragment ProspectMotive on ProspectMotive {
        id
        label
    }
`;
