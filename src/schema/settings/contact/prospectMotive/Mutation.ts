import gql from 'graphql-tag';
import { FRAGMENT_PROSPECT_MOTIVE } from './Fragment';

export const MUTATION_EDIT_PROSPECT_MOTIVE = gql`
    ${FRAGMENT_PROSPECT_MOTIVE}
    mutation EditProspectMotive($id: Int!, $prospectMotive: ProspectMotiveInput!) {
        editProspectMotive(id: $id, prospectMotive: $prospectMotive) {
            ...ProspectMotive
        }
    }
`;

export const MUTATION_CREATE_PROSPECT_MOTIVE = gql`
    ${FRAGMENT_PROSPECT_MOTIVE}
    mutation CreateProspectMotive($prospectMotive: ProspectMotiveInput!) {
        createProspectMotive(prospectMotive: $prospectMotive) {
            ...ProspectMotive
        }
    }
`;

export const MUTATION_DELETE_PROSPECT_MOTIVE = gql`
    mutation DeleteProspectMotive($id: Int!) {
        deleteProspectMotive(id: $id) {
            message
        }
    }
`;
