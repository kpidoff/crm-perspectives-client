import gql from 'graphql-tag';
import { FRAGMENT_PAGINATE } from 'schema/utils/Fragment';
import { FRAGMENT_PROSPECT_MOTIVE } from './Fragment';

export const QUERY_PROSPECT_MOTIVES = gql`
    ${FRAGMENT_PAGINATE}
    ${FRAGMENT_PROSPECT_MOTIVE}
    query ProspectMotives($paginate: Paginate, $sort: Sort, $filter: ProspectMotiveFilterInput) {
        prospectMotives(paginate: $paginate, sort: $sort, filter: $filter) {
            data {
                ...ProspectMotive
            }
            pageCursor {
                ...Paginate
            }
        }
    }
`;

export const QUERY_PROSPECT_MOTIVE_BY_LABEL = gql`
    ${FRAGMENT_PROSPECT_MOTIVE}
    query ProspectMotiveByLabel($label: String!) {
        prospectMotiveByLabel(label: $label) {
            ...ProspectMotive
        }
    }
`;
