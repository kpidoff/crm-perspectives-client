import gql from 'graphql-tag';
import { FRAGMENT_RECOVERY_MODE } from './Fragment';

export const MUTATION_EDIT_RECOVERY_MODE = gql`
    ${FRAGMENT_RECOVERY_MODE}
    mutation EditRecoveryMode($id: Int!, $recoveryMode: RecoveryModeInput!) {
        editRecoveryMode(id: $id, recoveryMode: $recoveryMode) {
            ...RecoveryMode
        }
    }
`;

export const MUTATION_CREATE_RECOVERY_MODE = gql`
    ${FRAGMENT_RECOVERY_MODE}
    mutation CreateRecoveryMode($recoveryMode: RecoveryModeInput!) {
        createRecoveryMode(recoveryMode: $recoveryMode) {
            ...RecoveryMode
        }
    }
`;

export const MUTATION_DELETE_RECOVERY_MODE = gql`
    mutation DeleteRecoveryMode($id: Int!) {
        deleteRecoveryMode(id: $id) {
            message
        }
    }
`;
