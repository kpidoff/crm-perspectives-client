import gql from 'graphql-tag';

// eslint-disable-next-line import/prefer-default-export
export const FRAGMENT_RECOVERY_MODE = gql`
    fragment RecoveryMode on RecoveryMode {
        id
        label
    }
`;
