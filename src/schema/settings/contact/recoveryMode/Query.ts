import gql from 'graphql-tag';
import { FRAGMENT_PAGINATE } from 'schema/utils/Fragment';
import { FRAGMENT_RECOVERY_MODE } from './Fragment';

export const QUERY_RECOVERY_MODES = gql`
    ${FRAGMENT_PAGINATE}
    ${FRAGMENT_RECOVERY_MODE}
    query RecoveryModes($paginate: Paginate, $sort: Sort, $filter: RecoveryModeFilterInput) {
        recoveryModes(paginate: $paginate, sort: $sort, filter: $filter) {
            data {
                ...RecoveryMode
            }
            pageCursor {
                ...Paginate
            }
        }
    }
`;

export const QUERY_RECOVERY_MODE_BY_LABEL = gql`
    ${FRAGMENT_RECOVERY_MODE}
    query RecoveryModeByLabel($label: String!) {
        recoveryModeByLabel(label: $label) {
            ...RecoveryMode
        }
    }
`;
