import gql from 'graphql-tag';
import { FRAGMENT_PAGINATE } from 'schema/utils/Fragment';
import { FRAGMENT_DESTINATION_PROJECT } from './Fragment';

export const QUERY_DESTINATIONS_PROJECT = gql`
    ${FRAGMENT_PAGINATE}
    ${FRAGMENT_DESTINATION_PROJECT}
    query DestinationsProject($paginate: Paginate, $sort: Sort, $filter: DestinationProjectFilterInput) {
        destinationsProject(paginate: $paginate, sort: $sort, filter: $filter) {
            data {
                ...DestinationProject
            }
            pageCursor {
                ...Paginate
            }
        }
    }
`;

export const QUERY_DESTINATION_PROJECT_BY_LABEL = gql`
    ${FRAGMENT_DESTINATION_PROJECT}
    query DestinationProjectByLabel($label: String!) {
        destinationProjectByLabel(label: $label) {
            ...DestinationProject
        }
    }
`;
