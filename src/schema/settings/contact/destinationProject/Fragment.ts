import gql from 'graphql-tag';

// eslint-disable-next-line import/prefer-default-export
export const FRAGMENT_DESTINATION_PROJECT = gql`
    fragment DestinationProject on DestinationProject {
        id
        label
    }
`;
