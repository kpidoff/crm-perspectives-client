import gql from 'graphql-tag';
import { FRAGMENT_DESTINATION_PROJECT } from './Fragment';

export const MUTATION_EDIT_DESTINATION_PROJECT = gql`
    ${FRAGMENT_DESTINATION_PROJECT}
    mutation EditDestinationProject($id: Int!, $destinationProject: DestinationProjectInput!) {
        editDestinationProject(id: $id, destinationProject: $destinationProject) {
            ...DestinationProject
        }
    }
`;

export const MUTATION_CREATE_DESTINATION_PROJECT = gql`
    ${FRAGMENT_DESTINATION_PROJECT}
    mutation CreateDestinationProject($destinationProject: DestinationProjectInput!) {
        createDestinationProject(destinationProject: $destinationProject) {
            ...DestinationProject
        }
    }
`;

export const MUTATION_DELETE_DESTINATION_PROJECT = gql`
    mutation DeleteDestinationProject($id: Int!) {
        deleteDestinationProject(id: $id) {
            message
        }
    }
`;
