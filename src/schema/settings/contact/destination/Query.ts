import gql from 'graphql-tag';
import { FRAGMENT_PAGINATE } from 'schema/utils/Fragment';
import { FRAGMENT_DESTINATION } from './Fragment';

export const QUERY_DESTINATIONS = gql`
    ${FRAGMENT_PAGINATE}
    ${FRAGMENT_DESTINATION}
    query Destinations($paginate: Paginate, $sort: Sort, $filter: DestinationFilterInput) {
        destinations(paginate: $paginate, sort: $sort, filter: $filter) {
            data {
                ...Destination
            }
            pageCursor {
                ...Paginate
            }
        }
    }
`;

export const QUERY_DESTINATION_BY_LABEL = gql`
    ${FRAGMENT_DESTINATION}
    query DestinationByLabel($label: String!) {
        destinationByLabel(label: $label) {
            ...Destination
        }
    }
`;
