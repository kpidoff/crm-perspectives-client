import gql from 'graphql-tag';
import { FRAGMENT_DESTINATION } from './Fragment';

export const MUTATION_EDIT_DESTINATION = gql`
    ${FRAGMENT_DESTINATION}
    mutation EditDestination($id: Int!, $destination: DestinationInput!) {
        editDestination(id: $id, destination: $destination) {
            ...Destination
        }
    }
`;

export const MUTATION_CREATE_DESTINATION = gql`
    ${FRAGMENT_DESTINATION}
    mutation CreateDestination($destination: DestinationInput!) {
        createDestination(destination: $destination) {
            ...Destination
        }
    }
`;

export const MUTATION_DELETE_DESTINATION = gql`
    mutation DeleteDestination($id: Int!) {
        deleteDestination(id: $id) {
            message
        }
    }
`;
