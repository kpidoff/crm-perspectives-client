import gql from 'graphql-tag';

// eslint-disable-next-line import/prefer-default-export
export const FRAGMENT_DESTINATION = gql`
    fragment Destination on Destination {
        id
        label
        type
    }
`;
