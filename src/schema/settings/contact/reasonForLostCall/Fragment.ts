import gql from 'graphql-tag';

// eslint-disable-next-line import/prefer-default-export
export const FRAGMENT_REASON_FOR_LOST_CALL = gql`
    fragment ReasonForLostCall on ReasonForLostCall {
        id
        label
    }
`;
