import gql from 'graphql-tag';
import { FRAGMENT_PAGINATE } from 'schema/utils/Fragment';
import { FRAGMENT_REASON_FOR_LOST_CALL } from './Fragment';

export const QUERY_REASONS_FOR_LOST_CALL = gql`
    ${FRAGMENT_PAGINATE}
    ${FRAGMENT_REASON_FOR_LOST_CALL}
    query ReasonsForLostCall($paginate: Paginate, $sort: Sort, $filter: ReasonForLostCallFilterInput) {
        reasonsForLostCall(paginate: $paginate, sort: $sort, filter: $filter) {
            data {
                ...ReasonForLostCall
            }
            pageCursor {
                ...Paginate
            }
        }
    }
`;

export const QUERY_REASON_FOR_LOST_CALL_BY_LABEL = gql`
    ${FRAGMENT_REASON_FOR_LOST_CALL}
    query ReasonForLostCallByLabel($label: String!) {
        reasonForLostCallByLabel(label: $label) {
            ...ReasonForLostCall
        }
    }
`;
