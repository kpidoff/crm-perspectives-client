import gql from 'graphql-tag';
import { FRAGMENT_REASON_FOR_LOST_CALL } from './Fragment';

export const MUTATION_EDIT_REASON_FOR_LOST_CALL = gql`
    ${FRAGMENT_REASON_FOR_LOST_CALL}
    mutation EditReasonForLostCall($id: Int!, $reasonForLostCall: ReasonForLostCallInput!) {
        editReasonForLostCall(id: $id, reasonForLostCall: $reasonForLostCall) {
            ...ReasonForLostCall
        }
    }
`;

export const MUTATION_CREATE_REASON_FOR_LOST_CALL = gql`
    ${FRAGMENT_REASON_FOR_LOST_CALL}
    mutation CreateReasonForLostCall($reasonForLostCall: ReasonForLostCallInput!) {
        createReasonForLostCall(reasonForLostCall: $reasonForLostCall) {
            ...ReasonForLostCall
        }
    }
`;

export const MUTATION_DELETE_REASON_FOR_LOST_CALL = gql`
    mutation DeleteReasonForLostCall($id: Int!) {
        deleteReasonForLostCall(id: $id) {
            message
        }
    }
`;
