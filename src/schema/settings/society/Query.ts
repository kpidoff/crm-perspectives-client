import gql from 'graphql-tag';
import { FRAGMENT_PAGINATE } from 'schema/utils/Fragment';
import { FRAGMENT_SOCIETY, FRAGMENT_SOCIETY_LIST } from './Fragment';

export const QUERY_SOCIETIES = gql`
    ${FRAGMENT_PAGINATE}
    ${FRAGMENT_SOCIETY_LIST}
    query Societies($paginate: Paginate, $sort: Sort, $filter: SocietyFilterInput) {
        societies(paginate: $paginate, sort: $sort, filter: $filter) {
            data {
                ...SocietyList
            }
            pageCursor {
                ...Paginate
            }
        }
    }
`;

export const QUERY_SOCIETY_BY_ID = gql`
    ${FRAGMENT_SOCIETY}
    query SocietyById($societyId: Int!) {
        societyById(societyId: $societyId) {
            ...Society
        }
    }
`;

export const QUERY_SOCIETY_BY_NAME = gql`
    ${FRAGMENT_SOCIETY}
    query SocietyByName($name: String!) {
        societyByName(name: $name) {
            ...Society
        }
    }
`;
