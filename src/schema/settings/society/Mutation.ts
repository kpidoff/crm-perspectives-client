import gql from 'graphql-tag';
import { FRAGMENT_SOCIETY } from './Fragment';

export const MUTATION_EDIT_SOCIETY = gql`
    ${FRAGMENT_SOCIETY}
    mutation EditSociety($society: SocietyEditInput, $societyId: Int!) {
        editSociety(society: $society, societyId: $societyId) {
            message
            society {
                ...Society
            }
        }
    }
`;

export const MUTATION_ADD_SOCIETY = gql`
    ${FRAGMENT_SOCIETY}
    mutation CreateSociety($society: SocietyCreateInput) {
        createSociety(society: $society) {
            message
            society {
                ...Society
            }
        }
    }
`;
