import gql from 'graphql-tag';

export const FRAGMENT_SOCIETY = gql`
    fragment Society on Society {
        id
        name
        activated
        interlocutor
        address
        postalCode
        siret
        ape
        phone
        email
        comment
        city
        smtpActivated
        smtpServeur
        smtpPort
        smtpTitle
        smtpMail
        smtpPassword
        lat
        lng
    }
`;

export const FRAGMENT_SOCIETY_LIST = gql`
    fragment SocietyList on Society {
        id
        name
        activated
        interlocutor
        address
        postalCode
        phone
        email
        city
    }
`;
