import { useLazyQuery, useMutation } from '@apollo/client';
import { capitalize } from 'lodash';
import { useCallback, useEffect } from 'react';
import { useIntl } from 'react-intl';
import {
    MUTATION_CREATE_DESTINATION,
    MUTATION_DELETE_DESTINATION,
    MUTATION_EDIT_DESTINATION
} from 'schema/settings/contact/destination/Mutation';
import { QUERY_DESTINATIONS, QUERY_DESTINATION_BY_LABEL } from 'schema/settings/contact/destination/Query';
import { ListParams, PageCursor } from 'types/graphql';
import { Destination } from 'types/settings/contact/destination';
import { toastError, toastSuccess } from 'utils/toast';

export type DestinationsResponse = {
    destinations: {
        data: Destination[];
        pageCursor: PageCursor;
    };
};

export type DestinationsFilterInput = {
    search?: string;
};

export function useDestinations() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useLazyQuery<DestinationsResponse>(QUERY_DESTINATIONS);

    const getDestinations = useCallback(
        (params: ListParams<DestinationsFilterInput> | undefined = undefined) =>
            request({
                variables: {
                    ...params
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'destinations-fetch-error' }));
    }, [error, intl]);

    return {
        destinations: data?.destinations.data,
        pageCursor: data?.destinations.pageCursor,
        loading,
        called,
        error,
        getDestinations
    };
}

export type DestinationByLabelResponse = {
    destinationByLabel: Destination;
};

export function useDestinationByLabel() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useLazyQuery<DestinationByLabelResponse>(QUERY_DESTINATION_BY_LABEL, {
        fetchPolicy: 'network-only'
    });

    const getDestinationByLabel = useCallback(
        (label: string) =>
            request({
                variables: {
                    label
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'destinations-fetch-error' }));
    }, [error, intl]);

    return {
        destination: data?.destinationByLabel,
        loading,
        called,
        error,
        getDestinationByLabel
    };
}

function formatDestination(destination: Partial<Pick<Destination, 'label'>>) {
    return {
        ...destination,
        label: destination.label ? capitalize(destination.label) : undefined
    };
}

export type EditDestinationResponse = {
    editDestination: Destination;
};

export function useEditDestination() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useMutation<EditDestinationResponse>(MUTATION_EDIT_DESTINATION);

    const editDestination = useCallback(
        ({ id, ...values }: Partial<Destination>) =>
            request({
                variables: {
                    id,
                    destination: formatDestination(values)
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'destination-edit-error' }));
    }, [error, intl]);

    useEffect(() => {
        data && toastSuccess(intl.formatMessage({ id: 'destination-edit-success' }));
    }, [data, intl]);

    return {
        destination: data,
        loading,
        called,
        error,
        editDestination
    };
}

export type CreateDestinationResponse = {
    createDestination: Destination;
};

export function useCreateDestination() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useMutation<CreateDestinationResponse>(MUTATION_CREATE_DESTINATION, {
        refetchQueries: [QUERY_DESTINATIONS]
    });

    const createDestination = useCallback(
        (destination: Partial<Destination>) =>
            request({
                variables: {
                    destination: formatDestination(destination)
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'destination-create-error' }));
    }, [error, intl]);

    useEffect(() => {
        data && toastSuccess(intl.formatMessage({ id: 'destination-create-success' }));
    }, [data, intl]);

    return {
        destination: data,
        loading,
        called,
        error,
        createDestination
    };
}

export type DeleteDestinationResponse = {
    deleteDestination: {
        message: string;
    };
};

export function useDeleteDestination() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useMutation<DeleteDestinationResponse>(MUTATION_DELETE_DESTINATION, {
        refetchQueries: [QUERY_DESTINATIONS]
    });

    const deleteDestination = useCallback(
        (id: number) =>
            request({
                variables: {
                    id
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'destination-delete-error' }));
    }, [error, intl]);

    useEffect(() => {
        data && toastSuccess(intl.formatMessage({ id: 'destination-delete-success' }));
    }, [data, intl]);

    return {
        message: data?.deleteDestination.message,
        loading,
        called,
        error,
        deleteDestination
    };
}
