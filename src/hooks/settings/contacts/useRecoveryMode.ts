import { useLazyQuery, useMutation } from '@apollo/client';
import { capitalize } from 'lodash';
import { useCallback, useEffect } from 'react';
import { useIntl } from 'react-intl';
import {
    MUTATION_CREATE_RECOVERY_MODE,
    MUTATION_DELETE_RECOVERY_MODE,
    MUTATION_EDIT_RECOVERY_MODE
} from 'schema/settings/contact/recoveryMode/Mutation';
import { QUERY_RECOVERY_MODES, QUERY_RECOVERY_MODE_BY_LABEL } from 'schema/settings/contact/recoveryMode/Query';
import { ListParams, PageCursor } from 'types/graphql';
import { RecoveryMode } from 'types/settings/contact/recoveryMode';
import { toastError, toastSuccess } from 'utils/toast';

export type RecoveryModesResponse = {
    recoveryModes: {
        data: RecoveryMode[];
        pageCursor: PageCursor;
    };
};

export type RecoveryModesFilterInput = {
    search?: string;
};

export function useRecoveryModes() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useLazyQuery<RecoveryModesResponse>(QUERY_RECOVERY_MODES);

    const getRecoveryModes = useCallback(
        (params: ListParams<RecoveryModesFilterInput> | undefined = undefined) =>
            request({
                variables: {
                    ...params
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'recoveryModes-fetch-error' }));
    }, [error, intl]);

    return {
        recoveryModes: data?.recoveryModes.data,
        pageCursor: data?.recoveryModes.pageCursor,
        loading,
        called,
        error,
        getRecoveryModes
    };
}

export type RecoveryModeByLabelResponse = {
    recoveryModeByLabel: RecoveryMode;
};

export function useRecoveryModeByLabel() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useLazyQuery<RecoveryModeByLabelResponse>(QUERY_RECOVERY_MODE_BY_LABEL, {
        fetchPolicy: 'network-only'
    });

    const getRecoveryModeByLabel = useCallback(
        (label: string) =>
            request({
                variables: {
                    label
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'recoveryModes-fetch-error' }));
    }, [error, intl]);

    return {
        recoveryMode: data?.recoveryModeByLabel,
        loading,
        called,
        error,
        getRecoveryModeByLabel
    };
}

function formatRecoveryMode(recoveryMode: Partial<Pick<RecoveryMode, 'label'>>) {
    return {
        ...recoveryMode,
        label: recoveryMode.label ? capitalize(recoveryMode.label) : undefined
    };
}

export type EditRecoveryModeResponse = {
    editRecoveryMode: RecoveryMode;
};

export function useEditRecoveryMode() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useMutation<EditRecoveryModeResponse>(MUTATION_EDIT_RECOVERY_MODE);

    const editRecoveryMode = useCallback(
        ({ id, ...values }: Partial<RecoveryMode>) =>
            request({
                variables: {
                    id,
                    recoveryMode: formatRecoveryMode(values)
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'recoveryMode-edit-error' }));
    }, [error, intl]);

    useEffect(() => {
        data && toastSuccess(intl.formatMessage({ id: 'recoveryMode-edit-success' }));
    }, [data, intl]);

    return {
        recoveryMode: data,
        loading,
        called,
        error,
        editRecoveryMode
    };
}

export type CreateRecoveryModeResponse = {
    createRecoveryMode: RecoveryMode;
};

export function useCreateRecoveryMode() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useMutation<CreateRecoveryModeResponse>(MUTATION_CREATE_RECOVERY_MODE, {
        refetchQueries: [QUERY_RECOVERY_MODES]
    });

    const createRecoveryMode = useCallback(
        (recoveryMode: Partial<RecoveryMode>) =>
            request({
                variables: {
                    recoveryMode: formatRecoveryMode(recoveryMode)
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'recoveryMode-create-error' }));
    }, [error, intl]);

    useEffect(() => {
        data && toastSuccess(intl.formatMessage({ id: 'recoveryMode-create-success' }));
    }, [data, intl]);

    return {
        recoveryMode: data,
        loading,
        called,
        error,
        createRecoveryMode
    };
}

export type DeleteRecoveryModeResponse = {
    deleteRecoveryMode: {
        message: string;
    };
};

export function useDeleteRecoveryMode() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useMutation<DeleteRecoveryModeResponse>(MUTATION_DELETE_RECOVERY_MODE, {
        refetchQueries: [QUERY_RECOVERY_MODES]
    });

    const deleteRecoveryMode = useCallback(
        (id: number) =>
            request({
                variables: {
                    id
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'recoveryMode-delete-error' }));
    }, [error, intl]);

    useEffect(() => {
        data && toastSuccess(intl.formatMessage({ id: 'recoveryMode-delete-success' }));
    }, [data, intl]);

    return {
        message: data?.deleteRecoveryMode.message,
        loading,
        called,
        error,
        deleteRecoveryMode
    };
}
