import { useLazyQuery, useMutation } from '@apollo/client';
import { capitalize } from 'lodash';
import { useCallback, useEffect } from 'react';
import { useIntl } from 'react-intl';
import {
    MUTATION_CREATE_DESTINATION_PROJECT,
    MUTATION_DELETE_DESTINATION_PROJECT,
    MUTATION_EDIT_DESTINATION_PROJECT
} from 'schema/settings/contact/destinationProject/Mutation';
import { QUERY_DESTINATIONS_PROJECT, QUERY_DESTINATION_PROJECT_BY_LABEL } from 'schema/settings/contact/destinationProject/Query';
import { ListParams, PageCursor } from 'types/graphql';
import { DestinationProject } from 'types/settings/contact/destinationProject';
import { toastError, toastSuccess } from 'utils/toast';

export type DestinationsProjectResponse = {
    destinationsProject: {
        data: DestinationProject[];
        pageCursor: PageCursor;
    };
};

export type DestinationsProjectFilterInput = {
    search?: string;
};

export function useDestinationsProject() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useLazyQuery<DestinationsProjectResponse>(QUERY_DESTINATIONS_PROJECT);

    const getDestinationsProject = useCallback(
        (params: ListParams<DestinationsProjectFilterInput> | undefined = undefined) =>
            request({
                variables: {
                    ...params
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'destinationsProject-fetch-error' }));
    }, [error, intl]);

    return {
        destinationsProject: data?.destinationsProject.data,
        pageCursor: data?.destinationsProject.pageCursor,
        loading,
        called,
        error,
        getDestinationsProject
    };
}

export type DestinationProjectByLabelResponse = {
    destinationProjectByLabel: DestinationProject;
};

export function useDestinationProjectByLabel() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useLazyQuery<DestinationProjectByLabelResponse>(
        QUERY_DESTINATION_PROJECT_BY_LABEL,
        {
            fetchPolicy: 'network-only'
        }
    );

    const getDestinationProjectByLabel = useCallback(
        (label: string) =>
            request({
                variables: {
                    label
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'destinationsProject-fetch-error' }));
    }, [error, intl]);

    return {
        destinationProject: data?.destinationProjectByLabel,
        loading,
        called,
        error,
        getDestinationProjectByLabel
    };
}

function formatDestinationProject(destinationProject: Partial<Pick<DestinationProject, 'label'>>) {
    return {
        ...destinationProject,
        label: destinationProject.label ? capitalize(destinationProject.label) : undefined
    };
}

export type EditDestinationProjectResponse = {
    editDestinationProject: DestinationProject;
};

export function useEditDestinationProject() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useMutation<EditDestinationProjectResponse>(MUTATION_EDIT_DESTINATION_PROJECT);

    const editDestinationProject = useCallback(
        ({ id, ...values }: Partial<DestinationProject>) =>
            request({
                variables: {
                    id,
                    destinationProject: formatDestinationProject(values)
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'destinationProject-edit-error' }));
    }, [error, intl]);

    useEffect(() => {
        data && toastSuccess(intl.formatMessage({ id: 'destinationProject-edit-success' }));
    }, [data, intl]);

    return {
        destinationProject: data,
        loading,
        called,
        error,
        editDestinationProject
    };
}

export type CreateDestinationProjectResponse = {
    createDestinationProject: DestinationProject;
};

export function useCreateDestinationProject() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useMutation<CreateDestinationProjectResponse>(MUTATION_CREATE_DESTINATION_PROJECT, {
        refetchQueries: [QUERY_DESTINATIONS_PROJECT]
    });

    const createDestinationProject = useCallback(
        (destinationProject: Partial<DestinationProject>) =>
            request({
                variables: {
                    destinationProject: formatDestinationProject(destinationProject)
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'destinationProject-create-error' }));
    }, [error, intl]);

    useEffect(() => {
        data && toastSuccess(intl.formatMessage({ id: 'destinationProject-create-success' }));
    }, [data, intl]);

    return {
        destinationProject: data,
        loading,
        called,
        error,
        createDestinationProject
    };
}

export type DeleteDestinationProjectResponse = {
    deleteDestinationProject: {
        message: string;
    };
};

export function useDeleteDestinationProject() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useMutation<DeleteDestinationProjectResponse>(MUTATION_DELETE_DESTINATION_PROJECT, {
        refetchQueries: [QUERY_DESTINATIONS_PROJECT]
    });

    const deleteDestinationProject = useCallback(
        (id: number) =>
            request({
                variables: {
                    id
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'destinationProject-delete-error' }));
    }, [error, intl]);

    useEffect(() => {
        data && toastSuccess(intl.formatMessage({ id: 'destinationProject-delete-success' }));
    }, [data, intl]);

    return {
        message: data?.deleteDestinationProject.message,
        loading,
        called,
        error,
        deleteDestinationProject
    };
}
