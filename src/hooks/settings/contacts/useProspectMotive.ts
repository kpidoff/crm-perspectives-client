import { useLazyQuery, useMutation } from '@apollo/client';
import { capitalize } from 'lodash';
import { useCallback, useEffect } from 'react';
import { useIntl } from 'react-intl';
import {
    MUTATION_CREATE_PROSPECT_MOTIVE,
    MUTATION_DELETE_PROSPECT_MOTIVE,
    MUTATION_EDIT_PROSPECT_MOTIVE
} from 'schema/settings/contact/prospectMotive/Mutation';
import { QUERY_PROSPECT_MOTIVES, QUERY_PROSPECT_MOTIVE_BY_LABEL } from 'schema/settings/contact/prospectMotive/Query';
import { ListParams, PageCursor } from 'types/graphql';
import { ProspectMotive } from 'types/settings/contact/prospectMotive';
import { toastError, toastSuccess } from 'utils/toast';

export type ProspectMotivesResponse = {
    prospectMotives: {
        data: ProspectMotive[];
        pageCursor: PageCursor;
    };
};

export type ProspectMotivesFilterInput = {
    search?: string;
};

export function useProspectMotives() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useLazyQuery<ProspectMotivesResponse>(QUERY_PROSPECT_MOTIVES);

    const getProspectMotives = useCallback(
        (params: ListParams<ProspectMotivesFilterInput> | undefined = undefined) =>
            request({
                variables: {
                    ...params
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'prospectMotives-fetch-error' }));
    }, [error, intl]);

    return {
        prospectMotives: data?.prospectMotives.data,
        pageCursor: data?.prospectMotives.pageCursor,
        loading,
        called,
        error,
        getProspectMotives
    };
}

export type ProspectMotiveByLabelResponse = {
    prospectMotiveByLabel: ProspectMotive;
};

export function useProspectMotiveByLabel() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useLazyQuery<ProspectMotiveByLabelResponse>(QUERY_PROSPECT_MOTIVE_BY_LABEL, {
        fetchPolicy: 'network-only'
    });

    const getProspectMotiveByLabel = useCallback(
        (label: string) =>
            request({
                variables: {
                    label
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'prospectMotives-fetch-error' }));
    }, [error, intl]);

    return {
        prospectMotive: data?.prospectMotiveByLabel,
        loading,
        called,
        error,
        getProspectMotiveByLabel
    };
}

function formatProspectMotive(prospectMotive: Partial<Pick<ProspectMotive, 'label'>>) {
    return {
        ...prospectMotive,
        label: prospectMotive.label ? capitalize(prospectMotive.label) : undefined
    };
}

export type EditProspectMotiveResponse = {
    editProspectMotive: ProspectMotive;
};

export function useEditProspectMotive() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useMutation<EditProspectMotiveResponse>(MUTATION_EDIT_PROSPECT_MOTIVE);

    const editProspectMotive = useCallback(
        ({ id, ...values }: Partial<ProspectMotive>) =>
            request({
                variables: {
                    id,
                    prospectMotive: formatProspectMotive(values)
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'prospectMotive-edit-error' }));
    }, [error, intl]);

    useEffect(() => {
        data && toastSuccess(intl.formatMessage({ id: 'prospectMotive-edit-success' }));
    }, [data, intl]);

    return {
        prospectMotive: data,
        loading,
        called,
        error,
        editProspectMotive
    };
}

export type CreateProspectMotiveResponse = {
    createProspectMotive: ProspectMotive;
};

export function useCreateProspectMotive() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useMutation<CreateProspectMotiveResponse>(MUTATION_CREATE_PROSPECT_MOTIVE, {
        refetchQueries: [QUERY_PROSPECT_MOTIVES]
    });

    const createProspectMotive = useCallback(
        (prospectMotive: Partial<ProspectMotive>) =>
            request({
                variables: {
                    prospectMotive: formatProspectMotive(prospectMotive)
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'prospectMotive-create-error' }));
    }, [error, intl]);

    useEffect(() => {
        data && toastSuccess(intl.formatMessage({ id: 'prospectMotive-create-success' }));
    }, [data, intl]);

    return {
        prospectMotive: data,
        loading,
        called,
        error,
        createProspectMotive
    };
}

export type DeleteProspectMotiveResponse = {
    deleteProspectMotive: {
        message: string;
    };
};

export function useDeleteProspectMotive() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useMutation<DeleteProspectMotiveResponse>(MUTATION_DELETE_PROSPECT_MOTIVE, {
        refetchQueries: [QUERY_PROSPECT_MOTIVES]
    });

    const deleteProspectMotive = useCallback(
        (id: number) =>
            request({
                variables: {
                    id
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'prospectMotive-delete-error' }));
    }, [error, intl]);

    useEffect(() => {
        data && toastSuccess(intl.formatMessage({ id: 'prospectMotive-delete-success' }));
    }, [data, intl]);

    return {
        message: data?.deleteProspectMotive.message,
        loading,
        called,
        error,
        deleteProspectMotive
    };
}
