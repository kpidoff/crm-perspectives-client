import { useLazyQuery, useMutation } from '@apollo/client';
import { capitalize } from 'lodash';
import { useCallback, useEffect } from 'react';
import { useIntl } from 'react-intl';
import {
    MUTATION_CREATE_REASON_FOR_LOST_CALL,
    MUTATION_DELETE_REASON_FOR_LOST_CALL,
    MUTATION_EDIT_REASON_FOR_LOST_CALL
} from 'schema/settings/contact/reasonForLostCall/Mutation';
import { QUERY_REASONS_FOR_LOST_CALL, QUERY_REASON_FOR_LOST_CALL_BY_LABEL } from 'schema/settings/contact/reasonForLostCall/Query';
import { ListParams, PageCursor } from 'types/graphql';
import { ReasonForLostCall } from 'types/settings/contact/reasonForLostCall';
import { toastError, toastSuccess } from 'utils/toast';

export type ReasonsForLostCallResponse = {
    reasonsForLostCall: {
        data: ReasonForLostCall[];
        pageCursor: PageCursor;
    };
};

export type ReasonsForLostCallFilterInput = {
    search?: string;
};

export function useReasonsForLostCall() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useLazyQuery<ReasonsForLostCallResponse>(QUERY_REASONS_FOR_LOST_CALL);

    const getReasonsForLostCall = useCallback(
        (params: ListParams<ReasonsForLostCallFilterInput> | undefined = undefined) =>
            request({
                variables: {
                    ...params
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'reasonsForLostCall-fetch-error' }));
    }, [error, intl]);

    return {
        reasonsForLostCall: data?.reasonsForLostCall.data,
        pageCursor: data?.reasonsForLostCall.pageCursor,
        loading,
        called,
        error,
        getReasonsForLostCall
    };
}

export type ReasonForLostCallByLabelResponse = {
    reasonForLostCallByLabel: ReasonForLostCall;
};

export function useReasonForLostCallByLabel() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useLazyQuery<ReasonForLostCallByLabelResponse>(
        QUERY_REASON_FOR_LOST_CALL_BY_LABEL,
        {
            fetchPolicy: 'network-only'
        }
    );

    const getReasonForLostCallByLabel = useCallback(
        (label: string) =>
            request({
                variables: {
                    label
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'reasonsForLostCall-fetch-error' }));
    }, [error, intl]);

    return {
        reasonForLostCall: data?.reasonForLostCallByLabel,
        loading,
        called,
        error,
        getReasonForLostCallByLabel
    };
}

function formatReasonForLostCall(reasonForLostCall: Partial<Pick<ReasonForLostCall, 'label'>>) {
    return {
        ...reasonForLostCall,
        label: reasonForLostCall.label ? capitalize(reasonForLostCall.label) : undefined
    };
}

export type EditReasonForLostCallResponse = {
    editReasonForLostCall: ReasonForLostCall;
};

export function useEditReasonForLostCall() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useMutation<EditReasonForLostCallResponse>(MUTATION_EDIT_REASON_FOR_LOST_CALL);

    const editReasonForLostCall = useCallback(
        ({ id, ...values }: Partial<ReasonForLostCall>) =>
            request({
                variables: {
                    id,
                    reasonForLostCall: formatReasonForLostCall(values)
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'reasonForLostCall-edit-error' }));
    }, [error, intl]);

    useEffect(() => {
        data && toastSuccess(intl.formatMessage({ id: 'reasonForLostCall-edit-success' }));
    }, [data, intl]);

    return {
        reasonForLostCall: data,
        loading,
        called,
        error,
        editReasonForLostCall
    };
}

export type CreateReasonForLostCallResponse = {
    createReasonForLostCall: ReasonForLostCall;
};

export function useCreateReasonForLostCall() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useMutation<CreateReasonForLostCallResponse>(MUTATION_CREATE_REASON_FOR_LOST_CALL, {
        refetchQueries: [QUERY_REASONS_FOR_LOST_CALL]
    });

    const createReasonForLostCall = useCallback(
        (reasonForLostCall: Partial<ReasonForLostCall>) =>
            request({
                variables: {
                    reasonForLostCall: formatReasonForLostCall(reasonForLostCall)
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'reasonForLostCall-create-error' }));
    }, [error, intl]);

    useEffect(() => {
        data && toastSuccess(intl.formatMessage({ id: 'reasonForLostCall-create-success' }));
    }, [data, intl]);

    return {
        reasonForLostCall: data,
        loading,
        called,
        error,
        createReasonForLostCall
    };
}

export type DeleteReasonForLostCallResponse = {
    deleteReasonForLostCall: {
        message: string;
    };
};

export function useDeleteReasonForLostCall() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useMutation<DeleteReasonForLostCallResponse>(MUTATION_DELETE_REASON_FOR_LOST_CALL, {
        refetchQueries: [QUERY_REASONS_FOR_LOST_CALL]
    });

    const deleteReasonForLostCall = useCallback(
        (id: number) =>
            request({
                variables: {
                    id
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'reasonForLostCall-delete-error' }));
    }, [error, intl]);

    useEffect(() => {
        data && toastSuccess(intl.formatMessage({ id: 'reasonForLostCall-delete-success' }));
    }, [data, intl]);

    return {
        message: data?.deleteReasonForLostCall.message,
        loading,
        called,
        error,
        deleteReasonForLostCall
    };
}
