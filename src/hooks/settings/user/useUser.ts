import { useLazyQuery, useMutation, useSubscription } from '@apollo/client';
import useAuth from 'hooks/authentication/useAuth';
import { capitalize } from 'lodash';
import { useCallback, useEffect } from 'react';
import { useIntl } from 'react-intl';
import { CHANGE_PASSWORD_USER, CREATE_USER, EDIT_USER, QUERY_USER_DELETE_NOTIFICATIONS } from 'schema/user/Mutation';
import {
    QUERY_USERS,
    QUERY_USERS_COMMERCIAL,
    QUERY_USER_DEVICES,
    QUERY_USER_DISCONNECT_EVERYWHERE,
    QUERY_USER_INTERFACE_ACCESS,
    QUERY_USER_NOTIFICATIONS,
    USER_BY_EMAIL,
    USER_BY_ID
} from 'schema/user/Query';
import { SUBSCRIPTION_USER_ADD_NOTIFICATION, SUBSCRIPTION_USER_DELETE_NOTIFICATION } from 'schema/user/Subscription';
import { ListParams, PageCursor, Paginate } from 'types/graphql';
import {
    User,
    UserDetails,
    UserDevice,
    UserEdit,
    UserInterfaceAccess,
    UserNotification,
    UserNotificationSubTypeEnum,
    UserNotificationTypeEnum,
    UserProfile,
    UserStatusEnum
} from 'types/user';
import { toastError, toastSuccess } from 'utils/toast';

export type LoginType = 'EMAIL_AUTHENTICATION' | 'LOGIN';

export type UserByIdResponse = {
    userById: UserDetails;
};

export function useUserProfilById() {
    const [request, { called, loading, data, error }] = useLazyQuery<UserByIdResponse>(USER_BY_ID);

    const getUser = useCallback(
        (userId: number) => {
            request({
                variables: {
                    userId
                }
            });
        },
        [request]
    );

    return {
        called,
        user: data?.userById,
        loading,
        error,
        getUser
    };
}

export type UserByEmailResponse = {
    userByEmail: User;
};

export function useUserByEmail() {
    const [request, { called, loading, data, error }] = useLazyQuery<UserByEmailResponse>(USER_BY_EMAIL);

    const userByEmail = useCallback(
        (email: string) =>
            request({
                variables: {
                    email
                }
            }),
        [request]
    );

    return {
        user: data?.userByEmail || null,
        loading,
        error,
        called,
        userByEmail
    };
}

export type EditUserResponse = {
    editUser: {
        user: UserDetails;
    };
};

function formatUser(user: Pick<User, 'lastname' | 'firstname'>) {
    return {
        ...user,
        lastname: user.lastname ? user.lastname.toUpperCase() : undefined,
        firstname: user.firstname ? capitalize(user.firstname) : undefined
    };
}

export function useEditUser() {
    const intl = useIntl();
    const { user: currentUser, updateUser } = useAuth();
    const [request, { data, loading, error, reset }] = useMutation<EditUserResponse>(EDIT_USER);

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'profile-edit-error' }));
    }, [error, intl]);

    useEffect(() => {
        if (data) {
            toastSuccess(intl.formatMessage({ id: 'profile-edit-success' }));
            if (data.editUser.user.id === currentUser?.id) {
                updateUser();
            }
        }
    }, [currentUser?.id, data, intl, updateUser]);

    const setUser = useCallback(
        (id: number, user: UserEdit) => {
            request({
                variables: {
                    id,
                    user: {
                        ...formatUser(user)
                    }
                }
            });
        },
        [request]
    );

    const activeTwoAuthentication = useCallback(
        (id: number, active: boolean) => {
            setUser(id, {
                emailAuthentication: active
            });
        },
        [setUser]
    );

    const changeStatus = useCallback(
        (id: number, status: UserStatusEnum) => {
            setUser(id, {
                status
            });
        },
        [setUser]
    );

    const changeAdministrator = useCallback(
        (id: number, administrator: boolean) => {
            setUser(id, {
                administrator
            });
        },
        [setUser]
    );

    return {
        user: data?.editUser.user,
        loading,
        error,
        setUser,
        changeStatus,
        activeTwoAuthentication,
        changeAdministrator,
        reset
    };
}

export type CreateUserResponse = {
    createUser: {
        user: UserDetails;
    };
};

export function useCreateUser() {
    const intl = useIntl();
    const [request, { data, loading, error }] = useMutation<CreateUserResponse>(CREATE_USER);

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'user-create-error' }));
    }, [error, intl]);

    useEffect(() => {
        data && toastSuccess(intl.formatMessage({ id: 'user-create-success' }));
    }, [data, intl]);

    const createUser = useCallback(
        (user: UserEdit) => {
            request({
                variables: {
                    user: {
                        ...formatUser(user)
                    }
                }
            });
        },
        [request]
    );

    return {
        user: data?.createUser.user,
        loading,
        error,
        createUser
    };
}

export type ChangePasswordResponse = {
    changePassword: {
        message: string;
        id: number;
    };
};

export function useChangePasswordUser() {
    const intl = useIntl();
    const [request, { data, loading, error }] = useMutation<ChangePasswordResponse>(CHANGE_PASSWORD_USER);

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'profile-security-changePassword-error' }));
    }, [error, intl]);

    useEffect(() => {
        data && toastSuccess(intl.formatMessage({ id: 'profile-security-changePassword-success' }));
    }, [data, intl]);

    const changePassword = (password: string) =>
        request({
            variables: {
                password
            }
        });

    return {
        reponse: data?.changePassword,
        loading,
        error,
        changePassword
    };
}

export type UsersResponse = {
    users: {
        data: UserProfile[];
        pageCursor: PageCursor;
    };
};

export type UserFilterInput = {
    search?: string;
    society?: number;
    status?: UserStatusEnum;
};

export type UserNotificationFilterInput = {
    search?: string;
    active?: boolean;
    type?: UserNotificationTypeEnum;
    subtype?: UserNotificationSubTypeEnum;
};

export function useUsers() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useLazyQuery<UsersResponse>(QUERY_USERS, {
        fetchPolicy: 'network-only'
    });

    const getUsers = useCallback(
        ({ filter, paginate, sort }: ListParams<UserFilterInput>) => {
            request({
                variables: {
                    filter,
                    paginate,
                    sort
                }
            });
        },
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'users-fetch-list-error' }));
    }, [error, intl]);

    return {
        users: data?.users.data,
        pageCursor: data?.users.pageCursor,
        loading,
        called,
        error,
        getUsers
    };
}

export type UserInterfaceAccessProps = {
    userInterfacesAccess: UserInterfaceAccess[];
};

export function useUserInterfaceAccess() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useLazyQuery<UserInterfaceAccessProps>(QUERY_USER_INTERFACE_ACCESS);

    const getUserInterfaceAccess = useCallback(
        (userId: number) => {
            request({
                variables: {
                    userId
                }
            });
        },
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'userInterfacesAccess-fetch-list-error' }));
    }, [error, intl]);

    return {
        userInterfacesAccess: data?.userInterfacesAccess,
        loading,
        called,
        error,
        getUserInterfaceAccess
    };
}

export type UserDevicesProps = {
    userDevices: {
        data: UserDevice[];
        pageCursor: PageCursor;
    };
};

export function useUserDevices() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useLazyQuery<UserDevicesProps>(QUERY_USER_DEVICES);

    const getUserDevices = useCallback(
        ({ userId, paginate }: { userId?: number; paginate?: Paginate }) => {
            request({
                variables: {
                    userId,
                    paginate
                }
            });
        },
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'userDevices-fetch-list-error' }));
    }, [error, intl]);

    return {
        userDevices: data?.userDevices.data,
        pageCursor: data?.userDevices.pageCursor,
        loading,
        called,
        error,
        getUserDevices
    };
}

export type UserDisconnectedEverywhere = {
    disconnectedEverywhere: {
        message: string;
    };
};

export function useUserDisconnectedEverywhere() {
    const [request, { called, loading, data, error }] = useLazyQuery<UserDisconnectedEverywhere>(QUERY_USER_DISCONNECT_EVERYWHERE);

    const disconnectedEverywhere = useCallback(
        (userId?: number) => {
            request({
                variables: {
                    userId
                }
            });
        },
        [request]
    );

    return {
        message: data?.disconnectedEverywhere.message,
        loading,
        called,
        error,
        disconnectedEverywhere
    };
}

export type UserDeleteNotification = {
    deleteNotification: {
        id: number;
    };
};

export function useDeleteNotification() {
    const [request, { data, loading, error }] = useMutation(QUERY_USER_DELETE_NOTIFICATIONS, {
        refetchQueries: [QUERY_USER_NOTIFICATIONS]
    });

    const deleteNotification = useCallback(
        (notificationId?: number) => {
            request({
                variables: {
                    notificationId
                }
            });
        },
        [request]
    );

    return {
        message: data?.deleteNotification.id,
        loading,
        error,
        deleteNotification
    };
}

export type UserDeleteSubscriptionNotification = {
    deleteNotification: UserNotification;
};

export function useDeleteUserSubscriptionNotification() {
    const { data, loading } = useSubscription<UserDeleteSubscriptionNotification>(SUBSCRIPTION_USER_DELETE_NOTIFICATION);
    return {
        deleteNotification: data?.deleteNotification,
        loading
    };
}

export type UserSubscriptionAddNotification = {
    notification: UserNotification;
};

export function useUserSubscriptionAddNotification() {
    const { data, loading } = useSubscription<UserSubscriptionAddNotification>(SUBSCRIPTION_USER_ADD_NOTIFICATION);
    return {
        notification: data?.notification,
        loading
    };
}

export type UserNotificationsReponse = {
    notifications: {
        data: UserNotification[];
        pageCursor: PageCursor;
    };
};

export function useUserNotifications() {
    const intl = useIntl();
    const [request, { called, loading, data, error, refetch }] = useLazyQuery<UserNotificationsReponse>(QUERY_USER_NOTIFICATIONS);

    const getNotifications = useCallback(
        ({ filter, paginate }: ListParams<UserNotificationFilterInput>) => {
            request({
                variables: {
                    paginate,
                    filter
                }
            });
        },
        [request]
    );

    const getActiveNotifications = useCallback(
        (page: number, perPage: number) => {
            getNotifications({
                paginate: {
                    page,
                    perPage
                },
                filter: {
                    active: true
                }
            });
        },
        [getNotifications]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'userNotifications-fetch-list-error' }));
    }, [error, intl]);

    return {
        notifications: data?.notifications.data,
        pageCursor: data?.notifications.pageCursor,
        loading,
        called,
        error,
        refetch,
        getNotifications,
        getActiveNotifications
    };
}

export type UsersCommercialResponse = {
    usersCommercial: {
        data: UserProfile[];
        pageCursor: PageCursor;
    };
};

export function useUsersCommercial() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useLazyQuery<UsersCommercialResponse>(QUERY_USERS_COMMERCIAL);

    const getUsersCommercial = useCallback(
        ({ filter, paginate, societyId }: { societyId: number } & ListParams<UserFilterInput>) => {
            request({
                variables: {
                    filter,
                    paginate,
                    societyId
                }
            });
        },
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'usersCommercial-fetch-list-error' }));
    }, [error, intl]);

    return {
        users: data?.usersCommercial.data,
        pageCursor: data?.usersCommercial.pageCursor,
        loading,
        called,
        error,
        getUsersCommercial
    };
}
