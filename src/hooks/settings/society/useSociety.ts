import { useLazyQuery, useMutation } from '@apollo/client';
import useAuth from 'hooks/authentication/useAuth';
import { capitalize } from 'lodash';
import { useCallback, useEffect } from 'react';
import { useIntl } from 'react-intl';
import { MUTATION_ADD_SOCIETY, MUTATION_EDIT_SOCIETY } from 'schema/settings/society/Mutation';
import { QUERY_SOCIETIES, QUERY_SOCIETY_BY_ID, QUERY_SOCIETY_BY_NAME } from 'schema/settings/society/Query';
import { ListParams, PageCursor } from 'types/graphql';
import { Society, SocietyList } from 'types/settings/society';
import { toastError, toastSuccess } from 'utils/toast';

export type SocietiesResponse = {
    societies: {
        data: SocietyList[];
        pageCursor: PageCursor;
    };
};

export type SocietyFilterInput = {
    search?: string;
    activated?: boolean;
};

export function useSocieties() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useLazyQuery<SocietiesResponse>(QUERY_SOCIETIES);

    const getSocieties = useCallback(
        (params: ListParams<SocietyFilterInput> | undefined = undefined) =>
            request({
                variables: {
                    ...params
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'societies-fetch-list-error' }));
    }, [error, intl]);

    return {
        societies: data?.societies.data,
        pageCursor: data?.societies.pageCursor,
        loading,
        called,
        error,
        getSocieties
    };
}

export type SocietyByIdResponse = {
    societyById: Society;
};
export function useSocietyById() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useLazyQuery<SocietyByIdResponse>(QUERY_SOCIETY_BY_ID);

    const getSociety = useCallback(
        (societyId: number) => {
            request({
                variables: {
                    societyId
                }
            });
        },
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'societies-fetch-error' }));
    }, [error, intl]);

    return {
        society: data?.societyById,
        loading,
        called,
        error,
        getSociety
    };
}

export type SocietyByNameResponse = {
    societyByName: Society;
};
export function useSocietyByName() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useLazyQuery<SocietyByNameResponse>(QUERY_SOCIETY_BY_NAME);

    const getSocietyByName = useCallback(
        (name: string) =>
            request({
                variables: {
                    name
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'societies-fetch-error' }));
    }, [error, intl]);

    return {
        society: data?.societyByName,
        loading,
        called,
        error,
        getSocietyByName
    };
}

function formatSociety(society: Partial<Pick<Society, 'name' | 'interlocutor'>>) {
    return {
        ...society,
        name: society.name ? society.name.toUpperCase() : undefined,
        interlocutor: society.interlocutor ? capitalize(society.interlocutor) : undefined
    };
}

export type EditSocietyResponse = {
    editSociety: {
        society: Society;
        message: string;
    };
};

export function useEditSociety() {
    const intl = useIntl();
    const { currentSociety, updateCurrentSociety } = useAuth();
    const [request, { data, loading, error, reset }] = useMutation<EditSocietyResponse>(MUTATION_EDIT_SOCIETY);

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'society-edit-error' }));
    }, [error, intl]);

    useEffect(() => {
        data && toastSuccess(intl.formatMessage({ id: 'society-edit-success' }));
    }, [data, intl]);

    useEffect(() => {
        if (data && currentSociety && currentSociety.id === data.editSociety.society.id) {
            updateCurrentSociety(data.editSociety.society);
        }
    }, [currentSociety, data, updateCurrentSociety]);

    const setSociety = (societyId: number, society: Partial<Society>) => {
        delete society.id;
        return request({
            variables: {
                societyId,
                society: {
                    ...formatSociety(society)
                }
            }
        });
    };

    const changeStatus = useCallback(
        (societyId: number, activated: boolean) =>
            request({
                variables: {
                    societyId,
                    society: {
                        activated
                    }
                }
            }),
        [request]
    );

    return {
        society: data?.editSociety.society,
        loading,
        error,
        setSociety,
        changeStatus,
        reset
    };
}

export type CreateSocietyResponse = {
    createSociety: {
        society: Society;
        message: string;
    };
};

export function useCreateSociety() {
    const intl = useIntl();
    const [request, { data, loading, error }] = useMutation<CreateSocietyResponse>(MUTATION_ADD_SOCIETY);

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'society-create-error' }));
    }, [error, intl]);

    useEffect(() => {
        data && toastSuccess(intl.formatMessage({ id: 'society-create-success' }));
    }, [data, intl]);

    const createSociety = (society: Partial<Society>) =>
        request({
            variables: {
                society: {
                    ...formatSociety(society)
                }
            }
        });

    return {
        society: data?.createSociety.society,
        loading,
        error,
        createSociety
    };
}
