import { useLazyQuery, useMutation } from '@apollo/client';
import { capitalize } from 'lodash';
import { useCallback, useEffect } from 'react';
import { useIntl } from 'react-intl';
import { MUTATION_CREATE_ROLE, MUTATION_DELETE_ROLE, MUTATION_EDIT_ROLE } from 'schema/settings/role/Mutation';
import { QUERY_ROLES, QUERY_ROLE_BY_NAME } from 'schema/settings/role/Query';
import { ListParams, PageCursor } from 'types/graphql';
import { Role, RoleType } from 'types/settings/role';
import { toastError, toastSuccess } from 'utils/toast';

export type RolesResponse = {
    roles: {
        data: Role[];
        pageCursor: PageCursor;
    };
};

export type RolesFilterInput = {
    search?: string;
    type?: RoleType;
};

export function useRoles() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useLazyQuery<RolesResponse>(QUERY_ROLES);

    const getRoles = useCallback(
        (params: ListParams<RolesFilterInput> | undefined = undefined) =>
            request({
                variables: {
                    ...params
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'roles-fetch-error' }));
    }, [error, intl]);

    return {
        roles: data?.roles.data,
        pageCursor: data?.roles.pageCursor,
        loading,
        called,
        error,
        getRoles
    };
}

export type RoleByNameResponse = {
    roleByName: Role;
};

export function useRoleByName() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useLazyQuery<RoleByNameResponse>(QUERY_ROLE_BY_NAME, {
        fetchPolicy: 'network-only'
    });

    const getRoleByName = useCallback(
        (name: string) =>
            request({
                variables: {
                    name
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'roles-fetch-error' }));
    }, [error, intl]);

    return {
        role: data?.roleByName,
        loading,
        called,
        error,
        getRoleByName
    };
}

function formatRole(role: Partial<Pick<Role, 'name'>>) {
    return {
        ...role,
        name: role.name ? capitalize(role.name) : undefined
    };
}

export type EditRoleResponse = {
    editRole: Role;
};

export function useEditRole() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useMutation<EditRoleResponse>(MUTATION_EDIT_ROLE);

    const editRole = useCallback(
        ({ id, ...values }: Partial<Role>) =>
            request({
                variables: {
                    id,
                    role: formatRole(values)
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'role-edit-error' }));
    }, [error, intl]);

    useEffect(() => {
        data && toastSuccess(intl.formatMessage({ id: 'role-edit-success' }));
    }, [data, intl]);

    return {
        role: data,
        loading,
        called,
        error,
        editRole
    };
}

export type CreateRoleResponse = {
    createRole: Role;
};

export function useCreateRole() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useMutation<CreateRoleResponse>(MUTATION_CREATE_ROLE, {
        refetchQueries: [QUERY_ROLES]
    });

    const createRole = useCallback(
        (role: Partial<Role>) =>
            request({
                variables: {
                    role: formatRole(role)
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'role-create-error' }));
    }, [error, intl]);

    useEffect(() => {
        data && toastSuccess(intl.formatMessage({ id: 'role-create-success' }));
    }, [data, intl]);

    return {
        role: data,
        loading,
        called,
        error,
        createRole
    };
}

export type DeleteRoleResponse = {
    deleteRole: {
        message: string;
    };
};

export function useDeleteRole() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useMutation<DeleteRoleResponse>(MUTATION_DELETE_ROLE, {
        refetchQueries: [QUERY_ROLES]
    });

    const deleteRole = useCallback(
        (id: number) =>
            request({
                variables: {
                    id
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'role-delete-error' }));
    }, [error, intl]);

    useEffect(() => {
        data && toastSuccess(intl.formatMessage({ id: 'role-delete-success' }));
    }, [data, intl]);

    return {
        message: data?.deleteRole.message,
        loading,
        called,
        error,
        deleteRole
    };
}
