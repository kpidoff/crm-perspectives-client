import { useLazyQuery } from '@apollo/client';
import { useCallback, useEffect } from 'react';
import { useIntl } from 'react-intl';
import { QUERY_POSTAL_CODE_BY_CITY, QUERY_POSTAL_CODE_BY_CODE } from 'schema/settings/postalCode/Query';
import { PostalCode } from 'types/settings/postalCode';
import { toastError } from 'utils/toast';

export type PostalCodeByCodeReponse = {
    postalCodesByCode: PostalCode[];
};
export function usePostalCodeByCode() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useLazyQuery<PostalCodeByCodeReponse>(QUERY_POSTAL_CODE_BY_CODE);

    const getPostalCodesByCode = useCallback(
        (code: string) => {
            request({
                variables: {
                    code
                }
            });
        },
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'postalCode-fetch-list-error' }));
    }, [error, intl]);

    return {
        postalCodes: data?.postalCodesByCode,
        loading,
        called,
        error,
        getPostalCodesByCode
    };
}

export type PostalCodeByCityReponse = {
    postalCodesByCity: PostalCode[];
};
export function usePostalCodeByCity() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useLazyQuery<PostalCodeByCityReponse>(QUERY_POSTAL_CODE_BY_CITY);

    const getPostalCodesByCity = useCallback(
        (city: string) => {
            request({
                variables: {
                    city
                }
            });
        },
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'postalCode-fetch-list-error' }));
    }, [error, intl]);

    return {
        postalCodes: data?.postalCodesByCity,
        loading,
        called,
        error,
        getPostalCodesByCity
    };
}
