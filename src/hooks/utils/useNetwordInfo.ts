import useAxios from 'axios-hooks';

type useNetwordInfoProps = {
    IPv4?: string;
    city?: string;
    country_code?: string;
    latitude?: number;
    longitude?: number;
    postal?: string;
    state?: string;
};
// eslint-disable-next-line import/prefer-default-export
export function useNetwordInfo() {
    const [{ data, loading, error }, refetch] = useAxios<useNetwordInfoProps>('https://geolocation-db.com/json/');

    return {
        infos: data,
        loading,
        error,
        refetch
    };
}
