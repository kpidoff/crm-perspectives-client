import { useLazyQuery, useMutation, useSubscription } from '@apollo/client';
import useAuth from 'hooks/authentication/useAuth';
import { useCallback, useEffect, useState } from 'react';
import { useIntl } from 'react-intl';
import {
    MUTATION_ADD_FOLDER,
    MUTATION_DELETE_FILES,
    MUTATION_MOVE_FILES,
    MUTATION_RENAME_FILES,
    MUTATION_UPLOAD_FILES
} from 'schema/utils/file-manager/Mutation';
import { QUERY_DOWLOAD_FILES, QUERY_FILES_MANAGER_BY_PARENT_ID, QUERY_FILE_MANAGER_BY_ID } from 'schema/utils/file-manager/Query';
import { SUBSCRIPTION_END_UPLOAD_FILE } from 'schema/utils/file-manager/Subscription';
import { FileManager } from 'types/utils/fileManager';
import { FileType } from 'utils/base64';
import { toastError, toastSuccess } from 'utils/toast';

export type FileManagerByIdResponse = {
    fileManagerById: FileManager;
};
export function useFileManagerById() {
    const intl = useIntl();
    const { currentSociety } = useAuth();
    const [request, { called, loading, data, error }] = useLazyQuery<FileManagerByIdResponse>(QUERY_FILE_MANAGER_BY_ID);

    const getFileManagerById = useCallback(
        (id: number) =>
            request({
                variables: {
                    id,
                    societyId: currentSociety?.id
                }
            }),
        [currentSociety?.id, request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'fileManager-fetch-error' }));
    }, [error, intl]);

    return {
        fileManager: data?.fileManagerById,
        loading,
        called,
        error,
        getFileManagerById
    };
}

export type FilesManagerByParentIdResponse = {
    filesManagerByParentId: FileManager[];
};
export function useFilesManagerByParentId() {
    const intl = useIntl();
    const { currentSociety } = useAuth();
    const [request, { called, loading, data, error, refetch }] =
        useLazyQuery<FilesManagerByParentIdResponse>(QUERY_FILES_MANAGER_BY_PARENT_ID);

    const getFilesManager = useCallback(
        (id: number) =>
            request({
                variables: {
                    id,
                    societyId: currentSociety?.id
                }
            }),
        [currentSociety?.id, request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'filesManager-fetch-error' }));
    }, [error, intl]);

    return {
        filesManager: data?.filesManagerByParentId,
        loading,
        called,
        error,
        refetch,
        getFilesManager
    };
}

export type FileManagerCreateFolderResponse = {
    fileManagerCreateFolder: FileManager;
};

export function useFileManagerCreateFolder() {
    const intl = useIntl();
    const { currentSociety } = useAuth();
    const [request, { called, loading, data, error, reset }] = useMutation<FileManagerCreateFolderResponse>(MUTATION_ADD_FOLDER, {
        refetchQueries: [QUERY_FILES_MANAGER_BY_PARENT_ID, QUERY_FILE_MANAGER_BY_ID]
    });

    const createFolder = useCallback(
        (name: string, parentId?: number) =>
            request({
                variables: {
                    name,
                    societyId: currentSociety?.id,
                    parentId
                }
            }),
        [currentSociety?.id, request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'fileManager-create-folder-error' }));
    }, [error, intl]);

    useEffect(() => {
        data && toastSuccess(intl.formatMessage({ id: 'fileManager-create-folder-success' }));
    }, [data, intl]);

    return {
        fileManager: data?.fileManagerCreateFolder,
        loading,
        called,
        error,
        createFolder,
        reset
    };
}

export type FileManagerUploadFilesResponse = {
    filesManagerUpload: {
        message: string;
    };
};

export function useFilesManagerUpload() {
    const intl = useIntl();
    const [filesSend, setFilesSend] = useState<FileType[] | undefined>(undefined);
    const { currentSociety, addFilesManager } = useAuth();
    const [request, { called, loading, data, error }] = useMutation<FileManagerUploadFilesResponse>(MUTATION_UPLOAD_FILES);

    const uploadFiles = useCallback(
        (files: FileType[], parentId?: number) => {
            request({
                variables: {
                    files,
                    societyId: currentSociety?.id,
                    parentId
                }
            });
            setFilesSend(files);
        },
        [currentSociety?.id, request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'fileManager-upload-files-error' }));
    }, [error, intl]);

    useEffect(() => {
        data && filesSend && addFilesManager(filesSend);
    }, [addFilesManager, data, filesSend]);

    return {
        message: data?.filesManagerUpload.message,
        loading,
        called,
        error,
        uploadFiles
    };
}

export type FileManagerSubscriptionEndUploadFile = {
    uploadFiles: FileManager;
};

export function useFileManagerSubscriptionEndUploadFile() {
    const { data, loading } = useSubscription<FileManagerSubscriptionEndUploadFile>(SUBSCRIPTION_END_UPLOAD_FILE);
    return {
        fileManager: data?.uploadFiles,
        loading
    };
}

export type FilesManagerDeleteResponse = {
    filesManagerDelete: { count: number };
};

export function useFilesManagerDelete() {
    const intl = useIntl();
    const { currentSociety } = useAuth();
    const [request, { called, loading, data, error }] = useMutation<FilesManagerDeleteResponse>(MUTATION_DELETE_FILES, {
        refetchQueries: [QUERY_FILES_MANAGER_BY_PARENT_ID, QUERY_FILE_MANAGER_BY_ID]
    });

    const deleteFiles = useCallback(
        (ids: number[]) => {
            request({
                variables: {
                    ids,
                    societyId: currentSociety?.id
                }
            });
        },

        [currentSociety?.id, request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'fileManager-delete-error' }));
    }, [error, intl]);

    useEffect(() => {
        data && toastSuccess(intl.formatMessage({ id: 'fileManager-delete-success' }));
    }, [data, intl]);

    return {
        count: data?.filesManagerDelete.count,
        loadingDelete: loading,
        called,
        errorDelete: error,
        deleteFiles
    };
}

export type FileManagerDownloadFilesResponse = {
    filesManagerDowload: {
        id: number;
        name: string;
        type: string;
        size: number;
        base64: string;
    }[];
};

export function useFilesManagerDownload() {
    const intl = useIntl();
    const { currentSociety } = useAuth();
    const [request, { called, loading, data, error }] = useLazyQuery<FileManagerDownloadFilesResponse>(QUERY_DOWLOAD_FILES);

    const downloadFiles = useCallback(
        (ids: number[]) => {
            request({
                variables: {
                    ids,
                    societyId: currentSociety?.id
                }
            });
        },
        [currentSociety?.id, request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'fileManager-dowload-files-error' }));
    }, [error, intl]);

    return {
        filesManager: data?.filesManagerDowload,
        loading,
        called,
        error,
        downloadFiles
    };
}

export type FilesManagerMoveResponse = {
    filesManagerMove: { count: number };
};

export function useFilesManagerMove() {
    const intl = useIntl();
    const { currentSociety } = useAuth();
    const [request, { called, loading, data, error }] = useMutation<FilesManagerMoveResponse>(MUTATION_MOVE_FILES, {
        refetchQueries: [QUERY_FILES_MANAGER_BY_PARENT_ID]
    });

    const moveFiles = useCallback(
        (ids: number[], parentId) => {
            request({
                variables: {
                    ids,
                    parentId,
                    societyId: currentSociety?.id
                }
            });
        },

        [currentSociety?.id, request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'fileManager-move-error' }));
    }, [error, intl]);

    useEffect(() => {
        data && toastSuccess(intl.formatMessage({ id: 'fileManager-move-success' }));
    }, [data, intl]);

    return {
        count: data?.filesManagerMove.count,
        loadingDelete: loading,
        called,
        errorDelete: error,
        moveFiles
    };
}

export type FilesManagerRenameResponse = {
    fileManagerRename: { count: number };
};

export function useFilesManagerRename() {
    const intl = useIntl();
    const { currentSociety } = useAuth();
    const [request, { called, loading, data, error, reset }] = useMutation<FilesManagerRenameResponse>(MUTATION_RENAME_FILES);

    const renameFile = useCallback(
        (id: number, name: string) => {
            request({
                variables: {
                    id,
                    name,
                    societyId: currentSociety?.id
                }
            });
        },

        [currentSociety?.id, request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'fileManager-rename-error' }));
    }, [error, intl]);

    useEffect(() => {
        data && toastSuccess(intl.formatMessage({ id: 'fileManager-rename-success' }));
    }, [data, intl]);

    return {
        fileManager: data?.fileManagerRename,
        loadingRename: loading,
        called,
        errorRename: error,
        renameFile,
        reset
    };
}
