import { useLazyQuery, useQuery } from '@apollo/client';
import { useCallback, useEffect } from 'react';
import { useIntl } from 'react-intl';
import { QUERY_INTERFACE_PARENT_CHILDREN_COUNT, QUERY_INTERFACE_CHILDREN_COUNT_BY_PARENT } from 'schema/utils/interface/Query';
import { InterfaceCountChildren } from 'types/utils/interface';
import { toastError } from 'utils/toast';

export type InterfacesParentCountChildren = {
    interfacesParent: InterfaceCountChildren[];
};

export function useInterfacesParentCountChildren() {
    const intl = useIntl();
    const { loading, error, data } = useQuery<InterfacesParentCountChildren>(QUERY_INTERFACE_PARENT_CHILDREN_COUNT);

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'interfaces-fetch-list-error' }));
    }, [error, intl]);

    return {
        interfaces: data?.interfacesParent,
        loading,
        error
    };
}

export type InterfacesCountChildrenByParent = {
    interfacesByParent: InterfaceCountChildren[];
};

export function useInterfacesCountByParent() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useLazyQuery<InterfacesCountChildrenByParent>(
        QUERY_INTERFACE_CHILDREN_COUNT_BY_PARENT
    );

    const getInterfacesByParent = useCallback(
        (parentId: number) => {
            request({
                variables: {
                    parentId
                }
            });
        },
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'interfaces-fetch-list-error' }));
    }, [error, intl]);

    return {
        interfacesByParent: data?.interfacesByParent,
        loading,
        called,
        error,
        getInterfacesByParent
    };
}
