import Geocode from 'react-geocode';
import config from 'config';
import { useCallback, useState } from 'react';

export type GeometryType = {
    lat: number;
    lng: number;
};

// eslint-disable-next-line import/prefer-default-export
export function useGeocode() {
    Geocode.setApiKey(config.keyGoogleMap);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState<string | undefined>(undefined);
    const [geometry, setGeometry] = useState<GeometryType | undefined>(undefined);

    const fromAddress = useCallback(
        ({ postalCode, city, address }: { postalCode: string | undefined; city: string | undefined; address: string | undefined }) => {
            setLoading(true);
            return Geocode.fromAddress(`${postalCode} ${city} ${address}`).then(
                (response) => {
                    const { lat, lng } = response.results[0].geometry.location;
                    setGeometry({
                        lat,
                        lng
                    });
                },
                (errorApi) => {
                    setError(errorApi);
                    setLoading(false);
                }
            );
        },
        []
    );

    return {
        fromAddress,
        loading,
        error,
        geometry
    };
}
