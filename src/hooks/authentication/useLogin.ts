import { useLazyQuery, useMutation } from '@apollo/client';
import { useCallback, useEffect } from 'react';
import { MUTATION_FORGOT_PASSWORD, MUTATION_RESET_PASSWORD } from 'schema/auth/Mutation';
import { QUERY_CURRENT_USER, QUERY_LOGIN, QUERY_LOGIN_WITH_CODE, QUERY_LOGOUT, QUERY_RESEND_CODE } from 'schema/auth/Query';
import { getUserAllowedAccess } from 'services/user.service';
import { UserLogin } from 'types/user';
import { browserName, osName, mobileModel, deviceType, osVersion } from 'react-device-detect';
import { useNetwordInfo } from 'hooks/utils/useNetwordInfo';
import { getCurrentBrowserFingerPrint } from '@rajesh896/broprint.js';
import _ from 'lodash';

export type LoginType = 'EMAIL_AUTHENTICATION' | 'LOGIN';

function useDeviceAuth() {
    const { infos } = useNetwordInfo();
    function getDefice() {
        return new Promise((resolve, reject) => {
            if (infos) {
                getCurrentBrowserFingerPrint()
                    .then((fingerprint) => {
                        navigator.geolocation.getCurrentPosition(
                            (position) => {
                                resolve({
                                    browserName,
                                    osName,
                                    osVersion,
                                    mobileModel,
                                    deviceType,
                                    lat: position.coords.latitude,
                                    lng: position.coords.longitude,
                                    IPv4: infos?.IPv4,
                                    countryCode: infos?.country_code,
                                    uuid: _.toString(fingerprint)
                                });
                            },
                            (errorGeo) => {
                                reject(errorGeo);
                            }
                        );
                    })
                    .catch((errUuid) => {
                        reject(errUuid);
                    });
            }
        });
    }

    return getDefice;
}

export type LoginResponse = {
    login: {
        type: LoginType;
        token: string;
        user: UserLogin;
    };
};

export function useLogin() {
    const [request, { called, loading, data, error }] = useLazyQuery<LoginResponse>(QUERY_LOGIN, {
        fetchPolicy: 'network-only'
    });
    const getDefice = useDeviceAuth();

    const loginWithMail = useCallback(
        (email: string, password: string) =>
            getDefice().then((values) =>
                request({
                    variables: {
                        email,
                        password,
                        device: values
                    }
                })
            ),
        [getDefice, request]
    );

    return {
        user: data?.login.user,
        token: data?.login.token,
        type: data?.login.type,
        loading,
        called,
        error,
        loginWithMail,
        request
    };
}

export type LogoutResponse = {
    logout: {
        message: string;
    };
};

export function useLogout() {
    const [request, { called, loading, data, error }] = useLazyQuery<LogoutResponse>(QUERY_LOGOUT);

    return {
        message: data?.logout.message,
        loading,
        called,
        error,
        logout: request
    };
}

export type LoginWithCodeResponse = {
    loginAuthentication: {
        type: LoginType;
        token: string;
        user: UserLogin;
    };
};

export function useLoginWithCode() {
    const [request, { called, loading, data, error }] = useLazyQuery<LoginWithCodeResponse>(QUERY_LOGIN_WITH_CODE);
    const getDefice = useDeviceAuth();

    const loginWithCode = useCallback(
        (userId: number, code: string) =>
            getDefice().then((values) =>
                request({
                    variables: {
                        userId,
                        code,
                        device: values
                    }
                })
            ),
        [getDefice, request]
    );

    return {
        user: data?.loginAuthentication.user,
        token: data?.loginAuthentication.token,
        type: data?.loginAuthentication.type,
        loading,
        called,
        error,
        loginWithCode
    };
}

export type resendCodeResponse = {
    resendCodeAuthentication: {
        message: string;
    };
};

export function useResendCode() {
    const [request, { called, loading, data, error }] = useLazyQuery<resendCodeResponse>(QUERY_RESEND_CODE);

    const resend = (email: string) =>
        request({
            variables: {
                email
            }
        });

    return {
        message: data?.resendCodeAuthentication.message,
        loading,
        called,
        error,
        resend
    };
}

export type CurrentUerResponse = {
    currentUser: UserLogin;
};

export function useCurrentUser() {
    const [request, { called, loading, data, error }] = useLazyQuery<CurrentUerResponse>(QUERY_CURRENT_USER);

    useEffect(() => {
        data && getUserAllowedAccess(data.currentUser, 'settings', 'access');
    }, [data]);

    return {
        currentUser: data?.currentUser,
        loading,
        called,
        error,
        getCurrentUser: request
    };
}

export type ForgotPasswordResponse = {
    forgotPassword: {
        message: string;
    };
};

export function useForgotPassword() {
    const [request, { called, loading, data, error }] = useMutation<ForgotPasswordResponse>(MUTATION_FORGOT_PASSWORD);

    const forgotPassword = (email: string) =>
        request({
            variables: {
                email
            }
        });

    return {
        message: data?.forgotPassword.message,
        loading,
        called,
        error,
        forgotPassword
    };
}

export type ResetPasswordResponse = {
    resetPassword: {
        message: string;
    };
};

export function useResetPassword() {
    const [request, { called, loading, data, error }] = useMutation<ResetPasswordResponse>(MUTATION_RESET_PASSWORD);

    const resetPassword = (token: string, password: string) =>
        request({
            variables: {
                token,
                password
            }
        });

    return {
        message: data?.resetPassword.message,
        loading,
        called,
        error,
        resetPassword
    };
}

export default useLogin;
