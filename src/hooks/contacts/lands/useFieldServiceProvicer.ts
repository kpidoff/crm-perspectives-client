import { useLazyQuery, useMutation } from '@apollo/client';
import useAuth from 'hooks/authentication/useAuth';
import { capitalize } from 'lodash';
import { useCallback, useEffect } from 'react';
import { useIntl } from 'react-intl';
import {
    MUTATION_CREATE_FIELD_SERVICE_PROVIDER,
    MUTATION_DELETE_FIELD_SERVICE_PROVIDER,
    MUTATION_EDIT_FIELD_SERVICE_PROVIDER
} from 'schema/contacts/lands/fieldServiceProvider/Mutation';
import { QUERY_FIELD_SERVICES_PROVIDER, QUERY_FIELD_SERVICE_PROVIDER_BY_LABEL } from 'schema/contacts/lands/fieldServiceProvider/Query';
import { FieldServiceProvider } from 'types/contacts/lands/fieldServiceProvider';
import { ListParams, PageCursor } from 'types/graphql';
import { toastError, toastSuccess } from 'utils/toast';

export type FieldServicesProviderResponse = {
    fieldServicesProvider: {
        data: FieldServiceProvider[];
        pageCursor: PageCursor;
    };
};

export type FieldServicesProviderFilterInput = {
    search?: string;
    societyId?: number;
};

export type FieldServiceProviderByLabelResponse = {
    fieldServiceProviderByLabel: FieldServiceProvider;
};

export function useFieldServiceProviderByLabel() {
    const intl = useIntl();
    const { currentSociety } = useAuth();
    const [request, { called, loading, data, error }] = useLazyQuery<FieldServiceProviderByLabelResponse>(
        QUERY_FIELD_SERVICE_PROVIDER_BY_LABEL,
        {
            fetchPolicy: 'network-only'
        }
    );

    const getFieldServiceProviderByLabel = useCallback(
        (label: string) =>
            request({
                variables: {
                    label,
                    societyId: currentSociety
                }
            }),
        [currentSociety, request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'fieldServiceProviders-fetch-error' }));
    }, [error, intl]);

    return {
        fieldServiceProvider: data?.fieldServiceProviderByLabel,
        loading,
        called,
        error,
        getFieldServiceProviderByLabel
    };
}

export function useFieldServicesProvider() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useLazyQuery<FieldServicesProviderResponse>(QUERY_FIELD_SERVICES_PROVIDER);

    const getFieldServicesProvider = useCallback(
        (params: ListParams<FieldServicesProviderFilterInput> | undefined = undefined) =>
            request({
                variables: {
                    ...params
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'field-services-provider-fetch-error' }));
    }, [error, intl]);

    return {
        fieldServicesProvider: data?.fieldServicesProvider.data,
        pageCursor: data?.fieldServicesProvider.pageCursor,
        loading,
        called,
        error,
        getFieldServicesProvider
    };
}

function formatFieldServiceProvider(fieldServiceProvider: Partial<Pick<FieldServiceProvider, 'label'>>) {
    return {
        ...fieldServiceProvider,
        label: fieldServiceProvider.label ? capitalize(fieldServiceProvider.label) : undefined
    };
}

export type EditFieldServiceProviderResponse = {
    editFieldServiceProvider: FieldServiceProvider;
};

export function useEditFieldServiceProvider() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useMutation<EditFieldServiceProviderResponse>(MUTATION_EDIT_FIELD_SERVICE_PROVIDER);

    const editFieldServiceProvider = useCallback(
        ({ id, ...values }: Partial<FieldServiceProvider>) =>
            request({
                variables: {
                    id,
                    societyId: values.societyId,
                    fieldServiceProvider: formatFieldServiceProvider(values)
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'fieldServiceProvider-edit-error' }));
    }, [error, intl]);

    useEffect(() => {
        data && toastSuccess(intl.formatMessage({ id: 'fieldServiceProvider-edit-success' }));
    }, [data, intl]);

    return {
        fieldServiceProvider: data,
        loading,
        called,
        error,
        editFieldServiceProvider
    };
}

export type CreateFieldServiceProviderResponse = {
    createFieldServiceProvider: FieldServiceProvider;
};

export function useCreateFieldServiceProvider() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useMutation<CreateFieldServiceProviderResponse>(
        MUTATION_CREATE_FIELD_SERVICE_PROVIDER,
        {
            refetchQueries: [QUERY_FIELD_SERVICES_PROVIDER]
        }
    );

    const createFieldServiceProvider = useCallback(
        (fieldServiceProvider: Partial<FieldServiceProvider>, societyId) =>
            request({
                variables: {
                    fieldServiceProvider: formatFieldServiceProvider(fieldServiceProvider),
                    societyId
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'fieldServiceProvider-create-error' }));
    }, [error, intl]);

    useEffect(() => {
        data && toastSuccess(intl.formatMessage({ id: 'fieldServiceProvider-create-success' }));
    }, [data, intl]);

    return {
        fieldServiceProvider: data,
        loading,
        called,
        error,
        createFieldServiceProvider
    };
}

export type DeleteFieldServiceProviderResponse = {
    deleteFieldServiceProvider: {
        message: string;
    };
};

export function useDeleteFieldServiceProvider() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useMutation<DeleteFieldServiceProviderResponse>(
        MUTATION_DELETE_FIELD_SERVICE_PROVIDER,
        {
            refetchQueries: [QUERY_FIELD_SERVICES_PROVIDER]
        }
    );

    const deleteFieldServiceProvider = useCallback(
        (fieldServiceProvider: Pick<FieldServiceProvider, 'id' | 'societyId'>) =>
            request({
                variables: {
                    id: fieldServiceProvider.id,
                    societyId: fieldServiceProvider.societyId
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'fieldServiceProvider-delete-error' }));
    }, [error, intl]);

    useEffect(() => {
        data && toastSuccess(intl.formatMessage({ id: 'fieldServiceProvider-delete-success' }));
    }, [data, intl]);

    return {
        message: data?.deleteFieldServiceProvider.message,
        loading,
        called,
        error,
        deleteFieldServiceProvider
    };
}
