import { useLazyQuery, useMutation } from '@apollo/client';
import useAuth from 'hooks/authentication/useAuth';
import { useCallback, useEffect } from 'react';
import { useIntl } from 'react-intl';
import { MUTATION_LAND_ARCHIVED } from 'schema/contacts/lands/land/Mutation';
import { QUERY_LANDS, QUERY_LANDS_LOCATION, QUERY_LAND_BY_ID, QUERY_LAND_IMAGES } from 'schema/contacts/lands/land/Query';
import { Land, LandImage, LandLocation, LandSpecific, LandType } from 'types/contacts/land';
import { ListParams, PageCursor } from 'types/graphql';
import { LocationFilterInputType } from 'ui-component/datas-components/components/datas-components-drawer/filters.types';
import { toastError, toastSuccess } from 'utils/toast';

export type LandsFilterInput = {
    search?: string;
    societyId?: number;
    types?: LandType[];
    location?: LocationFilterInputType;
    specific?: {
        [LandSpecific.BOUNDING]: boolean;
        [LandSpecific.SERVICED]: boolean;
        [LandSpecific.SEWER]: boolean;
    };
    archived?: boolean;
    area?: {
        min: number;
        max: number;
    };
    price?: {
        min: number;
        max: number;
    };
    rating?: number;
};

export type LandsResponse = {
    lands: {
        data: Land[];
        pageCursor: PageCursor;
    };
};

export function useLands() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useLazyQuery<LandsResponse>(QUERY_LANDS);

    const getLands = useCallback(
        (params: ListParams<LandsFilterInput> | undefined = undefined) =>
            request({
                variables: {
                    ...params
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'lands-fetch-error' }));
    }, [error, intl]);

    return {
        lands: data?.lands.data,
        pageCursor: data?.lands.pageCursor,
        loading,
        called,
        error,
        getLands
    };
}

export type LandsLocationResponse = {
    lands: {
        data: LandLocation[];
        pageCursor: PageCursor;
    };
};

export function useLandsLocation() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useLazyQuery<LandsLocationResponse>(QUERY_LANDS_LOCATION);

    const getLandsLocation = useCallback(
        (params: Omit<ListParams<LandsFilterInput>, 'paginate'> | undefined = undefined) =>
            request({
                variables: {
                    ...params
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'lands-location-fetch-error' }));
    }, [error, intl]);

    return {
        lands: data?.lands.data,
        loading,
        called,
        error,
        getLandsLocation
    };
}

export type LandByIdResponse = {
    landById: Land;
};
export function useLandById() {
    const intl = useIntl();
    const { currentSociety } = useAuth();
    const [request, { called, loading, data, error }] = useLazyQuery<LandByIdResponse>(QUERY_LAND_BY_ID);

    const getLandById = useCallback(
        (id: number) =>
            request({
                variables: {
                    id,
                    societyId: currentSociety?.id
                }
            }),
        [currentSociety?.id, request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'land-fetch-error' }));
    }, [error, intl]);

    return {
        land: data?.landById,
        loading,
        called,
        error,
        getLandById
    };
}

export type LandImagesResponse = {
    landImages: LandImage[];
};
export function useLandImages() {
    const intl = useIntl();
    const { currentSociety } = useAuth();
    const [request, { called, loading, data, error }] = useLazyQuery<LandImagesResponse>(QUERY_LAND_IMAGES);

    const getLandImages = useCallback(
        (landId: number) =>
            request({
                variables: {
                    landId,
                    societyId: currentSociety?.id
                }
            }),
        [currentSociety?.id, request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'landImages-fetch-error' }));
    }, [error, intl]);

    return {
        landImages: data?.landImages,
        loading,
        called,
        error,
        getLandImages
    };
}

export type LandArchivedResponse = {
    landArchived: Land;
};

export function useLandArchived() {
    const intl = useIntl();
    const [request, { called, loading, data, error }] = useMutation<LandArchivedResponse>(MUTATION_LAND_ARCHIVED);

    const landArchived = useCallback(
        (id: number, archived: boolean) =>
            request({
                variables: {
                    id,
                    archived
                }
            }),
        [request]
    );

    useEffect(() => {
        error && toastError(intl.formatMessage({ id: 'land-archived-error' }));
    }, [error, intl]);

    useEffect(() => {
        data && toastSuccess(intl.formatMessage({ id: 'land-archived-success' }));
    }, [data, intl]);

    return {
        land: data,
        loading,
        called,
        error,
        landArchived
    };
}
