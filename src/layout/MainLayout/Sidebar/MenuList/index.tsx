import { memo } from 'react';

// material-ui
import { Typography } from '@mui/material';

// project imports
import NavGroup from './NavGroup';
import menuItem, { MenuProps } from 'menu-items';
import { getUserAllowedAccess } from 'services/user.service';
import useAuth from 'hooks/authentication/useAuth';
import _ from 'lodash';

// ==============================|| SIDEBAR MENU LIST ||============================== //

const MenuList = () => {
    const { user } = useAuth();

    const generateMenu = (item: MenuProps) => {
        switch (item.type) {
            case 'group':
                return <NavGroup key={item.id} item={item} />;
            default:
                return (
                    <Typography key={item.id} variant="h6" color="error" align="center">
                        Menu Items Error
                    </Typography>
                );
        }
    };

    const navItems: JSX.Element[] = [];
    _.map(menuItem.items, (item) => {
        if (item.verifiedAccess) {
            user &&
                (getUserAllowedAccess(user, item.id, 'access') || getUserAllowedAccess(user, item.id, 'edit')) &&
                navItems.push(generateMenu(item));
        } else {
            navItems.push(generateMenu(item));
        }
    });

    return <>{navItems}</>;
};

export default memo(MenuList);
