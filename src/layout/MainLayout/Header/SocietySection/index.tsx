import { useEffect, useRef, useState } from 'react';

// material-ui
import { useTheme } from '@mui/material/styles';
import { Box, ClickAwayListener, Grid, List, ListItemButton, ListItemText, Paper, Popper, Typography, useMediaQuery } from '@mui/material';

// project imports
import Transitions from 'ui-component/extended/Transitions';
import { useSocieties, useSocietyById } from 'hooks/settings/society/useSociety';

// assets
import useConfig from 'hooks/useConfig';
import { IconBuilding } from '@tabler/icons';
import _ from 'lodash';
import useAuth from 'hooks/authentication/useAuth';
import { LoadingButton } from '@mui/lab';
import { FormattedMessage } from 'react-intl';

// ==============================|| LOCALIZATION ||============================== //

const SocietySection = () => {
    const { borderRadius } = useConfig();
    const { getSocieties, societies, loading } = useSocieties();
    const { getSociety, loading: loadingById, society } = useSocietyById();
    const { changeCurrentSociety, currentSociety } = useAuth();

    useEffect(() => {
        getSocieties();
    }, [getSocieties]);

    const theme = useTheme();
    const matchesXs = useMediaQuery(theme.breakpoints.down('md'));

    const [open, setOpen] = useState(false);
    const anchorRef = useRef<any>(null);
    const [societyId, setSocietyId] = useState<number | null | undefined>(currentSociety?.id);

    useEffect(() => {
        if (society) {
            changeCurrentSociety(society);
            setSocietyId(society.id);
            setOpen(false);
        }
    }, [changeCurrentSociety, society]);

    const handleToggle = () => {
        setOpen((prevOpen) => !prevOpen);
    };

    const handleClose = (event: MouseEvent | TouchEvent) => {
        if (anchorRef.current && anchorRef.current.contains(event.target)) {
            return;
        }
        setOpen(false);
    };

    const prevOpen = useRef(open);
    useEffect(() => {
        if (prevOpen.current === true && open === false) {
            anchorRef.current.focus();
        }
        prevOpen.current = open;
    }, [open]);

    const currentSocietySelect = _.find(societies, (societySelect) => societySelect.id === societyId);

    return (
        <>
            <Box
                sx={{
                    ml: 2,
                    [theme.breakpoints.down('md')]: {
                        ml: 1
                    }
                }}
            >
                <LoadingButton
                    endIcon={<IconBuilding stroke={1.5} size="1.3rem" />}
                    sx={{
                        ...theme.typography.commonAvatar,
                        border: '1px solid',
                        borderColor: theme.palette.mode === 'dark' ? theme.palette.dark.main : theme.palette.primary.light,
                        background: theme.palette.mode === 'dark' ? theme.palette.dark.main : theme.palette.primary.light,
                        color: theme.palette.primary.dark,
                        transition: 'all .2s ease-in-out',
                        '&[aria-controls="menu-list-grow"],&:hover': {
                            borderColor: theme.palette.primary.main,
                            background: theme.palette.primary.main,
                            color: theme.palette.primary.light
                        }
                    }}
                    ref={anchorRef}
                    aria-controls={open ? 'menu-list-grow' : undefined}
                    aria-haspopup="true"
                    onClick={handleToggle}
                    loading={loading || loadingById}
                >
                    {currentSocietySelect ? currentSocietySelect.name : <FormattedMessage id="app-unknown" />}
                </LoadingButton>
            </Box>

            <Popper
                placement={matchesXs ? 'bottom-start' : 'bottom'}
                open={open}
                anchorEl={anchorRef.current}
                role={undefined}
                transition
                disablePortal
                popperOptions={{
                    modifiers: [
                        {
                            name: 'offset',
                            options: {
                                offset: [matchesXs ? 0 : 0, 20]
                            }
                        }
                    ]
                }}
            >
                {({ TransitionProps }) => (
                    <ClickAwayListener onClickAway={handleClose}>
                        <Transitions position={matchesXs ? 'top-left' : 'top'} in={open} {...TransitionProps}>
                            <Paper elevation={16}>
                                {open && (
                                    <List
                                        component="nav"
                                        sx={{
                                            width: '100%',
                                            minWidth: 200,
                                            maxWidth: 280,
                                            bgcolor: theme.palette.background.paper,
                                            borderRadius: `${borderRadius}px`,
                                            [theme.breakpoints.down('md')]: {
                                                maxWidth: 250
                                            }
                                        }}
                                    >
                                        {_.map(societies, (societySelect) => (
                                            <ListItemButton
                                                selected={societyId === societySelect.id}
                                                onClick={() => getSociety(societySelect.id)}
                                                key={societySelect.id}
                                            >
                                                <ListItemText
                                                    primary={
                                                        <Grid container>
                                                            <Typography color="textPrimary">{societySelect.name}</Typography>
                                                        </Grid>
                                                    }
                                                />
                                            </ListItemButton>
                                        ))}
                                    </List>
                                )}
                            </Paper>
                        </Transitions>
                    </ClickAwayListener>
                )}
            </Popper>
        </>
    );
};

export default SocietySection;
