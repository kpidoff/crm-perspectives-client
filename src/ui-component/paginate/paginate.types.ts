export type PaginateContextType = {
    onChangePage: (page: number) => void;
    page: number;
    count: number | undefined;
};
