import { Fade, Pagination } from '@mui/material';
import { FC, useEffect, useState } from 'react';
import { scrollTop } from 'utils';
import { PaginateContextType } from './paginate.types';

const Paginate: FC<PaginateContextType> = ({ onChangePage, page, count }) => {
    const [currentCount, setCurrentCount] = useState(0);

    useEffect(() => {
        if (count !== undefined) {
            setCurrentCount(count);
        }
    }, [count]);

    return (
        <Fade in={currentCount > 1} unmountOnExit>
            <Pagination
                color="primary"
                page={page}
                count={currentCount}
                onChange={(_event, value) => {
                    onChangePage(value);
                    scrollTop();
                }}
            />
        </Fade>
    );
};

export default Paginate;
