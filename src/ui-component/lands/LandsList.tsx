import { Dialog, DialogContent, DialogTitle, Grid, ListItemButton, ListItemText, Stack, Typography } from '@mui/material';
import { LandsFilterInput, useLands } from 'hooks/contacts/lands/useLand';
import { useCallback, useEffect, useState } from 'react';
import { landFormatReference, landFormatTitle } from 'services/contacts/lands/land.service';
import { Land } from 'types/contacts/land';
import DatasListWrapper from 'ui-component/datas-components/components/datas-components-wrapper/datas-list-wrapper';
import { forceCapitalizeFirstLetter, formatArea, formatPrice } from 'utils';
import { LANDS_NUMBER_PER_PAGE } from 'views/contacts/lands/lands/constants.lands';

interface ListLandsProps {
    open: boolean;
    onClose: (land?: Land) => void;
}
const LandsList = ({ open, onClose }: ListLandsProps) => {
    const { getLands, pageCursor, lands, loading } = useLands();
    const [currentPage, setCurrentPage] = useState(1);
    const [filter, setFilter] = useState<LandsFilterInput | undefined>(undefined);

    useEffect(() => {
        getLands({
            paginate: {
                perPage: LANDS_NUMBER_PER_PAGE,
                page: currentPage
            },
            filter
        });
    }, [currentPage, filter, getLands]);

    const updateFilter = useCallback(
        (value: any, key: string) => {
            setFilter((prevState) => ({
                ...prevState,
                [key]: value
            }));
            setCurrentPage(1);
        },
        [setFilter]
    );

    return (
        <Dialog scroll="paper" fullWidth open={open} onClose={() => onClose()}>
            <DialogTitle>Liste des terrains</DialogTitle>
            <DialogContent dividers>
                <DatasListWrapper
                    dataComponent={{
                        datas: lands,
                        itemsPerPage: LANDS_NUMBER_PER_PAGE,
                        loading,
                        heightSkeleton: 60
                    }}
                    datasComponentsSearch={{
                        filterkey: 'search',
                        updateSearch: updateFilter
                    }}
                    datasComponentContent={{
                        component: (data: Land) => (
                            <ListItemButton onClick={() => onClose(data)}>
                                <ListItemText
                                    primary={
                                        <Grid container>
                                            <Grid item xs={6}>
                                                {landFormatReference(data.id)} - {landFormatTitle(data.title)}
                                            </Grid>
                                            <Grid item xs={6}>
                                                <Typography align="right">{forceCapitalizeFirstLetter(data.city)}</Typography>
                                            </Grid>
                                            <Grid item xs={12}>
                                                <Typography variant="subtitle2">{data.description}</Typography>
                                            </Grid>
                                            <Grid item xs={12}>
                                                <Stack direction="row" spacing={1} divider={<p> - </p>}>
                                                    <Typography>{formatPrice(data.price)} </Typography>
                                                    <Typography variant="subtitle2">{formatArea(data.area)}</Typography>
                                                </Stack>
                                            </Grid>
                                        </Grid>
                                    }
                                />
                            </ListItemButton>
                        )
                    }}
                    datasComponentPaginate={{
                        currentPage: pageCursor?.currentPage,
                        numberOfPage: pageCursor?.lastPage,
                        onChangePage: (page) => setCurrentPage(page)
                    }}
                />
            </DialogContent>
        </Dialog>
    );
};

export default LandsList;
