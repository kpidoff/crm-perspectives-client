import { Skeleton, SkeletonProps, Typography as TypographyMui, TypographyProps as TypographyPropsMui } from '@mui/material';

export type TypographyProps = TypographyPropsMui & {
    loading?: boolean;
    skeletonProps?: SkeletonProps;
};

const Typography = ({ loading, skeletonProps, ...props }: TypographyProps) =>
    loading ? <Skeleton {...skeletonProps} /> : <TypographyMui {...props} />;

export default Typography;
