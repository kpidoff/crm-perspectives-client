import { FC } from 'react';
import MaskedInput from 'react-text-mask';
import { SkeletonInput } from 'ui-component/skeleton';
import InputTextFieldBase, { TextFieldBaseProps } from '../components/InputTextFieldBase';

const InputPhone: FC<{ errorMessage?: string } & TextFieldBaseProps> = ({ errorMessage, ...props }) =>
    !props.skeleton ? (
        <MaskedInput
            mask={[/[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/, '.', /[0-9]/, /[0-9]/]}
            guide={false}
            render={(ref, otherProps) => <InputTextFieldBase inputRef={ref} {...otherProps} {...props} />}
        />
    ) : (
        <SkeletonInput />
    );

export default InputPhone;
