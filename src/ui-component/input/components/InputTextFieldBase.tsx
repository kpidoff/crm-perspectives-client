import { FC } from 'react';
import { TextField, TextFieldProps } from '@mui/material';
import ErrorMessageInput from './ErrorMessageInput';
import { useTheme } from '@mui/material/styles';
import { SkeletonInput } from 'ui-component/skeleton';
import { capitalizeFirstLetter } from 'utils';

export type TextFieldBaseProps = TextFieldProps & {
    required?: boolean;
    skeleton?: boolean;
    errorMessage?: string;
    capitalizeFirstCaractere?: boolean;
};

export function LabelRequired({ label, required }: { label: string | React.ReactNode; required: boolean }) {
    const theme = useTheme();
    if (required) {
        return (
            <span>
                {label}{' '}
                <span
                    style={{
                        color: theme.palette.mode === 'dark' ? theme.palette.error.light : theme.palette.error.dark
                    }}
                >
                    *
                </span>
            </span>
        );
    }
    return <span>{label}</span>;
}

const InputTextFieldBase: FC<TextFieldBaseProps> = ({ errorMessage, skeleton, capitalizeFirstCaractere, ...props }) => {
    const value = capitalizeFirstCaractere ? capitalizeFirstLetter(props.value as string) : props.value;
    return !skeleton ? (
        <>
            <TextField
                {...props}
                value={value || ''}
                label={<LabelRequired label={props.label} required={props.required || false} />}
                required={false}
            />
            <ErrorMessageInput errorMessage={errorMessage} />
        </>
    ) : (
        <SkeletonInput />
    );
};

export default InputTextFieldBase;
