import { InputAdornment, OutlinedInput, OutlinedInputProps } from '@mui/material';
import { IconSearch } from '@tabler/icons';
import _ from 'lodash';
import { FC } from 'react';

interface InputSearchProps {
    onChange: (value: string) => void;
    wait?: number;
}
const InputSearch: FC<Omit<OutlinedInputProps, 'onChange'> & InputSearchProps> = ({ onChange, wait, ...props }) => {
    const handleChange = _.debounce((valueSearch: string) => {
        onChange(valueSearch);
    }, wait);

    return (
        <OutlinedInput
            {...props}
            onChange={(event) => {
                handleChange(event.currentTarget.value);
            }}
            type="search"
            startAdornment={
                <InputAdornment position="start">
                    <IconSearch stroke={1.5} size="1rem" />
                </InputAdornment>
            }
        />
    );
};

export default InputSearch;
