import { FC } from 'react';
import { useTheme } from '@mui/material/styles';
import { OutlinedInputProps, FormControl, InputLabel, OutlinedInput } from '@mui/material';
import ErrorMessageInput from '../components/ErrorMessageInput';

const InputOutline: FC<{ errorMessage?: string; required?: boolean } & OutlinedInputProps> = ({ errorMessage, required, ...props }) => {
    const theme = useTheme();
    return (
        <FormControl fullWidth error={props.error} sx={{ ...theme.typography.customInput }}>
            <InputLabel htmlFor="outlined-adornment-email-login">
                {props.label} {required && '*'}
            </InputLabel>
            <OutlinedInput {...props} />
            <ErrorMessageInput errorMessage={errorMessage} />
        </FormControl>
    );
};

export default InputOutline;
