import { FormControl, FormGroup, FormControlLabel, Checkbox, CheckboxProps, FormControlProps } from '@mui/material';
import _ from 'lodash';
import { FC } from 'react';
import ErrorMessageInput from '../components/ErrorMessageInput';
import { LabelRequired, TextFieldBaseProps } from '../components/InputTextFieldBase';

export type InputMultipleCheckboxType = CheckboxProps & {
    label: React.ReactElement | string;
};

interface InputMultipleCheckboxProps {
    items: InputMultipleCheckboxType[];
}

const InputMultipleCheckbox: FC<FormControlProps & TextFieldBaseProps & InputMultipleCheckboxProps> = ({
    label,
    error,
    skeleton,
    required,
    errorMessage,
    items,
    ...props
}) => (
    <FormControl {...props}>
        <LabelRequired label={label} required={required || false} />

        <FormGroup>
            {_.map(items, ({ label: labelItem, ...propsItem }, index) => (
                <FormControlLabel key={index} control={<Checkbox {...propsItem} />} label={labelItem} />
            ))}
        </FormGroup>
        <ErrorMessageInput errorMessage={errorMessage} />
    </FormControl>
);

export default InputMultipleCheckbox;
