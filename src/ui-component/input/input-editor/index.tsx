import { FC } from 'react';
import { useTheme } from '@mui/material/styles';

import * as ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import { Grid, Skeleton } from '@mui/material';
import { gridSpacing } from 'store/constant';

const InputEditor: FC<ReactQuill.ReactQuillProps & { skeleton?: boolean }> = ({ skeleton, ...props }) => {
    const theme = useTheme();
    const Component = ReactQuill.default;
    const modules = {
        toolbar: [
            [{ header: [1, 2, false] }],
            ['bold', 'italic', 'underline', 'strike', 'blockquote'],
            [{ list: 'ordered' }, { list: 'bullet' }, { indent: '-1' }, { indent: '+1' }],
            ['link', { color: [] }]
        ]
    };
    return (
        <Grid container spacing={gridSpacing}>
            <Grid
                item
                xs={12}
                sx={{
                    '& .quill': {
                        bgcolor: theme.palette.mode === 'dark' ? 'dark.main' : 'grey.50',
                        borderRadius: '12px',
                        '& .ql-toolbar': {
                            bgcolor: theme.palette.mode === 'dark' ? 'dark.light' : 'grey.100',
                            borderColor: theme.palette.mode === 'dark' ? theme.palette.dark.light + 20 : 'primary.light',
                            borderTopLeftRadius: '12px',
                            borderTopRightRadius: '12px'
                        },
                        '& .ql-container': {
                            borderColor: theme.palette.mode === 'dark' ? `${theme.palette.dark.light + 20} !important` : 'primary.light',
                            borderBottomLeftRadius: '12px',
                            borderBottomRightRadius: '12px',
                            '& .ql-editor': {
                                minHeight: 135
                            }
                        }
                    }
                }}
            >
                {!skeleton ? <Component {...props} modules={modules} /> : <Skeleton height={150} />}
            </Grid>
        </Grid>
    );
};

export default InputEditor;
