import { FC } from 'react';
import InputTextFieldBase, { TextFieldBaseProps } from '../components/InputTextFieldBase';

const InputTextField: FC<TextFieldBaseProps> = ({ errorMessage, ...props }) => (
    <InputTextFieldBase errorMessage={errorMessage} {...props} />
);

export default InputTextField;
