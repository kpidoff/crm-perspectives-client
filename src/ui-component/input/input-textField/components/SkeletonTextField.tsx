import { FC } from 'react';
import { Skeleton, SkeletonProps } from '@mui/material';

const SkeletonTextField: FC<SkeletonProps> = ({ ...props }) => <Skeleton {...props} variant="rectangular" height={50} />;

export default SkeletonTextField;
