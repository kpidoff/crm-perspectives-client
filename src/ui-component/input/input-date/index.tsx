import { FC } from 'react';
import InputTextFieldBase, { TextFieldBaseProps } from '../components/InputTextFieldBase';
import { DatePicker, DatePickerProps } from '@mui/lab';

const InputDate: FC<{ errorMessage?: string; required?: boolean } & Omit<DatePickerProps, 'renderInput'> & TextFieldBaseProps> = ({
    errorMessage,
    ...props
}) => (
    <DatePicker
        inputFormat="dd/MM/yyyy"
        onChange={(value) => {
            props.onChange(value);
        }}
        value={props.value}
        renderInput={(params) => <InputTextFieldBase errorMessage={errorMessage} {...params} {...props} />}
    />
);

export default InputDate;
