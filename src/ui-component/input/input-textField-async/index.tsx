import { FC, ReactNode } from 'react';
import { CircularProgress, FormControl, InputLabel, OutlinedInput, OutlinedInputProps } from '@mui/material';
import { Box } from '@mui/system';
import DoneIcon from '@mui/icons-material/Done';
import CloseIcon from '@mui/icons-material/Close';
import SkeletonTextField from '../input-textField/components/SkeletonTextField';
import ErrorMessageInput from '../components/ErrorMessageInput';
import { LabelRequired } from '../components/InputTextFieldBase';

interface InputTextFieldSearchProps {
    errorMessage?: string;
    required?: boolean;
    skeleton?: boolean;
    loadingAsync: boolean;
    validateAsync: boolean;
    errorAsync: boolean;
}

const InputTextFieldSearch: FC<InputTextFieldSearchProps & Omit<OutlinedInputProps, 'endAdornment'>> = ({
    errorMessage,
    skeleton,
    required,
    loadingAsync,
    validateAsync,
    errorAsync,
    ...props
}) => {
    let endAdornment: ReactNode;
    if (loadingAsync) {
        endAdornment = (
            <Box sx={{ display: 'flex' }}>
                <CircularProgress size={20} />
            </Box>
        );
    }
    if (validateAsync && props.value) {
        endAdornment = <DoneIcon color="success" />;
    }
    if (errorAsync && props.value) {
        endAdornment = <CloseIcon color="error" />;
    }
    return !skeleton ? (
        <>
            <FormControl variant="outlined" fullWidth={props.fullWidth}>
                <InputLabel htmlFor={props.id}>
                    <LabelRequired label={props.label} required={required || false} />
                </InputLabel>
                <OutlinedInput
                    {...props}
                    endAdornment={endAdornment}
                    label={<LabelRequired label={props.label} required={required || false} />}
                />
            </FormControl>
            <ErrorMessageInput errorMessage={errorMessage} />
        </>
    ) : (
        <SkeletonTextField />
    );
};

export default InputTextFieldSearch;
