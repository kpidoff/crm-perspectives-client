import { InputAdornment } from '@mui/material';
import { FC } from 'react';
import { DEVISE } from 'constants/format';
import InputTextFieldBase, { TextFieldBaseProps } from '../components/InputTextFieldBase';

const InputPrice: FC<Omit<TextFieldBaseProps, 'capitalizeFirstCaractere'>> = ({ ...props }) => (
    <InputTextFieldBase
        {...props}
        type="number"
        InputProps={{
            endAdornment: <InputAdornment position="start">{DEVISE}</InputAdornment>
        }}
    />
);

export default InputPrice;
