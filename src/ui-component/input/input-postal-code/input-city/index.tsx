import Autocomplete from '@mui/material/Autocomplete';
import _ from 'lodash';
import { FC, useEffect, useState } from 'react';
import { CircularProgress } from '@mui/material';
import { PostalCode } from 'types/settings/postalCode';
import { DEBOUCE_TIME_OUT_POSTAL_CODE } from 'constants/debounce';
import { usePostalCodeByCity } from 'hooks/settings/postalCode/usePostalCode';
import InputTextFieldBase, { TextFieldBaseProps } from 'ui-component/input/components/InputTextFieldBase';

const NUMBER_CARACTERE_SEARCH = 2;

type TextFieldProps = {
    onChange?: (postalCode: string) => void;
    onSelect?: (postalCode: PostalCode) => void;
    value?: string | null;
};

const TextFieldCity: FC<Omit<TextFieldBaseProps, 'onSelect' | 'onChange'> & TextFieldProps> = ({ onChange, onSelect, value, ...props }) => {
    const { getPostalCodesByCity, postalCodes, loading } = usePostalCodeByCity();
    const [open, setOpen] = useState(false);
    const [valueInput, setValueInput] = useState(value || '');

    useEffect(() => {
        setValueInput(value || '');
    }, [value]);

    useEffect(() => {
        const debounce = _.debounce(() => {
            if (valueInput.length >= NUMBER_CARACTERE_SEARCH) {
                getPostalCodesByCity(valueInput);
            }
        }, DEBOUCE_TIME_OUT_POSTAL_CODE);

        debounce();

        return () => {
            debounce.cancel();
        };
    }, [valueInput, getPostalCodesByCity]);

    const handleChange = (valueChange: string) => {
        onChange && onChange(valueChange);
        setValueInput(valueChange);
    };

    const handleSelect = (valueSelect: any) => {
        if (valueSelect) {
            onChange && onChange(valueSelect.city);
            onSelect && onSelect(valueSelect);
            setValueInput(valueSelect.city);
        }
    };

    // if (valueInput.length >= NUMBER_CARACTERE_SEARCH && postalCodes?.length === 1) {
    //     onSelect && onSelect(postalCodes[0]);
    // }

    return (
        <Autocomplete
            open={open}
            onOpen={() => {
                setOpen(true);
            }}
            onClose={() => {
                setOpen(false);
            }}
            filterOptions={(x) => x}
            getOptionLabel={(option) => option.city}
            options={postalCodes || []}
            loading={loading}
            freeSolo
            onChange={(_event, valueChange) => {
                handleSelect(valueChange);
            }}
            onInputChange={(event, valueInputChange, reason) => {
                if (event && event.type === 'blur') {
                    handleChange('');
                } else if (reason !== 'reset') {
                    handleChange(valueInputChange);
                }
            }}
            loadingText="Chargement..."
            groupBy={(option) => option.postal_code}
            inputValue={valueInput}
            renderInput={(params) => (
                <>
                    <InputTextFieldBase
                        {...params}
                        {...props}
                        InputProps={{
                            ...params.InputProps,
                            endAdornment: (
                                <>
                                    {loading ? <CircularProgress color="inherit" size={20} /> : null}
                                    {params.InputProps.endAdornment}
                                </>
                            )
                        }}
                    />
                </>
            )}
        />
    );
};

export default TextFieldCity;
