import Autocomplete from '@mui/material/Autocomplete';
import _ from 'lodash';
import { FC, useEffect, useState } from 'react';
import { CircularProgress } from '@mui/material';
import removeAccents from 'remove-accents';
import { PostalCode } from 'types/settings/postalCode';
import { usePostalCodeByCode } from 'hooks/settings/postalCode/usePostalCode';
import InputTextFieldBase, { TextFieldBaseProps } from 'ui-component/input/components/InputTextFieldBase';
import { FormattedMessage } from 'react-intl';

const NUMBER_CARACTERE_SEARCH = 2;

type TextFieldPostalCodeProps = {
    onChange?: (postalCode: string) => void;
    onSelect?: (postalCode: PostalCode) => void;
    value?: string | null;
};

const TextFieldPostalCode: FC<Omit<TextFieldBaseProps, 'onSelect' | 'onChange'> & TextFieldPostalCodeProps> = ({
    onChange,
    onSelect,
    value,
    ...othersProps
}) => {
    const { getPostalCodesByCode, postalCodes, loading } = usePostalCodeByCode();
    const [open, setOpen] = useState(false);
    const [valueInput, setValueInput] = useState(value || '');

    useEffect(() => {
        setValueInput(value || '');
    }, [value]);

    useEffect(() => {
        const debounce = _.debounce(() => {
            if (valueInput.length >= NUMBER_CARACTERE_SEARCH) {
                getPostalCodesByCode(valueInput);
            }
        }, 150);

        debounce();

        return () => {
            debounce.cancel();
        };
    }, [valueInput, getPostalCodesByCode]);

    const handleChange = (valueChange: string) => {
        onChange && onChange(valueChange);
        setValueInput(valueChange);
    };

    const handleSelect = (valueSelect: any) => {
        if (valueSelect) {
            onChange && onChange(valueSelect.postal_code);
            onSelect && onSelect(valueSelect);
            setValueInput(valueSelect.postal_code);
        }
    };

    return (
        <Autocomplete
            open={open}
            onOpen={() => {
                setOpen(true);
            }}
            onClose={() => {
                setOpen(false);
            }}
            filterOptions={(x) => x}
            getOptionLabel={(option) => option.city}
            options={postalCodes || []}
            loading={loading}
            freeSolo
            onChange={(_event, valueChange) => {
                handleSelect(valueChange);
            }}
            onInputChange={(event, valueInputchange, reason) => {
                if (event && event.type === 'blur') {
                    handleChange('');
                } else if (reason !== 'reset') {
                    handleChange(valueInputchange);
                }
            }}
            loadingText={<FormattedMessage id="app-loading" />}
            groupBy={(option) => removeAccents(option.city[0].toUpperCase())}
            inputValue={valueInput}
            renderInput={(params) => (
                <>
                    <InputTextFieldBase
                        {...params}
                        {...othersProps}
                        InputProps={{
                            ...params.InputProps,
                            endAdornment: (
                                <>
                                    {loading ? <CircularProgress color="inherit" size={20} /> : null}
                                    {params.InputProps.endAdornment}
                                </>
                            )
                        }}
                    />
                </>
            )}
        />
    );
};

export default TextFieldPostalCode;
