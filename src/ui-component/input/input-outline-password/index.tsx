import { useTheme } from '@mui/material/styles';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import { FormControl, InputLabel, OutlinedInput, InputAdornment, IconButton, OutlinedInputProps, Grid, Typography } from '@mui/material';
import { useState, SyntheticEvent, FC } from 'react';
import { FormattedMessage } from 'react-intl';
import { Box } from '@mui/system';
import { strengthColor, strengthIndicator } from 'utils/password-strength';
import { StringColorProps } from 'types';
import ErrorMessageInput from '../components/ErrorMessageInput';

const InputOutlinePassword: FC<{ errorMessage?: string; strength?: boolean } & OutlinedInputProps> = ({
    errorMessage,
    strength,
    ...props
}) => {
    const theme = useTheme();
    const [showPassword, setShowPassword] = useState(false);
    const [strengthSize, setStrengthSize] = useState(0);
    const [level, setLevel] = useState<StringColorProps>();

    const changePassword = (value: string) => {
        const temp = strengthIndicator(value);
        setStrengthSize(temp);
        setLevel(strengthColor(temp));
    };

    const handleClickShowPassword = () => {
        setShowPassword(!showPassword);
    };

    const handleMouseDownPassword = (event: SyntheticEvent) => {
        event.preventDefault();
    };
    return (
        <>
            <FormControl fullWidth error={props.error} sx={{ ...theme.typography.customInput }}>
                <InputLabel htmlFor={props.id}>{props.label}</InputLabel>
                <OutlinedInput
                    {...props}
                    onChange={(e) => {
                        props.onChange && props.onChange(e);
                        strength && changePassword(e.target.value);
                    }}
                    type={showPassword ? 'text' : 'password'}
                    endAdornment={
                        <InputAdornment position="end">
                            <IconButton
                                aria-label="toggle password visibility"
                                onClick={handleClickShowPassword}
                                onMouseDown={handleMouseDownPassword}
                                edge="end"
                                size="large"
                            >
                                {showPassword ? <Visibility /> : <VisibilityOff />}
                            </IconButton>
                        </InputAdornment>
                    }
                    label={props.label}
                />
                <ErrorMessageInput errorMessage={errorMessage} />
            </FormControl>
            {strength && strengthSize !== 0 && (
                <FormControl fullWidth>
                    <Box sx={{ mb: 2 }}>
                        <Grid container spacing={2} alignItems="center">
                            <Grid item>
                                <Box
                                    style={{ backgroundColor: level?.color }}
                                    sx={{
                                        width: 85,
                                        height: 8,
                                        borderRadius: '7px'
                                    }}
                                />
                            </Grid>
                            <Grid item>
                                <Typography variant="subtitle1" fontSize="0.75rem">
                                    <FormattedMessage id={level?.label} />
                                </Typography>
                            </Grid>
                        </Grid>
                    </Box>
                </FormControl>
            )}
        </>
    );
};

export default InputOutlinePassword;
