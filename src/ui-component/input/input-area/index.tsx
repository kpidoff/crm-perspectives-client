import { InputAdornment } from '@mui/material';
import { AREA } from 'constants/format';
import { FC } from 'react';
import InputTextFieldBase, { TextFieldBaseProps } from '../components/InputTextFieldBase';

const InputArea: FC<Omit<TextFieldBaseProps, 'capitalizeFirstCaractere'>> = ({ ...props }) => (
    <InputTextFieldBase
        {...props}
        type="number"
        InputProps={{
            endAdornment: <InputAdornment position="start">{AREA}</InputAdornment>
        }}
    />
);

export default InputArea;
