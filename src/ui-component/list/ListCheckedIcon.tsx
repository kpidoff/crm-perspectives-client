import { List, ListItem, ListItemIcon, Grid, Skeleton } from '@mui/material';
import _ from 'lodash';
import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';

export type IconCheckedIconItemType = {
    label: string;
    checked: boolean | undefined;
};

export type IconCheckedIconProps = {
    items: IconCheckedIconItemType[];
    loading?: boolean;
};

const ListCheckedIcon = ({ items, loading }: IconCheckedIconProps) =>
    !loading ? (
        <List>
            {_.map(items, (item, index) => (
                <ListItem key={index}>
                    <ListItemIcon>
                        {item.checked ? <CheckIcon color="success" fontSize="small" /> : <CloseIcon color="error" fontSize="small" />}
                        {item.label}
                    </ListItemIcon>
                </ListItem>
            ))}
        </List>
    ) : (
        <List>
            {_.map(items, (item, index) => (
                <Grid key={index} container spacing={2}>
                    <Grid item xs={2}>
                        <Skeleton height={30} />
                    </Grid>
                    <Grid item xs={10}>
                        <Skeleton height={30} />
                    </Grid>
                </Grid>
            ))}
        </List>
    );

export default ListCheckedIcon;
