import { FC } from 'react';
import { FormControl, InputLabel, MenuItem, Select, SelectProps } from '@mui/material';
import ErrorMessageInput from './ErrorMessageSelect';
import { useTheme } from '@mui/material/styles';
import { SkeletonInput } from 'ui-component/skeleton';

export type SelectBaseProps = SelectProps & {
    required?: boolean;
    skeleton?: boolean;
    errorMessage?: string;
    label: React.ReactNode;
    children: React.ReactNode;
    enabledAll?: boolean;
    enabledNone?: boolean;
};

export function LabelRequired({ label, required }: { label: string | React.ReactNode; required: boolean }) {
    const theme = useTheme();
    if (required) {
        return (
            <span>
                {label}{' '}
                <span
                    style={{
                        color: theme.palette.mode === 'dark' ? theme.palette.error.light : theme.palette.error.dark
                    }}
                >
                    *
                </span>
            </span>
        );
    }
    return <span>{label}</span>;
}

const SelectBase: FC<SelectBaseProps> = ({
    errorMessage,
    skeleton,
    label,
    children,
    required,
    value,
    enabledAll,
    enabledNone,
    ...props
}) => {
    let valueGenerate = value;
    if (enabledAll && !value) {
        valueGenerate = 0;
    }
    if (!enabledAll && !value) {
        valueGenerate = '';
    }
    if (enabledNone && value === null) {
        valueGenerate = 0;
    }

    return !skeleton ? (
        <>
            <FormControl fullWidth>
                <InputLabel id="select-base-label">
                    <LabelRequired label={label} required={required || false} />
                </InputLabel>
                <Select
                    labelId="select-base-label"
                    {...props}
                    value={valueGenerate}
                    label={<LabelRequired label={label} required={required || false} />}
                >
                    {enabledAll && <MenuItem value={0}>Tous</MenuItem>}
                    {enabledNone && <MenuItem value={0}>Aucun</MenuItem>}
                    {children}
                </Select>
            </FormControl>
            <ErrorMessageInput errorMessage={errorMessage} />
        </>
    ) : (
        <SkeletonInput />
    );
};

export default SelectBase;
