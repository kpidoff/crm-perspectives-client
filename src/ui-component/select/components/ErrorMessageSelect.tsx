import { FormHelperText } from '@mui/material';
import { FormattedMessage } from 'react-intl';

const ErrorMessageSelect = ({ errorMessage }: { errorMessage: string | undefined }) =>
    errorMessage ? (
        <FormHelperText error id="standard-weight-helper-text-email-login">
            <FormattedMessage id={errorMessage} />
        </FormHelperText>
    ) : null;

export default ErrorMessageSelect;
