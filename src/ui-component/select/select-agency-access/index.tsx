import { MenuItem } from '@mui/material';
import useAuth from 'hooks/authentication/useAuth';
import _ from 'lodash';
import SelectBase, { SelectBaseProps } from '../components/SelectBase';

type SelectAgencyAccessProps = Omit<SelectBaseProps, 'onChange' | 'children'> & {
    label: React.ReactNode;
    value: number | undefined;
    onChange: (id: number) => void;
};

const SelectAgencyAccess = ({ label, onChange, ...props }: SelectAgencyAccessProps) => {
    const { user } = useAuth();
    return (
        <SelectBase {...props} label={label} onChange={(values) => onChange(_.toInteger(values.target.value))}>
            {_.map(user?.societiesAccess, (society) => (
                <MenuItem key={society.societyId} value={society.societyId}>
                    {society.name}
                </MenuItem>
            ))}
        </SelectBase>
    );
};

export default SelectAgencyAccess;
