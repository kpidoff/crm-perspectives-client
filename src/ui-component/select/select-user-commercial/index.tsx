// import { MenuItem } from '@mui/material';
// import useAuth from 'hooks/authentication/useAuth';
// import { useUsersCommercial } from 'hooks/settings/user/useUser';
// import _ from 'lodash';
// import { useEffect } from 'react';
// import { userFullName } from 'services/user.service';
// import SelectBase, { SelectBaseProps } from '../components/SelectBase';

// type SelectUserCommercialProps = Omit<SelectBaseProps, 'onChange' | 'children'> & {
//     label: React.ReactNode;
//     value: number | undefined | null;
//     onChange: (id: number) => void;
//     societyId: number | null;
// };

// const SelectUserCommercial = ({ label, onChange, societyId, value, ...props }: SelectUserCommercialProps) => {
//     const { getUsersCommercial, users } = useUsersCommercial();

//     useEffect(() => {
//         societyId &&
//             getUsersCommercial({
//                 societyId
//             });
//     }, [getUsersCommercial, societyId]);

//     return (
//         <SelectBase {...props} value={value || null} label={label} onChange={(values) => onChange(_.toInteger(values.target.value))}>
//             {_.map(users, (user) => (
//                 <MenuItem key={user.id} value={user.id}>
//                     {userFullName(user.lastname, user.firstname)}
//                 </MenuItem>
//             ))}
//         </SelectBase>
//     );
// };

// export default SelectUserCommercial;

import { CircularProgress, Autocomplete, AutocompleteProps } from '@mui/material';
import { useUsersCommercial } from 'hooks/settings/user/useUser';
import _ from 'lodash';
import { FC, useEffect } from 'react';
import { userFullName } from 'services/user.service';
import { UserProfile } from 'types/user';
import InputTextFieldBase from 'ui-component/input/components/InputTextFieldBase';
import { SkeletonInput } from 'ui-component/skeleton';

interface SelectUserCommercialProps {
    label?: string;
    value: number | null;
    onChange?: (user: UserProfile | null) => void;
    skeleton: boolean;
    societyId: number | undefined;
}

const SelectUserCommercial: FC<
    Omit<AutocompleteProps<UserProfile, undefined, undefined, undefined>, 'value' | 'renderInput' | 'options' | 'onChange'> &
        SelectUserCommercialProps
> = ({ value, label, onChange, skeleton, societyId, ...props }) => {
    const { getUsersCommercial, users, loading } = useUsersCommercial();

    useEffect(() => {
        societyId &&
            getUsersCommercial({
                societyId
            });
    }, [getUsersCommercial, societyId]);

    return !skeleton ? (
        <Autocomplete
            getOptionLabel={(option) => userFullName(option.lastname, option.firstname)}
            options={users || []}
            isOptionEqualToValue={(optionUser, valueUser) => optionUser?.id === valueUser?.id}
            value={!loading && users ? _.find(users, (user) => user.id === value) || null : null}
            loading={loading}
            onChange={(__, user) => {
                onChange && onChange(user || null);
            }}
            {...props}
            renderInput={(params) => (
                <InputTextFieldBase
                    {...params}
                    label={label}
                    InputProps={{
                        ...params.InputProps,
                        endAdornment: (
                            <>
                                {loading ? <CircularProgress color="inherit" size={20} /> : null}
                                {params.InputProps.endAdornment}
                            </>
                        )
                    }}
                />
            )}
        />
    ) : (
        <SkeletonInput />
    );
};

export default SelectUserCommercial;
