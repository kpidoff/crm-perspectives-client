import { CircularProgress, Autocomplete, AutocompleteProps } from '@mui/material';
import { useRoles } from 'hooks/settings/role/useRole';
import _ from 'lodash';
import { FC, useEffect } from 'react';
import { Role } from 'types/settings/role';
import InputTextFieldBase from 'ui-component/input/components/InputTextFieldBase';
import { SkeletonInput } from 'ui-component/skeleton';

interface SelectRoleProps {
    label?: string;
    value: number | null;
    onChange?: (role: Role | null) => void;
    skeleton: boolean;
}

const SelectRole: FC<
    Omit<AutocompleteProps<Role, undefined, undefined, undefined>, 'value' | 'renderInput' | 'options' | 'onChange'> & SelectRoleProps
> = ({ value, label, onChange, skeleton, ...props }) => {
    const { getRoles, roles, loading } = useRoles();

    useEffect(() => {
        getRoles();
    }, [getRoles]);

    return !skeleton ? (
        <Autocomplete
            getOptionLabel={(option) => option.name || ''}
            options={roles || []}
            isOptionEqualToValue={(optionRole, valueRole) => optionRole?.id === valueRole?.id}
            value={!loading && roles ? _.find(roles, (role) => role.id === value) || null : null}
            loading={loading}
            onChange={(__, role) => {
                onChange && onChange(role || null);
            }}
            {...props}
            renderInput={(params) => (
                <InputTextFieldBase
                    {...params}
                    label={label}
                    InputProps={{
                        ...params.InputProps,
                        endAdornment: (
                            <>
                                {loading ? <CircularProgress color="inherit" size={20} /> : null}
                                {params.InputProps.endAdornment}
                            </>
                        )
                    }}
                />
            )}
        />
    ) : (
        <SkeletonInput />
    );
};

export default SelectRole;
