import { Marker, MarkerProps } from '@react-google-maps/api';
import { CursorMapType, CursorsMapType, CURSORS_MAP, CURSORS_MAP_NUMBER, CURSORS_MAP_STAR } from 'constants/cursor';

export type MarkerIconProps = Omit<MarkerProps, 'icon'> & {
    icon?: {
        type: CursorMapType;
        icon: CursorsMapType;
    };
};

const MarkerIcon = ({ icon, ...props }: MarkerIconProps) => {
    function generateIcon() {
        if (icon) {
            switch (icon.type) {
                case 'default':
                    return CURSORS_MAP[icon.icon];
                case 'star':
                    return CURSORS_MAP_STAR[icon.icon];
                case 'number':
                    return CURSORS_MAP_NUMBER[icon.icon];
            }
        }
        return CURSORS_MAP.RED;
    }

    return (
        <Marker
            {...props}
            icon={{
                url: generateIcon(),
                anchor: new google.maps.Point(11, 35),
                scaledSize: new google.maps.Size(20, 35)
            }}
        />
    );
};

export default MarkerIcon;
