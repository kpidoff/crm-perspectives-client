import { GoogleMap, useJsApiLoader } from '@react-google-maps/api';
import config from 'config';
import { DEFAULT_GEOMETRY } from 'constants/geometry';
import { GeometryType } from 'hooks/utils/useGeocode';

type GoogleMapWrapperProps = {
    children?: JSX.Element;
    center?: {
        lat?: number;
        lng?: number;
    };
    containerStyle?: React.CSSProperties;
};

const GoogleMapWrapper = ({
    center = DEFAULT_GEOMETRY,
    children,
    containerStyle = {
        width: '100%',
        height: '400px'
    }
}: GoogleMapWrapperProps) => {
    const { isLoaded } = useJsApiLoader({
        id: 'google-map-script',
        googleMapsApiKey: config.keyGoogleMap
    });

    function generateCenter(): GeometryType {
        if (center && center.lat && center.lng) {
            return {
                lat: center.lat,
                lng: center.lng
            };
        }

        return DEFAULT_GEOMETRY;
    }

    return isLoaded ? (
        <GoogleMap mapContainerStyle={containerStyle} center={generateCenter()} zoom={10}>
            {children}
        </GoogleMap>
    ) : (
        <></>
    );
};

export default GoogleMapWrapper;
