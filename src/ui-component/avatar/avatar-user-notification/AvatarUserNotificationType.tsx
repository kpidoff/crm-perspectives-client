import { Avatar } from '@mui/material';
import { useTheme } from '@mui/material/styles';
import { UserNotificationTypeEnum } from 'types/user';
import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import InfoIcon from '@mui/icons-material/Info';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import WarningIcon from '@mui/icons-material/Warning';
import ReportIcon from '@mui/icons-material/Report';

const AvatarUserNotificationType = ({ type, height, width }: { type: UserNotificationTypeEnum; height?: number; width?: number }) => {
    const theme = useTheme();

    switch (type) {
        case 'ERROR':
            return (
                <Avatar sx={{ bgcolor: theme.palette.error.light, width, height }}>
                    <ErrorOutlineIcon color="error" />
                </Avatar>
            );
        case 'INFO':
            return (
                <Avatar sx={{ bgcolor: theme.palette.info.light, width, height }}>
                    <InfoIcon color="info" />
                </Avatar>
            );
        case 'VALID':
            return (
                <Avatar sx={{ bgcolor: theme.palette.success.light, width, height }}>
                    <CheckCircleOutlineIcon color="success" />
                </Avatar>
            );
        case 'WARNING':
            return (
                <Avatar sx={{ bgcolor: theme.palette.warning.light, width, height }}>
                    <WarningIcon color="warning" />
                </Avatar>
            );
        default:
            return (
                <Avatar sx={{ bgcolor: theme.palette.warning.light, width, height }}>
                    <ReportIcon color="warning" />
                </Avatar>
            );
    }
};

export default AvatarUserNotificationType;
