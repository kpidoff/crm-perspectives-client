import { CardMedia as CardMediaMui, CardMediaProps as CardMediaMuiProps, Skeleton, useMediaQuery } from '@mui/material';
import { useTheme } from '@mui/material/styles';
import NoImageCard from './cards/NoImageCard';

export type CardMediaProps = Omit<CardMediaMuiProps, 'image'> & {
    loading?: boolean;
    image: string | null;
};

const CardMedia = ({ loading = false, image, ...props }: CardMediaProps) => {
    const theme = useTheme();
    const matchDownSM = useMediaQuery(theme.breakpoints.down('sm'));
    if (loading) {
        return <Skeleton variant="rectangular" height={matchDownSM ? '200px' : '100%'} />;
    }
    if (!image) {
        return <NoImageCard sx={{ ...props.sx, height: matchDownSM ? '200px' : '100%', width: '100%' }} />;
    }
    console.log(matchDownSM);
    return <CardMediaMui {...props} image={image} sx={{ ...props.sx, height: matchDownSM ? '200px' : '100%', width: '100%' }} />;
};

export default CardMedia;
