import { Button, ButtonProps, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Typography } from '@mui/material';
import { FC, useState } from 'react';
import { useTheme } from '@mui/material/styles';
import { LoadingButton } from '@mui/lab';

export type ButtonConfirmProps = {
    title: JSX.Element | string;
    content: JSX.Element | string;
    labelButtonConfirm: JSX.Element | string;
    labelButtonCancel: JSX.Element | string;
    onValidate: () => void;
    onCancel?: () => void;
    loading?: boolean;
};
const ButtonConfirm: FC<Omit<ButtonProps, 'onClick' | 'title'> & ButtonConfirmProps> = ({
    title,
    content,
    labelButtonConfirm,
    labelButtonCancel,
    children,
    onValidate,
    onCancel,
    loading,
    ...props
}) => {
    const theme = useTheme();
    const [open, setOpen] = useState(false);
    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
        onCancel?.();
    };

    const handleConfirm = () => {
        onValidate();
    };

    return (
        <>
            <Button {...props} onClick={handleClickOpen}>
                {children}
            </Button>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                sx={{ p: 3 }}
            >
                {open && (
                    <>
                        <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                <Typography variant="body2" component="span">
                                    {content}
                                </Typography>
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions sx={{ pr: 2.5 }}>
                            <Button
                                sx={{ color: theme.palette.error.dark, borderColor: theme.palette.error.dark }}
                                onClick={handleClose}
                                color="secondary"
                            >
                                {labelButtonCancel}
                            </Button>
                            <LoadingButton loading={loading} variant="contained" size="small" onClick={handleConfirm} autoFocus>
                                {labelButtonConfirm}
                            </LoadingButton>
                        </DialogActions>
                    </>
                )}
            </Dialog>
        </>
    );
};

export default ButtonConfirm;
