import { Button, CardContent, Divider, Grid, Stack, useMediaQuery } from '@mui/material';
import { DATE } from 'constants/date';
import _ from 'lodash';
import { FormattedMessage } from 'react-intl';
import Moment from 'react-moment';
import { landDefaultImageUrl } from 'services/contacts/lands/land.service';
import { gridSpacing } from 'store/constant';
import { Land } from 'types/contacts/land';
import CardMedia from 'ui-component/CardMedia';
import MainCard from 'ui-component/cards/MainCard';
import { textTruncate } from 'ui-component/datas-components/datasComponents.utils';
import ListCheckedIcon, { IconCheckedIconItemType } from 'ui-component/list/ListCheckedIcon';
import Rating from 'ui-component/rate/Rating';
import Typography from 'ui-component/typography/Typography';
import { formatArea, formatPrice } from 'utils';

const CardLand = ({ land, loading = false }: { land: Land | undefined; loading?: boolean }) => {
    const items: IconCheckedIconItemType[] = [
        {
            label: 'Viabilisé',
            checked: land?.serviced
        },
        {
            label: "Tout à l'égout",
            checked: land?.sewer
        },
        {
            label: 'Bornage',
            checked: land?.bounding
        }
    ];

    return (
        <MainCard
            content={false}
            boxShadow
            sx={{
                '&:hover': {
                    transform: 'scale3d(1.02, 1.02, 1)',
                    transition: 'all .4s ease-in-out'
                }
            }}
        >
            <Grid container spacing={gridSpacing}>
                <Grid item xs={12} md={9}>
                    <Grid container>
                        <Grid item xs={12} md={3}>
                            <CardMedia loading={loading} image={landDefaultImageUrl(land)} />
                        </Grid>
                        <Grid item xs={9}>
                            <CardContent sx={{ p: 2 }}>
                                <Grid item xs={12}>
                                    <Stack
                                        direction="row"
                                        spacing={1}
                                        divider={
                                            <Typography loading={loading} variant="h6" sx={{ color: 'grey.500', paddingTop: '2px' }}>
                                                -
                                            </Typography>
                                        }
                                    >
                                        <Typography
                                            loading={loading}
                                            skeletonProps={{
                                                sx: {
                                                    width: '20%'
                                                }
                                            }}
                                            variant="subtitle1"
                                            sx={{ textDecoration: 'none' }}
                                        >
                                            <Moment format={DATE}>{land?.createdDate}</Moment>
                                        </Typography>

                                        <Typography
                                            loading={loading}
                                            skeletonProps={{
                                                sx: {
                                                    width: '20%'
                                                }
                                            }}
                                            variant="h6"
                                            sx={{ color: 'grey.500', paddingTop: '3px' }}
                                        >
                                            {_.capitalize(land?.city)}
                                        </Typography>
                                    </Stack>
                                </Grid>
                                <Grid item xs={12}>
                                    <Typography
                                        loading={loading}
                                        variant="body2"
                                        sx={{
                                            overflow: 'hidden'
                                        }}
                                    >
                                        {textTruncate(land?.description)}
                                    </Typography>
                                </Grid>
                                <ListCheckedIcon loading={loading} items={items} />
                                <Grid item xs={12}>
                                    <Typography loading={loading} variant="h6" sx={{ color: 'grey.500' }}>
                                        <FormattedMessage id="area" />: {formatArea(land?.area)}
                                    </Typography>
                                </Grid>
                            </CardContent>
                        </Grid>
                    </Grid>
                </Grid>

                <Grid item xs={12} md={3}>
                    <Divider orientation="vertical" flexItem sx={{ height: '50px' }} />
                    <CardContent sx={{ p: 2 }}>
                        <Stack spacing={2} direction="column" justifyContent="center" alignItems="center">
                            <Typography loading={loading} variant="h4">
                                {formatPrice(land?.price)}
                            </Typography>
                            <Button variant="outlined" fullWidth>
                                Archiver
                            </Button>
                            <Button variant="contained" fullWidth>
                                Modifier
                            </Button>
                        </Stack>
                    </CardContent>
                </Grid>
            </Grid>
        </MainCard>
    );
};

export default CardLand;
