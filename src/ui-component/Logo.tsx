// material-ui
import logo from '../assets/Logo.png';

/**
 * if you want to use image instead of <svg> uncomment following.
 *
 * import logoDark from 'assets/images/logo-dark.svg';
 * import logo from 'assets/images/logo.svg';
 *
 */

// ==============================|| LOGO SVG ||============================== //

const Logo = () => <img src={logo} style={{ marginLeft: 20, marginTop: -2 }} height={50} alt="logo" />;

export default Logo;
