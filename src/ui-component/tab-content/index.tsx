import { Tabs, Tab, Badge } from '@mui/material';
import { Box } from '@mui/system';
import _ from 'lodash';
import { useState } from 'react';
import { useTheme } from '@mui/material/styles';
import { Link } from 'react-router-dom';

export interface TopContentProps {
    title: string;
    component: JSX.Element | null | undefined;
    skeleton?: JSX.Element;
    error?: boolean;
    icon?: JSX.Element;
    disabled?: boolean;
}

interface TabTopProps {
    content: TopContentProps[];
    loading?: boolean;
    children?: JSX.Element;
}

interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
}

function a11yProps(index: number) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`
    };
}

function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;

    return (
        <div role="tabpanel" hidden={value !== index} id={`simple-tabpanel-${index}`} aria-labelledby={`simple-tab-${index}`} {...other}>
            {value === index && <Box sx={{ p: 3 }}>{children}</Box>}
        </div>
    );
}

const TabContent = ({ children, content, loading }: TabTopProps) => {
    const theme = useTheme();
    const [value, setValue] = useState(0);

    const handleChange = (event: React.SyntheticEvent, newValue: number) => {
        setValue(newValue);
    };

    return (
        <>
            <Tabs
                value={value}
                indicatorColor="primary"
                textColor="primary"
                onChange={handleChange}
                aria-label="simple tabs example"
                variant="scrollable"
                sx={{
                    mb: 3,
                    '& a': {
                        minHeight: 'auto',
                        minWidth: 10,
                        py: 1.5,
                        px: 1,
                        mr: 2.25,
                        color: theme.palette.grey[600],
                        display: 'flex',
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center'
                    },
                    '& a.Mui-selected': {
                        color: theme.palette.primary.main
                    },
                    '& .MuiTabs-indicator': {
                        bottom: 2
                    },
                    '& a > svg': {
                        marginBottom: '0px !important',
                        mr: 1.25
                    }
                }}
            >
                {children}
                {content.map((tab, index) => (
                    <Tab
                        disabled={tab.disabled}
                        key={index}
                        icon={tab.icon}
                        component={Link}
                        to="#"
                        label={
                            <Badge sx={{ paddingRight: '8px' }} badgeContent={tab.error ? ' ' : 0} color="error" variant="dot">
                                {tab.title}
                            </Badge>
                        }
                        {...a11yProps(index)}
                    />
                ))}
            </Tabs>
            {_.map(content, (c, index) =>
                !c.disabled ? (
                    <TabPanel key={index} value={value} index={index}>
                        {c.skeleton && loading ? c.skeleton : c.component}
                    </TabPanel>
                ) : null
            )}
        </>
    );
};

export default TabContent;
