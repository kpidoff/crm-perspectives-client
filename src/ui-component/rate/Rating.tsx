import { Skeleton, Rating as RatingMui, RatingProps as RatingPropsMui, Stack } from '@mui/material';
import _ from 'lodash';

export type RateProps = RatingPropsMui & {
    loading?: boolean;
};

const Rating = ({ loading, ...props }: RateProps) =>
    loading ? (
        <Stack direction="row" alignItems="center" spacing={1}>
            {_.map(Array(props.max || 5), (__, index) => (
                <Skeleton key={index} width={12} />
            ))}
        </Stack>
    ) : (
        <RatingMui {...props} />
    );

export default Rating;
