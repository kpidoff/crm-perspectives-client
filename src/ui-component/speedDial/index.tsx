import { useTheme } from '@mui/material/styles';

import SpeedDialIcon from '@mui/material/SpeedDialIcon';
import { useState } from 'react';
import { SpeedDial, SpeedDialAction, Tooltip } from '@mui/material';
import _ from 'lodash';

export type SpeedDialButtonType = {
    icon?: JSX.Element;
    name: string | JSX.Element;
    action: () => void;
    hidden?: boolean;
};

interface SpeedDialButtonProps {
    actions: SpeedDialButtonType[];
}

const SpeedDialButton = ({ actions }: SpeedDialButtonProps) => {
    const theme = useTheme();
    const [open, setOpen] = useState(false);

    const handleClose = () => {
        setOpen(false);
    };

    const handleClick = (action: () => void) => {
        setOpen(false);
        action();
    };

    const handleOpen = () => {
        setOpen(true);
    };

    const actionsFilterHidden = _.filter(actions, (action) => action.hidden !== true);

    return (
        <Tooltip title={actions.length === 1 && actions[0].name} placement="right">
            <SpeedDial
                sx={{
                    position: 'fixed',
                    paddingRight: '10px',
                    '&.MuiSpeedDial-directionUp, &.MuiSpeedDial-directionLeft': {
                        bottom: theme.spacing(2),
                        right: theme.spacing(2)
                    },
                    '&.MuiSpeedDial-directionDown, &.MuiSpeedDial-directionRight': {
                        top: theme.spacing(2),
                        left: theme.spacing(2)
                    }
                }}
                ariaLabel="SpeedDial"
                icon={<SpeedDialIcon />}
                onClick={actions.length === 1 ? () => handleClick(actions[0].action) : undefined}
                onClose={handleClose}
                onOpen={handleOpen}
                open={open}
                hidden={actionsFilterHidden.length === 0}
            >
                {actions.length > 1 &&
                    _.map(actionsFilterHidden, (action, index) => (
                        <SpeedDialAction
                            key={index}
                            icon={action.icon}
                            tooltipTitle={action.name}
                            onClick={() => handleClick(action.action)}
                        />
                    ))}
            </SpeedDial>
        </Tooltip>
    );
};

export default SpeedDialButton;
