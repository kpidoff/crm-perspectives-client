import { CardMedia, CardMediaProps } from '@mui/material';
import { useTheme } from '@mui/material/styles';
import NoImageGrey from 'assets/images/no-image/no-image-grey.png';
import NoImageWhite from 'assets/images/no-image/no-image-white.png';

const NoImageCard = ({ ...props }: Omit<CardMediaProps, 'image' | 'title'>) => {
    const theme = useTheme();
    return <CardMedia {...props} image={theme.palette.mode === 'dark' ? NoImageWhite : NoImageGrey} title="No image" />;
};

export default NoImageCard;
