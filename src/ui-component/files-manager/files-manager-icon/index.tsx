import { defaultStyles, FileIcon } from 'react-file-icon';
import styleDefObj from './style-customize';

const FileManagerIcon = ({ ext, size }: { ext: string; size?: number }) => {
    const customDefaultLabelColor = styleDefObj[ext] ? styleDefObj[ext].labelColor ?? '#777' : '#777';
    // @ts-ignore
    const libDefaultGlyphColor = defaultStyles[ext] && defaultStyles[ext].labelColor;
    return (
        <div style={{ width: size, marginRight: '10px' }}>
            <FileIcon
                extension={ext}
                glyphColor={libDefaultGlyphColor ?? customDefaultLabelColor}
                labelColor={customDefaultLabelColor}
                // @ts-ignore
                {...defaultStyles[ext]}
                {...styleDefObj[ext]}
            />
        </div>
    );
};

export default FileManagerIcon;
