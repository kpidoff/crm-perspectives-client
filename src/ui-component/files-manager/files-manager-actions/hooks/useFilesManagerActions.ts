import { useFilesManagerDownload, useFilesManagerUpload } from 'hooks/utils/useFileManager';
import _ from 'lodash';
import { useEffect, useState } from 'react';
import { base64ToBlob, fileInputChange, FileType } from 'utils/base64';
import { toastError } from 'utils/toast';
import { saveAs } from 'file-saver';

export function useFilesManagerActionDownload() {
    const { downloadFiles, filesManager } = useFilesManagerDownload();

    useEffect(() => {
        if (filesManager) {
            console.log(filesManager);
            _.map(filesManager, (file) => {
                base64ToBlob(file.base64, file.type).then((value) => {
                    saveAs(value, file.name);
                });
            });
        }
    }, [filesManager]);

    return {
        downloadFiles
    };
}

export function useFilesManagerActionUpload() {
    const { uploadFiles: uploadFilesServer } = useFilesManagerUpload();
    const [loadingUpload, setLoadinUpload] = useState(false);
    const [files, setFiles] = useState<FileType[] | undefined>(undefined);
    const [currentParentId, setCurrentParentId] = useState<number | undefined>(undefined);

    function uploadFiles(filesList: FileList | null, parentId: number) {
        if (!filesList) return;
        setCurrentParentId(parentId);
        setLoadinUpload(true);
        let numberFiles = 0;
        const filesGenerate: FileType[] = [];
        _.map(filesList, (file) => {
            fileInputChange(file)
                .then((values) => {
                    values && filesGenerate.push(values);
                })
                .catch((reason) => {
                    toastError(`Erreur lors de la convertion du fichier : ${file.name} celui-ci ne sera pas envoyé sur le serveur`);
                })
                .finally(() => {
                    numberFiles += 1;
                    if (numberFiles === filesList?.length) {
                        setFiles(filesGenerate);
                        setLoadinUpload(false);
                    }
                });
        });
    }

    useEffect(() => {
        if (files) {
            uploadFilesServer(files, currentParentId);
            setFiles(undefined);
        }
    }, [currentParentId, files, uploadFilesServer]);

    return {
        uploadFiles,
        loadingUpload
    };
}
