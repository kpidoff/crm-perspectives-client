import { useFilesManagerDelete } from 'hooks/utils/useFileManager';
import _ from 'lodash';
import { useCallback, useState } from 'react';
import { FileManager } from 'types/utils/fileManager';
import { useFilesManagerContext } from '../filesManager.context';
import FilesManagerActionAddFolder from './component/files-manager-action-add-folder';
import FilesManagerActionRename from './component/files-manager-action-rename';
import { filesManagerActionsContext } from './filesManagerActions.context';
import { FilesManagerActionDefaultAction, FilesManagerActionsProps } from './filesManagerActions.type';
import { useFilesManagerActionDownload, useFilesManagerActionUpload } from './hooks/useFilesManagerActions';

const FilesManagerActions = ({ actions, children }: FilesManagerActionsProps) => {
    const { fileManagerRootId } = useFilesManagerContext();
    const { deleteFiles } = useFilesManagerDelete();
    const { downloadFiles } = useFilesManagerActionDownload();
    const { uploadFiles } = useFilesManagerActionUpload();
    const [openModalAddFolder, setOpenModalAddFolder] = useState(false);
    const [openModalRename, setOpenModalRename] = useState<FileManager | null>(null);

    const [selected, setSelected] = useState<FileManager[]>([]);

    const onSelect = useCallback(
        (fileManager: FileManager, event: React.MouseEvent<HTMLTableRowElement | HTMLButtonElement, MouseEvent> | undefined) => {
            if (event && event.ctrlKey) {
                setSelected((prevState) => {
                    if (_.find(prevState, (state) => state.id === fileManager.id)) {
                        return _.filter(prevState, (state) => state.id !== fileManager.id);
                    }
                    return [...prevState, fileManager];
                });
            } else {
                setSelected([fileManager]);
            }
        },
        []
    );

    const addSelect = useCallback((fileManager: FileManager) => {
        setSelected((prevState) => {
            if (_.find(prevState, (state) => state.id === fileManager.id)) {
                return _.filter(prevState, (state) => state.id !== fileManager.id);
            }
            return [...prevState, fileManager];
        });
    }, []);

    const isSelected = useCallback((id: number) => !!_.find(selected, (select) => select.id === id), [selected]);

    const actionsFolder = _.filter(actions, (action) => action.type === 'folder' || action.type === 'all');
    const actionsFile = _.filter(actions, (action) => action.type === 'file' || action.type === 'all');

    const defaultActions: FilesManagerActionDefaultAction[] = [
        {
            key: 'onDelete',
            onClick: (files) => deleteFiles(_.map(files, (file) => file.id)),
            selection: 'multiple'
        },
        {
            key: 'onDownload',
            onClick: (files) => downloadFiles(_.map(files, (file) => file.id)),
            selection: 'multiple'
        },
        {
            key: 'onView',
            onClick: (files) => console.log('ok'),
            selection: 'unique'
        },
        {
            key: 'onRename',
            onClick: (file) => setOpenModalRename(file),
            selection: 'unique'
        }
    ];

    return (
        <filesManagerActionsContext.Provider
            value={{
                actions,
                actionsFile,
                actionsFolder,
                defaultActions,
                addFolder: () => setOpenModalAddFolder(true),
                addFiles: (e) => uploadFiles(e.target.files, selected.length > 0 ? selected[0].id : fileManagerRootId),
                isSelected,
                onSelect,
                addSelect,
                selected
            }}
        >
            <>
                <FilesManagerActionAddFolder
                    parentId={(selected.length > 0 && selected[0].id) || fileManagerRootId}
                    open={openModalAddFolder}
                    onClose={() => setOpenModalAddFolder(false)}
                />
                {openModalRename && <FilesManagerActionRename file={openModalRename} onClose={() => setOpenModalRename(null)} />}
                {/* {selected.length > 0 && selected[0].id && <FilesManagerActionViewVile file={selected[0]} />} */}
                {children}
            </>
        </filesManagerActionsContext.Provider>
    );
};

export default FilesManagerActions;
