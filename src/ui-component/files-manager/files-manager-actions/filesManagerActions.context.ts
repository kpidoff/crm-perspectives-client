import { createContext, useContext } from 'react';
import { FilesManagerActionsContextProps } from './filesManagerActions.type';

export const filesManagerActionsContext = createContext<FilesManagerActionsContextProps | null>(null);

export const useFilesManagerActionsContext = () => {
    const context = useContext(filesManagerActionsContext);

    if (!context) throw new Error('Context file manager actions must be use inside provider');

    return context;
};
