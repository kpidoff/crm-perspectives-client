import * as Yup from 'yup';

const validationSchema = Yup.object().shape({
    name: Yup.string().required('required-field')
});

export default validationSchema;
