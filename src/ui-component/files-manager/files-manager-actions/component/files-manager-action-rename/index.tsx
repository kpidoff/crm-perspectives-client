import { LoadingButton } from '@mui/lab';
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button, Grid } from '@mui/material';
import { Form, Formik } from 'formik';
import { useFilesManagerRename } from 'hooks/utils/useFileManager';
import { useEffect } from 'react';
import { FormattedMessage } from 'react-intl';
import { gridSpacing } from 'store/constant';
import { FileManager } from 'types/utils/fileManager';
import { InputTextField } from 'ui-component/input';
import validationSchema from './validation';
import path from 'path';
import { isFolder } from 'ui-component/files-manager/utils';

const FilesManagerActionRename = ({ onClose, file }: { file: FileManager; onClose: () => void }) => {
    const { renameFile, loadingRename, fileManager, reset } = useFilesManagerRename();

    useEffect(() => {
        if (fileManager) {
            onClose();
            reset();
        }
    }, [fileManager, onClose, reset]);

    return (
        <Dialog open={!!file} onClose={onClose}>
            <DialogTitle>
                {isFolder(file) ? (
                    <FormattedMessage id="fileManager-modal-title-folder" />
                ) : (
                    <FormattedMessage id="fileManager-modal-title-file" />
                )}
            </DialogTitle>
            <Formik
                initialValues={{ name: file.name.replace(/\.[^/.]+$/, '') }}
                validationSchema={validationSchema}
                onSubmit={(values) => {
                    renameFile(file.id, values.name + path.extname(file.name));
                }}
            >
                {({ errors, handleBlur, handleChange, touched, values, isValid, dirty, setFieldValue }) => (
                    <Form>
                        <DialogContent>
                            <Grid container spacing={gridSpacing}>
                                <Grid item xs={12}>
                                    <DialogContentText>
                                        {isFolder(file) ? (
                                            <FormattedMessage id="fileManager-modal-text-folder" />
                                        ) : (
                                            <FormattedMessage id="fileManager-modal-text-file" />
                                        )}
                                    </DialogContentText>
                                </Grid>
                                <Grid item xs={12}>
                                    <InputTextField
                                        value={values.name}
                                        name="name"
                                        id="name"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        label={isFolder(file) ? <FormattedMessage id="name-folder" /> : <FormattedMessage id="name-file" />}
                                        error={Boolean(touched.name && errors.name)}
                                        errorMessage={errors.name}
                                        required
                                        fullWidth
                                    />
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <DialogActions>
                            <Button variant="contained" color="error" onClick={onClose}>
                                <FormattedMessage id="cancel" />
                            </Button>
                            <LoadingButton disabled={!isValid || !dirty} loading={loadingRename} type="submit">
                                <FormattedMessage id="rename" />
                            </LoadingButton>
                        </DialogActions>
                    </Form>
                )}
            </Formik>
        </Dialog>
    );
};

export default FilesManagerActionRename;
