import { LoadingButton } from '@mui/lab';
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button, Grid } from '@mui/material';
import { Form, Formik } from 'formik';
import { useFileManagerCreateFolder } from 'hooks/utils/useFileManager';
import { useEffect } from 'react';
import { FormattedMessage } from 'react-intl';
import { gridSpacing } from 'store/constant';
import { InputTextField } from 'ui-component/input';
import validationSchema from './validation';

const FilesManagerActionAddFolder = ({ open, onClose, parentId }: { parentId: number; open: boolean; onClose: () => void }) => {
    const { createFolder, loading, fileManager, reset } = useFileManagerCreateFolder();

    useEffect(() => {
        if (fileManager) {
            onClose();
            reset();
        }
    }, [fileManager, onClose, reset]);

    return open ? (
        <Dialog open={open} onClose={onClose}>
            <DialogTitle>Création d&apos;un nouveau répertoire</DialogTitle>
            <Formik
                initialValues={{ name: '' }}
                validationSchema={validationSchema}
                onSubmit={(values) => {
                    createFolder(values.name, parentId);
                }}
            >
                {({ errors, handleBlur, handleChange, touched, values, isValid, dirty, setFieldValue }) => (
                    <Form>
                        <DialogContent>
                            <Grid container spacing={gridSpacing}>
                                <Grid item xs={12}>
                                    <DialogContentText>
                                        Saisissez le nom du répertoire que vous souhaitez ajouter à la gestion de la documentation.
                                    </DialogContentText>
                                </Grid>
                                <Grid item xs={12}>
                                    <InputTextField
                                        value={values.name}
                                        name="name"
                                        id="name"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        label="Nom du répertoire"
                                        error={Boolean(touched.name && errors.name)}
                                        errorMessage={errors.name}
                                        required
                                        fullWidth
                                    />
                                </Grid>
                            </Grid>
                        </DialogContent>
                        <DialogActions>
                            <Button variant="contained" color="error" onClick={onClose}>
                                <FormattedMessage id="cancel" />
                            </Button>
                            <LoadingButton disabled={!isValid || !dirty} loading={loading} type="submit">
                                <FormattedMessage id="add" />
                            </LoadingButton>
                        </DialogActions>
                    </Form>
                )}
            </Formik>
        </Dialog>
    ) : null;
};

export default FilesManagerActionAddFolder;
