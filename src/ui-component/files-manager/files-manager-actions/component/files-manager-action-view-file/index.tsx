import { FileManager } from 'types/utils/fileManager';
import { useFilesManagerDownload } from 'hooks/utils/useFileManager';
import { useEffect } from 'react';

const FilesManagerActionViewVile = ({ file }: { file: FileManager }) => {
    const { downloadFiles, filesManager } = useFilesManagerDownload();

    useEffect(() => {
        downloadFiles([file.id]);
    }, [downloadFiles, file.id]);

    return filesManager ? (
        <>
            {/* <FileViewer fileType="jpg" filePath="https://192.168.1.101/1/90f0ad05fb111f9a77a9706af80807075e96a204.jpg" /> */}
            <p>test</p>
        </>
    ) : null;
};

export default FilesManagerActionViewVile;
