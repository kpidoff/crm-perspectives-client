import { ReactNode } from 'react';
import { FileManager } from 'types/utils/fileManager';

export type FilesManagerActionsContextProps = {
    actions?: FilesManagerActionType[];
    actionsFolder: FilesManagerActionType[];
    actionsFile: FilesManagerActionType[];
    defaultActions: FilesManagerActionDefaultAction[];
    addFolder: () => void;
    selected: FileManager[];
    onSelect: (fileManager: FileManager, event?: React.MouseEvent<HTMLTableRowElement | HTMLButtonElement, MouseEvent>) => void;
    addSelect: (fileManager: FileManager) => void;
    isSelected: (id: number) => boolean;
    addFiles: (e: React.ChangeEvent<HTMLInputElement>) => void;
};

export type FilesManagerActionType = FilesManagerActionTypeDefault &
    (
        | FilesManagerActionCustomType
        | FilesManagerActionOnDeleteType
        | FilesManagerActionOnDownloadType
        | FilesManagerActionAddFolderType
        | FilesManagerActionAddFilesType
        | FilesManagerActionViewFilesType
        | FilesManagerActionRenameFilesType
    );

type FilesManagerActionTypeDefault = (FilesManagerActionTypeUniqueSelection | FilesManagerActionTypeMultipleSelection) & {
    icon: ReactNode;
    label: string;
    type: 'file' | 'folder' | 'all';
    loading?: boolean;
};

type FilesManagerActionTypeUniqueSelection = {
    selection: 'unique';
    onClick?: (file: FileManager) => void;
};

type FilesManagerActionTypeMultipleSelection = {
    selection: 'multiple';
    onClick?: (files: FileManager[]) => void;
};

type FilesManagerActionCustomType = {
    typeActions: 'custom';
};

export type FilesManagerActionOnDeleteType = {
    typeActions: 'onDelete';
};

export type FilesManagerActionOnDownloadType = {
    typeActions: 'onDownload';
};

export type FilesManagerActionAddFolderType = {
    typeActions: 'addFolder';
};

export type FilesManagerActionAddFilesType = {
    typeActions: 'addFiles';
};

export type FilesManagerActionViewFilesType = {
    typeActions: 'onView';
};

export type FilesManagerActionRenameFilesType = {
    typeActions: 'onRename';
};

export type FilesManagerActionDefaultAction = (FilesManagerActionDefaultMultipleFileAction | FilesManagerActionDefaultUniqueFileAction) & {
    key: FilesManagerActionType['typeActions'];
    onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
};

export type FilesManagerActionDefaultMultipleFileAction = {
    selection: 'multiple';
    onClick?: (files: FileManager[]) => void;
};

export type FilesManagerActionDefaultUniqueFileAction = {
    selection: 'unique';
    onClick?: (file: FileManager) => void;
};

export type FilesManagerActionsProps = {
    actions?: FilesManagerActionType[];
    children: JSX.Element;
};
