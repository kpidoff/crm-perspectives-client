import { CardContent, Drawer, Grid, InputLabel, List, ListItemButton, ListItemIcon, ListItemText, useMediaQuery } from '@mui/material';
import { useTheme } from '@mui/material/styles';
import useConfig from 'hooks/useConfig';
import { drawerWidth, gridSpacing } from 'store/constant';
import MainCard from 'ui-component/cards/MainCard';
import { useFilesManagerContext } from 'ui-component/files-manager/filesManager.context';
import CreateNewFolderIcon from '@mui/icons-material/CreateNewFolder';
import NoteAddIcon from '@mui/icons-material/NoteAdd';
import SettingsApplicationsIcon from '@mui/icons-material/SettingsApplications';
import { useFilesManagerActionsContext } from '../files-manager-actions/filesManagerActions.context';
import { styled } from '@mui/system';

const Input = styled('input')({
    display: 'none'
});

const FilesManagerDrawer = () => {
    const { openDrawer, onClickOpenDrawer } = useFilesManagerContext();
    const { addFolder, addFiles } = useFilesManagerActionsContext();
    const theme = useTheme();
    const { borderRadius } = useConfig();
    const matchDownSM = useMediaQuery(theme.breakpoints.down('xl'));
    return (
        <Drawer
            sx={{
                width: drawerWidth,
                flexShrink: 0,
                zIndex: { xs: 1200, xl: 0 },
                '& .MuiDrawer-paper': {
                    height: 'auto',
                    width: drawerWidth,
                    boxSizing: 'border-box',
                    position: 'relative',
                    border: 'none',
                    borderRadius: matchDownSM ? 0 : `${borderRadius}px`
                }
            }}
            variant={matchDownSM ? 'temporary' : 'persistent'}
            anchor="left"
            open={openDrawer}
            ModalProps={{ keepMounted: true }}
            onClose={() => onClickOpenDrawer(false)}
        >
            <MainCard
                sx={{
                    bgcolor: theme.palette.mode === 'dark' ? 'dark.main' : 'grey.50'
                }}
                border={!matchDownSM}
                content={false}
            >
                <CardContent sx={{ height: matchDownSM ? '100vh' : 'auto' }}>
                    <Grid container spacing={gridSpacing}>
                        <Grid item xs={12}>
                            <List
                                component="nav"
                                sx={{
                                    '& .MuiListItem-root': {
                                        mb: 0.75,
                                        borderRadius: `${borderRadius}px`,
                                        '& .MuiChip-root': {
                                            color:
                                                theme.palette.mode === 'dark' ? theme.palette.primary.main : theme.palette.secondary.main,
                                            bgcolor: theme.palette.mode === 'dark' ? theme.palette.dark.dark : theme.palette.secondary.light
                                        }
                                    },
                                    '& .MuiListItem-root.Mui-selected': {
                                        bgcolor: theme.palette.mode === 'dark' ? theme.palette.dark.dark : theme.palette.secondary.light,
                                        '& .MuiListItemText-primary': {
                                            color: theme.palette.mode === 'dark' ? theme.palette.primary.main : theme.palette.secondary.main
                                        },
                                        '& .MuiChip-root': {
                                            color:
                                                theme.palette.mode === 'dark' ? theme.palette.primary.main : theme.palette.secondary.light,
                                            bgcolor: theme.palette.mode === 'dark' ? theme.palette.dark.main : theme.palette.secondary.main
                                        }
                                    }
                                }}
                            >
                                <ListItemButton onClick={() => addFolder()}>
                                    <ListItemIcon>
                                        <CreateNewFolderIcon />
                                    </ListItemIcon>
                                    <ListItemText primary="Ajouter un répertoire" />
                                </ListItemButton>
                                <InputLabel htmlFor="icon-button-file">
                                    <ListItemButton>
                                        <Input id="icon-button-file" multiple type="file" onChange={(e) => addFiles(e)} />
                                        <ListItemIcon>
                                            <NoteAddIcon />
                                        </ListItemIcon>
                                        <ListItemText primary="Ajouter un fichier" />
                                    </ListItemButton>
                                </InputLabel>
                                <ListItemButton>
                                    <ListItemIcon>
                                        <SettingsApplicationsIcon />
                                    </ListItemIcon>
                                    <ListItemText primary="Paramétrage" />
                                </ListItemButton>
                            </List>
                        </Grid>
                    </Grid>
                </CardContent>
            </MainCard>
        </Drawer>
    );
};

export default FilesManagerDrawer;
