import { Box } from '@mui/system';
import FilesManagerContext from '..';
import FilesManagerContent from '../files-manager-content';
import FilesManagerDrawer from '../files-manager-drawer';
import FilesManagerHeader from '../files-manager-header';
import { drawerWidth, gridSpacing } from 'store/constant';
import { Grid } from '@mui/material';
import { useTheme, Theme, styled } from '@mui/material/styles';
import { useFilesManagerContext } from '../filesManager.context';
import DeleteIcon from '@mui/icons-material/Delete';
import DownloadIcon from '@mui/icons-material/Download';
import { FilesManagerActionType } from '../files-manager-actions/filesManagerActions.type';
import DriveFileRenameOutlineIcon from '@mui/icons-material/DriveFileRenameOutline';

const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })(({ theme, open }: { theme: Theme; open: boolean }) => ({
    width: 'calc(100% - 320px)',
    flexGrow: 1,
    paddingLeft: open ? theme.spacing(3) : 0,
    transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.shorter
    }),
    marginLeft: `-${drawerWidth}px`,
    [theme.breakpoints.down('xl')]: {
        paddingLeft: 0,
        marginLeft: 0
    },
    ...(open && {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.shorter
        }),
        marginLeft: 0
    })
}));

const actions: FilesManagerActionType[] = [
    {
        typeActions: 'onDelete',
        icon: <DeleteIcon fontSize="small" />,
        label: 'Supprimé',
        type: 'all',
        selection: 'multiple'
    },
    {
        typeActions: 'onDownload',
        icon: <DownloadIcon fontSize="small" />,
        label: 'Téléchargé',
        type: 'file',
        selection: 'multiple'
    },
    {
        typeActions: 'onRename',
        icon: <DriveFileRenameOutlineIcon fontSize="small" />,
        label: 'Renommé',
        type: 'all',
        selection: 'unique'
    }
];

const FilesManager = () => {
    const theme = useTheme();
    const { openDrawer } = useFilesManagerContext();
    return (
        <>
            <FilesManagerDrawer />
            <Main theme={theme} open={openDrawer}>
                <Grid container spacing={gridSpacing}>
                    <Grid item xs={12}>
                        <FilesManagerHeader />
                    </Grid>
                    <Grid item xs={12}>
                        <FilesManagerContent />
                    </Grid>
                </Grid>
            </Main>
        </>
    );
};

const FilesManagerWrapper = () => (
    <Box sx={{ display: 'flex' }}>
        <FilesManagerContext fileManagerRootId={1} actions={actions}>
            <FilesManager />
        </FilesManagerContext>
    </Box>
);

export default FilesManagerWrapper;
