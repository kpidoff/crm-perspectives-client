import { createContext, useContext } from 'react';
import { FilesManagerContextProps } from './filesManager.type';

export const filesManagerContext = createContext<FilesManagerContextProps | null>(null);

export const useFilesManagerContext = () => {
    const context = useContext(filesManagerContext);

    if (!context) throw new Error('Context file manager must be use inside provider');

    return context;
};
