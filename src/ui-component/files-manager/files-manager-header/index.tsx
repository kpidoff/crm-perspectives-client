import { Grid, IconButton, Stack } from '@mui/material';
import MenuRoundedIcon from '@mui/icons-material/MenuRounded';
import { useFilesManagerContext } from '../filesManager.context';

const FilesManagerHeader = () => {
    const { onClickOpenDrawer, openDrawer } = useFilesManagerContext();
    return (
        <Grid container alignItems="center" justifyContent="space-between">
            <Grid item xs>
                <Stack direction="row" alignItems="center" justifyContent="flex-start" spacing={1.5}>
                    <IconButton onClick={() => onClickOpenDrawer(!openDrawer)} size="small">
                        <MenuRoundedIcon fontSize="small" />
                    </IconButton>
                </Stack>
            </Grid>
        </Grid>
    );
};

export default FilesManagerHeader;
