import { FileManager } from 'types/utils/fileManager';
import { FilesManagerActionType } from './files-manager-actions/filesManagerActions.type';

export type FilesManagerContextProps = {
    loading: boolean;
    openDrawer: boolean;
    onClickOpenDrawer: (open: boolean) => void;
    fileManagerRootId: number;
    files: FileManager[] | undefined;
    fileSubscription: FileManager | undefined;
};

export type FilesManagerProps = {
    children: JSX.Element;
    fileManagerRootId: number;
    actions?: FilesManagerActionType[];
};
