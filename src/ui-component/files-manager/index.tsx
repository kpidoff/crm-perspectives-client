import { useFileManagerSubscriptionEndUploadFile, useFilesManagerByParentId } from 'hooks/utils/useFileManager';
import { useEffect, useState } from 'react';
import FilesManagerActions from './files-manager-actions';
import { filesManagerContext } from './filesManager.context';
import { FilesManagerProps } from './filesManager.type';

const FilesManagerContext = ({ children, fileManagerRootId, actions }: FilesManagerProps) => {
    const [openDrawer, setOpenDrawer] = useState(false);
    const { getFilesManager, filesManager, loading } = useFilesManagerByParentId();
    const { fileManager: fileManagerSubscription } = useFileManagerSubscriptionEndUploadFile();

    useEffect(() => {
        getFilesManager(fileManagerRootId);
    }, [fileManagerRootId, getFilesManager]);

    useEffect(() => {
        fileManagerSubscription?.parentId === fileManagerRootId && getFilesManager(fileManagerRootId);
    }, [fileManagerRootId, fileManagerSubscription, getFilesManager]);

    return (
        <filesManagerContext.Provider
            value={{
                loading,
                onClickOpenDrawer: setOpenDrawer,
                openDrawer,
                fileManagerRootId,
                files: filesManager,
                fileSubscription: fileManagerSubscription
            }}
        >
            <FilesManagerActions actions={actions}>{children}</FilesManagerActions>
        </filesManagerContext.Provider>
    );
};

export default FilesManagerContext;
