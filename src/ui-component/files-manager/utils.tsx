import { THOUSAND_SEPARATOR } from 'constants/format';
import NumberFormat from 'react-number-format';
import { FileManager } from 'types/utils/fileManager';
import path from 'path';

export function isFolder(file: FileManager) {
    return file.type === 'folder';
}

export function isFile(file: string) {
    return file.substring(file.lastIndexOf('/') + 1);
}

export function fileExtension(name: string, withDot: boolean = true) {
    return withDot ? path.extname(name) : path.extname(name).replace(/[./^]/, '');
}

export function formatSize(size: number) {
    return size ? (
        <NumberFormat value={size / 1000} decimalScale={0} displayType="text" thousandSeparator={THOUSAND_SEPARATOR} suffix=" ko" />
    ) : (
        '-'
    );
}
