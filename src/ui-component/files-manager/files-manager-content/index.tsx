/* eslint-disable @typescript-eslint/dot-notation */
import { Paper, Table, TableContainer } from '@mui/material';
import { useFilesManagerMove } from 'hooks/utils/useFileManager';
import _ from 'lodash';
import { useCallback } from 'react';
import { useFilesManagerActionsContext } from '../files-manager-actions/filesManagerActions.context';
import { useFilesManagerContext } from '../filesManager.context';
import FilesManagerContentHeader from './components/FilesManagerContentHeader';
import FilesManagerContentRow from './components/FilesManagerContentRow';
import { filesManagerContentContext } from './filesManagerContent.context';

export default function FilesManagerContent() {
    const { files } = useFilesManagerContext();
    const { selected } = useFilesManagerActionsContext();
    const { moveFiles: moveFilesRequest } = useFilesManagerMove();

    const moveFiles = useCallback(
        (idParent: number) => {
            moveFilesRequest(
                _.map(selected, (file) => file.id),
                idParent
            );
        },
        [moveFilesRequest, selected]
    );

    return (
        <filesManagerContentContext.Provider
            value={{
                moveFiles
            }}
        >
            <TableContainer component={Paper}>
                <Table>
                    <FilesManagerContentHeader />
                </Table>
                {_.map(files, (file) => (
                    <FilesManagerContentRow key={file.id} file={file} />
                ))}
            </TableContainer>
        </filesManagerContentContext.Provider>
    );
}
