import { createContext, useContext } from 'react';
import { FilesManagerContentContextProps } from './filesManagerContent.type';

export const filesManagerContentContext = createContext<FilesManagerContentContextProps | null>(null);

export const useFilesManagerContentContext = () => {
    const context = useContext(filesManagerContentContext);

    if (!context) throw new Error('Context file manager content must be use inside provider');

    return context;
};
