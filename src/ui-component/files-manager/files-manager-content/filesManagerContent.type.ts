export type FilesManagerContentContextProps = {
    moveFiles: (parentId: number) => void;
};
