import { Badge, Button, Tooltip } from '@mui/material';
import _ from 'lodash';
import { FileManager } from 'types/utils/fileManager';
import { useFilesManagerActionsContext } from 'ui-component/files-manager/files-manager-actions/filesManagerActions.context';
import { FilesManagerActionType } from 'ui-component/files-manager/files-manager-actions/filesManagerActions.type';

const FilesManagerContentRowAction = ({ actions, file }: { actions?: FilesManagerActionType[]; file: FileManager }) => {
    const { defaultActions, selected } = useFilesManagerActionsContext();

    if (selected.length > 1 && _.filter(actions, (action) => action.selection === 'multiple').length === 0) {
        return null;
    }

    return (
        <div>
            {_.map(actions, (action, index) => {
                if (action.selection === 'unique' && selected.length > 1) {
                    return null;
                }
                const handleClickCustom = () => {
                    action.selection === 'unique' ? action.onClick?.(file) : action.onClick?.([...selected, file]);
                };
                const handleClickDefault = () => {
                    const actionDefault = _.find(defaultActions, (defaultAction) => defaultAction.key === action.typeActions);
                    actionDefault && actionDefault.selection === 'unique'
                        ? actionDefault?.onClick?.(file)
                        : actionDefault?.onClick?.([...selected, file]);
                };
                return (
                    <Tooltip key={index} title={action.label}>
                        <Button
                            size="large"
                            onClick={(e) => (action.typeActions === 'custom' ? handleClickCustom() : handleClickDefault())}
                        >
                            <Badge badgeContent={selected.length > 1 ? selected.length : 0} color="primary">
                                {action.icon}
                            </Badge>
                        </Button>
                    </Tooltip>
                );
            })}
        </div>
    );
};

export default FilesManagerContentRowAction;
