import { TableHead, TableRow, TableCell as MuiTableCell } from '@mui/material';
import { styled } from '@mui/material/styles';

const TableCell = styled(MuiTableCell)({
    width: '20%'
});

const TableCellName = styled(MuiTableCell)({
    width: '60%'
});
const FilesManagerContentHeader = () => (
    <TableHead>
        <TableRow>
            <TableCellName>Nom</TableCellName>
            <TableCell>Taille</TableCell>
            <TableCell>Dernière modification</TableCell>
        </TableRow>
    </TableHead>
);

export default FilesManagerContentHeader;
