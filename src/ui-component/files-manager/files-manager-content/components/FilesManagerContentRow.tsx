import { useTheme, styled } from '@mui/material/styles';
import { Table, TableBody, TableRow, TableCell as MuiTableCell, Stack, IconButton, Collapse } from '@mui/material';
import { useFileManagerById, useFilesManagerByParentId } from 'hooks/utils/useFileManager';
import { FileManager } from 'types/utils/fileManager';
import FileManagerIcon from 'ui-component/files-manager/files-manager-icon';
import { fileExtension, isFolder, formatSize } from 'ui-component/files-manager/utils';
import FolderIcon from '@mui/icons-material/Folder';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import FilesManagerContentRowAction from './FilesManagerContentRowActions';
import Moment from 'react-moment';
import { DATE_TIME } from 'constants/date';
import { useFilesManagerActionsContext } from 'ui-component/files-manager/files-manager-actions/filesManagerActions.context';
import { useEffect, useRef, useState } from 'react';
import _ from 'lodash';
import { useFilesManagerContext } from 'ui-component/files-manager/filesManager.context';
import { useDrag, useDrop } from 'react-dnd';
import { useFilesManagerContentContext } from '../filesManagerContent.context';

const TableCell = styled(MuiTableCell)({
    width: '20%'
});

const TableCellName = styled(MuiTableCell)({
    width: '60%'
});

const FilesManagerContentRowChildren = ({ open, parentId, level }: { open: boolean; parentId: number; level: number }) => {
    const { getFilesManager, filesManager, loading } = useFilesManagerByParentId();
    const [files, setFiles] = useState<FileManager[] | null>(null);
    const { fileSubscription } = useFilesManagerContext();

    useEffect(() => {
        open && files === null && getFilesManager(parentId);
    }, [files, getFilesManager, open, parentId]);

    useEffect(() => {
        filesManager && setFiles(filesManager);
    }, [filesManager]);

    useEffect(() => {
        fileSubscription && fileSubscription.parentId === parentId && getFilesManager(parentId);
    }, [fileSubscription, getFilesManager, parentId]);

    return (
        <Collapse in={open && !loading}>
            {_.map(files, (file) => (
                <FilesManagerContentRow key={file.id} file={file} level={level} />
            ))}
        </Collapse>
    );
};

const FilesManagerContentRow = ({ file, level = -1 }: { file: FileManager; level?: number }) => {
    const theme = useTheme();
    const { fileSubscription } = useFilesManagerContext();
    const { actionsFile, actionsFolder, isSelected, onSelect, selected } = useFilesManagerActionsContext();
    const { moveFiles } = useFilesManagerContentContext();
    const { getFileManagerById, fileManager } = useFileManagerById();
    level += 1;
    const [over, setOver] = useState(false);
    const [open, setOpen] = useState(false);
    const [currentFile, setCurrentFile] = useState(file);
    const [dnd, setDnd] = useState<number | null>(null);

    useEffect(() => {
        file && setCurrentFile(file);
    }, [currentFile, file]);

    useEffect(() => {
        if (dnd) {
            moveFiles(dnd);
            setDnd(null);
        }
    }, [dnd, moveFiles]);

    const [, drag] = useDrag(
        () => ({
            type: 'folder',
            item: () => {
                let dragFields;
                if (selected.find((field) => field.id === file.id)) {
                    dragFields = file;
                } else {
                    dragFields = [...selected, file];
                }
                onSelect(file);
                return dragFields;
            },
            end: (item, monitor) => {
                const dropResult: {
                    file: FileManager;
                } | null = monitor.getDropResult();
                if (item && dropResult && file.parentId !== dropResult.file.id) {
                    setDnd(dropResult.file.id);
                }
            }
        }),
        []
    );

    const ref = useRef(null);
    const [{ handlerId }, drop] = useDrop({
        accept: file.type,
        drop: () => ({ file }),
        collect(monitor) {
            return {
                handlerId: monitor.getHandlerId()
            };
        }
    });

    useEffect(() => {
        fileSubscription && fileSubscription.parentId === currentFile.id && getFileManagerById(currentFile.id);
    }, [currentFile.id, fileSubscription, getFileManagerById]);

    useEffect(() => {
        fileManager && setCurrentFile(fileManager);
    }, [fileManager]);

    const marginLevel = level * 30;

    const fileExtention = fileExtension(currentFile.name, false);

    const actions = isFolder(currentFile) ? actionsFolder : actionsFile;

    drag(drop(ref));
    return (
        <>
            <Table>
                <TableBody>
                    <TableRow
                        ref={ref}
                        data-handler-id={handlerId}
                        onClick={(event) => onSelect(currentFile, event)}
                        selected={isSelected(currentFile.id)}
                        onMouseOver={() => setOver(true)}
                        onMouseLeave={() => setOver(false)}
                        hover
                        sx={{
                            '& td:last-of-type>div': {
                                position: 'absolute',
                                top: '50%',
                                right: 5,
                                transform: 'translateY(-50%)',
                                display: 'none',
                                background: theme.palette.mode === 'dark' ? theme.palette.dark[800] : '#fff',
                                boxShadow: '0px 1px 4px rgba(0, 0, 0, 0.1)',
                                borderRadius: '12px',
                                py: 1,
                                px: 1.75,
                                '& button + button': {
                                    ml: 0.625
                                }
                            },
                            '&:hover': {
                                '& td:last-of-type>div': {
                                    display: 'block'
                                }
                            }
                        }}
                    >
                        <TableCellName>
                            <Stack direction="row" justifyContent="space-between">
                                <div style={{ marginLeft: marginLevel, display: 'flex' }}>
                                    {currentFile._count.children > 0 && (
                                        <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
                                            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                                        </IconButton>
                                    )}
                                    {isFolder(currentFile) ? (
                                        <div
                                            style={{
                                                display: 'flex',
                                                alignItems: 'center',
                                                flexWrap: 'wrap'
                                            }}
                                        >
                                            <FolderIcon sx={{ marginRight: '5px' }} />

                                            {currentFile.name}
                                        </div>
                                    ) : (
                                        <Stack direction="row" justifyContent="space-between">
                                            {fileExtention && <FileManagerIcon ext={fileExtention} size={20} />}
                                            {currentFile.name}
                                        </Stack>
                                    )}
                                </div>
                            </Stack>
                        </TableCellName>
                        {!over ? (
                            <>
                                <TableCell>{formatSize(currentFile.size)}</TableCell>
                                <TableCell>
                                    <Moment format={DATE_TIME}>{currentFile.updatedAt}</Moment>
                                </TableCell>
                            </>
                        ) : (
                            <MuiTableCell sx={{ position: 'relative', width: '40%' }}>
                                {actions.length > 0 && <FilesManagerContentRowAction actions={actions} file={currentFile} />}
                            </MuiTableCell>
                        )}
                    </TableRow>
                </TableBody>
            </Table>
            <FilesManagerContentRowChildren open={open} parentId={currentFile.id} level={level} />
        </>
    );
};

export default FilesManagerContentRow;
