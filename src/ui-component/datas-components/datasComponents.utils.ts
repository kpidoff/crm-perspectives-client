import { TRUNCATE_TEXT_NB_CARACT } from './datasComponents.constants';

// eslint-disable-next-line import/prefer-default-export
export const textTruncate = (text: string | null | undefined) => {
    if (!text) {
        return '';
    }
    return text?.length > TRUNCATE_TEXT_NB_CARACT ? `${text.substring(0, TRUNCATE_TEXT_NB_CARACT)}...` : text;
};
