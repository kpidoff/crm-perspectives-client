import { createContext, useContext } from 'react';
import { SearchContextType } from 'ui-component/datas-components/datasComponents.type';

export const searchContext = createContext<SearchContextType | null>(null);

export const useSearchContext = () => {
    const context = useContext(searchContext);

    if (!context) throw new Error('Context page search must be use inside provider');

    return context;
};
