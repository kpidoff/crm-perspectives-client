import _ from 'lodash';
import { useEffect, useState } from 'react';
import { useDatasComponentsContext } from 'ui-component/datas-components/datasComponents.context';
import { DatasComponentsSearchInputProps, DatasComponentsSearchProps } from 'ui-component/datas-components/datasComponents.type';
import DatasComponentsSearchCenter from './components/DatasComponentsSearchCenter';
import DatasComponentsSearchFullWidth from './components/DatasComponentsSearchFullWidth';
import DatasComponentsSearchLeft from './components/DatasComponentsSearchLeft';
import DatasComponentsSearchNone from './components/DatasComponentsSearchNone';
import DatasComponentsSearchRight from './components/DatasComponentsSearchRight';
import { searchContext } from './search.context';

const DatasComponentsSearch = ({
    updateSearch,
    filterkey,
    wait,
    ...props
}: DatasComponentsSearchProps & DatasComponentsSearchInputProps) => {
    const { matchDownXS } = useDatasComponentsContext();
    const [currentValue, setCurrentValue] = useState(props.initialValue || undefined);

    const handleSearch = (value: any) => {
        setCurrentValue(value);
    };

    useEffect(() => {
        const debounce = _.debounce(() => {
            currentValue !== undefined && updateSearch?.(currentValue, filterkey);
        }, wait);

        debounce();

        return () => {
            debounce.cancel();
        };
    }, [updateSearch, filterkey, currentValue, wait]);

    const GenereateInput = () => {
        if (!matchDownXS) {
            switch (props.align) {
                case 'center':
                    return <DatasComponentsSearchCenter {...props} />;
                case 'fullWidth':
                    return <DatasComponentsSearchFullWidth {...props} />;
                case 'start':
                    return <DatasComponentsSearchLeft {...props} />;
                case 'end':
                    return <DatasComponentsSearchRight {...props} />;
                case 'none':
                    return <DatasComponentsSearchNone {...props} />;
                default:
                    return null;
            }
        }
        return <DatasComponentsSearchFullWidth {...props} />;
    };
    return (
        <searchContext.Provider value={{ onSearch: handleSearch, currentValue }}>
            <GenereateInput />
        </searchContext.Provider>
    );
};

export default DatasComponentsSearch;
