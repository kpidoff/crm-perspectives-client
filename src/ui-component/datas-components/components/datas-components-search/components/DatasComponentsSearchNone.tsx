import { DatasComponentsSearchInputProps } from 'ui-component/datas-components/datasComponents.type';
import DatasComponentsSearchInput from './DatasComponentsSearchInput';

const DatasComponentsSearchNone = ({ ...props }: DatasComponentsSearchInputProps) => <DatasComponentsSearchInput {...props} />;

export default DatasComponentsSearchNone;
