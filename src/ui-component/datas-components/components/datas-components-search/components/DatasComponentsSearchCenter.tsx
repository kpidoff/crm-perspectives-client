import { Stack } from '@mui/material';
import { DatasComponentsSearchInputProps } from 'ui-component/datas-components/datasComponents.type';
import DatasComponentsSearchInput from './DatasComponentsSearchInput';

const DatasComponentsSearchCenter = ({ ...props }: DatasComponentsSearchInputProps) => (
    <Stack direction="row" justifyContent="center" alignItems="center" spacing={2}>
        <DatasComponentsSearchInput {...props} />
    </Stack>
);

export default DatasComponentsSearchCenter;
