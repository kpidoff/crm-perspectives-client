import { InputAdornment, TextField } from '@mui/material';
import { IconSearch } from '@tabler/icons';
import { memo } from 'react';
import { DatasComponentsSearchInputProps } from 'ui-component/datas-components/datasComponents.type';
import { useSearchContext } from '../search.context';

const DatasComponentsSearchInput = ({ ...props }: DatasComponentsSearchInputProps) => {
    const { onSearch, currentValue } = useSearchContext();

    return (
        <>
            <TextField
                {...props}
                value={currentValue}
                sx={{ ...props.sx }}
                onChange={(values) => {
                    onSearch(values.currentTarget.value);
                }}
                InputProps={{
                    startAdornment: (
                        <InputAdornment position="start">
                            <IconSearch stroke={1.5} size="1rem" />
                        </InputAdornment>
                    )
                }}
                type="search"
            />
        </>
    );
};

export default memo(DatasComponentsSearchInput);
