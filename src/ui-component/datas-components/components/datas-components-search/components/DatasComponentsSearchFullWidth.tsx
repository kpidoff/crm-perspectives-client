import { DatasComponentsSearchInputProps } from 'ui-component/datas-components/datasComponents.type';
import DatasComponentsSearchInput from './DatasComponentsSearchInput';

const DatasComponentsSearchFullWidth = ({ ...props }: DatasComponentsSearchInputProps) => (
    <DatasComponentsSearchInput {...props} fullWidth />
);

export default DatasComponentsSearchFullWidth;
