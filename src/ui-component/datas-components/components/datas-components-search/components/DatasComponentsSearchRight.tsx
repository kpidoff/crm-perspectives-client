import { Stack } from '@mui/material';
import { DatasComponentsSearchInputProps } from 'ui-component/datas-components/datasComponents.type';
import DatasComponentsSearchInput from './DatasComponentsSearchInput';

const DatasComponentsSearchRight = ({ ...props }: DatasComponentsSearchInputProps) => (
    <Stack direction="row" justifyContent="flex-end" alignItems="center" spacing={2}>
        <DatasComponentsSearchInput {...props} />
    </Stack>
);

export default DatasComponentsSearchRight;
