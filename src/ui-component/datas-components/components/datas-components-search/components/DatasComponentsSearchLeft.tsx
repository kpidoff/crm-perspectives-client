import { Stack } from '@mui/material';
import { DatasComponentsSearchInputProps } from 'ui-component/datas-components/datasComponents.type';
import DatasComponentsSearchInput from './DatasComponentsSearchInput';

const DatasComponentsSearchLeft = ({ ...props }: DatasComponentsSearchInputProps) => (
    <Stack direction="row" justifyContent="flex-start" alignItems="center" spacing={2}>
        <DatasComponentsSearchInput {...props} />
    </Stack>
);

export default DatasComponentsSearchLeft;
