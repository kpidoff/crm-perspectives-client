import { Grid } from '@mui/material';
import { gridSpacing } from 'store/constant';

interface DatasComponentsHeaderProps {
    children: JSX.Element | JSX.Element[];
}

export const DatasComponentsHeader = ({ children }: DatasComponentsHeaderProps) => (
    <Grid
        sx={{ paddingBottom: '10px', paddingTop: '20px' }}
        container
        alignItems="center"
        justifyContent="space-between"
        spacing={gridSpacing}
    >
        {children}
    </Grid>
);

export default DatasComponentsHeader;
