import { Button, ButtonGroup, Stack } from '@mui/material';
import GridViewIcon from '@mui/icons-material/GridView';
import ViewListIcon from '@mui/icons-material/ViewList';
import { useDatasComponentsContext } from 'ui-component/datas-components/datasComponents.context';

const DatasComponentsDisplay = () => {
    const { onChangeDisplay, matchDownXS, ...props } = useDatasComponentsContext();

    return !matchDownXS && props.type === 'table' ? (
        <Stack direction="row" justifyContent="flex-end" alignItems="center" spacing={2}>
            <ButtonGroup variant="outlined">
                <Button size="large" disabled={props.currentDisplay === 'grid'} onClick={() => onChangeDisplay('grid')}>
                    <GridViewIcon fontSize="inherit" />
                </Button>
                <Button size="large" disabled={props.currentDisplay === 'list'} onClick={() => onChangeDisplay('list')}>
                    <ViewListIcon fontSize="inherit" />
                </Button>
            </ButtonGroup>
        </Stack>
    ) : null;
};

export default DatasComponentsDisplay;
