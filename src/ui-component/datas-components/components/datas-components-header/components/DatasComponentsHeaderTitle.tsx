import { Grid, IconButton, Stack, Typography } from '@mui/material';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import { DatasComponentsHeaderTitleProps } from 'ui-component/datas-components/datasComponents.type';

const DatasComponentsHeaderTitle = ({ title }: DatasComponentsHeaderTitleProps) => (
    <Grid item>
        {title && (
            <Stack direction="row" alignItems="center" spacing={1}>
                <Typography variant="h4">{title}</Typography>
                <IconButton size="large">
                    <ArrowForwardIosIcon sx={{ width: '0.875rem', height: '0.875rem', fontWeight: 500, color: 'grey.500' }} />
                </IconButton>
            </Stack>
        )}
    </Grid>
);

export default DatasComponentsHeaderTitle;
