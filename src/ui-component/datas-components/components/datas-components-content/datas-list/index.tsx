import { Grid, List, Skeleton, Typography } from '@mui/material';
import _ from 'lodash';
import { FC, Fragment } from 'react';
import { FormattedMessage } from 'react-intl';
import { gridSpacing } from 'store/constant';
import { DataComponentsList } from 'ui-component/datas-components/datasComponents.type';
import SkeletonWrapper from 'ui-component/skeleton/skeleton-wrapper';
import { DatasComponentsContentListProps, DatasComponentsContentProps } from '../dataComponentsContent.type';

const DatasList: FC<DataComponentsList & DatasComponentsContentProps & DatasComponentsContentListProps> = ({
    datas,
    loading,
    itemsPerPage,
    heightSkeleton,
    spacing,
    component
}) => {
    if (!loading && datas && datas.length === 0) {
        return (
            <Grid container sx={{ marginTop: '10px' }} spacing={gridSpacing} direction="column" alignItems="center" justifyContent="center">
                <Grid item xs={12}>
                    <Typography variant="caption">
                        <FormattedMessage id="no-data" />
                    </Typography>
                </Grid>
            </Grid>
        );
    }

    return (
        <List>
            {_.map(datas || Array(itemsPerPage), (data, index) => (
                <Fragment key={index}>
                    <div style={{ paddingBottom: `${spacing}px` }}>{component(data, loading)}</div>
                </Fragment>
            ))}
        </List>
    );
};

export default DatasList;
