import { Grid, Skeleton, Typography } from '@mui/material';
import { FC, Fragment } from 'react';
import { FormattedMessage } from 'react-intl';
import { gridSpacing } from 'store/constant';
import { DataComponentsListName } from 'ui-component/datas-components/datasComponents.type';
import SkeletonWrapper from 'ui-component/skeleton/skeleton-wrapper';
import { useTheme } from '@mui/material/styles';
import { DatasComponentsContentListProps, DatasComponentsContentProps } from '../dataComponentsContent.type';

const DatasListName: FC<DataComponentsListName & DatasComponentsContentProps & DatasComponentsContentListProps> = ({
    sortName,
    loading,
    datas,
    onClick,
    onDoubleClick,
    component,
    itemsPerPage
}) => {
    const theme = useTheme();

    const datasFormated = datas?.reduce((a: any, curr) => {
        const firstLatter = curr[sortName]![0].toUpperCase();
        if (Object.prototype.hasOwnProperty.call(a, firstLatter)) {
            a[firstLatter].push(curr);
        } else {
            a[firstLatter] = [curr];
        }
        return a;
    }, {});

    const GenerateData = () => {
        if (!loading && datas && datas.length > 0) {
            return (
                <>
                    {Object.keys(datasFormated).map((key, index) => (
                        <Grid
                            container
                            key={index}
                            sx={{
                                padding: '20px'
                            }}
                        >
                            <Grid
                                item
                                xs={12}
                                sx={{
                                    borderBottom: theme.palette.mode === 'dark' ? 'none' : '1px solid',
                                    borderColor: `${theme.palette.grey[100]}!important`,
                                    paddingBottom: '20px'
                                }}
                            >
                                <Typography variant="h4" color="primary" sx={{ fontSize: '1rem' }}>
                                    {key.toUpperCase()}
                                </Typography>
                            </Grid>
                            <Grid
                                item
                                xs={12}
                                sx={{
                                    paddingTop: '24px'
                                }}
                            >
                                <Grid container spacing={gridSpacing}>
                                    {datasFormated[key].map((data: any, i: number) => (
                                        <Grid
                                            onDoubleClick={() => onDoubleClick?.(data) || undefined}
                                            onClick={() => onClick?.(data) || undefined}
                                            item
                                            xs={12}
                                            key={i}
                                            sx={{
                                                paddingTop: '10px',
                                                paddingBottom: '20px',
                                                '&:hover': {
                                                    backgroundColor: theme.palette.action.hover
                                                },
                                                borderBottom: theme.palette.mode === 'dark' ? 'none' : '1px solid',
                                                borderColor: `${theme.palette.grey[100]}!important`,
                                                cursor: onDoubleClick || onClick ? 'pointer' : 'default'
                                            }}
                                        >
                                            {/* {component(data)} */}
                                        </Grid>
                                    ))}
                                </Grid>
                            </Grid>
                        </Grid>
                    ))}
                </>
            );
        }

        if (loading) {
            return (
                <SkeletonWrapper
                    itemsPerPage={itemsPerPage}
                    skeleton={
                        <Grid item xs={12}>
                            <Skeleton />
                        </Grid>
                    }
                />
            );
        }

        return (
            <Grid container sx={{ marginTop: '10px' }} spacing={gridSpacing} direction="column" alignItems="center" justifyContent="center">
                <Grid item xs={12}>
                    <Typography variant="caption">
                        <FormattedMessage id="no-data" />
                    </Typography>
                </Grid>
            </Grid>
        );
    };

    return (
        <Grid container spacing={gridSpacing}>
            <GenerateData />
        </Grid>
    );
};

export default DatasListName;
