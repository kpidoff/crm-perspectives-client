import { useDatasComponentsContext } from 'ui-component/datas-components/datasComponents.context';
import DatasList from './datas-list';
import DatasListName from './datas-list-name';
import { styled } from '@mui/material/styles';
import { appDrawerWidth } from 'store/constant';
import { Box } from '@mui/system';
import DatasComponentsDrawer from '../datas-components-drawer';
import { DatasComponentsContentProps } from './dataComponentsContent.type';
import DatasTable from './data-table';
import Drawer from '../datas-components-drawer/components/Drawer';
import DatasGrid from './datas-grid';
import DatasGridComponent from './datas-grid-component';

const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' && prop !== 'editMode' && prop !== 'filterActive' })<{
    open?: boolean;
    editMode?: boolean;
    filterActive?: boolean;
}>(({ theme, open, editMode, filterActive }) => ({
    flexGrow: 1,
    transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
    }),
    marginRight: editMode || !filterActive ? 0 : -appDrawerWidth,
    [theme.breakpoints.down('xl')]: {
        paddingRight: 0,
        marginRight: 0
    },
    ...(open && {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen
        }),
        marginRight: 0
    })
}));

const DatasComponentsContent = ({ ...props }: DatasComponentsContentProps) => {
    const { currentOpenDrawer, openDrawer, filter, ...otherProps } = useDatasComponentsContext();
    const GenerateContent = () => {
        switch (otherProps.type) {
            case 'list':
                return props.contentType === 'defaultList' ? <DatasList {...props} {...otherProps} /> : <p>Error type content</p>;
            case 'listName':
                return props.contentType === 'defaultList' ? <DatasListName {...props} {...otherProps} /> : <p>Error type content</p>;
            case 'table':
                return props.contentType === 'defaultTable' ? <DatasTable {...props} /> : <p>Error type content</p>;
            case 'grid':
                switch (props.contentType) {
                    case 'defaultGrid':
                        return <DatasGrid {...props} {...otherProps} />;
                    case 'gridComponent':
                        return <DatasGridComponent {...props} {...otherProps} />;
                    default:
                        return <p>Error type content</p>;
                }
            default:
                return null;
        }
    };

    return (
        <Box sx={{ display: 'flex' }}>
            <Main filterActive={!!filter} open={currentOpenDrawer} editMode={props.editMode}>
                <GenerateContent />
            </Main>
            {filter && !props.editMode && (
                <Drawer open={currentOpenDrawer} onClose={() => openDrawer(false)}>
                    <DatasComponentsDrawer />
                </Drawer>
            )}
        </Box>
    );
};

export default DatasComponentsContent;
