import { Grid, Typography } from '@mui/material';
import _ from 'lodash';
import { FC } from 'react';
import { FormattedMessage } from 'react-intl';
import { gridSpacing } from 'store/constant';
import { DataComponentsGrid } from 'ui-component/datas-components/datasComponents.type';
import { DatasComponentsContentGridComponentProps, DatasComponentsContentProps } from '../dataComponentsContent.type';

const DatasGridComponent: FC<DataComponentsGrid & DatasComponentsContentProps & DatasComponentsContentGridComponentProps> = ({
    datas,
    loading,
    itemsPerPage,
    component,
    onClick,
    onDoubleClick
}) => {
    if (!loading && datas?.length === 0) {
        return (
            <Grid container sx={{ marginTop: '10px' }} spacing={gridSpacing} direction="column" alignItems="center" justifyContent="center">
                <Grid item xs={12}>
                    <Typography variant="caption">
                        <FormattedMessage id="no-data" />
                    </Typography>
                </Grid>
            </Grid>
        );
    }

    return (
        <Grid container spacing={gridSpacing}>
            {_.map(!loading ? datas : Array(itemsPerPage), (data, index) => (
                <Grid
                    key={index}
                    item
                    xs={12}
                    sm={6}
                    md={4}
                    lg={3}
                    onDoubleClick={() => onDoubleClick?.(data) || undefined}
                    onClick={() => onClick?.(data) || undefined}
                    sx={{
                        cursor: onDoubleClick || onClick ? 'pointer' : 'default'
                    }}
                >
                    {component(data, loading)}
                </Grid>
            ))}
        </Grid>
    );
};

export default DatasGridComponent;
