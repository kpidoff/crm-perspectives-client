import { CardHeader, IconButton, Menu, MenuItem, Skeleton } from '@mui/material';
import _ from 'lodash';
import { FC, useState } from 'react';
import {
    DatasComponentsContentTableComponentsProps,
    DatasComponentsContentTableTypeActionProps,
    DatasComponentsContentTableTypeHeaderProps
} from 'ui-component/datas-components/components/datas-components-content/dataComponentsContent.type';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import { useDatasComponentsContext } from 'ui-component/datas-components/datasComponents.context';

const DataTableGenerateGridHeader: FC<{ components: DatasComponentsContentTableComponentsProps[] } & { data: any }> = ({
    data,
    components
}) => {
    const { loading } = useDatasComponentsContext();
    const [anchorEl, setAnchorEl] = useState<Element | ((element: Element) => Element) | null | undefined>(null);
    const handleClick = (event: React.MouseEvent<HTMLButtonElement> | undefined) => {
        setAnchorEl(event?.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const header = _.find(components, (component) => component.content(data).type === 'header')?.content(data) as
        | DatasComponentsContentTableTypeHeaderProps
        | undefined;
    const actions = _.find(components, (component) => component.content(data).type === 'action')?.content(data) as
        | DatasComponentsContentTableTypeActionProps
        | undefined;

    if (!loading && (!!header || !!actions)) {
        return (
            <CardHeader
                action={
                    actions && (
                        <>
                            <IconButton onClick={handleClick} aria-label="settings">
                                <MoreVertIcon />
                            </IconButton>
                            <Menu
                                id="menu-user-details-card"
                                anchorEl={anchorEl}
                                keepMounted
                                open={Boolean(anchorEl)}
                                onClose={handleClose}
                                variant="selectedMenu"
                                anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'right'
                                }}
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right'
                                }}
                            >
                                {_.map(actions.items, (item, index) => (
                                    <MenuItem
                                        key={index}
                                        onClick={() => {
                                            item.onClick();
                                            handleClose();
                                        }}
                                    >
                                        {item.label}
                                    </MenuItem>
                                ))}
                            </Menu>
                        </>
                    )
                }
                title={header?.component}
            />
        );
    }
    if (loading && (!!header || !!actions)) {
        return (
            <CardHeader
                action={actions && <Skeleton height={30} width={30} />}
                title={
                    header && (
                        <>
                            <Skeleton width="80%" />
                            <Skeleton width="60%" height="20px" />
                        </>
                    )
                }
            />
        );
    }

    return null;
};

export default DataTableGenerateGridHeader;
