import { Card, CardContent, Grid, Typography } from '@mui/material';
import { FC } from 'react';
import { gridSpacing } from 'store/constant';
import { useDatasComponentsContext } from 'ui-component/datas-components/datasComponents.context';
import { DatasComponentsContentProps, DatasComponentsContentTableProps } from '../../../dataComponentsContent.type';
import { useTheme } from '@mui/material/styles';
import _ from 'lodash';
import DataTableGenerateGridHeader from './components/DataTableGenerateGridHeader';
import DataTableGenerateType from '../components/DataTableGenerateType';
import { FormattedMessage } from 'react-intl';

const DatasTableGenerateGrid: FC<DatasComponentsContentProps & DatasComponentsContentTableProps> = ({
    components,
    onClick,
    onDoubleClick
}) => {
    const theme = useTheme();
    const { datas, loading, itemsPerPage } = useDatasComponentsContext();

    if (!loading && datas?.length === 0) {
        return (
            <Typography variant="subtitle2" align="center">
                <FormattedMessage id="no-registration" />
            </Typography>
        );
    }

    return (
        <Grid container spacing={gridSpacing}>
            {_.map(datas || Array(itemsPerPage), (data, index) => (
                <Grid key={index} xs={12} sm={6} md={4} item>
                    <Card
                        sx={{
                            background: theme.palette.mode === 'dark' ? theme.palette.dark.main : theme.palette.grey[50],
                            border: theme.palette.mode === 'dark' ? '1px solid transparent' : `1px solid${theme.palette.grey[100]}`,
                            '&:hover': {
                                borderColor: theme.palette.primary.main
                            },
                            cursor: onClick || onDoubleClick ? 'pointer' : 'default'
                        }}
                        onClick={() => onClick?.(data)}
                        onDoubleClick={() => onDoubleClick?.(data)}
                    >
                        <DataTableGenerateGridHeader data={data} components={components} />
                        <CardContent>
                            <Grid container spacing={gridSpacing}>
                                {_.map(
                                    _.filter(
                                        components,
                                        (component) =>
                                            component.content(data).type !== 'action' &&
                                            component.content(data).type !== 'header' &&
                                            component.content(data).gridActive !== false
                                    ),
                                    (component, indexComponent) => (
                                        <Grid key={indexComponent} item xs={6}>
                                            <Typography variant="caption">{component.title}</Typography>
                                            <Typography variant="h6">
                                                <DataTableGenerateType propsTable={component.content(data)} />
                                            </Typography>
                                        </Grid>
                                    )
                                )}
                            </Grid>
                        </CardContent>
                    </Card>
                </Grid>
            ))}
        </Grid>
    );
};

export default DatasTableGenerateGrid;
