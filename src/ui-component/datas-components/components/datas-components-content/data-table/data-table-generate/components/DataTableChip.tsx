import { DatasComponentsContentTableTypeChipProps } from '../../../dataComponentsContent.type';
import { Chip, Skeleton } from '@mui/material';
import { useDatasComponentsContext } from 'ui-component/datas-components/datasComponents.context';

const DataTableChip = ({ ...props }: DatasComponentsContentTableTypeChipProps) => {
    const { loading } = useDatasComponentsContext();
    return !loading ? <Chip size="small" {...props} /> : <Skeleton height={30} width={80} />;
};

export default DataTableChip;
