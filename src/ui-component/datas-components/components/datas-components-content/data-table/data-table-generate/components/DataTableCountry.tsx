import { IconButton, Skeleton } from '@mui/material';
import { IconAlien } from '@tabler/icons';
import ReactCountryFlag from 'react-country-flag';
import { useDatasComponentsContext } from 'ui-component/datas-components/datasComponents.context';
import { DatasComponentsContentTableTypeCountryProps } from '../../../dataComponentsContent.type';

const DataTableCountry = ({ country }: DatasComponentsContentTableTypeCountryProps) => {
    const { loading } = useDatasComponentsContext();

    if (loading) {
        return <Skeleton />;
    }

    if (country) {
        return (
            <ReactCountryFlag
                countryCode={country}
                svg
                style={{
                    width: '20px',
                    height: '20px'
                }}
            />
        );
    }

    if (!country) {
        return (
            <IconButton>
                <IconAlien />
            </IconButton>
        );
    }

    return null;
};

export default DataTableCountry;
