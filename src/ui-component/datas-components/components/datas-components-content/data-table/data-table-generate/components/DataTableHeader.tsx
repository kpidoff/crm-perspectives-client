import { Skeleton } from '@mui/material';
import { useDatasComponentsContext } from 'ui-component/datas-components/datasComponents.context';
import { DatasComponentsContentTableTypeHeaderProps } from '../../../dataComponentsContent.type';

const DataTableHeader = ({ component }: DatasComponentsContentTableTypeHeaderProps) => {
    const { loading } = useDatasComponentsContext();
    return !loading ? component || null : <Skeleton />;
};

export default DataTableHeader;
