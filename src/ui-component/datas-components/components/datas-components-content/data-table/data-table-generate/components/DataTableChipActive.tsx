import { DatasComponentsContentTableTypeChipActiveProps } from '../../../dataComponentsContent.type';
import { useTheme } from '@mui/material/styles';
import { Chip, Skeleton } from '@mui/material';
import { useDatasComponentsContext } from 'ui-component/datas-components/datasComponents.context';

const DataTableChipActive = ({ active, sx, ...props }: DatasComponentsContentTableTypeChipActiveProps) => {
    const theme = useTheme();
    const { loading } = useDatasComponentsContext();
    return !loading ? (
        <Chip
            label={active ? 'Activé' : 'Désactivé'}
            size="small"
            sx={
                active
                    ? {
                          ...sx,
                          background: theme.palette.mode === 'dark' ? theme.palette.dark.main : theme.palette.success.light + 60,
                          color: theme.palette.success.dark
                      }
                    : {
                          ...sx,
                          background: theme.palette.mode === 'dark' ? theme.palette.dark.main : theme.palette.orange.light + 80,
                          color: theme.palette.orange.dark
                      }
            }
            {...props}
        />
    ) : (
        <Skeleton height={30} width={50} />
    );
};

export default DataTableChipActive;
