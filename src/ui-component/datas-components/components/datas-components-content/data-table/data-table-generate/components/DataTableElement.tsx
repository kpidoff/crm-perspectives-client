import { Skeleton } from '@mui/material';
import { useDatasComponentsContext } from 'ui-component/datas-components/datasComponents.context';
import { DatasComponentsContentTableTypeElementProps } from '../../../dataComponentsContent.type';

const DataTableElement = ({ component }: DatasComponentsContentTableTypeElementProps) => {
    const { loading } = useDatasComponentsContext();
    return !loading ? component || null : <Skeleton />;
};

export default DataTableElement;
