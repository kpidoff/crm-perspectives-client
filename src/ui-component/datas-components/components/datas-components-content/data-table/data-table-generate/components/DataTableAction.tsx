import { IconButton, Skeleton, Stack, Tooltip } from '@mui/material';
import _ from 'lodash';
import { useDatasComponentsContext } from 'ui-component/datas-components/datasComponents.context';
import {
    DatasComponentsContentTableTypeActionItemProps,
    DatasComponentsContentTableTypeActionProps
} from '../../../dataComponentsContent.type';

const DataTableAction = ({ items }: DatasComponentsContentTableTypeActionProps) => {
    const { loading } = useDatasComponentsContext();

    const ButtonGenerate = ({ item }: { item: DatasComponentsContentTableTypeActionItemProps }) => {
        const Icon = item.icon;
        return item.visible !== false ? (
            <Tooltip placement="top" title={item.label}>
                <IconButton color="primary" size="large" {...item.buttonProps} onClick={item.onClick}>
                    <Icon sx={{ fontSize: '1.1rem' }} />
                </IconButton>
            </Tooltip>
        ) : null;
    };
    return (
        <Stack direction="row" justifyContent="center" alignItems="center">
            {_.map(items, (item, index) =>
                !loading ? (
                    <ButtonGenerate key={index} item={item} />
                ) : (
                    <Skeleton variant="circular" key={index} height={30} width={30} sx={{ marginLeft: '5px' }} />
                )
            )}
        </Stack>
    );
};

export default DataTableAction;
