import { Skeleton } from '@mui/material';
import { useDatasComponentsContext } from 'ui-component/datas-components/datasComponents.context';
import { formatAddress } from 'utils';
import { DatasComponentsContentTableTypeFormatAddressProps } from '../../../dataComponentsContent.type';

const DataTableFormatAddress = ({ address, city, postalCode }: DatasComponentsContentTableTypeFormatAddressProps) => {
    const { loading } = useDatasComponentsContext();
    return !loading ? (
        <span>{formatAddress(postalCode, city, address)}</span>
    ) : (
        <>
            <Skeleton />
            <Skeleton />
        </>
    );
};

export default DataTableFormatAddress;
