import { Skeleton } from '@mui/material';
import { useDatasComponentsContext } from 'ui-component/datas-components/datasComponents.context';
import { formatEmail } from 'utils';
import { DatasComponentsContentTableTypeFormatEmailProps } from '../../../dataComponentsContent.type';

const DataTableFormatEmail = ({ email }: DatasComponentsContentTableTypeFormatEmailProps) => {
    const { loading } = useDatasComponentsContext();
    return !loading ? <span>{formatEmail(email)}</span> : <Skeleton />;
};

export default DataTableFormatEmail;
