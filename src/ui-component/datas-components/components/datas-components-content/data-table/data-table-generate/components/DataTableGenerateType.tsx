import { DatasComponentsContentTableTypeProps } from '../../../dataComponentsContent.type';
import DataTableAction from './DataTableAction';
import DataTableChip from './DataTableChip';
import DataTableChipActive from './DataTableChipActive';
import DataTableCountry from './DataTableCountry';
import DataTableElement from './DataTableElement';
import DataTableFormatAddress from './DataTableFormatAddress';
import DataTableFormatEmail from './DataTableFormatEmail';
import DataTableFormatPhone from './DataTableFormatPhone';
import DataTableHeader from './DataTableHeader';

const DataTableGenerateType = ({ propsTable }: { propsTable: DatasComponentsContentTableTypeProps }) => {
    switch (propsTable.type) {
        case 'chipActive':
            return <DataTableChipActive {...propsTable} />;
        case 'chip':
            return <DataTableChip {...propsTable} />;
        case 'formatAddress':
            return <DataTableFormatAddress {...propsTable} />;
        case 'formatEmail':
            return <DataTableFormatEmail {...propsTable} />;
        case 'formatPhone':
            return <DataTableFormatPhone {...propsTable} />;
        case 'action':
            return <DataTableAction {...propsTable} />;
        case 'element':
            return <DataTableElement {...propsTable} />;
        case 'header':
            return <DataTableHeader {...propsTable} />;
        case 'country':
            return <DataTableCountry {...propsTable} />;
        default:
            return <p>Error type row</p>;
    }
};

export default DataTableGenerateType;
