import { Skeleton } from '@mui/material';
import { useDatasComponentsContext } from 'ui-component/datas-components/datasComponents.context';
import { formatPhone } from 'utils';
import { DatasComponentsContentTableTypeFormatPhoneProps } from '../../../dataComponentsContent.type';

const DataTableFormatPhone = ({ phone }: DatasComponentsContentTableTypeFormatPhoneProps) => {
    const { loading } = useDatasComponentsContext();
    return !loading ? <span>{formatPhone(phone)}</span> : <Skeleton />;
};

export default DataTableFormatPhone;
