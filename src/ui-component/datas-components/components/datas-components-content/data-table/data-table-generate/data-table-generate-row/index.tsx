import { TableContainer, Table, TableHead, TableRow, TableCell, TableBody, Card, TableFooter } from '@mui/material';
import _ from 'lodash';
import { FC } from 'react';
import { FormattedMessage } from 'react-intl';
import { useDatasComponentsContext } from 'ui-component/datas-components/datasComponents.context';
import {
    DatasComponentsContentProps,
    DatasComponentsContentTableProps,
    DatasComponentsContentTableTypeActionProps
} from '../../../dataComponentsContent.type';
import DataTableGenerateType from '../components/DataTableGenerateType';

const DatasTableGenerateRow: FC<DatasComponentsContentProps & DatasComponentsContentTableProps> = ({
    components,
    onClick,
    onDoubleClick
}) => {
    const { datas, itemsPerPage, loading } = useDatasComponentsContext();

    const GenerateBody = () => {
        if (!loading && datas?.length === 0) {
            return (
                <TableFooter>
                    <TableRow>
                        <TableCell align="center" colSpan={999}>
                            <FormattedMessage id="no-registration" />
                        </TableCell>
                    </TableRow>
                </TableFooter>
            );
        }
        return (
            <TableBody>
                {_.map(!loading ? datas : Array(itemsPerPage), (data, index) => (
                    <TableRow
                        hover
                        role="checkbox"
                        key={index}
                        sx={{
                            cursor: onClick || onDoubleClick ? 'pointer' : 'default'
                        }}
                        onClick={() => onClick?.(data)}
                        onDoubleClick={() => onDoubleClick?.(data)}
                    >
                        {_.map(components, (component, indexComponent) => (
                            <TableCell key={indexComponent}>
                                <DataTableGenerateType propsTable={component.content(data)} />
                            </TableCell>
                        ))}
                    </TableRow>
                ))}
            </TableBody>
        );
    };
    return (
        <Card>
            <TableContainer>
                <Table stickyHeader aria-label="sticky table">
                    <TableHead>
                        <TableRow>
                            {_.map(components, (component, index) => {
                                if (component.content(undefined).type === 'action') {
                                    const componentAction = component.content(undefined) as DatasComponentsContentTableTypeActionProps;
                                    const listItemsVisible = _.filter(componentAction.items, (item) => item.visible !== false);
                                    if (listItemsVisible.length === 0) return null;
                                }

                                return <TableCell key={index}>{component.title}</TableCell>;
                            })}
                        </TableRow>
                    </TableHead>
                    <GenerateBody />
                </Table>
            </TableContainer>
        </Card>
    );
};

export default DatasTableGenerateRow;
