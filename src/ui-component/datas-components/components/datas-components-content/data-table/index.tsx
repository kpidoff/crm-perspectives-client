import { FC } from 'react';
import { useDatasComponentsContext } from 'ui-component/datas-components/datasComponents.context';
import { DatasComponentsContentProps, DatasComponentsContentTableProps } from '../dataComponentsContent.type';
import DatasTableGenerateGrid from './data-table-generate/data-table-generate-grid';
import DatasTableGenerateRow from './data-table-generate/data-table-generate-row';

const DatasTable: FC<DatasComponentsContentProps & DatasComponentsContentTableProps> = ({ ...props }) => {
    const { currentDisplay } = useDatasComponentsContext();
    if (currentDisplay === 'list') {
        return <DatasTableGenerateRow {...props} />;
    }
    return <DatasTableGenerateGrid {...props} />;
};

export default DatasTable;
