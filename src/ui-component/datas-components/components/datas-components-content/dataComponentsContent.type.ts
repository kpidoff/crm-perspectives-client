import { ChipProps, IconButtonProps, SxProps } from '@mui/material';
import { Theme } from '@mui/system';
import { ReactNode } from 'react';
import { OverrideIcon } from 'types';

export type DatasComponentsContentListProps = {
    component: (data: any | undefined, loading: boolean) => JSX.Element | Element;
    spacing?: number;
    contentType: 'defaultList';
};

export type DatasComponentsContentTableComponentsProps = {
    title: string | JSX.Element;
    content: (data: any | undefined | unknown) => DatasComponentsContentTableTypeProps;
};

export type DatasComponentsContentGridProps = {
    contentType: 'defaultGrid';
    components: (data: any | undefined | unknown) => DatasComponentsContentGridTypeProps[];
};
export type DatasComponentsContentGridComponentProps = {
    contentType: 'gridComponent';
    component: (data: any | undefined | unknown, loading: boolean) => ReactNode;
};

export type DatasComponentsContentTableProps = {
    contentType: 'defaultTable';
    components: DatasComponentsContentTableComponentsProps[];
};

export type DatasComponentsContentTableTypeDefaultProps = {
    gridActive?: boolean;
    subType?: 'subHeader';
};

export type DatasComponentsContentGridTypeProps =
    | DatasComponentsContentGridTypeMediaProps
    | DatasComponentsContentGridTypeTitleProps
    | DatasComponentsContentGridTypeBodyProps
    | DatasComponentsContentGridTypeRatingProps
    | DatasComponentsContentGridTypeFooterProps
    | DatasComponentsContentGridTypeListProps
    | DatasComponentsContentGridTypeListCheckedProps
    | DatasComponentsContentGridTypeSubTitleProps;

export type DatasComponentsContentGridTypeMediaProps = {
    type: 'media';
    image: string | undefined | null;
    title: string;
    to?: string;
};

export type DatasComponentsContentGridTypeTitleProps = {
    type: 'title';
    title: string | undefined | null | ReactNode;
    subTitle?: string | undefined | null | ReactNode;
};

export type DatasComponentsContentGridTypeSubTitleProps = {
    type: 'subTitle';
    title: string | undefined | null | ReactNode;
    subTitle?: string | undefined | null | ReactNode;
};

export type DatasComponentsContentGridTypeBodyProps = {
    type: 'body';
    content: string | undefined | null;
};

export type DatasComponentsContentGridTypeListProps = {
    type: 'list';
    items: ReactNode[];
};

export type DatasComponentsContentGridTypeListCheckedProps = {
    type: 'listChecked';
    items: {
        label: string;
        checked: boolean | undefined;
    }[];
};

export type DatasComponentsContentGridTypeFooterProps = {
    type: 'footer';
    footer: {
        label?: ReactNode;
        subLabel?: ReactNode;
        button?: ReactNode;
    };
};

export type DatasComponentsContentGridTypeRatingProps = {
    type: 'rating';
    value: number | undefined | null;
    precision?: number;
    label?: string;
};

export type DatasComponentsContentTableTypeProps = DatasComponentsContentTableTypeDefaultProps &
    (
        | DatasComponentsContentTableTypeChipActiveProps
        | DatasComponentsContentTableTypeElementProps
        | DatasComponentsContentTableTypeFormatAddressProps
        | DatasComponentsContentTableTypeFormatEmailProps
        | DatasComponentsContentTableTypeFormatPhoneProps
        | DatasComponentsContentTableTypeHeaderProps
        | DatasComponentsContentTableTypeActionProps
        | DatasComponentsContentTableTypeChipProps
        | DatasComponentsContentTableTypeCountryProps
    );

export type DatasComponentsContentTableTypeChipActiveProps = Omit<ChipProps, 'label' | 'size'> & {
    type: 'chipActive';
    active: boolean | undefined;
};

export type DatasComponentsContentTableTypeChipProps = Omit<ChipProps, 'label' | 'size'> & {
    type: 'chip';
};

export type DatasComponentsContentTableTypeHeaderProps = {
    type: 'header';
    component: JSX.Element | undefined;
};

export type DatasComponentsContentTableTypeActionItemProps = {
    label: JSX.Element | string;
    icon: OverrideIcon;
    onClick: () => void;
    buttonProps?: Omit<IconButtonProps, 'onClick'>;
    visible?: boolean;
};

export type DatasComponentsContentTableTypeActionProps = {
    type: 'action';
    items: DatasComponentsContentTableTypeActionItemProps[];
};

export type DatasComponentsContentTableTypeElementProps = {
    type: 'element';
    component: JSX.Element | undefined;
};

export type DatasComponentsContentTableTypeCountryProps = {
    type: 'country';
    country: string | undefined;
};

export type DatasComponentsContentTableTypeFormatAddressProps = {
    type: 'formatAddress';
    postalCode?: string | null;
    city?: string | null;
    address?: string | null;
};

export type DatasComponentsContentTableTypeFormatEmailProps = {
    type: 'formatEmail';
    email?: string | null;
};

export type DatasComponentsContentTableTypeFormatPhoneProps = {
    type: 'formatPhone';
    phone?: string | null;
};

type DatasComponentsContentAllType =
    | DatasComponentsContentListProps
    | DatasComponentsContentTableProps
    | DatasComponentsContentGridProps
    | DatasComponentsContentGridComponentProps;

export type DatasComponentsContentProps<T = DatasComponentsContentAllType> = T & {
    onClick?: (data: any | undefined) => void;
    onDoubleClick?: (data: any | undefined) => void;
    editMode?: boolean;
    sx?: SxProps<Theme>;
};
