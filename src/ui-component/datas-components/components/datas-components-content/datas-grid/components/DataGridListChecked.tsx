import { Grid, List, ListItem, ListItemIcon, Skeleton } from '@mui/material';
import _ from 'lodash';
import { useDatasComponentsContext } from 'ui-component/datas-components/datasComponents.context';
import { DatasComponentsContentGridTypeListCheckedProps } from '../../dataComponentsContent.type';
import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';

const DataGridListChecked = ({ items }: DatasComponentsContentGridTypeListCheckedProps) => {
    const { loading } = useDatasComponentsContext();
    return !loading ? (
        <List>
            {_.map(items, (item, index) => (
                <ListItem key={index}>
                    <ListItemIcon>
                        {item.checked ? <CheckIcon color="success" fontSize="small" /> : <CloseIcon color="error" fontSize="small" />}
                        {item.label}
                    </ListItemIcon>
                </ListItem>
            ))}
        </List>
    ) : (
        <List>
            {_.map(items, (item, index) => (
                <Grid key={index} container spacing={2}>
                    <Grid item xs={2}>
                        <Skeleton height={30} />
                    </Grid>
                    <Grid item xs={10}>
                        <Skeleton height={30} />
                    </Grid>
                </Grid>
            ))}
        </List>
    );
};

export default DataGridListChecked;
