import { Grid, Skeleton, Typography } from '@mui/material';
import { useDatasComponentsContext } from 'ui-component/datas-components/datasComponents.context';
import { DatasComponentsContentGridTypeSubTitleProps } from '../../dataComponentsContent.type';

const DataGridSubTitle = ({ title, subTitle }: DatasComponentsContentGridTypeSubTitleProps) => {
    const { loading } = useDatasComponentsContext();
    return !loading ? (
        <Grid item xs={12}>
            {!!title && (
                <Typography variant="h6" sx={{ color: 'grey.500', paddingBottom: '3px', fontStyle: 'italic' }}>
                    {title}
                </Typography>
            )}
        </Grid>
    ) : (
        <Grid item xs={12}>
            <Skeleton height={30} />
        </Grid>
    );
};

export default DataGridSubTitle;
