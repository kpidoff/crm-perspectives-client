import { Grid, Skeleton, Typography } from '@mui/material';
import { useDatasComponentsContext } from 'ui-component/datas-components/datasComponents.context';
import { textTruncate } from 'ui-component/datas-components/datasComponents.utils';
import { DatasComponentsContentGridTypeBodyProps } from '../../dataComponentsContent.type';

const DataGridBody = ({ content }: DatasComponentsContentGridTypeBodyProps) => {
    const { loading } = useDatasComponentsContext();
    return !loading ? (
        <Grid item xs={12}>
            <Typography
                variant="body2"
                sx={{
                    overflow: 'hidden'
                }}
            >
                {textTruncate(content)}
            </Typography>
        </Grid>
    ) : (
        <>
            <Skeleton height={15} />
            <Skeleton height={15} />
        </>
    );
};

export default DataGridBody;
