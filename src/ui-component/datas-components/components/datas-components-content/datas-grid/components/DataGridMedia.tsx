import { CardMedia, Skeleton } from '@mui/material';
import { Link } from 'react-router-dom';
import { useDatasComponentsContext } from 'ui-component/datas-components/datasComponents.context';
import { DatasComponentsContentGridTypeMediaProps } from '../../dataComponentsContent.type';
import { useTheme } from '@mui/material/styles';
import NoImageGrey from 'assets/images/no-image/no-image-grey.png';
import NoImageWhite from 'assets/images/no-image/no-image-white.png';

const DataGridMedia = ({ image, title, to }: DatasComponentsContentGridTypeMediaProps) => {
    const theme = useTheme();
    const { loading } = useDatasComponentsContext();
    if (loading) {
        return <Skeleton variant="rectangular" height={220} />;
    }

    if (image) {
        return <CardMedia sx={{ height: 220 }} image={image} title={title} component={Link} to={to || ''} />;
    }

    return (
        <CardMedia
            sx={{ height: 220 }}
            image={theme.palette.mode === 'dark' ? NoImageWhite : NoImageGrey}
            title={title}
            component={Link}
            to={to || ''}
        />
    );
};

export default DataGridMedia;
