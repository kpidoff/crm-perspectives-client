import { Grid, Rating, Skeleton, Stack, Typography } from '@mui/material';
import { useDatasComponentsContext } from 'ui-component/datas-components/datasComponents.context';
import { DatasComponentsContentGridTypeRatingProps } from '../../dataComponentsContent.type';

const DataGridRating = ({ value, precision, label }: DatasComponentsContentGridTypeRatingProps) => {
    const { loading } = useDatasComponentsContext();
    return !loading ? (
        <Grid item xs={12} sx={{ pt: '8px !important' }}>
            <Stack direction="row" alignItems="center" spacing={1}>
                <Rating precision={precision || 1} name="size-small" value={value} size="small" readOnly />
                {label && <Typography variant="caption">({label})</Typography>}
            </Stack>
        </Grid>
    ) : (
        <Stack direction="row" spacing={1}>
            <Skeleton height={30} width={20} />
            <Skeleton height={30} width={20} />
            <Skeleton height={30} width={20} />
            <Skeleton height={30} width={20} />
            <Skeleton height={30} width={20} />
        </Stack>
    );
};

export default DataGridRating;
