import { List, ListItem, Skeleton } from '@mui/material';
import _ from 'lodash';
import { useDatasComponentsContext } from 'ui-component/datas-components/datasComponents.context';
import { DatasComponentsContentGridTypeListProps } from '../../dataComponentsContent.type';

const DataGridList = ({ items }: DatasComponentsContentGridTypeListProps) => {
    const { loading } = useDatasComponentsContext();
    return !loading ? (
        <List>
            {_.map(items, (item, index) => (
                <ListItem key={index}>{item}</ListItem>
            ))}
        </List>
    ) : (
        <List>
            {_.map(items, (item, index) => (
                <ListItem key={index}>
                    <Skeleton key={index} height={15} width="100%" />
                </ListItem>
            ))}
        </List>
    );
};

export default DataGridList;
