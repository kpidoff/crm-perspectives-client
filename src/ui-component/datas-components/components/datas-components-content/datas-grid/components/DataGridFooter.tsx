import { Grid, Skeleton, Stack, Typography } from '@mui/material';
import { useDatasComponentsContext } from 'ui-component/datas-components/datasComponents.context';
import { DatasComponentsContentGridTypeFooterProps } from '../../dataComponentsContent.type';

const DataGridFooter = ({ footer }: DatasComponentsContentGridTypeFooterProps) => {
    const { loading } = useDatasComponentsContext();
    return !loading ? (
        <Grid item xs={12}>
            <Stack direction="row" justifyContent="space-between" alignItems="center">
                <Grid container spacing={1}>
                    <Grid item>
                        <Typography variant="h4">{footer.label}</Typography>
                    </Grid>
                    <Grid item>
                        <Typography variant="h6" sx={{ color: 'grey.500' }}>
                            {footer.subLabel}
                        </Typography>
                    </Grid>
                </Grid>
                {footer.button}
            </Stack>
        </Grid>
    ) : (
        <Grid item xs={12}>
            <Stack direction="row" justifyContent="space-between" alignItems="center">
                <Grid container spacing={1}>
                    {footer.label && (
                        <Grid sx={{ marginTop: '10px' }} item xs={6}>
                            <Skeleton height={30} />
                        </Grid>
                    )}
                    {footer.subLabel && (
                        <Grid sx={{ marginTop: '15px' }} item xs={4}>
                            <Typography variant="h6" sx={{ color: 'grey.500' }}>
                                <Skeleton height={20} />
                            </Typography>
                        </Grid>
                    )}
                    {footer.button && (
                        <Grid item xs={2}>
                            <Skeleton height={50} />
                        </Grid>
                    )}
                </Grid>
            </Stack>
        </Grid>
    );
};

export default DataGridFooter;
