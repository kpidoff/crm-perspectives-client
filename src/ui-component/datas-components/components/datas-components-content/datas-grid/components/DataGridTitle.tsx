import { Grid, Skeleton, Stack, Typography } from '@mui/material';
import { useDatasComponentsContext } from 'ui-component/datas-components/datasComponents.context';
import { DatasComponentsContentGridTypeTitleProps } from '../../dataComponentsContent.type';

const DataGridTitle = ({ title, subTitle }: DatasComponentsContentGridTypeTitleProps) => {
    const { loading } = useDatasComponentsContext();
    return !loading ? (
        <Grid item xs={12}>
            <Stack
                direction="row"
                spacing={1}
                divider={
                    <Typography variant="h6" sx={{ color: 'grey.500', paddingTop: '2px' }}>
                        -
                    </Typography>
                }
            >
                {!!title && (
                    <Typography variant="subtitle1" sx={{ textDecoration: 'none' }}>
                        {title}
                    </Typography>
                )}

                {!!subTitle && (
                    <Typography variant="h6" sx={{ color: 'grey.500', paddingTop: '3px' }}>
                        {subTitle}
                    </Typography>
                )}
            </Stack>
        </Grid>
    ) : (
        <Grid item xs={12}>
            <Skeleton height={30} />
        </Grid>
    );
};

export default DataGridTitle;
