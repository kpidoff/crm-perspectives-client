import { DatasComponentsContentGridTypeProps } from '../../dataComponentsContent.type';
import DataGridBody from './DataGridBody';
import DataGridFooter from './DataGridFooter';
import DataGridList from './DataGridList';
import DataGridListChecked from './DataGridListChecked';
import DataGridMedia from './DataGridMedia';
import DataGridRating from './DataGridRating';
import DataGridSubTitle from './DataGridSubTitle';
import DataGridTitle from './DataGridTitle';

const DataGridGenerateType = ({ propsGrid }: { propsGrid: DatasComponentsContentGridTypeProps }) => {
    switch (propsGrid.type) {
        case 'media':
            return <DataGridMedia {...propsGrid} />;
        case 'title':
            return <DataGridTitle {...propsGrid} />;
        case 'subTitle':
            return <DataGridSubTitle {...propsGrid} />;
        case 'body':
            return <DataGridBody {...propsGrid} />;
        case 'rating':
            return <DataGridRating {...propsGrid} />;
        case 'footer':
            return <DataGridFooter {...propsGrid} />;
        case 'list':
            return <DataGridList {...propsGrid} />;
        case 'listChecked':
            return <DataGridListChecked {...propsGrid} />;
        default:
            return <p>Error type row</p>;
    }
};

export default DataGridGenerateType;
