import { CardContent, Grid, Typography } from '@mui/material';
import _ from 'lodash';
import { FC } from 'react';
import { FormattedMessage } from 'react-intl';
import { gridSpacing } from 'store/constant';
import MainCard from 'ui-component/cards/MainCard';
import { DataComponentsGrid } from 'ui-component/datas-components/datasComponents.type';
import { DatasComponentsContentGridProps, DatasComponentsContentProps } from '../dataComponentsContent.type';
import DataGridGenerateType from './components/DataGridGenerateType';

const DatasGrid: FC<DataComponentsGrid & DatasComponentsContentProps & DatasComponentsContentGridProps> = ({
    datas,
    loading,
    itemsPerPage,
    components
}) => {
    if (!loading && datas?.length === 0) {
        return (
            <Grid container sx={{ marginTop: '10px' }} spacing={gridSpacing} direction="column" alignItems="center" justifyContent="center">
                <Grid item xs={12}>
                    <Typography variant="caption">
                        <FormattedMessage id="no-data" />
                    </Typography>
                </Grid>
            </Grid>
        );
    }

    return (
        <Grid container spacing={gridSpacing}>
            {_.map(!loading ? datas : Array(itemsPerPage), (data, index) => (
                <Grid key={index} item xs={12} sm={6} md={4} lg={3}>
                    <MainCard
                        content={false}
                        boxShadow
                        sx={{
                            '&:hover': {
                                transform: 'scale3d(1.02, 1.02, 1)',
                                transition: 'all .4s ease-in-out'
                            }
                        }}
                    >
                        {_.map(
                            _.filter(components(data), (component) => component.type === 'media'),
                            (component, indexComponent) => (
                                <DataGridGenerateType key={indexComponent} propsGrid={component} />
                            )
                        )}
                        <CardContent sx={{ p: 2 }}>
                            {_.map(
                                _.filter(components(data), (component) => component.type !== 'media'),
                                (component, indexComponent) => (
                                    <DataGridGenerateType key={indexComponent} propsGrid={component} />
                                )
                            )}
                        </CardContent>
                    </MainCard>
                </Grid>
            ))}
        </Grid>
    );
};

export default DatasGrid;
