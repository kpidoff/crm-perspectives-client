import { Divider, Stack } from '@mui/material';
import _ from 'lodash';
import { FormattedMessage } from 'react-intl';
import DataComponents, { DatasComponentsSearch, DatasComponentsContent, DatasComponentsPaginate } from 'ui-component/datas-components';
import { DataComponentsList, DatasComponentsSearchProps } from 'ui-component/datas-components/datasComponents.type';
import DatasComponentsButtonFilter from '../../datas-components-button-filter';
import {
    DatasComponentsContentGridComponentProps,
    DatasComponentsContentGridProps,
    DatasComponentsContentProps
} from '../../datas-components-content/dataComponentsContent.type';
import { DatasComponentsPaginateProps } from '../../datas-components-paginate';
import { useTheme } from '@mui/material/styles';
import DatasComponentsButtonAction, { DatasComponentsButtonActionProps } from '../../datas-components-button-action';

type DatasGridWrapperProps = {
    dataComponent: Omit<DataComponentsList, 'type' | 'children'>;
    datasHeaderComponent?: JSX.Element;
    actions?: DatasComponentsButtonActionProps[];
    datasComponentContent: DatasComponentsContentProps<DatasComponentsContentGridProps | DatasComponentsContentGridComponentProps>;
    datasComponentPaginate: DatasComponentsPaginateProps;
    datasComponentsSearch?: Omit<DatasComponentsSearchProps, 'align' | 'sx' | 'autoFocus'>;
};

const DatasGridWrapper = ({
    dataComponent,
    datasComponentContent,
    datasComponentPaginate,
    datasComponentsSearch,
    datasHeaderComponent,
    actions
}: DatasGridWrapperProps) => {
    const theme = useTheme();
    return (
        <DataComponents type="grid" {...dataComponent}>
            {(!!datasComponentsSearch || !!dataComponent.filter) && (
                <Stack
                    direction="row"
                    justifyContent="flex-end"
                    alignItems="center"
                    spacing={1}
                    divider={<Divider sx={{ borderColor: theme.palette.primary.dark, mt: 0.625, mb: 1.875 }} />}
                    sx={{ paddingBottom: '20px' }}
                >
                    {datasComponentsSearch && (
                        <DatasComponentsSearch
                            {...datasComponentsSearch}
                            wait={datasComponentsSearch.wait || 250}
                            autoFocus
                            label={datasComponentsSearch.label || <FormattedMessage id="search" />}
                            align="fullWidth"
                        />
                    )}
                    {dataComponent.filter && <DatasComponentsButtonFilter />}
                    {actions && _.map(actions, (action, index) => <DatasComponentsButtonAction key={index} {...action} />)}
                </Stack>
            )}

            {datasHeaderComponent}

            <DatasComponentsContent {...datasComponentContent} />

            <DatasComponentsPaginate {...datasComponentPaginate} />
        </DataComponents>
    );
};

export default DatasGridWrapper;
