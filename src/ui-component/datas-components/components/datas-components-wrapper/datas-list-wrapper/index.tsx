import { Stack } from '@mui/material';
import _ from 'lodash';
import { FormattedMessage } from 'react-intl';
import DataComponents, { DatasComponentsSearch, DatasComponentsContent, DatasComponentsPaginate } from 'ui-component/datas-components';
import { DataComponentsList, DatasComponentsSearchProps } from 'ui-component/datas-components/datasComponents.type';
import DatasComponentsButtonAction, { DatasComponentsButtonActionProps } from '../../datas-components-button-action';
import DatasComponentsButtonFilter from '../../datas-components-button-filter';
import { DatasComponentsContentListProps, DatasComponentsContentProps } from '../../datas-components-content/dataComponentsContent.type';
import { DatasComponentsPaginateProps } from '../../datas-components-paginate';

type DatasListWrapperProps = {
    dataComponent: Omit<DataComponentsList, 'type' | 'children'>;
    datasComponentContent: Omit<DatasComponentsContentProps<DatasComponentsContentListProps>, 'contentType'>;
    datasComponentPaginate: DatasComponentsPaginateProps;
    datasComponentsSearch?: Omit<DatasComponentsSearchProps, 'align' | 'sx' | 'autoFocus'>;
    datasHeaderComponent?: JSX.Element;
    actions?: DatasComponentsButtonActionProps[];
};

const DatasListWrapper = ({
    dataComponent,
    datasComponentContent,
    datasComponentPaginate,
    datasComponentsSearch,
    datasHeaderComponent,
    actions
}: DatasListWrapperProps) => (
    <DataComponents type="list" {...dataComponent}>
        {(!!datasComponentsSearch || !!dataComponent.filter) && (
            <Stack direction="row" justifyContent="flex-end" alignItems="center" spacing={2}>
                {datasComponentsSearch && (
                    <DatasComponentsSearch
                        {...datasComponentsSearch}
                        wait={datasComponentsSearch.wait || 250}
                        autoFocus
                        label={datasComponentsSearch.label || <FormattedMessage id="search" />}
                        align="fullWidth"
                    />
                )}
                {dataComponent.filter && <DatasComponentsButtonFilter />}
                {actions && _.map(actions, (action, index) => <DatasComponentsButtonAction key={index} {...action} />)}
            </Stack>
        )}

        {datasHeaderComponent && <div style={{ paddingTop: '15px' }}>{datasHeaderComponent}</div>}

        <DatasComponentsContent {...datasComponentContent} contentType="defaultList" />

        <DatasComponentsPaginate {...datasComponentPaginate} />
    </DataComponents>
);

export default DatasListWrapper;
