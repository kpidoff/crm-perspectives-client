import { Stack, Divider } from '@mui/material';
import { FormattedMessage } from 'react-intl';
import DataComponents, {
    DatasComponentsSearch,
    DatasComponentsContent,
    DatasComponentsPaginate,
    DatasComponentDisplay,
    DatasComponentsHeader,
    DatasComponentsHeaderTitle
} from 'ui-component/datas-components';
import {
    DataComponentsTable,
    DatasComponentsHeaderTitleProps,
    DatasComponentsSearchProps
} from 'ui-component/datas-components/datasComponents.type';
import DatasComponentsButtonFilter from '../../datas-components-button-filter';
import { DatasComponentsContentTableProps, DatasComponentsContentProps } from '../../datas-components-content/dataComponentsContent.type';
import { DatasComponentsPaginateProps } from '../../datas-components-paginate';

type DatasComponentsTableProps = {
    dataComponent: Omit<DataComponentsTable, 'type' | 'children'>;
    datasComponentContent: Omit<DatasComponentsContentProps<DatasComponentsContentTableProps>, 'contentType'>;
    datasComponentPaginate: DatasComponentsPaginateProps;
    datasComponentHeader?: {
        datasComponentsSearch?: Omit<DatasComponentsSearchProps, 'align' | 'sx' | 'autoFocus'>;
        datasComponentsHeaderTitle?: DatasComponentsHeaderTitleProps;
    };
};

const DatasTableWrapper = ({
    dataComponent,
    datasComponentContent,
    datasComponentPaginate,
    datasComponentHeader
}: DatasComponentsTableProps) => (
    <DataComponents type="table" {...dataComponent} initialDisplay={dataComponent.initialDisplay || 'list'}>
        <DatasComponentDisplay />
        {datasComponentHeader && (
            <>
                <DatasComponentsHeader>
                    <DatasComponentsHeaderTitle title={datasComponentHeader.datasComponentsHeaderTitle?.title} />
                </DatasComponentsHeader>
                <Stack direction="row" justifyContent="flex-end" alignItems="center" spacing={2}>
                    {datasComponentHeader.datasComponentsSearch && (
                        <DatasComponentsSearch
                            {...datasComponentHeader.datasComponentsSearch}
                            wait={datasComponentHeader.datasComponentsSearch.wait || 250}
                            autoFocus
                            label={datasComponentHeader.datasComponentsSearch.label || <FormattedMessage id="search" />}
                            align="end"
                        />
                    )}
                    {dataComponent.filter && <DatasComponentsButtonFilter />}
                </Stack>
            </>
        )}

        <Divider sx={{ borderColor: 'grey.400', paddingTop: 2 }} />

        <DatasComponentsContent {...datasComponentContent} contentType="defaultTable" />

        <DatasComponentsPaginate {...datasComponentPaginate} />
    </DataComponents>
);

export default DatasTableWrapper;
