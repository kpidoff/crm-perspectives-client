import { SyntheticEvent } from 'react';

// material-ui
import { useTheme } from '@mui/material/styles';
import { Divider, Grid, IconButton } from '@mui/material';

// project imports
import SubCard from 'ui-component/cards/SubCard';
import { gridSpacing } from 'store/constant';

// assets
import HighlightOffTwoToneIcon from '@mui/icons-material/HighlightOffTwoTone';

// ==============================|| CONTACT CARD/LIST USER DETAILS ||============================== //

export interface DatasListNameEditingProps {
    onClose?: (e: SyntheticEvent) => void;
    children: JSX.Element;
    header?: JSX.Element;
}

const DatasListNameEditing = ({ onClose, children, header }: DatasListNameEditingProps) => {
    const theme = useTheme();

    return (
        <SubCard
            sx={{
                background: theme.palette.mode === 'dark' ? theme.palette.dark.main : theme.palette.grey[50]
            }}
        >
            <Grid container spacing={gridSpacing}>
                <Grid item xs={12}>
                    <Grid container alignItems="center" spacing={1}>
                        <Grid item xs zeroMinWidth>
                            <Grid container spacing={1}>
                                <Grid item xs={12}>
                                    {header}
                                </Grid>
                            </Grid>
                        </Grid>
                        {onClose && (
                            <Grid item>
                                <IconButton onClick={onClose} size="large">
                                    <HighlightOffTwoToneIcon />
                                </IconButton>
                            </Grid>
                        )}
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <Divider />
                </Grid>
                <Grid item xs={12}>
                    {children}
                </Grid>
            </Grid>
        </SubCard>
    );
};

export default DatasListNameEditing;
