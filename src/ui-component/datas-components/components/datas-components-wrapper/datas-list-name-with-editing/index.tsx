import { Stack, Grid } from '@mui/material';
import { FormattedMessage } from 'react-intl';
import DataComponents, { DatasComponentsSearch, DatasComponentsContent, DatasComponentsPaginate } from 'ui-component/datas-components';
import { DataComponentsListName, DatasComponentsSearchProps } from 'ui-component/datas-components/datasComponents.type';
import DatasComponentsButtonFilter from '../../datas-components-button-filter';
import { DatasComponentsContentListProps, DatasComponentsContentProps } from '../../datas-components-content/dataComponentsContent.type';
import { DatasComponentsPaginateProps } from '../../datas-components-paginate';

type DatasListNameWithEditWrapperProps = {
    dataComponent: Omit<DataComponentsListName, 'type' | 'children'>;
    datasComponentContent: Omit<DatasComponentsContentProps<DatasComponentsContentListProps>, 'contentType'>;
    datasComponentPaginate: DatasComponentsPaginateProps;
    datasComponentsSearch?: Omit<DatasComponentsSearchProps, 'align' | 'sx' | 'autoFocus'>;
    datasListNameEditing?: (data: any) => JSX.Element | null;
    entity: any | undefined;
};

const DatasListNameWithEditWrapper = ({
    dataComponent,
    datasComponentContent,
    datasListNameEditing,
    datasComponentPaginate,
    datasComponentsSearch,
    entity
}: DatasListNameWithEditWrapperProps) => (
    <DataComponents type="listName" {...dataComponent}>
        <Stack direction="row" justifyContent="flex-end" alignItems="center" spacing={2}>
            {datasComponentsSearch && (
                <DatasComponentsSearch
                    {...datasComponentsSearch}
                    wait={datasComponentsSearch.wait || 250}
                    autoFocus
                    label={datasComponentsSearch.label || <FormattedMessage id="search" />}
                    align="fullWidth"
                    sx={{ display: entity !== undefined ? { xs: 'none', md: 'flex' } : 'flex' }}
                />
            )}
            {dataComponent.filter && <DatasComponentsButtonFilter />}
        </Stack>

        <Grid container>
            <Grid
                item
                md={datasListNameEditing && entity !== undefined ? 8 : 12}
                sx={{ display: entity !== undefined ? { xs: 'none', md: 'block' } : 'block' }}
            >
                <DatasComponentsContent {...datasComponentContent} contentType="defaultList" editMode={!!entity} />
            </Grid>
            {datasListNameEditing && entity !== undefined && (
                <Grid sx={{ marginTop: '30px' }} item xs={12} md={4}>
                    {datasListNameEditing(entity)}
                </Grid>
            )}
        </Grid>

        <DatasComponentsPaginate {...datasComponentPaginate} />
    </DataComponents>
);

export default DatasListNameWithEditWrapper;
