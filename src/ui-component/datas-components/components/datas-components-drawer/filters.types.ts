import { PostalCode } from 'types/settings/postalCode';

export type FilterBase = {
    label: React.ReactNode | string;
    filterKey: string;
    disabled?: boolean;
    expanded?: boolean;
    defaultExpand?: boolean | undefined;
    hidden?: boolean;
    wait?: number;
};

export type TextFilterType = FilterBase & {
    type: 'text';
    value?: string;
    mapper?: (value: string) => any;
};

export type LocationFilterInputType = {
    city?: string;
    postal_code?: string;
    lat?: number;
    lng?: number;
    radius?: number;
};

export type LocationFilterType = FilterBase & {
    type: 'location';
    value?: LocationFilterInputType;
    mapper?: (value: Partial<PostalCode> | undefined) => any;
};

export type RatingFilterType = FilterBase & {
    type: 'rating';
    value?: number;
    mapper?: (value: number | undefined) => any;
    precision: number;
};

export type SocietiesFilterType = Omit<FilterBase, 'wait'> & {
    type: 'societies';
    value?: number;
    mapper?: (value: number | undefined) => any;
};

export type SliderRangeFilterType = FilterBase & {
    min: number;
    max: number;
    step: number;
    type: 'sliderRange';
    value?: number[] | undefined;
    mapper?: (value: number[] | undefined) => any;
};

export type CheckboxesFilterMappers = 'KeyValueMapper' | 'InMapper';
export type CheckboxesFilterItem = { key: string; label: string; value?: boolean };
export type CheckboxesFilterType = Omit<FilterBase, 'wait'> & {
    type: 'checkboxes';
    mapper?: (items: CheckboxesFilterItem[]) => any;
    items: CheckboxesFilterItem[];
};

export type SelectFilterMappers = 'KeyMapper' | 'KeyOptionMapper';
export type SelectFilterItemKey = string | number;
export type SelectFilterItem = {
    key: SelectFilterItemKey;
    value: any;
    label: string | React.ReactNode;
};
export type SelectFilterType = Omit<FilterBase, 'wait'> & {
    value?: any;
    type: 'select';
    all?: boolean;
    mapper?: (item: SelectFilterItem | undefined) => any;
    items: SelectFilterItem[];
};

export type FilterType =
    | TextFilterType
    | CheckboxesFilterType
    | SelectFilterType
    | SliderRangeFilterType
    | RatingFilterType
    | SocietiesFilterType
    | LocationFilterType;

export type FiltersContextType = {
    updateFilter: (filterKey: FilterBase['filterKey'], value: any) => void;
    clearFilter?: () => void;
};
