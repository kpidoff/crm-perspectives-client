import { Drawer as DrawerMui, useMediaQuery } from '@mui/material';
import { appDrawerWidth } from 'store/constant';
import { useTheme } from '@mui/material/styles';
import useConfig from 'hooks/useConfig';

const Drawer = ({ open, onClose, children }: { open: boolean; children: React.ReactNode; onClose: (open: boolean) => void }) => {
    const theme = useTheme();
    const { borderRadius } = useConfig();
    const matchDownLG = useMediaQuery(theme.breakpoints.down('xl'));
    return (
        <DrawerMui
            sx={{
                ml: open ? 3 : 0,
                height: matchDownLG ? '100vh' : 'auto',
                flexShrink: 0,
                overflowX: 'hidden',
                width: appDrawerWidth,
                '& .MuiDrawer-paper': {
                    height: matchDownLG ? '100%' : 'auto',
                    width: appDrawerWidth,
                    position: matchDownLG ? 'fixed' : 'relative',
                    border: 'none',
                    borderRadius: matchDownLG ? 0 : `${borderRadius}px`
                }
            }}
            variant={matchDownLG ? 'temporary' : 'persistent'}
            anchor="right"
            open={open}
            // ModalProps={{ keepMounted: true }}
            onClose={onClose}
        >
            {children}
        </DrawerMui>
    );
};

export default Drawer;
