import { SelectFilterItem } from '../../filters.types';

function allOrKey(value: SelectFilterItem | undefined) {
    if (value && value.key === 'all') {
        return undefined;
    }
    return value?.key;
}

const selectFilterMappers = {
    allOrKey
};

export default selectFilterMappers;
