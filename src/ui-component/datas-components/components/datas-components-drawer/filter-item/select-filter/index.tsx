import { FormControl, InputLabel, MenuItem, Select } from '@mui/material';
import _ from 'lodash';
import { useDatasComponentsContext } from 'ui-component/datas-components/datasComponents.context';
import { SelectFilterItem, SelectFilterType } from '../../filters.types';

const selectFilterItemAll: SelectFilterItem = {
    key: 'all',
    label: '<Tous>',
    value: 'all'
};

const SelectFilter = (props: SelectFilterType) => {
    const { filterKey, mapper, items, label, value, all } = props;
    const { filter } = useDatasComponentsContext();

    const handleChange = (newValue: SelectFilterItem | undefined) => {
        filter?.updateFilter(mapper ? mapper(newValue) : newValue, filterKey);
    };

    let currentValue = value;
    if (!_.toString(currentValue)) {
        all ? (currentValue = 'all') : (currentValue = '');
    }

    return (
        <FormControl fullWidth>
            <InputLabel id="select-label">{label}</InputLabel>
            <Select
                labelId="select-label"
                id="simple-select"
                label={label}
                value={currentValue}
                onChange={(values) => handleChange(_.find(items, (item) => item.value === values.target.value))}
            >
                {all && (
                    <MenuItem key={selectFilterItemAll.key} value={selectFilterItemAll.value}>
                        {selectFilterItemAll.label}
                    </MenuItem>
                )}
                {_.map(items, (i) => (
                    <MenuItem key={i.key} value={i.value}>
                        {i.label}
                    </MenuItem>
                ))}
            </Select>
        </FormControl>
    );
};

export default SelectFilter;
