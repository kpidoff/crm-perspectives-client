import { FormControl, FormGroup, FormControlLabel, Checkbox } from '@mui/material';
import _ from 'lodash';
import { useFiltersContext } from '../../filters.context';
import { CheckboxesFilterItem, CheckboxesFilterType } from '../../filters.types';

const CheckboxesFilter = (props: CheckboxesFilterType) => {
    const { items, filterKey, mapper } = props;
    const { updateFilter } = useFiltersContext();

    const handleChange = (checkbox: CheckboxesFilterItem, event: React.ChangeEvent<HTMLInputElement>) => {
        const newCheckboxes = [
            ..._.filter(items, (i) => i.key !== checkbox.key),
            {
                ...checkbox,
                value: event.target.checked
            }
        ];
        updateFilter(mapper ? mapper(newCheckboxes) : newCheckboxes, filterKey);
    };
    return (
        <FormControl component="fieldset" variant="standard">
            <FormGroup>
                {_.map(items, (i) => (
                    <FormControlLabel
                        key={i.key}
                        control={<Checkbox onChange={(event) => handleChange(i, event)} checked={i.value} name={i.label} />}
                        label={i.label}
                    />
                ))}
            </FormGroup>
        </FormControl>
    );
};

export default CheckboxesFilter;
