import _ from 'lodash';
import { keyValArrToObj } from 'utils';
import { CheckboxesFilterItem } from '../../filters.types';

function checkboxesToArray(items: CheckboxesFilterItem[]) {
    return keyValArrToObj(
        _.map(items, (i) => ({
            key: i.key,
            value: i.value
        }))
    );
}

function checkboxesToArrayKey(items: CheckboxesFilterItem[]) {
    return _.map(
        _.filter(items, (item) => item.value === true),
        (i) => i.key
    );
}

function checkboxesToBooleanArray(values: { key: string; value: any }[]) {
    const array = _.filter(values, (v) => v.value === true);
    return _.map(array, (v) => v.key);
}

function checkboxesToBooleanArrayNumericKey(items: CheckboxesFilterItem[]) {
    const array = _.filter(
        _.map(items, (i) => ({
            key: i.key,
            value: i.value
        })),
        (v) => v.value === true
    );
    return _.map(array, (v) => _.toInteger(v.key));
}

const checkboxesFilterMappers = {
    checkboxesToArray,
    checkboxesToBooleanArray,
    checkboxesToBooleanArrayNumericKey,
    checkboxesToArrayKey
};

export default checkboxesFilterMappers;
