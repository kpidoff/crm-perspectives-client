import { PostalCode } from 'types/settings/postalCode';

function location(item: Partial<PostalCode> | undefined) {
    return {
        city: item?.city,
        postal_code: item?.postal_code,
        lat: item?.lat,
        lng: item?.lng
    };
}

const locationFilterMappers = {
    location
};

export default locationFilterMappers;
