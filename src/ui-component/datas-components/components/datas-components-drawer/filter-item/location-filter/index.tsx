import { Grid, TextField } from '@mui/material';
import _ from 'lodash';
import { useEffect, useState } from 'react';
import { gridSpacing } from 'store/constant';
import TextFieldCity from 'ui-component/input/input-postal-code/input-city';
import TextFieldPostalCode from 'ui-component/input/input-postal-code/input-postal-code';
import { useFiltersContext } from '../../filters.context';
import { LocationFilterInputType, LocationFilterType } from '../../filters.types';

const DEFAULT_LOCATION: LocationFilterInputType = {
    city: '',
    postal_code: '',
    radius: 0,
    lat: 0,
    lng: 0
};

const LocationFilter = (props: LocationFilterType) => {
    const { value, mapper, filterKey } = props;
    const { updateFilter } = useFiltersContext();
    const [location, setLocation] = useState<LocationFilterInputType>(value || DEFAULT_LOCATION);

    useEffect(() => {
        !value && setLocation(DEFAULT_LOCATION);
    }, [value]);

    const handleSelect = _.debounce((newLocation: LocationFilterInputType) => {
        setLocation(newLocation);
        updateFilter(mapper ? mapper(newLocation) : newLocation, filterKey);
    }, 150);

    return (
        <Grid container spacing={gridSpacing}>
            <Grid item xs={12}>
                <TextFieldPostalCode
                    label="Code postal"
                    onSelect={(postal) =>
                        handleSelect({ ...location, postal_code: postal.postal_code, city: postal.city, lat: postal.lat, lng: postal.lng })
                    }
                    value={location.postal_code || ''}
                    fullWidth
                />
            </Grid>
            <Grid item xs={12}>
                <TextFieldCity
                    label="Ville"
                    onSelect={(postal) =>
                        handleSelect({ ...location, postal_code: postal.postal_code, city: postal.city, lat: postal.lat, lng: postal.lng })
                    }
                    value={location.city || ''}
                    fullWidth
                />
            </Grid>
            <Grid item xs={12}>
                <TextField
                    defaultValue={location?.radius}
                    onChange={(values) => handleSelect({ ...location, radius: _.toInteger(values.currentTarget.value) })}
                    label="Rayon (km)"
                    type="number"
                    fullWidth
                />
            </Grid>
        </Grid>
    );
};

export default LocationFilter;
