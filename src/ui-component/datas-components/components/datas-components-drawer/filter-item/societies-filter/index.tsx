import SelectAgencyAccess from 'ui-component/select/select-agency-access';
import { useFiltersContext } from '../../filters.context';
import { SocietiesFilterType } from '../../filters.types';

const SocietiesFilter = (props: SocietiesFilterType) => {
    const { label, value, filterKey, mapper } = props;
    const { updateFilter } = useFiltersContext();

    const handleChange = (id: number) => {
        updateFilter(mapper ? mapper(id) : id, filterKey);
    };
    return <SelectAgencyAccess onChange={handleChange} label={label} value={value} enabledAll />;
};

export default SocietiesFilter;
