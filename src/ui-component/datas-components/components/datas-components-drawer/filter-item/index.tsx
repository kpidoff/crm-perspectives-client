import { FilterType } from '../filters.types';
import CheckboxesFilter from './checkboxes-filter';
import LocationFilter from './location-filter';
import RatingFilter from './rating-filter';
import SelectFilter from './select-filter';
import SliderRangeFilter from './slider-range-filter';
import SocietiesFilter from './societies-filter';
import TextFilter from './text-filter';

const FilterItem = ({ filter }: { filter: FilterType }) => {
    switch (filter.type) {
        case 'checkboxes':
            return <CheckboxesFilter {...filter} />;
        case 'text':
            return <TextFilter {...filter} />;
        case 'select':
            return <SelectFilter {...filter} />;
        case 'sliderRange':
            return <SliderRangeFilter {...filter} />;
        case 'rating':
            return <RatingFilter {...filter} />;
        case 'societies':
            return <SocietiesFilter {...filter} />;
        case 'location':
            return <LocationFilter {...filter} />;
        default:
            return null;
    }
};

export default FilterItem;
