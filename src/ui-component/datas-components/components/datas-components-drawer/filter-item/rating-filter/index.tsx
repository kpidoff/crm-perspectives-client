import { IconButton, Rating, Stack } from '@mui/material';
import _ from 'lodash';
import { IconCircleX } from '@tabler/icons';
import { useTheme } from '@mui/material/styles';
import { useEffect, useState } from 'react';
import { useFiltersContext } from '../../filters.context';
import { RatingFilterType } from '../../filters.types';

const RatingFilter = (props: RatingFilterType) => {
    const theme = useTheme();
    const { value, filterKey, mapper, wait, precision } = props;
    const [rate, setRate] = useState(value);
    const { updateFilter } = useFiltersContext();

    useEffect(() => {
        setRate(value);
    }, [value]);

    useEffect(() => {
        const debounce = _.debounce(() => {
            updateFilter(mapper ? mapper(rate) : rate, filterKey);
        }, wait);

        debounce();

        return () => {
            debounce.cancel();
        };
    }, [filterKey, rate, mapper, updateFilter, wait]);

    return (
        <Stack direction="row" alignItems="center" spacing={1}>
            <Rating precision={precision} name="size-small" value={rate || 0} onChange={(values, newValue) => setRate(newValue || 0)} />
            {rate && rate > 0 ? (
                <IconButton aria-label="delete" size="small" onClick={() => setRate(0)}>
                    <IconCircleX color={theme.palette.grey[900]} />
                </IconButton>
            ) : null}
        </Stack>
    );
};

export default RatingFilter;
