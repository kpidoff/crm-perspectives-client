function toMinMax(value: number[] | undefined) {
    if (value) {
        return {
            min: value[0],
            max: value[1]
        };
    }
    return null;
}

const sliderRangeFilterMappers = {
    toMinMax
};

export default sliderRangeFilterMappers;
