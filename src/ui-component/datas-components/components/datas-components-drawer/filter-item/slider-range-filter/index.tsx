import { Slider } from '@mui/material';
import _ from 'lodash';
import { useEffect, useState } from 'react';
import { useFiltersContext } from '../../filters.context';
import { SliderRangeFilterType } from '../../filters.types';

const SliderRangeFilter = (props: SliderRangeFilterType) => {
    const { min, max, step, value, filterKey, wait, mapper } = props;
    const [valueSlider, setValueSlider] = useState<number[] | undefined>(value);
    const { updateFilter } = useFiltersContext();

    useEffect(() => {
        setValueSlider(value);
    }, [value]);

    // eslint-disable-next-line consistent-return
    useEffect(() => {
        // TODO quentin : voir comment on peut optimisé sa
        if (value !== valueSlider) {
            const debounce = _.debounce(() => {
                updateFilter(mapper ? mapper(valueSlider) : valueSlider, filterKey);
            }, wait);

            debounce();

            return () => {
                debounce.cancel();
            };
        }
    }, [valueSlider, mapper, updateFilter, wait, filterKey, value]);

    return (
        <Slider
            min={min}
            step={step}
            max={max}
            value={valueSlider}
            onChange={(values, newValue) => setValueSlider(newValue as number[])}
            valueLabelDisplay="auto"
        />
    );
};

export default SliderRangeFilter;
