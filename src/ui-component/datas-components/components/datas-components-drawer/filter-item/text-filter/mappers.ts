import _ from 'lodash';

function textToInteger(value: string) {
    return _.toInteger(value);
}

const textFilterMappers = {
    textToInteger
};

export default textFilterMappers;
