import { TextField } from '@mui/material';
import _ from 'lodash';
import { useFiltersContext } from '../../filters.context';
import { TextFilterType } from '../../filters.types';

const TextFilter = (props: TextFilterType) => {
    const { value, label, filterKey, mapper, wait } = props;
    const { updateFilter } = useFiltersContext();

    const handleChange = _.debounce((newValue) => {
        updateFilter(mapper ? mapper(newValue) : newValue, filterKey);
    }, wait);

    return <TextField label={label} type="text" defaultValue={value} onChange={(values) => handleChange(values.currentTarget.value)} />;
};

export default TextFilter;
