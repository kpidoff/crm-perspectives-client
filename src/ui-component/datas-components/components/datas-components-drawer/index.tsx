import { LoadingButton } from '@mui/lab';
import { CardContent, Grid, Stack } from '@mui/material';
import _ from 'lodash';
import { FormattedMessage } from 'react-intl';
import { gridSpacing } from 'store/constant';
import MainCard from 'ui-component/cards/MainCard';
import { useDatasComponentsContext } from 'ui-component/datas-components/datasComponents.context';
import Accordion from 'ui-component/extended/Accordion';
import FilterItem from './filter-item';
import { filtersContext } from './filters.context';

const DatasComponentsDrawer = () => {
    const { filter, matchDownLG } = useDatasComponentsContext();
    return filter ? (
        <filtersContext.Provider value={{ updateFilter: filter.updateFilter, clearFilter: filter.onClear }}>
            <MainCard border={!matchDownLG} content={false} sx={{ overflow: 'visible' }}>
                <CardContent sx={{ p: 1, height: matchDownLG ? '100vh' : 'auto' }}>
                    <Grid container spacing={gridSpacing}>
                        <Grid item xs={12}>
                            <Accordion
                                data={filter.schema
                                    .filter((f) => !f.hidden)
                                    .map((f, index) => ({
                                        id: _.toString(index),
                                        title: f.label,
                                        content: <FilterItem filter={f} />,
                                        disabled: f.disabled,
                                        expanded: f.expanded,
                                        defaultExpand: f.defaultExpand
                                    }))}
                            />
                        </Grid>
                        <Grid item xs={12} sx={{ m: 1 }}>
                            <Stack direction="row" justifyContent="center" alignItems="center">
                                <LoadingButton variant="contained" fullWidth color="error" onClick={filter.onClear}>
                                    <FormattedMessage id="clearAll" />
                                </LoadingButton>
                            </Stack>
                        </Grid>
                    </Grid>
                </CardContent>
            </MainCard>
        </filtersContext.Provider>
    ) : null;
};

export default DatasComponentsDrawer;
