import { createContext, useContext } from 'react';
import { FiltersContextType } from './filters.types';

export const filtersContext = createContext<FiltersContextType | null>(null);

export const useFiltersContext = () => {
    const context = useContext(filtersContext);

    if (!context) throw new Error('Context page filters must be use inside provider');

    return context;
};
