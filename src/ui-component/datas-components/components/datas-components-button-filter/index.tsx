import { ButtonBase, Typography } from '@mui/material';
import FilterAltIcon from '@mui/icons-material/FilterAlt';
import { useDatasComponentsContext } from 'ui-component/datas-components/datasComponents.context';
import { FormattedMessage } from 'react-intl';
import _ from 'lodash';

const DatasComponentsButtonFilter = () => {
    const { currentOpenDrawer, openDrawer, filter } = useDatasComponentsContext();
    return _.filter(filter?.schema, (type) => type.hidden === false || type.hidden === undefined).length > 0 ? (
        <ButtonBase disableRipple onClick={() => openDrawer(!currentOpenDrawer)}>
            <FilterAltIcon sx={{ fontWeight: 500, color: 'secondary.200' }} />
            <Typography variant="h5" sx={{ mt: 0.5, display: { xs: 'none', sm: 'block' } }}>
                <FormattedMessage id="filter" />
            </Typography>
        </ButtonBase>
    ) : null;
};

export default DatasComponentsButtonFilter;
