import { Fade, Grid, Pagination } from '@mui/material';
import { useEffect, useState } from 'react';
import { scrollTop } from 'utils';

export interface DatasComponentsPaginateProps {
    numberOfPage: number | undefined;
    currentPage: number | undefined;
    onChangePage: (page: number) => void;
}

export const DatasComponentsPaginate = ({ numberOfPage, onChangePage, currentPage }: DatasComponentsPaginateProps) => {
    const [currentCount, setCurrentCount] = useState(0);
    const [page, setPage] = useState(1);

    useEffect(() => {
        setPage(currentPage || 1);
    }, [currentPage]);

    useEffect(() => {
        if (numberOfPage !== undefined) {
            setCurrentCount(numberOfPage);
        }
    }, [numberOfPage]);

    return (
        <Grid container spacing={0} sx={{ marginTop: '15px' }} direction="column" alignItems="center" justifyContent="center">
            <Grid item xs={12}>
                <Fade in={currentCount > 1} unmountOnExit>
                    <Pagination
                        color="primary"
                        page={page}
                        count={currentCount}
                        onChange={(_event, value) => {
                            setPage(value);
                            onChangePage(value);
                            scrollTop();
                        }}
                    />
                </Fade>
            </Grid>
        </Grid>
    );
};

export default DatasComponentsPaginate;
