import { ButtonBase, ButtonBaseProps, SvgIconTypeMap, Typography } from '@mui/material';
import { OverridableComponent } from '@mui/material/OverridableComponent';

export type DatasComponentsButtonActionProps = Omit<ButtonBaseProps, 'disableRipple'> & {
    label: string;
    icon: OverridableComponent<SvgIconTypeMap<{}, 'svg'>> & {
        muiName: string;
    };
};
const DatasComponentsButtonAction = ({ icon, label, ...props }: DatasComponentsButtonActionProps) => {
    const Icon = icon;
    return (
        <ButtonBase {...props} disableRipple>
            <Icon sx={{ fontWeight: 500, color: 'secondary.200' }} />
            <Typography variant="h5" sx={{ mt: 0.5, display: { xs: 'none', sm: 'block' } }}>
                {label}
            </Typography>
        </ButtonBase>
    );
};

export default DatasComponentsButtonAction;
