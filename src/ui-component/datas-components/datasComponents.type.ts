import { TextFieldProps } from '@mui/material';
import { FilterBase, FilterType } from './components/datas-components-drawer/filters.types';

type DataComponentsBaseProps = {
    loading: boolean;
    heightSkeleton?: number;
    datas: any[] | undefined;
    itemsPerPage: number;
    filter?: {
        schema: FilterType[];
        onClear: () => void;
        updateFilter: (value: any, filterKey: FilterBase['filterKey']) => void;
    };
};

export type DisplayType = 'grid' | 'list';

export type DataComponentsListName = DataComponentsBaseProps & {
    type: 'listName';
    sortName: string;
};

export type DataComponentsList = DataComponentsBaseProps & {
    type: 'list';
};

export type DataComponentsGrid = DataComponentsBaseProps & {
    type: 'grid';
};

export type DataComponentsTable = DataComponentsBaseProps & {
    type: 'table';
    initialDisplay?: DisplayType;
};

export type DataComponentsProps = DataComponentType & {
    children: React.ReactNode | string;
};

export type DataComponentType = DataComponentsList | DataComponentsListName | DataComponentsTable | DataComponentsGrid;

export type DatasComponentsContextProps = DataComponentType & {
    onChangeDisplay: (display: DisplayType) => void;
    currentDisplay: DisplayType | undefined;
    matchDownXS: boolean;
    matchDownLG: boolean;
    currentOpenDrawer: boolean;
    openDrawer: (open: boolean) => void;
};

export type DatasComponentsSearchInputProps = Omit<TextFieldProps, 'type' | 'onChange' | 'InputProps' | 'value'> & {
    align?: 'center' | 'end' | 'start' | 'fullWidth' | 'none';
    initialValue?: string | undefined;
    updateSearch?: (value: any, filterkey: string) => void;
};
export type DatasComponentsSearchProps = DatasComponentsSearchInputProps & {
    filterkey: string;
    wait?: number;
};
export type DatasComponentsHeaderTitleProps = {
    title?: string | JSX.Element;
};
export type SearchContextType = {
    onSearch: (value: any) => void;
    currentValue: string | undefined;
};
