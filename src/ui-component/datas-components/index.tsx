import { useEffect, useState } from 'react';
import { datasComponentsContext } from './datasComponents.context';
import { DataComponentsProps, DisplayType } from './datasComponents.type';
import { useTheme } from '@mui/material/styles';
import { useMediaQuery } from '@mui/material';

export { default as DatasComponentsPaginate } from './components/datas-components-paginate';
export { default as DatasComponentsHeader } from './components/datas-components-header';
export { default as DatasComponentsHeaderTitle } from './components/datas-components-header/components/DatasComponentsHeaderTitle';
export { default as DatasComponentDisplay } from './components/datas-components-header/components/DatasComponentsHeaderDisplay';
export { default as DatasComponentsContent } from './components/datas-components-content';
export { default as DatasComponentsSearch } from './components/datas-components-search';
export { default as DatasComponentsButtonFilter } from './components/datas-components-button-filter';

const DataComponents = ({ children, ...props }: DataComponentsProps) => {
    const theme = useTheme();
    const matchDownXS = useMediaQuery(theme.breakpoints.down('md'));
    const matchDownLG = useMediaQuery(theme.breakpoints.down('xl'));

    const { type } = props;
    const [currentDisplay, setCurrentDisplay] = useState<DisplayType | undefined>(type === 'table' ? props.initialDisplay : undefined);
    const [currentOpenDrawer, setOpenDrawer] = useState(false);

    useEffect(() => {
        if (type === 'table') {
            matchDownXS ? setCurrentDisplay('grid') : setCurrentDisplay('list');
        }
    }, [matchDownXS, setCurrentDisplay, type]);

    return (
        <datasComponentsContext.Provider
            value={{
                ...props,
                onChangeDisplay: setCurrentDisplay,
                currentDisplay,
                matchDownXS,
                matchDownLG,
                currentOpenDrawer,
                openDrawer: setOpenDrawer
            }}
        >
            {children}
        </datasComponentsContext.Provider>
    );
};

export default DataComponents;
