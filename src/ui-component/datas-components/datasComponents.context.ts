import { createContext, useContext } from 'react';
import { DatasComponentsContextProps } from './datasComponents.type';

export const datasComponentsContext = createContext<DatasComponentsContextProps | null>(null);

export const useDatasComponentsContext = () => {
    const context = useContext(datasComponentsContext);

    if (!context) throw new Error('Context datas component must be use inside provider');

    return context;
};
