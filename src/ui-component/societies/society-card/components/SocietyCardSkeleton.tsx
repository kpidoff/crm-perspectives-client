import { Card, Grid, Typography, Stack, Skeleton } from '@mui/material';
import { gridSpacing } from 'store/constant';
import { useTheme } from '@mui/material/styles';

const SocietyCardSkeleton = () => {
    const theme = useTheme();
    return (
        <Card
            sx={{
                p: 2,
                background: theme.palette.mode === 'dark' ? theme.palette.dark.main : theme.palette.grey[50],
                border: theme.palette.mode === 'dark' ? '1px solid transparent' : `1px solid${theme.palette.grey[100]}`,
                '&:hover': {
                    borderColor: theme.palette.primary.main
                }
            }}
        >
            <Grid container spacing={gridSpacing}>
                <Grid item xs={12}>
                    <Stack direction="row" justifyContent="flex-end" alignItems="flex-start" sx={{ mt: -0.75, mr: -0.75 }}>
                        <Skeleton width={20} />
                    </Stack>
                </Grid>
                <Grid item xs={12}>
                    <Typography variant="h3" component="div">
                        <Grid container spacing={2} alignItems="center">
                            <Grid item xs zeroMinWidth>
                                <Grid container spacing={1}>
                                    <Grid item>
                                        <Skeleton width={150} />
                                    </Grid>
                                    <Grid item>
                                        <Skeleton width={20} />
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Typography>
                    <Skeleton width={200} />
                </Grid>
                <Grid item xs={12}>
                    <Skeleton />
                    <Skeleton />
                </Grid>
                <Grid item xs={12}>
                    <Grid container spacing={gridSpacing}>
                        <Grid item xs={6}>
                            <Skeleton />
                            <Skeleton />
                        </Grid>
                        <Grid item xs={6}>
                            <Skeleton />
                            <Skeleton />
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <Grid container spacing={1}>
                        <Grid item xs={6}>
                            <Skeleton height={70} />
                        </Grid>
                        <Grid item xs={6}>
                            <Skeleton height={70} />
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Card>
    );
};

export default SocietyCardSkeleton;
