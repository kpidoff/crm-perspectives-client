import React, { useEffect } from 'react';

// material-ui
import { useTheme } from '@mui/material/styles';
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Typography } from '@mui/material';
import { useEditSociety } from 'hooks/settings/society/useSociety';
import { LoadingButton } from '@mui/lab';
import { FormattedMessage } from 'react-intl';

// ===============================|| UI DIALOG - SWEET ALERT ||=============================== //

interface SocietyChangeStatusProps {
    societyId: number;
    activated: boolean | undefined;
    open: boolean;
    onClose: () => void;
}

const SocietyChangeStatus = ({ societyId, activated, open, onClose }: SocietyChangeStatusProps) => {
    const { changeStatus, loading, society, reset } = useEditSociety();
    const theme = useTheme();

    useEffect(() => {
        if (society) {
            onClose();
            reset();
        }
    }, [onClose, society, reset]);

    return (
        <Dialog
            open={open}
            onClose={onClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
            sx={{ p: 3 }}
        >
            <>
                <DialogTitle id="alert-dialog-title">
                    <FormattedMessage id="user-activated-title" />
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        <Typography variant="body2" component="span">
                            {activated ? (
                                <FormattedMessage id="user-activated-content-disable" />
                            ) : (
                                <FormattedMessage id="user-activated-content-enable" />
                            )}
                        </Typography>
                    </DialogContentText>
                </DialogContent>
                <DialogActions sx={{ pr: 2.5 }}>
                    <Button
                        sx={{ color: theme.palette.error.dark, borderColor: theme.palette.error.dark }}
                        onClick={onClose}
                        color="secondary"
                    >
                        <FormattedMessage id="no" />
                    </Button>
                    <LoadingButton variant="contained" size="small" loading={loading} onClick={() => changeStatus(societyId, !activated)}>
                        <FormattedMessage id="yes" />
                    </LoadingButton>
                </DialogActions>
            </>
        </Dialog>
    );
};

export default SocietyChangeStatus;
