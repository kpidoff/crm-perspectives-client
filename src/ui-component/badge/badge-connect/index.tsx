import { Badge, BadgeProps, Stack, Tooltip } from '@mui/material';
import { styled } from '@mui/material/styles';
import { FC } from 'react';

const StyledBadge = styled(Badge)(({ theme, color }) => ({
    '& .MuiBadge-badge': {
        backgroundColor: '#44b700',
        color: '#44b700',
        boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
        '&::after': {
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            borderRadius: '50%',
            animation: 'ripple 1.2s infinite ease-in-out',
            border: '1px solid currentColor',
            content: '""'
        }
    },
    '@keyframes ripple': {
        '0%': {
            transform: 'scale(.8)',
            opacity: 1
        },
        '100%': {
            transform: 'scale(2.4)',
            opacity: 0
        }
    }
}));

const BadgeConnect: FC<BadgeProps & { children: JSX.Element; onLine?: boolean; tooltip?: string }> = ({
    children,
    onLine,
    tooltip,
    ...props
}) => (
    <Stack direction="row" spacing={2}>
        {onLine ? (
            <Tooltip title={tooltip || ''}>
                <StyledBadge overlap="circular" anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }} variant="dot" {...props}>
                    {children}
                </StyledBadge>
            </Tooltip>
        ) : (
            children
        )}
    </Stack>
);

export default BadgeConnect;
