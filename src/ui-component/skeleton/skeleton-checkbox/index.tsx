import { Grid, Skeleton } from '@mui/material';
import { FC } from 'react';

const SkeletonCheckbox: FC<{ label?: boolean }> = ({ label = true }) => (
    <Grid container spacing={2}>
        <Grid item>
            <Skeleton height={35} width={20} />
        </Grid>
        <Grid item>{label && <Skeleton variant="text" height={35} width={350} />}</Grid>
    </Grid>
);

export default SkeletonCheckbox;
