interface SkeletonWrappeListProps {
    skeleton: JSX.Element;
    itemsPerPage: number;
    [key: string]: any;
}

const SkeletonWrapper = ({ skeleton, itemsPerPage }: SkeletonWrappeListProps) => {
    const Skeleton = () => skeleton;
    const GenereSkeleton = () => {
        if (itemsPerPage > 1) {
            const skeletons = [];
            for (let index = 0; index < itemsPerPage; index += 1) {
                skeletons.push(<Skeleton key={index} />);
            }
            return <>{skeletons}</>;
        }

        return skeleton;
    };
    return <GenereSkeleton />;
};

export default SkeletonWrapper;
