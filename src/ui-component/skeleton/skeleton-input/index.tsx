import { Skeleton, SkeletonProps } from '@mui/material';
import { FC } from 'react';

const SkeletonInput: FC<{ height?: number } & SkeletonProps> = ({ height = 80, ...props }) => (
    <Skeleton {...props} height={height} sx={{ ...props.sx, marginBottom: -2, marginTop: -2 }} />
);

export default SkeletonInput;
