import { CardMedia, Grid, useMediaQuery } from '@mui/material';
import { Box } from '@mui/system';
import _ from 'lodash';
import Slider, { Settings } from 'react-slick';
import { gridSpacing } from 'store/constant';
import Avatar from 'ui-component/extended/Avatar';
import { useTheme } from '@mui/material/styles';
import useConfig from 'hooks/useConfig';
import { useEffect, useState } from 'react';

export interface SliderViewProps {
    defaultItem: string | undefined;
    items: string[] | undefined;
    loading?: boolean;
}

const SliderView = ({ defaultItem, items, loading = false }: SliderViewProps) => {
    const theme = useTheme();
    const { borderRadius } = useConfig();
    const [selected, setSelected] = useState<string | undefined>(undefined);
    const [modal, setModal] = useState(false);
    const matchDownLG = useMediaQuery(theme.breakpoints.up('lg'));

    useEffect(() => {
        setSelected(defaultItem);
    }, [defaultItem]);

    useEffect(() => {
        !selected && items && items?.length > 0 && setSelected(items[0]);
    }, [items, selected]);

    const lgNo = matchDownLG ? 4 : 3;

    let numberSlides = 1;
    if (items) {
        numberSlides = items.length > 3 ? lgNo : items?.length;
    }

    const settings: Settings = {
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: numberSlides,
        slidesToScroll: numberSlides
    };

    return !loading ? (
        <Grid container alignItems="center" justifyContent="center" spacing={gridSpacing}>
            <Grid item xs={12}>
                <CardMedia
                    onClick={() => setModal(!modal)}
                    component="img"
                    image={selected}
                    sx={{ borderRadius: `${borderRadius}px`, overflow: 'hidden', cursor: 'zoom-in' }}
                />
            </Grid>
            <Grid item xs={11} sm={7} md={9} lg={10} xl={8}>
                <Slider {...settings}>
                    {_.map(items, (item, index) => (
                        <Box key={index} onClick={() => setSelected(item)} sx={{ p: 1 }}>
                            <Avatar
                                outline={selected === item}
                                size={matchDownLG ? 'lg' : 'md'}
                                color="primary"
                                src={item}
                                variant="rounded"
                                sx={{ m: '0 auto', cursor: 'pointer' }}
                            />
                        </Box>
                    ))}
                </Slider>
            </Grid>
        </Grid>
    ) : (
        <p>chargement</p>
    );
};

export default SliderView;
