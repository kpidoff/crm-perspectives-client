import React, { FC, ReactNode, useEffect } from 'react';

// material-ui
import { useTheme } from '@mui/material/styles';
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    ButtonProps,
    Tooltip,
    Typography,
    IconButton
} from '@mui/material';
import { useEditUser } from 'hooks/settings/user/useUser';
import { LoadingButton } from '@mui/lab';
import { FormattedMessage } from 'react-intl';

// ===============================|| UI DIALOG - SWEET ALERT ||=============================== //

export type UserChangeAdministratorButtonProps = Omit<ButtonProps, 'variant'> & {
    variant?: 'none' | 'text' | 'outlined' | 'contained';
    tooltip?: string;
};

const UserChangeAdministrator: FC<
    UserChangeAdministratorButtonProps & { userId: number; administrator: boolean | undefined; children?: JSX.Element | ReactNode | string }
> = ({ userId, administrator, children, ...props }) => {
    const { variant, tooltip, ...otherProps } = props;
    const { changeAdministrator, loading, user } = useEditUser();
    const theme = useTheme();
    const [open, setOpen] = React.useState(false);
    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    useEffect(() => {
        user && setOpen(false);
    }, [user]);

    return (
        <>
            <Tooltip placement="top" title={tooltip || ''}>
                {variant !== 'none' ? (
                    <Button {...otherProps} variant={variant} onClick={handleClickOpen}>
                        {children}
                    </Button>
                ) : (
                    <IconButton onClick={handleClickOpen} color={otherProps.color} size="large">
                        {otherProps.startIcon}
                    </IconButton>
                )}
            </Tooltip>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                sx={{ p: 3 }}
            >
                {open && (
                    <>
                        <DialogTitle id="alert-dialog-title">
                            <FormattedMessage id="user-administrator-title" />
                        </DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                <Typography variant="body2" component="span">
                                    {!administrator ? (
                                        <FormattedMessage id="user-administrator-content-enable" />
                                    ) : (
                                        <FormattedMessage id="user-administrator-content-desable" />
                                    )}
                                </Typography>
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions sx={{ pr: 2.5 }}>
                            <Button
                                sx={{ color: theme.palette.error.dark, borderColor: theme.palette.error.dark }}
                                onClick={handleClose}
                                color="secondary"
                            >
                                Non
                            </Button>
                            <LoadingButton
                                variant="contained"
                                size="small"
                                loading={loading}
                                onClick={() => changeAdministrator(userId, !administrator)}
                            >
                                Oui
                            </LoadingButton>
                        </DialogActions>
                    </>
                )}
            </Dialog>
        </>
    );
};

export default UserChangeAdministrator;
