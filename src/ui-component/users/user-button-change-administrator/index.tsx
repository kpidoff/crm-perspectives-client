import { FC } from 'react';
import BlockIcon from '@mui/icons-material/Block';
import UserChangeAdministrator, { UserChangeAdministratorButtonProps } from './components/UserChangeAdministrator';
import DoneIcon from '@mui/icons-material/Done';
import { FormattedMessage, useIntl } from 'react-intl';
import { UserProfile } from 'types/user';

interface UserButtonChangeAdministratorProps {
    userId: UserProfile['id'];
    userAdministrator: UserProfile['administrator'] | undefined;
}

const UserButtonChangeAdministrator: FC<UserButtonChangeAdministratorProps & UserChangeAdministratorButtonProps> = ({
    userAdministrator,
    userId,
    ...props
}) => {
    const intl = useIntl();

    return (
        <UserChangeAdministrator
            userId={userId}
            administrator={userAdministrator}
            color={userAdministrator ? 'error' : 'success'}
            startIcon={userAdministrator ? <BlockIcon sx={{ fontSize: '1.1rem' }} /> : <DoneIcon sx={{ fontSize: '1.1rem' }} />}
            tooltip={
                userAdministrator
                    ? intl.formatMessage({ id: 'user-desable-administrator' })
                    : intl.formatMessage({ id: 'user-enable-administrator' })
            }
            {...props}
        >
            <FormattedMessage id="admin" />
        </UserChangeAdministrator>
    );
};

export default UserButtonChangeAdministrator;
