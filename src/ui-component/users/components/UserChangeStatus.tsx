import { useEffect } from 'react';

// material-ui
import { useTheme } from '@mui/material/styles';
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Typography } from '@mui/material';
import { useEditUser } from 'hooks/settings/user/useUser';
import { LoadingButton } from '@mui/lab';
import { UserStatusEnum } from 'types/user';

// ===============================|| UI DIALOG - SWEET ALERT ||=============================== //

export interface UserChangeStatusButtonProps {
    userId: number;
    status: UserStatusEnum;
    open: boolean;
    onClose: () => void;
}

const UserChangeStatus = ({ userId, status, open, onClose }: UserChangeStatusButtonProps) => {
    const { changeStatus, loading, user, reset } = useEditUser();

    const theme = useTheme();

    useEffect(() => {
        if (user) {
            onClose();
            reset();
        }
    }, [onClose, user, reset]);

    return (
        <Dialog
            open={open}
            onClose={onClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
            sx={{ p: 3 }}
        >
            {open && (
                <>
                    <DialogTitle id="alert-dialog-title">Modification du status</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            <Typography variant="body2" component="span">
                                {status === 'ACTIF'
                                    ? "Attention! Le blocage de l'utilisateur l'empêchera d'utiliser le logiciel, voulez-vous vraiment bloquer l'utilisateur?"
                                    : "L'activation de l'utilisateur lui autorisera l'accès au logiciel, voulez-vous continuer ?"}
                            </Typography>
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions sx={{ pr: 2.5 }}>
                        <Button
                            sx={{ color: theme.palette.error.dark, borderColor: theme.palette.error.dark }}
                            onClick={onClose}
                            color="secondary"
                        >
                            Non
                        </Button>
                        <LoadingButton
                            variant="contained"
                            size="small"
                            loading={loading}
                            onClick={() => changeStatus(userId, status === 'ACTIF' ? 'BLOCKED' : 'ACTIF')}
                        >
                            Oui
                        </LoadingButton>
                    </DialogActions>
                </>
            )}
        </Dialog>
    );
};

export default UserChangeStatus;
