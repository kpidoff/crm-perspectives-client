import { ListItem, ListItemAvatar, ListItemText, ListItemSecondaryAction, Grid, Tooltip, Typography, Stack, Divider } from '@mui/material';
import { useDeleteNotification } from 'hooks/settings/user/useUser';
import moment from 'moment';
import { Fragment } from 'react';
import AvatarUserNotificationType from 'ui-component/avatar/avatar-user-notification/AvatarUserNotificationType';
import CloseIcon from '@mui/icons-material/Close';
import { UserNotification } from 'types/user';
import { LoadingButton } from '@mui/lab';
import parse from 'html-react-parser';

const UserNotificationList = ({ notification }: { notification: UserNotification }) => {
    const { deleteNotification, loading } = useDeleteNotification();
    return (
        <Fragment key={notification.id}>
            <ListItem alignItems="flex-start">
                <ListItemAvatar>
                    <AvatarUserNotificationType type={notification.type} />
                </ListItemAvatar>
                <ListItemText>
                    <Grid container>
                        <Grid item xs={12}>
                            <Typography sx={{ display: 'inline' }} variant="h5">
                                {notification.title}
                            </Typography>
                        </Grid>
                        <Grid item xs={12}>
                            {notification.subTitle && (
                                <>
                                    {' '}
                                    <Typography sx={{ display: 'inline' }} component="span" variant="body2" color="text.primary">
                                        {notification.subTitle}
                                    </Typography>{' '}
                                    —{' '}
                                </>
                            )}
                            {parse(notification.content)}
                        </Grid>
                    </Grid>
                </ListItemText>
                {notification.active && (
                    <ListItemSecondaryAction>
                        <Grid container justifyContent="flex-end">
                            <Grid item xs={4}>
                                <Tooltip title="Supprimer la notification">
                                    <LoadingButton
                                        sx={{ marginLeft: '-40px' }}
                                        color="error"
                                        onClick={() => deleteNotification(notification.id)}
                                        loading={loading}
                                    >
                                        <CloseIcon />
                                    </LoadingButton>
                                </Tooltip>
                            </Grid>
                        </Grid>
                    </ListItemSecondaryAction>
                )}
            </ListItem>
            <Stack direction="row" justifyContent="flex-end" sx={{ marginInlineEnd: '5px' }}>
                <Typography variant="caption" display="block" gutterBottom>
                    {moment(notification.createdAt).fromNow()}
                </Typography>
            </Stack>
            <Divider />
        </Fragment>
    );
};

export default UserNotificationList;
