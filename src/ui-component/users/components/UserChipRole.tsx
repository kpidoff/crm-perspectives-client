import { Chip } from '@mui/material';
import { useTheme } from '@mui/material/styles';
import { FormattedMessage } from 'react-intl';

const UserChipRole = ({ name }: { name: string | undefined }) => {
    const theme = useTheme();
    return (
        <Chip
            label={name || <FormattedMessage id="none" />}
            size="small"
            sx={
                name
                    ? {
                          background: theme.palette.mode === 'dark' ? theme.palette.dark.main : theme.palette.success.light + 60,
                          color: theme.palette.success.dark
                      }
                    : {
                          background: theme.palette.mode === 'dark' ? theme.palette.dark.main : theme.palette.orange.light + 80,
                          color: theme.palette.orange.dark
                      }
            }
        />
    );
};

export default UserChipRole;
