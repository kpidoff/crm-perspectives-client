import { UserDevice } from 'types/user';
import { BadgeConnect } from 'ui-component/badge';
import { typeDeviceToIcon } from 'utils';

const DeviceType = ({ device }: { device: Pick<UserDevice, 'actived' | 'deviceType'> }) => (
    <BadgeConnect tooltip="Connecté" onLine={device?.actived} anchorOrigin={{ vertical: 'top', horizontal: 'right' }}>
        {typeDeviceToIcon(device.deviceType)}
    </BadgeConnect>
);

export default DeviceType;
