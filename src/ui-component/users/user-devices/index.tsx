import { LoadingButton } from '@mui/lab';
import { useUserDevices, useUserDisconnectedEverywhere } from 'hooks/settings/user/useUser';
import { useEffect, useState } from 'react';
import SubCard from 'ui-component/cards/SubCard';
import { DEVICES_NUMER_PER_PAGE } from './userDevices.constants';
import { UserDevice } from 'types/user';
import { FormattedMessage } from 'react-intl';
import useAuth from 'hooks/authentication/useAuth';
import DatasComponentsTable from 'ui-component/datas-components/components/datas-components-wrapper/datas-components-table-wrapper';
import DeviceType from './components/DeviceType';
import { typeBrowserToIcon } from 'utils';
import Moment from 'react-moment';
import { DATE_TIME } from 'constants/date';

const UserDevices = ({ userId }: { userId?: number }) => {
    const { userDevices, getUserDevices, pageCursor, loading } = useUserDevices();
    const { disconnectedEverywhere, loading: loadingDisconnect } = useUserDisconnectedEverywhere();
    const [currentPage, setCurrentPage] = useState(1);
    const { user } = useAuth();

    useEffect(() => {
        getUserDevices({
            userId,
            paginate: {
                page: currentPage,
                perPage: DEVICES_NUMER_PER_PAGE
            }
        });
    }, [currentPage, getUserDevices, userId]);

    return (
        <SubCard
            title={<FormattedMessage id="user-security-device-title" />}
            secondary={
                <LoadingButton loading={loadingDisconnect} onClick={() => disconnectedEverywhere(userId)} variant="outlined" color="error">
                    {user?.id === userId ? (
                        <FormattedMessage id="user-security-device-button-disconnectedEverywhere" />
                    ) : (
                        <FormattedMessage id="user-security-device-button-the-disconnectedEverywhere" />
                    )}
                </LoadingButton>
            }
        >
            <DatasComponentsTable
                dataComponent={{
                    datas: userDevices,
                    itemsPerPage: DEVICES_NUMER_PER_PAGE,
                    loading
                }}
                datasComponentPaginate={{
                    currentPage: pageCursor?.currentPage,
                    numberOfPage: pageCursor?.lastPage,
                    onChangePage: (page) => setCurrentPage(page)
                }}
                datasComponentContent={{
                    components: [
                        {
                            title: <FormattedMessage id="peripheral" />,
                            content: (data: UserDevice | undefined) => ({
                                type: 'element',
                                component: data && <DeviceType device={data} />
                            })
                        },
                        {
                            title: <FormattedMessage id="browser" />,
                            content: (data: UserDevice | undefined) => ({
                                type: 'element',
                                component: data && typeBrowserToIcon(data.browserName)
                            })
                        },
                        {
                            title: <FormattedMessage id="os" />,
                            content: (data: UserDevice | undefined) => ({
                                type: 'element',
                                component: data && (
                                    <span>
                                        {data.osName} ({data.osVersion})
                                    </span>
                                )
                            })
                        },
                        {
                            title: <FormattedMessage id="country" />,
                            content: (data: UserDevice | undefined) => ({
                                type: 'country',
                                country: data && data.countryCode
                            })
                        },
                        {
                            title: <FormattedMessage id="ipAddress" />,
                            content: (data: UserDevice | undefined) => ({
                                type: 'element',
                                component: data && <span>{data.IPv4}</span>
                            })
                        },
                        {
                            title: <FormattedMessage id="loginDate" />,
                            content: (data: UserDevice | undefined) => ({
                                type: 'element',
                                component: data && <Moment format={DATE_TIME}>{data.createdAt}</Moment>
                            })
                        }
                    ]
                }}
            />
        </SubCard>
    );
};

export default UserDevices;
