// routing
import Routes from 'routes';

// project imports
import Locales from 'ui-component/Locales';
import NavigationScroll from 'layout/NavigationScroll';
import RTLLayout from 'ui-component/RTLLayout';
import Snackbar from 'ui-component/extended/Snackbar';
import ThemeCustomization from 'themes';
import AdapterDateFns from '@mui/lab/AdapterDateFns';

// auth provider
import { ApolloWrapper as AuthProvider } from 'contexts/ApolloContext';
import { LocalizationProvider } from '@mui/lab';

import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
// ==============================|| APP ||============================== //

const App = () => (
    <ThemeCustomization>
        {/* RTL layout */}
        <RTLLayout>
            <DndProvider backend={HTML5Backend}>
                <Locales>
                    <LocalizationProvider dateAdapter={AdapterDateFns}>
                        <ToastContainer newestOnTop />

                        <NavigationScroll>
                            <AuthProvider>
                                <>
                                    <Routes />
                                    <Snackbar />
                                </>
                            </AuthProvider>
                        </NavigationScroll>
                    </LocalizationProvider>
                </Locales>
            </DndProvider>
        </RTLLayout>
    </ThemeCustomization>
);

export default App;
