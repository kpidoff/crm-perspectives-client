// types
import { ConfigProps } from 'types/config';

export const JWT_API = {
    secret: 'SECRET-KEY',
    timeout: '1 days'
};

export const APOLLO_API = {
    uri: process.env.REACT_APP_URL_API,
    uriWs: process.env.REACT_APP_URL_WS
};

// basename: only at build time to set, and Don't add '/' at end off BASENAME for breadcrumbs, also Don't put only '/' use blank('') instead,
// like '/berry-material-react/react/default'
export const BASE_PATH = '';

export const DASHBOARD_PATH = '/dashboard/default';

const config: ConfigProps = {
    nameApp: 'ArloGestion',
    fontFamily: `'Roboto', sans-serif`,
    borderRadius: 8,
    outlinedFilled: true,
    navType: 'light', // light, dark
    presetColor: 'default', // default, theme1, theme2, theme3, theme4, theme5, theme6
    locale: 'fr', // 'en' - English, 'fr' - French, 'ro' - Romanian, 'zh' - Chinese
    rtlLayout: false,
    container: false,
    urlDocument: process.env.REACT_APP_URL_DOCUMENT as string,
    keyGoogleMap: process.env.REACT_APP_KEY_GOOGLE_MAP as string
};

export default config;
