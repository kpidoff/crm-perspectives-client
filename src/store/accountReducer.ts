// action - state management
import {
    EMAIL_AUTHENTICATION,
    LOGIN,
    LOGOUT,
    ADD_NOTIFICATION,
    DELETE_NOTIFICATION,
    CHANGE_CURRENT_SOCIETY,
    CURRENT_USER,
    UPDATE_CURRENT_SOCIETY,
    ADD_FILES_MANAGER,
    END_UPLOAD_FILES_MANAGER
} from './actions';
import { InitialLoginContextProps } from 'types/auth';
import { toastNotification } from 'utils/toast';
import _ from 'lodash';

// ==============================|| ACCOUNT REDUCER ||============================== //

const initialState: InitialLoginContextProps = {
    isLoggedIn: false,
    isInitialized: false,
    user: null,
    notifications: [],
    society: null,
    filesManager: []
};

const accountReducer = (state = initialState, action: any): InitialLoginContextProps => {
    switch (action.type) {
        case LOGIN: {
            const { user, notifications } = action.payload!;
            return {
                ...state,
                isLoggedIn: true,
                isInitialized: true,
                notifications,
                user
            };
        }
        case EMAIL_AUTHENTICATION: {
            const { user, notifications } = action.payload!;
            return {
                ...state,
                isLoggedIn: false,
                isInitialized: true,
                notifications,
                user
            };
        }
        case LOGOUT: {
            localStorage.clear();
            return {
                ...state,
                isInitialized: true,
                isLoggedIn: false,
                user: null
            };
        }
        case ADD_NOTIFICATION: {
            const { notification } = action.payload!;
            toastNotification(notification);
            return {
                ...state,
                notifications: state.notifications ? [notification, ...state.notifications] : [notification]
            };
        }
        case DELETE_NOTIFICATION: {
            const { notification } = action.payload!;
            return {
                ...state,
                notifications: _.filter(state.notifications, (stateNotification) => stateNotification.id !== notification.id)
            };
        }
        case CHANGE_CURRENT_SOCIETY: {
            const { society } = action.payload!;
            localStorage.setItem(`${state.user?.id}_society`, JSON.stringify(society));
            return {
                ...state,
                society
            };
        }
        case UPDATE_CURRENT_SOCIETY: {
            const { society } = action.payload!;
            localStorage.setItem(`${state.user?.id}_society`, JSON.stringify(society));
            return {
                ...state,
                society
            };
        }
        case CURRENT_USER: {
            const { user, notifications } = action.payload!;
            return {
                ...state,
                isLoggedIn: true,
                isInitialized: true,
                notifications,
                user,
                society: JSON.parse(_.toString(localStorage.getItem(`${user?.id}_society`)))
            };
        }
        case ADD_FILES_MANAGER: {
            const { filesManager } = action.payload!;
            return {
                ...state,
                filesManager: state.filesManager
                    ? [
                          {
                              ...filesManager,
                              inProgress: true
                          },
                          ...state.filesManager
                      ]
                    : [
                          {
                              ...filesManager,
                              inProgress: true
                          }
                      ]
            };
        }
        case END_UPLOAD_FILES_MANAGER: {
            const { fileManager } = action.payload!;
            let newFiles = _.find(state.filesManager, (files) => files.name === fileManager.name);
            if (!newFiles) {
                return {
                    ...state
                };
            }
            newFiles = {
                ...newFiles,
                inProgress: false
            };
            return {
                ...state,
                filesManager: state.filesManager
                    ? [newFiles, ..._.filter(state.filesManager, (file) => file.name !== fileManager.name)]
                    : [newFiles]
            };
        }
        default: {
            return { ...state };
        }
    }
};

export default accountReducer;
