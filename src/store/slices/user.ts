// third-party
import { createSlice } from '@reduxjs/toolkit';

// types
import { DefaultRootStateProps } from 'types';

// ----------------------------------------------------------------------

const initialState: DefaultRootStateProps['user'] = {
    error: null,
    notifications: []
};

const slice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        // HAS ERROR
        hasError(state, action) {
            state.error = action.payload;
        },
        addNewNotification(state, action) {
            state.notifications = [
                {
                    id: 1,
                    active: true,
                    type: 'ERROR',
                    subtype: 'MESSAGE',
                    content: 'test',
                    createdAt: new Date(),
                    title: 'titre',
                    userId: 1
                }
            ];
        }
    }
});

// Reducer
export default slice.reducer;

export const { addNewNotification } = slice.actions;
